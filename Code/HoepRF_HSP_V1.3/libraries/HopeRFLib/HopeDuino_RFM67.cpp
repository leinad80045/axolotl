/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 *
 * website: www.HopeRF.com
 *          www.HopeRF.cn     
 */

/*! 
 * file       RFduino_RFM67.cpp
 * brief      driver for RFM67 Tx
 * hardware   HopeRF's RFduino TRx & HopeRF RFM67 COB rf-module   
 *            
 *
 * version    1.0
 * date       May 31 2016
 * author     QY Ruan
 */

#include "HopeDuino_RFM67.h"

/********************************************************** 
**RF67 Regsister define                                      
**********************************************************/ 
#define		RegMode				0x00
#define		RegBrMsb			0x01
#define		RegBrLsb			0x02
#define		RegFdevMsb			0x03
#define		RegFdevLsb			0x04
#define		RegFrfMsb			0x05
#define		RegFrfMid			0x06
#define		RegFrfLsb			0x07
#define		RegPaCtrl			0x08
#define		RegPaFskRamp		0x09
#define		RegPllStat			0x0A
#define		RegVcoCtrl1			0x0B
#define		RegVcoCtrl2			0x0C
#define		RegVcoCtrl3			0x0D
#define		RegVcoCtrl4			0x0E
#define		RegClockCtrl		0x0F
#define		RegEeprom			0x10
#define		RegClockSel			0x11
#define		RegEolCtrl			0x12
#define		RegPaOcpCtrl		0x13
#define		RegPerDivider		0x17
#define		RegBtnDeb			0x18

/**********************************************************      
**RF67 mode status                                          
**********************************************************/      
#define		RADIO_SLEEP			(0x00<<4)
#define		RADIO_STANDBY		(0x01<<4)
#define		RADIO_FS			(0x02<<4)
#define		RADIO_TX			(0x03<<4)

#define		FskMode				(0<<2)
#define		OokMode				(1<<2)

#define 	MODE_MASK			0x8F

#define		Shaping				2

/**********************************************************
**Name:     vInitialize
**Function: initialize rfm67
**Input:    none
**Output:   none
**********************************************************/
void rf67Class::vInitialize(void)
{
 RF67_DclkIn();
 RF67_PllLockIn();
 RF67_EolIn();
 
 RF67_DatOut();
 RF67_ClrDat();
  
 RF67_ClrPOR();
 RF67_PORIn();
 Spi.vSpiInit();		
 
 //�˿ڳ�ʼ�� for 32MHz
 FrequencyValue.Freq = (Frequency<<11)/125;		//Calc. Freq
 BitRateValue  = (SymbolTime<<5)/1000;			//Calc. BitRate
 DevationValue = (Devation<<11)/125;			//Calc. Fdev 
 
 SampleRate    = (SymbolTime/1000)>>7;			
 
 vConfig();
 vGoStandby();
}

/**********************************************************
**Name:     vConfig
**Function: config RF67 
**Input:    none
**Output:   none
**********************************************************/
void rf67Class::vConfig(void)
{
 byte i;
 vRF67Reset();
 vGoStandby();

 //Frequency
 Spi.vSpiWrite(((word)RegFrfMsb<<8)+FrequencyValue.freq.FreqH);
 Spi.vSpiWrite(((word)RegFrfMid<<8)+FrequencyValue.freq.FreqM);
 Spi.vSpiWrite(((word)RegFrfLsb<<8)+FrequencyValue.freq.FreqL);

 i = Spi.bSpiRead(RegMode);
 i&= MODE_MASK;
 // Mode
 switch(Modulation)
 	{
 	case OOK:
		i |= OokMode+Shaping;
 		break;
 	case GFSK:
		i |= FskMode+Shaping;
 		break;
 	case FSK:
 	default:
 		i |= FskMode;
 		break;
 	}
 Spi.vSpiWrite(((word)RegMode<<8)+i);	
 
 //BitRate 
 Spi.vSpiWrite(((word)RegBrMsb<<8)+(byte)(BitRateValue>>8));
 Spi.vSpiWrite(((word)RegBrLsb<<8)+(byte)BitRateValue);
 
 //Devation
 Spi.vSpiWrite(((word)RegFdevMsb<<8)+(((byte)(DevationValue>>8))&0x3F));
 Spi.vSpiWrite(((word)RegFdevLsb<<8)+(byte)(DevationValue&0xFF));
 
 //PA
 Spi.vSpiWrite(((word)RegPaCtrl<<8)+0x60+(OutputPower&0x1F));	 

 //Ramping
 Spi.vSpiWrite(((word)RegPaFskRamp<<8)+bSelectRamping(SymbolTime));			
 
 //Ocp
 Spi.vSpiWrite(((word)RegPaOcpCtrl<<8)+0x0F);		//Disable Ocp
}

/**********************************************************
**Name:     vGoFs
**Function: set rf67 to FS
**Input:    none
**Output:   none
**********************************************************/
void rf67Class::vGoFs(void)
{
 byte tmp;
 tmp = Spi.bSpiRead(RegMode);
 tmp&= MODE_MASK;
 tmp |= RADIO_FS;
 Spi.vSpiWrite(((word)RegMode<<8)+tmp);
}	

/**********************************************************
**Name:     vGoStandby
**Function: set rf67 to standby mode
**Input:    none
**Output:   none
**********************************************************/
void rf67Class::vGoStandby(void)
{
 byte tmp;
 tmp = Spi.bSpiRead(RegMode);
 tmp&= MODE_MASK;
 tmp |= RADIO_STANDBY;
 Spi.vSpiWrite(((word)RegMode<<8)+tmp);
}	

/**********************************************************
**Name:     vGoSleep
**Function: set rf67 to sleep mode
**Input:    none
**Output:   none
**********************************************************/
void rf67Class::vGoSleep(void)
{
 byte tmp;
 tmp = Spi.bSpiRead(RegMode);
 tmp&= MODE_MASK;
 tmp |= RADIO_SLEEP;
 Spi.vSpiWrite(((word)RegMode<<8)+tmp);
}

/**********************************************************
**Name:     vGoTx
**Function: set rf67 to Tx
**Input:    none
**Output:   none
**********************************************************/
void rf67Class::vGoTx(void)
{
 byte tmp;
 tmp = Spi.bSpiRead(RegMode);
 tmp&= MODE_MASK;
 tmp |= RADIO_TX;
 Spi.vSpiWrite(((word)RegMode<<8)+tmp);
}
	

/**********************************************************
**Name:     bSendMessage
**Function: Send Message via RF67
**Input:    msg------for which message to send
            length---message length
**Output:   true-----send ok
            false----send error/over time
**********************************************************/
bool rf67Class::bSendMessage(byte msg[], byte length)
{
 byte tmp;
 byte sendbuf[200];
 byte tx_length;

 tx_length = bAssembleMsg(msg, length, sendbuf);

 RF67_PllLockIn();			//for Chk PllLock
 RF67_DclkIn();
 RF67_DatOut();
 RF67_ClrDat();
 
 vGoTx();
 for(tmp=0;tmp<100;tmp++)		//about 50ms for overtime
 	{
 	if(RF67_PLL_H())
 		break; 	
 	_delay_us(500);
 	}
 
 if(tmp>=100)
 	return(false);
 	
 for(tmp=0; tmp<tx_length; tmp++)
 	{
	if(!bTxPolling(sendbuf[tmp]))
 		break;
 	}
 if(tmp>=tx_length)
 	return(true);
 else
 	return(false);
}

/**********************************************************
**Name:     vRF67Reset
**Function: hardware reset rf67 chipset
**Input:    none
**Output:   none
**********************************************************/
void rf67Class::vRF67Reset(void)
{
 RF67_POROut();
 RF67_SetPOR();
 _delay_us(300);					//at least 100us for reset
 RF67_ClrPOR();
 RF67_PORIn();						//set POR for free
 _delay_ms(10);						//wait for ready
}	

/**********************************************************
**Name:     bSelectRamping
**Function: 
**Input:    symbol time
**Output:   ramping value
**********************************************************/
byte rf67Class::bSelectRamping(lword symbol)
{
 lword SymbolRate;
 
 SymbolRate = symbol/1000;			//ns->us
 SymbolRate = SymbolRate/4;			// 1/4 ramping
 
 if(SymbolRate<=10)		
 	return 0x0F;					//10us
 else if(SymbolRate<=12)			
 	return 0x0E;					//12us
 else if(SymbolRate<=15)			
 	return 0x0D;					//15us
 else if(SymbolRate<=20)
 	return 0x0C;					//20us
 else if(SymbolRate<=25)
 	return 0x0B;					//25us
 else if(SymbolRate<=31)
 	return 0x0A;					//31us
 else if(SymbolRate<=40)
 	return 0x09;					//40us
 else if(SymbolRate<=50)
 	return 0x08;					//50us
 else if(SymbolRate<=62)
 	return 0x07;					//62us
 else if(SymbolRate<=100)
 	return 0x06;					//100us
 else if(SymbolRate<=125)
 	return 0x05;					//125us
 else if(SymbolRate<=250)
 	return 0x04;					//250us
 else if(SymbolRate<=500)
 	return 0x03;					//500us
 else if(SymbolRate<=1000)
	return 0x02;					//1000us
 else if(SymbolRate<=2000)
 	return 0x01;					//2000us
 else 
 	return 0x00;
}	

/**********************************************************
**Name:     vChangeFreq
**Function: Change Frequency
**Input:    none
**Output:   none
**********************************************************/
void rf67Class::vChangeFreq(lword freq)	
{
 FreqStruct ChangeFreq;
 
 vGoStandby();
 ChangeFreq.Freq = (freq<<11)/125;		//Calc. Freq
 
 //Frequency
 Spi.vSpiWrite(((word)RegFrfMsb<<8)+FrequencyValue.freq.FreqH);
 Spi.vSpiWrite(((word)RegFrfMid<<8)+FrequencyValue.freq.FreqM);
 Spi.vSpiWrite(((word)RegFrfLsb<<8)+FrequencyValue.freq.FreqL);
}

/**********************************************************
**Name:     bAssembleMsg
**Function: set rf67 to sleep mode
**Input:    inptr----for which message to send
            length---input message length
            outptr---assemble message output pointer
**Output:   assemble message length
**********************************************************/
byte rf67Class::bAssembleMsg(byte inptr[], byte length, byte outptr[])
{
 byte i;
 byte j;
 word crc_buf;
 
 crc_buf = CrcSeed;
 for(i=0; i<(PreambleLength&0x1F); i++)
 	outptr[i] = 0x55;
 
 for(j=0; j<SyncLength; j++)
 	outptr[i++] = SyncWord[j];
 
 if(NodeBefore)
 	{	
 	if(!NodeDisable)
 		{
 		outptr[i] = NodeAddr;
		crc_buf = wCRC16Bit(outptr[i], crc_buf);
		i++;
		}
 	if(!FixedPktLength)	
 		{
 		outptr[i] = length;
 		if(!NodeDisable)
 			outptr[i] += 1;
		crc_buf = wCRC16Bit(outptr[i], crc_buf);
		i++;
		}
	}
 else
 	{
 	if(!FixedPktLength)	
 		{
 		outptr[i] = length;
 		if(!NodeDisable)
 			outptr[i] += 1;
		crc_buf = wCRC16Bit(outptr[i], crc_buf);
		i++;
		} 	
 	if(!NodeDisable)
 		{
 		outptr[i] = NodeAddr;
		crc_buf = wCRC16Bit(outptr[i], crc_buf);
		i++;
		} 	
 	}
 	
 for(j=0; j<length; j++)
 	{
 	outptr[i] = inptr[j];
	crc_buf = wCRC16Bit(outptr[i], crc_buf);
	i++;
	}
 
 if(!CrcDisable) 
	{
	if(CrcInverse)	
 		crc_buf ^= 0xFFFF;
	
	outptr[i++] = (byte)(crc_buf>>8);
	outptr[i++] = (byte)crc_buf;
	}
 return(i);
}

word rf67Class::wCRC16Bit(byte CRCData, word CRCResult)
{
 byte i;
 word tmp;
 
 for(i=8;i!=0;i--)
 	{
 	tmp = (word)CRCData<<8;
 	if((CRCResult^tmp)&0x8000)
 		{
 		CRCResult<<=1;	
 		if(!CrcMode)
 			CRCResult ^= 0x1021;		//X16+X12+X5+X1 CCITT-CRC16
 		else
 			CRCResult ^= 0x8005;	 	//X16+X15+X2+X1 IBM-CRC16	
 		}
 	else
 		CRCResult<<=1;	
 	CRCData <<= 1;	
 	}
 
 return(CRCResult);
}

/**********************************************************
**Name:     bTxPolling
**Function: send one byte buffer via RF67
**Input:    dat----wait for send
**Output:   true---send success
**          false--send faild
**********************************************************/
bool rf67Class::bTxPolling(byte dat)
{
 byte i;
 word j;

 for(i=0x80; i!=0; i>>=1)
 	{
 	j = 0;	
 	while(RF67_Dclk_H())
 		{
 		if(SampleRate!=0)
 			_delay_us(SampleRate);
 		j++;
 		if(j>=200)
			return(false);
 		}
 	while(RF67_Dclk_L())
 		{
 		if(SampleRate!=0)
 			_delay_us(SampleRate);
 		j++;
 		if(j>=200)
			return(false); 		
 		}
 	if(dat&i)
 		RF67_SetDat();
 	else
 		RF67_ClrDat();
 	}
 return(true);

}
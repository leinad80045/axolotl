/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 *
 * website: www.HopeRF.com
 *          www.HopeRF.cn     
 */

/*! 
 * file       CMT2300A_EasyDriver
 * brief      how to use CMT2300A
 * hardware   HopeDuino with RFM300
 *            
 *
 * version    1.0
 * date       Dec 14 2016
 * author     QY Ruan
 *
 * version    1.1
 * date	      Dec 22 2016
 * modify:     
 *            1. 把寄存器定义独立一份文件；
 *
 * version    1.2
 * date       Feb 3 2017
 * modify:
 *            1. Change CSMA
 * 
 * version    1.3
 * date       Feb 9 2017
 * modify:
 *            1. change use Rssi_dBm for CSMA, but not Rssi_code, for better detect range
 *
 * version    1.4
 * date       Feb 16 2017
 * modify:   
 *            1. vEnableRdFifo should be use in bGoRx, but not in bGetMessage, 
 *               because vEnableRdFifo is active when FIFO merge active;
 *            2. add clear fifo after vEnableWrFifo
 *            3. add depend PayloadLength value, select enable FIFO Merge, in vEnableRdFifo & vEnableWrFifo
 */
 
 #ifndef CMT2300A_EasyDriver_h
	#define CMT2300A_EasyDriver_h

	#ifndef F_CPU
		#define F_CPU 16000000UL	//HopeDuino use 16MHz Xo
	#endif	

	#ifndef	byte
		typedef unsigned char byte;
	#endif
	
	#ifndef word
		typedef unsigned int  word;
	#endif	

	#include <avr/io.h>
	#include <util/delay.h>
	#include <HopeDuino_SPI3.h>
	#include <CMT2300A_Register.h>
	
	/** Hardware brief **/    
	//PORTB						//DDRx		PORTx		PINx
	#define _GPO1    0x01       // 0          0          0
	
	//PORTD
	#define _GPO2    0x80       // 0          0          0
	#define _GPO3    0x40       // 0          0          0
	#define _GPO4    0x20       // 0          0          0 
	
	#define	GPO1In()	(DDRB &= (~_GPO1))
	#define GPO1_H() 	(PINB&_GPO1)	
	#define GPO1_L()	((PINB&_GPO1)==0)

	#define	GPO2In()	(DDRD &= (~_GPO2))
	#define GPO2_H() 	(PIND&_GPO2)	
	#define GPO2_L()	((PIND&_GPO2)==0)

	#define	GPO3In()	(DDRD &= (~_GPO3))
	#define GPO3_H() 	(PIND&_GPO3)	
	#define GPO3_L()	((PIND&_GPO3)==0)

	#define	GPO4In()	(DDRD &= (~_GPO4))
	#define GPO4_H() 	(PIND&_GPO4)	
	#define GPO4_L()	((PIND&_GPO4)==0)
	
	class cmt2300aEasy
	{
	 public:	
/**/	bool  FixedPktLength;						//false: for contain packet length in Tx message, the same mean with variable lenth
 		                                            //true : for doesn't include packet length in Tx message, the same mean with fixed length
/**/	byte  PayloadLength;						//unit: byte  range: 1-255, effect FIFO buffer                                   
		byte  PktRssi;								//with Sign
		byte  PktRssiCode;
		bool  PktRevError;

	 	//State Ctrl
	 	void  vSoftReset(void);
	 	bool  bGoSleep(void);	
	 	bool  bGoStandby(void);	
	 	bool  bGoTx(void);
	 	bool  bGoRx(void);
		bool  bGoSwitch(void);
	 	byte  bReadStatus(void);
	 	byte  bReadRssi(bool unit_dbm);

		//GPIO & Interrupt 
		void  vGpioFuncCfg(byte io_cfg);
		void  vIntSrcCfg(byte int_1, byte int_2);
		void  vInt1SrcCfg(byte int_1);
		void  vInt2SrcCfg(byte int_2);     
		void  vIntSrcEnable(byte en_int);
		byte  bIntSrcFlagClr(void);
		void  vEnableAntSwitch(byte mode);			
			
		//Fifo packet handle
		void  vClearFifo(void);
		byte  bReadFifoFlag(void);
		word  wReadIntFlag(void);
		void  vEnableRdFifo(void);
		void  vEnableWrFifo(void);
		void  vSetPayloadLength(bool mode, byte length);
	    void  vAckPacket(bool en);
		void  vWriteFifo(byte dat);
		byte  bReadFifo(void);
	
		//Config
		void  vInit(void);
		void  vCfgBank(word cfg[], byte length);
        void  vAfterCfg(void);
		
		//appliciation
		byte  bGetMessage(byte msg[]);
		byte  bGetMessageByFlag(byte msg[]);
		void  vSendMessage(byte msg[], byte length);
		bool  bSendMessageByFlag(byte msg[], byte length);


		//FHSS Part
		byte  FhssChannelRange;
		byte  FhssChannel;
  signed int  FhssRssiAvg;
 signed char  FhssRssiTH;
		void  vSetChannelOffset(word interval);
		void  vSetChannel(word channel);
		void  vSetTxPreamble(word length);
		byte  bFHSSDetect(void);
					
	 private:
	 	bool  RssiTrig;
	 	bool  FhssLockTrig;
	 	byte  FhssLockChannel;
 signed char  RssiPeakLock;
	 	byte  FhssPeakChannel;
	 	spi3Class Spi3;
	 	
	};
#else
	#warning "CMT2300A_EasyDriver.h have been defined!"

#endif

/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */

/*! 
 * file       rfm300_Rx.ino
 * hardware   HopeDuino
 * software   send message via RFM300
 * note       can talk to HopeRF's EVB or DK demo           
 *
 * version    1.0
 * date       Jan 16 2017
 * author     Suncity
 */


#include <RFM300_Driver.h> 
#include <HopeDuino_UART.h>

#define LEN 21

byte str[LEN] = {'H','o','p','e','R','F',' ','R','F','M',' ','C','O','B','R','F','M','3','0','0','A'};
byte getstr[LEN+1];

cmt2300aEasy radio;
uartClass uart;

/*******CMT Configuration********/
word CMTBank[12] = {
					0x0000,
					0x0166,
					0x026C,
					0x034D,
					0x04F0,
					0x0580,
					0x0614,
					0x0708,
					0x08B1,
					0x0903,
					0x0A00,
					0x0B78
				   };

/*******System Configuration********/			
word SystemBank[12] = {
				    0x0CA5,     // rx current regulation
				    0x0DE0,     // Duty-Cycle Control Register
				    0x0E30,     // Duty-Cycle Control Register
				    0x0F00,     //Sleep Timer 
				    0x1000,     //Sleep Timer 
				    0x11F4,     //RX Sleep T1 Timer 
				    0x1210,     //RX Sleep T1 Timer 
				    0x13E2,     //RX Sleep T2 Timer 
				    0x1442,     //RX Sleep T2 Timer 
				    0x1520,     //SLP Register
				    0x1600,     // PJD Register
				    0x1781		// PJD Register			  
				    };

/*******Frequency Configuration********/
/*************433.92Mhz***************/
word FrequencyBank[8] = {
					0x1842,
					0x1971,
					0x1ACE,
					0x1B1C,
					0x1C42,
					0x1D5B,
					0x1E1C,
					0x1F1C				    
					     };

/*******Data_Rate = Configuration********/				
word DataRateBank[24] = {
					0x204C,
					0x2193,
					0x2211,
					0x2310,
					0x24A2,
					0x2512,
					0x260B,
					0x270A,
					0x289F,
					0x295A,
					0x2A29,
					0x2B28,    //CDR Control Register
					0x2CC0,    //CDR Control Register
					0x2D8A,    //CDR Control Register
					0x2E02,    //CDR Control Register
					0x2F53,
					0x3018,
					0x3100,
					0x32B4,
					0x3300,
					0x3400,
					0x3501,
					0x3600,
					0x3700
						};     

/*******Basebank Configuration********/
word BasebandBank[29] = {
					0x3812,     //Data_mode Register
					0x3908,     //TX_preamble_size Rigister
					0x3A00,     //TX_preamble_size Rigister
					0x3BAA,     //TX_preamble_value Rigister (0xAA)
					0x3C04,     //Sysc_Word_Configuration Register
					0x3D00,     //Sysc_Word_Value 
					0x3E00,     //Sysc_Word_Value 
					0x3F00,     //Sysc_Word_Value 
					0x4000,     //Sysc_Word_Value 
					0x4100,     //Sysc_Word_Value 
					0x42D4,     //Sysc_Word_Value 
					0x432D,     //Sysc_Word_Value 
					0x44AA,     //Sysc_Word_Value 
					0x4501,     //Payload_Configuration
					0x461F,     //Payload_length
					0x4700,     //Node_ID_Configuration
					0x4800,     //Node_ID_Value 
					0x4900,     //Node_ID_Value 
					0x4A00,     //Node_ID_Value 
					0x4B00,     //Node_ID_Value 
					0x4C11,     //FEC_Configuration & CRC_Configuration
					0x4D0F,     //CRC_Seed
					0x4E1D,     //CRC_Seed
					0x4F00,     //CRC_Bit_Order
					0x5000,     //Whiten_seed
					0x5102,     //Tx_Prefix_type value
					0x5200,     //Tx_packet_number register
					0x5320,     //Tx_packet_gap register
					0x5410	
						};	

/*******Tx Configuration********/
word TXBank[11] = {
					0x5550,
					0x5657,
					0x570B,
					0x5800,
					0x5903,
					0x5A50,
					0x5B00,
					0x5C22,    //13dBm
					0x5D00,
					0x5E3F,
					0x5F7F															
				};						

void setup()
{
 radio.FixedPktLength    = false;				
 radio.PayloadLength     = LEN;	

 radio.vInit();
 radio.vCfgBank(CMTBank, 12);
 radio.vCfgBank(SystemBank, 12);
 radio.vCfgBank(FrequencyBank, 8);
 radio.vCfgBank(DataRateBank, 24);
 radio.vCfgBank(BasebandBank, 29);
 radio.vCfgBank(TXBank, 11);

 radio.vEnableAntSwitch(0);
 radio.vGpioFuncCfg(GPIO1_INT1+GPIO2_INT2+GPIO3_INT2+GPIO4_DOUT);   //GPIO Maping

 radio.vIntSrcCfg(INT_FIFO_WBYTE_RX, INT_CRC_PASS);    //IRQ  Maping
 radio.vIntSrcEnable(PKT_DONE_EN+CRC_PASS_EN+PREAMBLE_PASS_EN+SYNC_PASS_EN+TX_DONE_EN);   
 
 radio.vClearFIFO();
 radio.bGoSleep();
 radio.bGoRx();              //for Rx
 uart.vUartInit(9600, _8N1);
}

void loop()
{
 byte tmp;
 if(GPO3_H())
      {
       tmp = radio.bGetMessage(getstr);
       radio.bIntSrcFlagClr();
       radio.vClearFIFO(); 
       radio.bReadStatus();
       uart.vUartPutNByte(getstr, tmp);
       uart.vUartNewLine();
      }
}

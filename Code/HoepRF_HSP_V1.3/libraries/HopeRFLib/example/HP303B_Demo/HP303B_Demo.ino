
/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */

/*! 
 * file       press_hpxxx.cpp
 * brief      for HopeRF's EVB to use read press data
 * hardware   HopeRF's EVB
 *            
 *
 * version    1.0
 * date       mar 09 2016
 * author     mjl
 */
/********************************************************
------ include file xxx.h
********************************************************/
#include <HopeHp203b_IIC.h>
#include <HopeHp206c_IIC.h>
#include <HopeTh06_IIC.h>
#include <HopeDuino_Disp.h>
#include <HopeDuino_LCD.h>
#include <HopeDuino_UART.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <HP203B_H.C>
#include <EEPROM.h>
#include "HP303B.C"
/********************************************************
------ consant define
*******************************************************/
#define HP20X_SOFT_RST    0x06
#define HP20X_ADDRESSCMD  0xec      //csb = VCC

#define HP20XX_RDY      0x8d

#define HP20X_READ_P      0x30      //read_p command
#define HP20X_READ_A      0x31      //read_a command
#define HP20X_READ_T      0x32      //read_t command
#define HP20X_READ_PT   0x10      //read_pt command
#define HP20X_READ_AT   0x11      //read_at command

#define HP20X_WR_CONVERT_CMD   0x40
#define HP20X_CONVERT_OSR4096  0<<2
#define HP20X_CONVERT_OSR2048  1<<2
#define HP20X_CONVERT_OSR1024  2<<2
#define HP20X_CONVERT_OSR512   3<<2
#define HP20X_CONVERT_OSR256   4<<2
#define HP20X_CONVERT_OSR128   5<<2

#define TH06_ADDRESSCMD		0x80
#define TH06_SOFT_RST		0xfe
#define TH06_USER_REG		0xe7
#define TH06_READ_HUMI		0xf5
#define TH06_READ_TEMP		0xf3

#define HP03S_ADDRESSCMD		0xee
#define HP03S_E_ADDRESSCMD		0xa0
#define HP03S_EEPROMADDR		0x10

/********************************************************
------ public define
*******************************************************/
Hp203bI2cClass hp203b;
Hp206cI2cClass	hp206c;
Th06I2cClass	th06;

EEPROMClass hp20x_EEPROM;

dispClass	disp;
uartClass   uart;
/********************************************************
------ void define
*******************************************************/
static bool Th06ReadHumiTemperature(void);

static bool Hp203bReadPressureTemperature(void);
static bool Hp206cReadPressureTemperature(void);

static bool ReadTemperaturePreesureAD(void);
static void MCLKOn(void);
static void MCLKOff(void);
static word IIC_ReadTempretureAD(void);
static word IIC_ReadPressureAD(void);
static bool IIC_ReadCalData(void);

static void CalculatePressTemp(void);
static long int Get2_x(byte i);

static void Hex_to_Dec(unsigned long hexinput);
static void DispDataNop(void);

static bool Delay_uMs(byte d_delay);

static void T2_CTC_Init(void);
static void T1_Ovf_init(void);

extern void Save_Hp203bAvgAltitudeBuf(void);
extern void Save_Hp203bAvgPressureBuf(void);
static void Save_Hp206cAvgAltitudeBuf(void);
static void Save_Hp206cAvgPressureBuf(void);

static void Key_Scaning(void);
static void KeyDo(void);
byte ReadKey(void);
static void Hex_to_Ascll(byte HexInput);
void ReadMode0(void);
void DispMode0(void);
void DispMode1(void);
void DispMode2(void);
void DispMode3(void);
void DispMode4(void);
/********************************************************
------ var define
*******************************************************/
byte Rdy_Hp203b;//上电判断是否有传感器
byte Rdy_Hp206c;
byte Rdy_Th06;

byte DispBuf[10];

byte Disp_mode0_index;//显示的界面
                //0=hp203b-h
                //1=hp203b/206c
                //2=th06
byte Mode;


word Osr_Value;
byte AvgCount_Value=16;//平均次数

long Threshold_P=4;//气压门限阀值
long Threshold_A=20;//高度门限阀值
byte Osr_Cfg;
byte Osr_ConvertTime;

byte KeyTemp;
byte KeyCode;
byte KeyBounce;


bool F_key;
bool F_KeyValume;
bool F_Tm2Flag;
bool F_HalfSec;
bool F_Hp03sFlag;
bool F_ClearScreen;

word TimeCount;

word Hp03s_C1;
word Hp03s_C2;
word Hp03s_C3;
word Hp03s_C4;
word Hp03s_C5;
word Hp03s_C6;
word Hp03s_C7;
byte Hp03s_AA;
byte Hp03s_BB;
byte Hp03s_CC;
byte Hp03s_DD;

word Hp03s_D1 ;
word Hp03s_D2 ;

lword Hp03sTemperature;
lword Hp03sPressure;

lword	Hp203bPressure;
lword	Hp203bAvgPressure;
long	Hp203bTemperature;
long	Hp203bAltitude;
long	Hp203bAvgAltitude;
long	Hp203b_AltRel;
long	Hp203b_AltRelRef;

extern lword       Hp203b_hPressure;
extern lword       Hp203b_hAltitude;



lword	Hp206cPressure;
lword	Hp206cAvgPressure;
long	Hp206cTemperature;
long	Hp206cAltitude;
long	Hp206cAvgAltitude;
long	Hp206c_AltRel;
long	Hp206c_AltRelRef;


lword Hp203bPressureBuf[32];
long Hp203bAltitudeBuf[32];
lword Hp206cPressureBuf[32];
long Hp206cAltitudeBuf[32];

long Th06Temp;
lword Th06Humi;

/********************************************************
------ table define
*******************************************************/
byte Disp_HP203B[] = {"HP303: "};
byte Disp_HP206C[] = {"HP203B/206C: "};
byte Disp_HP03S[]  = {"HP03S: "};
byte Disp_TH06[]   = {"TH06: "};

byte Disp_OSR[]    = {"OSR: "};
byte Disp_UART[]   = {"UART: "};
byte Disp_AvgCount[]   = {"AvgCount: "};
byte Disp_Threshold[]   = {"Threshold: "};
byte Disp_Threshold_P[]   = {"P: "};
byte Disp_Threshold_A[]   = {"A: "};
byte Disp_SetBackLight[]   = {"BackLight: "};
byte Disp_SetUart[]   = {"Uart: "};
byte Disp_SetUartCycle[]   = {"UartCycle: "};


byte F_BackLight=0;

byte Disp_ON[]     = {"      ON "};
byte Disp_OFF[]    = {"      OFF "};
byte Disp_ON1[]     = {"  --> ON "};
byte Disp_OFF1[]    = {"  --> OFF "};


byte Disp_PRESS[]        = {"P: "};
byte Disp_TEMPERATURE[]  = {"T: "};
byte Disp_ALTITUDE[]     = {"A: "};
byte Disp_HUMI[]         = {"H: "};
byte Disp_ALTREL[]       = {"R: "};

byte Disp_PRESS_UNIT[]   = {" Hpa"};
byte Disp_TEMP_UNIT[]    = {" C"};
byte Disp_ALT_UNIT[]     = {" M"};
byte Disp_HUMI_UNIT[]    = {" %"};

byte FirstRead=0;
byte FirstReadCount=0;
byte F_Uart=0;

#define   _EeprmUseAddr       0x00
#define   _BackLightAddr       0x01
#define   _AvgCountAddr        0x02
#define   _Threshold_P_Addr        0x03
#define   _Threshold_A_Addr        0x04
#define   _Osr_Value_AddrL        0x05
#define   _Osr_Value_AddrH        0x06
#define   _F_UartAddr               0x07
#define   _F_UartCycleAddr        0x08
word   UartTxTimeCnt=0;
byte    UartTxCycleCnt=5;//uart 发射周期计数
byte F_WithKey=0;

/********************************************************
------ initialize: hp203b,hp206c,th02
------
------
********************************************************/
void setup()
{
	byte res;
	byte i;
	bool resHp203;
	MCUCR &= (~PUD);
	hp203b.HP203B_vIICInit();
	hp206c.HP206c_vIICInit();
	th06.Th06_vIICInit();
    uart.vUartInit(38400,_8N1);
    
    disp.vDispLight(0x01);
    
	Osr_Value = 1024;
	Osr_Cfg = HP20X_CONVERT_OSR1024;
	Osr_ConvertTime = 16;

     //  hp20x_EEPROM.write(_EeprmUseAddr, 0x23);
    i=hp20x_EEPROM.read(_EeprmUseAddr);
    if(i==0xaa){
        F_BackLight = hp20x_EEPROM.read(_BackLightAddr);
        AvgCount_Value = hp20x_EEPROM.read(_AvgCountAddr);
        Threshold_P = hp20x_EEPROM.read(_Threshold_P_Addr);
        Threshold_A = hp20x_EEPROM.read(_Threshold_A_Addr);
        Osr_Value = hp20x_EEPROM.read(_Osr_Value_AddrH);
        Osr_Value<<=8;
        Osr_Value |= hp20x_EEPROM.read(_Osr_Value_AddrL);
        F_Uart = hp20x_EEPROM.read(_F_UartAddr);
        
        UartTxCycleCnt=hp20x_EEPROM.read(_F_UartCycleAddr);
    }
    else{
        
        hp20x_EEPROM.write(_BackLightAddr, F_BackLight);
        delay(10);
        hp20x_EEPROM.write(_AvgCountAddr, AvgCount_Value);
        delay(10);
        hp20x_EEPROM.write(_Threshold_P_Addr, Threshold_P);
        delay(10);
        hp20x_EEPROM.write(_Threshold_A_Addr, Threshold_A);
        delay(10);
        hp20x_EEPROM.write(_Osr_Value_AddrL, Osr_Value);
         delay(10);
        hp20x_EEPROM.write(_Osr_Value_AddrH, Osr_Value>>8);
        
        delay(10);
        hp20x_EEPROM.write(_EeprmUseAddr, 0xaa);
        delay(10);
        hp20x_EEPROM.write(_F_UartCycleAddr, UartTxCycleCnt);
        delay(10);
        hp20x_EEPROM.write(_F_UartAddr, F_Uart);
        
    }
    


	hp203b.HP203B_bIICWriteSingle(HP20X_ADDRESSCMD,HP20X_SOFT_RST);
	hp206c.HP206C_bIICWriteSingle(HP20X_ADDRESSCMD,HP20X_SOFT_RST);
	th06.Th06_bIICWriteSingle(TH06_ADDRESSCMD,TH06_SOFT_RST);
	_delay_ms(20);
        delay(100);
	hp203b.HP203B_bIICWriteSingle(HP20X_ADDRESSCMD,0x8d);
	Rdy_Hp203b = hp203b.HP203B_bIICReadSingle(HP20X_ADDRESSCMD);
	if(Rdy_Hp203b==0x40)
	{

		Hp203bReadPressureTemperature();
		Hp203bAvgPressure = Hp203bPressure;
		Hp203b_hPressure=Hp203bAvgPressure;
		
		Hp203bAvgAltitude = Hp203bAltitude;
		
		Hp203b_hAltitude=Hp203bAvgAltitude;
		
		Hp203b_AltRelRef = Hp203bAltitude;
		for( i=0;i<32;i++)
		{
			Hp203bAltitudeBuf[i] = Hp203bAltitude;
			Hp203bPressureBuf[i] = Hp203bPressure;
		}
	
		
	}
	hp206c.HP206C_bIICWriteSingle(HP20X_ADDRESSCMD,0x8d);
	Rdy_Hp206c = hp206c.HP206C_bIICReadSingle(HP20X_ADDRESSCMD);
	Rdy_Hp206c=0x40;
	if(Rdy_Hp206c==0x40)
	{

		Hp206cReadPressureTemperature();
		Hp206cAvgPressure = Hp206cPressure;
		Hp206cAvgAltitude = Hp206cAltitude;
		Hp206c_AltRelRef = Hp206cAltitude;
		for(byte i=0;i<32;i++)
		{
			Hp206cAltitudeBuf[i] = Hp206cAltitude;
			Hp206cPressureBuf[i] = Hp206cPressure;
		}
	}
	th06.Th06_bIICWriteSingle(TH06_ADDRESSCMD,TH06_USER_REG);
	Rdy_Th06 = th06.Th06_bIICReadSingle(TH06_ADDRESSCMD);
	if(Rdy_Th06==0x3a)
	{

		Th06ReadHumiTemperature();
	}
	F_Hp03sFlag = 0;
	res = IIC_ReadCalData();
	if(res)
	{
		F_Hp03sFlag = 1;
	}
	T2_CTC_Init();

	disp.vDispInit();
	disp.vDispOn();
	disp.vDispClear();
	disp.vDispLight(0x01);
	disp.vDisHopeRFLogo(0x03,0x01);
	_delay_ms(200);
	delay(50);
	disp.vDispClear();
	Mode = 0;
	
	Disp_mode0_index = 0;
	sei();
	DispMode0();
	ReadCalCoef();
	
		
}

/********************************************************
------
------
------
********************************************************/
void loop()
{
	bool res;
    Key_Scaning();
    KeyDo();
    ReadCalCoef();
    delay(10);
    ReadHP303B();
    ReadId();
 //  delay(200);
//    uart.vUartPutString("T=");
//    SendUartData((unsigned long)HP303B_T);
//    uart.vUartPutString("P=");
//    SendUartData((unsigned long)HP303B_P);
    
    
    
    switch(Mode){
        case 0://正常采样模式
           // ReadMode0();
            DispMode0Data();
            break;
        case 1://OSR设置模式
            DispMode1();
            break;
        
        case 2: //平均次数
            DispMode2();
            break; 
        case 3: //压力门限值
            DispMode3();
            break;  
        case 4: //高度门限值
            DispMode4();
            break;
        case 5: //setup BackLight 
            DispMode5();
            break;    
        case 6: //setup uart
        case 7:    
            DispMode6();
            break;    
            
            
            
                                      
    }
    
    
//    if(F_BackLight)
//        {
            disp.vDispLight(0x01);            
//        }
//        else{
//            disp.vDispLight(0x00);
//            
//        }
    
}
    
/****************************************************************************************/    
//正常采样模式   
void ReadMode0(void)
{
    bool res;
    
    if(F_KeyValume == 1) return;
    if(F_WithKey==1)return;
    switch(Disp_mode0_index)
    {
        case 0://hp203b-h
            if(Rdy_Hp203b==0x40)
            {												
                res = Hp203bReadPressureTemperature();
                if(ReadKey()!=0)return;
                if(res == false) return;
                
                    Save_Hp203bAvgPressureBuf();
                    Save_Hp203bAvgAltitudeBuf();
                    
                    UartTxTimeCnt++;
                if(F_Uart){ 
                    if(UartTxTimeCnt>UartTxCycleCnt){  
                        UartTxTimeCnt=0; 
                        uart.vUartPutString("P=");
                        //uart.vUartPutByte(0x09);
                        SendUartData(Hp203bAvgPressure);
                        uart.vUartPutByte(0x09);
                        uart.vUartPutString("T=");
                        //uart.vUartPutByte(0x09);
                        SendUartData(Hp203bTemperature);
                        uart.vUartPutByte(0x09);
                        uart.vUartPutString("A=");
                        //uart.vUartPutByte(0x09);
                        SendUartData(Hp203bAvgAltitude);
                        uart.vUartPutByte(0x09);
                        uart.vUartPutString("R=");
                        //uart.vUartPutByte(0x09);
                        SendUartData(Hp203b_AltRel);
                        uart.vUartPutByte(0x0a);
                        uart.vUartPutByte(0x0d);
                    }
                }
                
                FirstReadCount++;
                if(FirstRead==0){
                    if(FirstReadCount<120){
                       Hp203b_AltRelRef = Hp203bAvgAltitude; 
                    }
                    else{
                        FirstRead=1;
                    }
                    
                    
                }
                
                
                //uart.vUartPutByte('\r');
            }
            break;
        case 1://hp203b/hp206c
            if(Rdy_Hp206c==0x40)
            {
                res = Hp206cReadPressureTemperature();
                if(res == 0) break;
                Save_Hp206cAvgPressureBuf();
                Save_Hp206cAvgAltitudeBuf();
            }
            break;
        case 2://th06
            if(Rdy_Th06==0x3a)
            {
                if(F_KeyValume==0)
                {
                    Th06ReadHumiTemperature();
                    if(F_Hp03sFlag)
                    {
                        res = ReadTemperaturePreesureAD();
                        if(res == 0) break;
                        CalculatePressTemp();
                    }
                }
            }
            break;
        default:
            break;
    }        
}

/****************************************************************************************/    
void SendUartData(unsigned long data)
{
    
//    byte UartBuff[3];
//    UartBuff[0]=data>>16;
//    UartBuff[1]=data>>8;
//    UartBuff[2]=data;
//    uart.vUartPutNByte(UartBuff,3);

    byte i;
    byte j;
    byte  UartBuff[8];
    Hex_to_Dec(data);
    j=0;
    for(i=0;i<8;i++)
    {
        if(DispBuf[i]!=0x20){
            
            UartBuff[j]=DispBuf[i];
            j++;
        }
        
    }
    uart.vUartPutNByte(UartBuff,j);




    
}

/****************************************************************************************/    
void DispMode0(void)
{
    if(Mode!=0) return;
    switch(Disp_mode0_index)
    {
        case 0://HP203B-h
            disp.vDispString8x8(0x01,0x01,Disp_HP203B);
            disp.vDispString8x8(0x03,0x01,Disp_PRESS);
            disp.vDispString8x8(0x04,0x01,Disp_TEMPERATURE);
            disp.vDispString8x8(0x05,0x01,Disp_ALTITUDE);
            disp.vDispString8x8(0x06,0x01,Disp_ALTREL);
            disp.vDispString8x8(0x03,88,Disp_PRESS_UNIT);
            disp.vDispString8x8(0x04,88,Disp_TEMP_UNIT);                
            disp.vDispString8x8(0x05,88,Disp_ALT_UNIT);                
            disp.vDispString8x8(0x06,88,Disp_ALT_UNIT);
            
/*            if(Rdy_Hp203b==0x40)
            {
                Hex_to_Dec(Hp203bTemperature);
                disp.vDispString8x8(0x04,24,DispBuf);
                Hex_to_Dec(Hp203bAvgPressure);
                disp.vDispString8x8(0x03,24,DispBuf);
                Hex_to_Dec(Hp203bAvgAltitude);
                disp.vDispString8x8(0x05,24,DispBuf);
                Hp203b_AltRel = Hp203bAvgAltitude - Hp203b_AltRelRef;
                Hex_to_Dec(Hp203b_AltRel);
                disp.vDispString8x8(0x06,24,DispBuf);                               
            }
            else
            {
                DispDataNop();
                disp.vDispString8x8(0x03,24,DispBuf);
                disp.vDispString8x8(0x04,24,DispBuf);
                disp.vDispString8x8(0x05,24,DispBuf);
                disp.vDispString8x8(0x06,24,DispBuf);            
            }*/
            break;
        case 1://hp203b/206c
            disp.vDispString8x8(0x01,0x01,Disp_HP206C);
            disp.vDispString8x8(0x03,0x01,Disp_PRESS);
            disp.vDispString8x8(0x04,0x01,Disp_TEMPERATURE);
            disp.vDispString8x8(0x05,0x01,Disp_ALTITUDE);
            disp.vDispString8x8(0x06,0x01,Disp_ALTREL);
            disp.vDispString8x8(0x03,88,Disp_PRESS_UNIT);
            disp.vDispString8x8(0x04,88,Disp_TEMP_UNIT);            	
            disp.vDispString8x8(0x05,88,Disp_ALT_UNIT);            	
            disp.vDispString8x8(0x06,88,Disp_ALT_UNIT);
            
/*            if(Rdy_Hp206c==0x40)
            {
            	Hex_to_Dec(Hp206cTemperature);
            	disp.vDispString8x8(0x04,24,DispBuf);
            	Hex_to_Dec(Hp206cAvgPressure);
            	disp.vDispString8x8(0x03,24,DispBuf);
            	Hex_to_Dec(Hp206cAvgAltitude);
            	disp.vDispString8x8(0x05,24,DispBuf);
            	Hp206c_AltRel = Hp206cAvgAltitude - Hp206c_AltRelRef;
            	Hex_to_Dec(Hp206c_AltRel);
            	disp.vDispString8x8(0x06,24,DispBuf);       	
            }
            else
            {
            	DispDataNop();
            	disp.vDispString8x8(0x03,24,DispBuf);
            	disp.vDispString8x8(0x04,24,DispBuf);
            	disp.vDispString8x8(0x05,24,DispBuf);
            	disp.vDispString8x8(0x06,24,DispBuf);
            }*/
            break;
        case 2://th06
            disp.vDispString8x8(0x01,0x01,Disp_HP03S);
            disp.vDispString8x8(0x03,0x01,Disp_PRESS);
            disp.vDispString8x8(0x04,0x01,Disp_TEMPERATURE);
            disp.vDispString8x8(0x06,0x01,Disp_TH06);
            disp.vDispString8x8(0x07,0x01,Disp_TEMPERATURE);
            disp.vDispString8x8(0x08,0x01,Disp_HUMI);
            
            disp.vDispString8x8(0x03,88,Disp_PRESS_UNIT);
            disp.vDispString8x8(0x04,88,Disp_TEMP_UNIT);           
            disp.vDispString8x8(0x07,88,Disp_TEMP_UNIT);
            disp.vDispString8x8(0x08,88,Disp_HUMI_UNIT);
            
            
            if(Rdy_Th06==0x3a)
            {
            	if(F_Hp03sFlag)
            	{
            		Hex_to_Dec(Hp03sPressure);
            		disp.vDispString8x8(0x03,24,DispBuf);            		
            		Hex_to_Dec(Hp03sTemperature);
            		disp.vDispString8x8(0x04,24,DispBuf);
            		
            	}
            	else
            	{
            		DispDataNop();
            		disp.vDispString8x8(0x03,24,DispBuf);
            		disp.vDispString8x8(0x04,24,DispBuf);
            	}
            	Hex_to_Dec(Th06Temp);
            	disp.vDispString8x8(0x07,24,DispBuf);
            	
            	Hex_to_Dec(Th06Humi);
            	disp.vDispString8x8(0x08,24,DispBuf);
            	
            }
            else
            {
            	if(F_Hp03sFlag)
            	{
            		Hex_to_Dec(Hp03sPressure);
            		disp.vDispString8x8(0x03,24,DispBuf);
            		
            		Hex_to_Dec(Hp03sTemperature);
            		disp.vDispString8x8(0x04,24,DispBuf);
            		
            	}
            	else
            	{
            		DispDataNop();
            		disp.vDispString8x8(0x03,24,DispBuf);
            		
            		disp.vDispString8x8(0x04,24,DispBuf);
            		
            	}
            	DispDataNop();
            	disp.vDispString8x8(0x07,24,DispBuf);
            	
            	disp.vDispString8x8(0x08,24,DispBuf);
            	
            }
            break;
    }   
}


/**************************************************************************************/
void DispMode0Data(void)
{
    byte i;
     switch(Disp_mode0_index)
    {
        case 0://HP203B-h
            if(Rdy_Hp203b==0x40)
                {
                    Hex_to_Dec(Hp203bTemperature);
                    disp.vDispString8x8(0x04,24,DispBuf);
                    Hex_to_Dec(Hp203bAvgPressure);
                    disp.vDispString8x8(0x03,24,DispBuf);
                    Hex_to_Dec(Hp203bAvgAltitude);
                    disp.vDispString8x8(0x05,24,DispBuf);
                    Hp203b_AltRel = Hp203bAvgAltitude - Hp203b_AltRelRef;
                    Hex_to_Dec(Hp203b_AltRel);
                    disp.vDispString8x8(0x06,24,DispBuf);  
                    
//                    Hex_to_Dec(Hp203b_AltRelRef);
//                    disp.vDispString8x8(0x07,24,DispBuf); 
                                                 
                }
                else
                {
//                    DispDataNop();
//                    disp.vDispString8x8(0x03,24,DispBuf);
//                    disp.vDispString8x8(0x04,24,DispBuf);
//                    disp.vDispString8x8(0x05,24,DispBuf);
//                    disp.vDispString8x8(0x06,24,DispBuf); 
                    Hex_to_Dec((long)(HP303B_T*100));
                    disp.vDispString8x8(0x04,24,DispBuf);
                    Hex_to_Dec((long)(HP303B_P));
                    disp.vDispString8x8(0x03,24,DispBuf);
                    Hex_to_Dec(Hp203bAvgAltitude);
                    disp.vDispString8x8(0x05,24,DispBuf);
                    Hp203b_AltRel = Hp203bAvgAltitude - Hp203b_AltRelRef;
                    Hex_to_Dec(Hp203b_AltRel);
                    disp.vDispString8x8(0x06,24,DispBuf);
                    for(i=0;i<4;i++)
                    {
                        //Id[i]=i*3;
                        Hex_to_Ascll(Id[i]);
                        disp.vDispString8x8(0x07,14+i*18,DispBuf);
                    }
           
                }
                break;
                
        case 1:
            if(Rdy_Hp206c==0x40)
            {
            	Hex_to_Dec(Hp206cTemperature);
            	disp.vDispString8x8(0x04,24,DispBuf);
            	Hex_to_Dec(Hp206cAvgPressure);
            	disp.vDispString8x8(0x03,24,DispBuf);
            	Hex_to_Dec(Hp206cAvgAltitude);
            	disp.vDispString8x8(0x05,24,DispBuf);
            	Hp206c_AltRel = Hp206cAvgAltitude - Hp206c_AltRelRef;
            	Hex_to_Dec(Hp206c_AltRel);
            	disp.vDispString8x8(0x06,24,DispBuf);       	
            }
            else
            {
            	DispDataNop();
            	disp.vDispString8x8(0x03,24,DispBuf);
            	disp.vDispString8x8(0x04,24,DispBuf);
            	disp.vDispString8x8(0x05,24,DispBuf);
            	disp.vDispString8x8(0x06,24,DispBuf);
            }
            break;                
        case 2:
            if(Rdy_Th06==0x3a)
            {
            	if(F_Hp03sFlag)
            	{
            		Hex_to_Dec(Hp03sPressure);
            		disp.vDispString8x8(0x03,24,DispBuf);            		
            		Hex_to_Dec(Hp03sTemperature);
            		disp.vDispString8x8(0x04,24,DispBuf);           		
            	}
            	else
            	{
            		DispDataNop();
            		disp.vDispString8x8(0x03,24,DispBuf);
            		disp.vDispString8x8(0x04,24,DispBuf);
            	}
            	Hex_to_Dec(Th06Temp);
            	disp.vDispString8x8(0x07,24,DispBuf);            	
            	Hex_to_Dec(Th06Humi);
            	disp.vDispString8x8(0x08,24,DispBuf);            	
            }
            else
            {
            	if(F_Hp03sFlag)
            	{
            		Hex_to_Dec(Hp03sPressure);
            		disp.vDispString8x8(0x03,24,DispBuf);            		
            		Hex_to_Dec(Hp03sTemperature);
            		disp.vDispString8x8(0x04,24,DispBuf);            		
            	}
            	else
            	{
            		DispDataNop();
            		disp.vDispString8x8(0x03,24,DispBuf);            		
            		disp.vDispString8x8(0x04,24,DispBuf);            		
            	}
            	DispDataNop();
            	disp.vDispString8x8(0x07,24,DispBuf);            	
            	disp.vDispString8x8(0x08,24,DispBuf);            	
            }
            break;  
        }              
}

/****************************************************************************************/    
void DispMode1(void)//显示OSR
{
    disp.vDispString8x8(0x01,0x01,Disp_OSR);
    Hex_to_Dec(Osr_Value);
    disp.vDispString8x8(0x03,32,DispBuf);    
}
/****************************************************************************************/    
void DispMode2(void)//平均次数
{
    disp.vDispString8x8(0x01,0x01,Disp_AvgCount);
    Hex_to_Dec(AvgCount_Value);
    disp.vDispString8x8(0x03,32,DispBuf);
     
}

/****************************************************************************************/    
void DispMode3(void)////压力门限值
{
    byte i;
    disp.vDispString8x8(0x01,0x01,Disp_Threshold);
    disp.vDispString8x8(0x03,1,Disp_Threshold_P);
    disp.vDispString8x8(0x04,1,Disp_Threshold_A);
   
    disp.vDispString8x8(0x03,88,Disp_PRESS_UNIT);                          
    disp.vDispString8x8(0x04,88,Disp_ALT_UNIT);
    
    
    Hex_to_Dec(Threshold_P);
            
    DispBuf[0]=0x2d;//"-"
    DispBuf[1]=0x2d;//"-"
    DispBuf[2]=0x3e;//">"
    disp.vDispString8x8(0x03,25,DispBuf);
   
    Hex_to_Dec(Threshold_A);
    disp.vDispString8x8(0x04,25,DispBuf); 
}
/****************************************************************************************/    
void DispMode4(void)////高度门限值
{
    byte i;
    disp.vDispString8x8(0x01,0x01,Disp_Threshold);
    disp.vDispString8x8(0x03,1,Disp_Threshold_P);
    disp.vDispString8x8(0x04,1,Disp_Threshold_A);
    disp.vDispString8x8(0x03,88,Disp_PRESS_UNIT);                          
    disp.vDispString8x8(0x04,88,Disp_ALT_UNIT);
    Hex_to_Dec(Threshold_P);
    disp.vDispString8x8(0x03,25,DispBuf);
    
   
    Hex_to_Dec(Threshold_A);
    DispBuf[0]=0x2d;//"-"
    DispBuf[1]=0x2d;//"-"
    DispBuf[2]=0x3e;//">"
     disp.vDispString8x8(0x04,25,DispBuf);
}

/****************************************************************************************/    
void DispMode5(void)////背光设置
{
    byte i;
    disp.vDispString8x8(0x01,0x01,Disp_SetBackLight);
   
    
    if(F_BackLight){
        disp.vDispString8x8(0x03,0x01,Disp_ON);
    }
    else{
        disp.vDispString8x8(0x03,0x01,Disp_OFF);
    }
      
    
}    
/****************************************************************************************/    
void DispMode6(void)////UART设置
{
    byte i;
   
    disp.vDispString8x8(0x01,0x01,Disp_SetUart);
    
    if(F_Uart){
        if(Mode==6){
            disp.vDispString8x8(0x03,0x01,Disp_ON1);
        }
        else{
            disp.vDispString8x8(0x03,0x01,Disp_ON);
        }
    }
    else{
        if(Mode==6){
            disp.vDispString8x8(0x03,0x01,Disp_OFF1);
        }
        else{
            disp.vDispString8x8(0x03,0x01,Disp_OFF);
        }
        
    }
   
   disp.vDispString8x8(0x05,0x01,Disp_SetUartCycle); 
   


//    disp.vDispString8x8(0x07,20,DispBuf);
    Hex_to_Dec(UartTxCycleCnt);// 
    if(Mode==7){
            DispBuf[0]=0x2d;//"-"
            DispBuf[1]=0x2d;//"-"
            DispBuf[2]=0x3e;//">"        
    }   
    disp.vDispString8x8(0x07,20,DispBuf);
    
   
}   

/**********************************************************/

byte ReadKey(void)
{
    byte key_buf;
	byte temp;

	temp |= (PINC&0x30)^0x30;
	temp = temp>>4;
	key_buf |= (PIND&0x30)^0x30;
	key_buf |= temp;
	switch(key_buf)
	{
		case 0x01:
		case 0x02:
		case 0x10:
		case 0x20:
			temp = key_buf;
			break;
		default:
			temp = 0;
			break;
	}
    return temp;
}
/**********************************************************
**Name:     Key_Scaning
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void Key_Scaning(void)
{
	byte key_buf;
	byte temp;

	temp |= (PINC&0x30)^0x30;
	temp = temp>>4;
	key_buf |= (PIND&0x30)^0x30;
	key_buf |= temp;
	switch(key_buf)
	{
		case 0x01:
		case 0x02:
		case 0x10:
		case 0x20:
			KeyTemp = key_buf;
			break;
		default:
			KeyTemp = 0;
			break;
	}
	if(KeyTemp == 0)
	{
		KeyCode = 0;
		KeyBounce = 0;
		F_key = 0;
		F_KeyValume = 0;
		F_WithKey=0;
	}
	else
	{
		F_WithKey=1;
		if(F_key==0)
		{
			KeyBounce ++;
			if(KeyBounce > 2)
			{
				KeyCode = KeyTemp;
				KeyBounce = 0;
				F_key = 1;
			}
		}
	}
}

/**********************************************************
**Name:     KeyDo
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void KeyDo(void)
{
	if(F_key == 0) return;
	if(F_KeyValume == 1) return;
	F_KeyValume = 1;
	switch(KeyCode)
	{
	    
		case 0x10://S1
			Mode++;
			if(Mode>7)
			    { 
			        Mode = 0;	
			    }		
			
			break;
		case 0x20://s2
			Mode = 0;
			Disp_mode0_index++;//显示的界面
			if(Disp_mode0_index>2) Disp_mode0_index = 0;
			    
			    
			    
			    
			break;
		case 0x01://s3
		    
		    switch(Mode)
			{
				case 0:
				    Hp203b_AltRelRef = Hp203bAvgAltitude;
				    Hp206c_AltRelRef = Hp206cAvgAltitude;
				    break;
				case 1:
				    break;    
				case 2:
				    break;    
				case 3:
				    Threshold_P-=1;
                    if (Threshold_P<0)
                    {
                        Threshold_P=50;					        
                    }
                    hp20x_EEPROM.write(_Threshold_P_Addr, Threshold_P);
                    
                     delay(10);
				    break; 
				case 4:
				    Threshold_A-=5;
                    if (Threshold_A<0)
                    {
                        Threshold_A=200;					        
                    } 
                    hp20x_EEPROM.write(_Threshold_A_Addr, Threshold_A);
                     delay(10);
				    break;
				case 7:
				    UartTxCycleCnt--;                    
                    hp20x_EEPROM.write(_F_UartCycleAddr, UartTxCycleCnt);
                     delay(10);
				    break;    
				    
            }       
						
			break;
		case 0x02://s4
			switch(Mode)
			{
				case 1://osr
					switch(Osr_Value)
					{
						case 128:
							Osr_Value = 256;
							Osr_Cfg = HP20X_CONVERT_OSR256;
							Osr_ConvertTime = 4;
							break;
						case 256:
							Osr_Value = 512;
							Osr_Cfg = HP20X_CONVERT_OSR512;
							Osr_ConvertTime = 8;
							break;
						case 512:
							Osr_Value = 1024;
							Osr_Cfg = HP20X_CONVERT_OSR1024;
							Osr_ConvertTime = 16;
							break;
						case 1024:
							Osr_Value = 2048;
							Osr_Cfg = HP20X_CONVERT_OSR2048;
							Osr_ConvertTime = 31;
							break;
						case 2048:
							Osr_Value = 4096;
							Osr_Cfg = HP20X_CONVERT_OSR4096;
							Osr_ConvertTime = 52;
							break;
						case 4096:
							Osr_Value = 128;
							Osr_Cfg = HP20X_CONVERT_OSR128;
							Osr_ConvertTime = 2;
							break;
					}
					hp20x_EEPROM.write(_Osr_Value_AddrL, Osr_Value);
                    delay(10);
                    hp20x_EEPROM.write(_Osr_Value_AddrH, Osr_Value>>8);
						
					break;
                case 2://平均次数
					AvgCount_Value<<=1;
					if (AvgCount_Value>32)
					    {
					        AvgCount_Value=1;					        
					    }	
                    hp20x_EEPROM.write(_AvgCountAddr, AvgCount_Value);
                     delay(10);				
					break;
				case 3://压力门限阀值

                    Threshold_P+=1;
                    if (Threshold_P>50)
                    {
                        Threshold_P=0;					        
                    }
                    hp20x_EEPROM.write(_Threshold_P_Addr, Threshold_P);
                     delay(10);
                    break;
				case 4://高度门限阀值	    
                    Threshold_A+=5;
                    if (Threshold_A>200)
                    {
                        Threshold_A=0;					        
                    }    
                    hp20x_EEPROM.write(_Threshold_A_Addr, Threshold_A);	
                     delay(10);				
					break;	
					
				case 5://背光设置	    
                    F_BackLight= ~F_BackLight;	
                    
                     hp20x_EEPROM.write(_BackLightAddr, F_BackLight);
                     delay(10);				
					break;	
					
				case 6://UART设置	    
                    F_Uart= ~F_Uart;	
                    
                     hp20x_EEPROM.write(_F_UartAddr, F_Uart);
                     delay(10);				
					break;		
				case 7://UART设置	    
                    	
                    UartTxCycleCnt++;
                     hp20x_EEPROM.write(_F_UartCycleAddr, UartTxCycleCnt);
                     delay(10);				
					break;											
			}
			break;
	}
	disp.vDispClear();
	DispMode0();
	//TimeCount = 0;
	//F_HalfSec = 1;
	KeyCode = 0;
	KeyBounce = 0;
}

/**********************************************************
**Name:     T1_Ovf_init
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void T1_Ovf_init(void)
{
	TCCR1B = 0x00;
	TCCR1A = 0x00;
	TCCR1B = 001;
	TCNT1 = 0xc2f6;
	TIMSK1 = 0x01;
}
/**********************************************************
**Name:     T0_CTC_Init
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void T2_CTC_Init(void)
{
	TCCR2B = 0x00;
	TCCR2A = 0x00;
	TCCR2B = 7;
	TCNT2 = 0xc2f6;
	TIMSK2 = 0x01;
}
/**********************************************************
**Name:     TIMER0_OVF_vect
**Function: 10ms
**Input:    none
**Output:   none
**********************************************************/
ISR(TIMER2_OVF_vect)
{
	TCNT2 = 100;
	TimeCount++;
	F_Tm2Flag = 1;	
	
	if(TimeCount>50)//0.5s
	    {
	        TimeCount=0;
	        F_HalfSec=~F_HalfSec;
	    }
	
	
	
} 

/**********************************************************
**Name:     TIMER1_OVF_vect
**Function: 
**Input:    none
**Output:   none
**********************************************************/
ISR(TIMER1_OVF_vect)
{
	TCNT1 = 0xff08;
	if(Hp03sMCLK_H())
		ClrHp03sMCLK();
	else
		SetHp03sMCLK();
}
/**********************************************************
**Name:     ReadTemperaturePreesureAD
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static bool ReadTemperaturePreesureAD(void)
{
	MCLKOn();
	Hp03s_D1 = IIC_ReadPressureAD();
	if(Hp03s_D1 == 0) return(false);
	Hp03s_D2 = IIC_ReadTempretureAD();
	if(Hp03s_D2 == 0) return(false);
	MCLKOff();
	return(true);
}

/**********************************************************
**Name:     MCLKOn
**Function: 		HP03S_XCLR = PB.1;	HP03S_MCLK = PB.5
**Input:    none
**Output:   none
**********************************************************/
static void MCLKOn(void)
{
	DDRD |= ((1<<DDD6)|(1<<DDD7));
	T1_Ovf_init();
 	SetHp03sXCLR();
 	ClrHp03sMCLK();
 	_delay_ms(1);
}

/**********************************************************
**Name:     MCLKOff
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void MCLKOff(void)
{
	TCCR1A = 0x00;
	TCCR1B = 000;
	TCNT1 = 0x0;
	TIMSK1 = 0x00;
	ClrHp03sXCLR();
	ClrHp03sMCLK();
}

/**********************************************************
**Name:     IIC_ReadTempretureAD
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static word IIC_ReadTempretureAD(void)
{ 
	byte DataBuf[2];
	word wADT;
	bool res;

	DataBuf[0] = 0xe8;
	res = hp206c.HP206C_bIICBurstWrite(HP03S_ADDRESSCMD,0xff,DataBuf,1);
	if(res == 0) return(false);
	res = Delay_uMs(32);
	if(res == 0) return(false);

	res = hp206c.HP206C_bIICWriteSingle(HP03S_ADDRESSCMD,0xfd);
	if(res == 0) return(false);
	res = hp206c.HP206C_bIICReadSeveral(HP03S_ADDRESSCMD,DataBuf,2);
	if(res == 0) return(false);
	wADT = DataBuf[0];
	wADT <<= 8;
	wADT |= DataBuf[1];
	
	return wADT;
}

/**********************************************************
**Name:     IIC_ReadPressureAD
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static word IIC_ReadPressureAD(void)
{ 
	byte DataBuf[2];
	word wADT;
	bool res;

	DataBuf[0] = 0xf0;
	res = hp206c.HP206C_bIICBurstWrite(HP03S_ADDRESSCMD,0xff,DataBuf,1);
	if(res == 0) return(false);
	res = Delay_uMs(32);
	if(res == 0) return(false);

	res = hp206c.HP206C_bIICWriteSingle(HP03S_ADDRESSCMD,0xfd);
	if(res == 0) return(false);
	res = hp206c.HP206C_bIICReadSeveral(HP03S_ADDRESSCMD,DataBuf,2);
	if(res == 0) return(false);
	wADT = DataBuf[0];
	wADT <<= 8;
	wADT |= DataBuf[1];
	
	return wADT;
}

/**********************************************************
**Name:     CalculatePressTemp
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void CalculatePressTemp(void)
{
	float MiddleData1;
	float MiddleData2;
	float MiddleData3;
	float MiddleData4;
	float DUT;
	float OFF;
	float SENS;
	float X;

	MiddleData1 = (long)Hp03s_D2-Hp03s_C5;
	MiddleData2 = MiddleData1/128;
	MiddleData2 = MiddleData2*MiddleData2;
	MiddleData3 = Get2_x(Hp03s_CC);
	MiddleData3 = MiddleData2/MiddleData3;
	if(Hp03s_D2 < Hp03s_C5)
	{
		MiddleData4 = MiddleData3*Hp03s_BB;
	}
	else
	{
		MiddleData4 = MiddleData3*Hp03s_AA;
	}
	DUT = MiddleData1 - MiddleData4;
	MiddleData1 = Get2_x(14);
	MiddleData2 = DUT/MiddleData1;
	MiddleData3 = (long)Hp03s_C4-1024;
	MiddleData4 = MiddleData3*MiddleData2;
	MiddleData1 = (long)Hp03s_C2+MiddleData4;
	OFF = MiddleData1*4;
	MiddleData1 = Get2_x(10);
	MiddleData2 = DUT/MiddleData1;
	MiddleData3 = (long)Hp03s_C3*MiddleData2;
	SENS = (long)Hp03s_C1+MiddleData3;
	MiddleData1 = (long)Hp03s_D1-7168;
	MiddleData2 = Get2_x(14);
	MiddleData3 = MiddleData1/MiddleData2;
	MiddleData4 = MiddleData3*SENS;
	X = MiddleData4-OFF;
	MiddleData1 = X*0.3125;
	MiddleData2 = (long)Hp03s_C7+MiddleData1;
	MiddleData2	= MiddleData2*10;
	Hp03sPressure = (unsigned long)MiddleData2;

	MiddleData1 = DUT*Hp03s_C6/Get2_x(16)-DUT/Get2_x(Hp03s_DD);
	MiddleData1 = 250 + MiddleData1;
	MiddleData1 = MiddleData1*10;
	Hp03sTemperature = (unsigned long)MiddleData1;
}

/***************************************************
**function:calculate powl for 2
**input   :
**output  :
***************************************************/
static long int Get2_x(byte i)
{
	long int uiData;

	uiData=2;
	i=i-1;
	while(i)
	{
		uiData <<= 1;
		i--;
	}
	return uiData;
}
/**********************************************************
**Name:     IIC_ReadCalData
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static bool IIC_ReadCalData(void)
{
	byte DataBuf[19];
	bool res;

	res = hp206c.HP206C_bIICWriteSingle(HP03S_E_ADDRESSCMD,HP03S_EEPROMADDR);
	if(res == 0) return(false);
	res = hp206c.HP206C_bIICReadSeveral(HP03S_E_ADDRESSCMD,DataBuf,18);
	if(res == 0) return(false);
	Hp03s_C1 = DataBuf[0];
	Hp03s_C1 <<= 8;
	Hp03s_C1 |= DataBuf[1];
	
	Hp03s_C2 = DataBuf[2];
	Hp03s_C2 <<= 8;
	Hp03s_C2 |= DataBuf[3];
	
	Hp03s_C3 = DataBuf[4];
	Hp03s_C3 <<= 8;
	Hp03s_C3 |= DataBuf[5];
	
	Hp03s_C4 = DataBuf[6];
	Hp03s_C4 <<= 8;
	Hp03s_C4 |= DataBuf[7];
	
	Hp03s_C5 = DataBuf[8];
	Hp03s_C5 <<= 8;
	Hp03s_C5 |= DataBuf[9];
	
	Hp03s_C6 = DataBuf[10];
	Hp03s_C6 <<= 8;
	Hp03s_C6 |= DataBuf[11];
	
	Hp03s_C7 = DataBuf[12];
	Hp03s_C7 <<= 8;
	Hp03s_C7 |= DataBuf[13];
	
	Hp03s_AA = DataBuf[14];
	Hp03s_BB = DataBuf[15];
	Hp03s_CC = DataBuf[16];
	Hp03s_DD = DataBuf[17];

	return(true);
}
/**********************************************************
**Name:     TimeDelay
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static bool Delay_uMs(byte d_delay)
{
	byte i;

	for(i=0;i<d_delay;i++)
	{
		_delay_us(1700);
		if(F_KeyValume) return(false);
	}
	return(true);
}

/**********************************************************
**Name:     Th06ReadHumiTemperature
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static bool Th06ReadHumiTemperature(void)
{
	byte DataBuf[2];
	bool res;

	res = th06.Th06_bIICWriteSingle(TH06_ADDRESSCMD,TH06_READ_HUMI);
	if(res == 0) return(false);
	res = Delay_uMs(20);
	if(res == 0) return(false);

	res = th06.Th06_bIICReadSeveral(TH06_ADDRESSCMD,DataBuf,2);
	if(res == 0) return(false);
	Th06Humi = DataBuf[0];
	Th06Humi <<= 8;
	Th06Humi |= DataBuf[1];
	Th06Humi *= 15625;
	Th06Humi >>= 13;
	Th06Humi -= 6000;
	Th06Humi /= 10;

	res = th06.Th06_bIICWriteSingle(TH06_ADDRESSCMD,TH06_READ_TEMP);
	if(res == 0) return(false);
	res = Delay_uMs(20);
	if(res == 0) return(false);

	res = th06.Th06_bIICReadSeveral(TH06_ADDRESSCMD,DataBuf,2);
	if(res == 0) return(false);
	Th06Temp = DataBuf[0];
	Th06Temp <<= 8;
	Th06Temp |= DataBuf[1];
	Th06Temp *= 21965;
	Th06Temp >>= 13;
	Th06Temp /= 10;
	if(Th06Temp >= 4685)
	{
		Th06Temp -= 4685;
	}
	else
	{
		Th06Temp = 4685-Th06Temp;
	}
	return(true);
}


/**Input:    none
**Output:   none
**********************************************************/
static bool Hp203bReadPressureTemperature(void)
{
	byte DataBuf[6];
	bool res;
    long	Hp203bTemperatureTemp;
    lword	Hp203bPressureTemp;
    long	Hp203bAltitudeTemp;
    

	res = hp203b.HP203B_bIICWriteSingle(HP20X_ADDRESSCMD,HP20X_WR_CONVERT_CMD|Osr_Cfg);
	if(res == 0) return(false);
	    
	    
//	res = Delay_uMs(Osr_ConvertTime*2);
    switch(Osr_Value)
    {
        case 1024:
            delay(8);
            break;
        case 2048:
            delay(10);
            break;    
        case 4096:
            delay(20);
            break;
       default:
            delay(7);
            break;     
    }
	
	if(res == 0) return(false);
  
	res = hp203b.HP203B_bIICWriteSingle(HP20X_ADDRESSCMD,HP20X_READ_PT);
	if(res == 0) return(false);
	res = hp203b.HP203B_bIICReadSeveral(HP20X_ADDRESSCMD,DataBuf,6);
	if(res == 0) return(false);
	Hp203bTemperatureTemp = DataBuf[0];
	Hp203bTemperatureTemp <<= 8;
	Hp203bTemperatureTemp |= DataBuf[1];
	Hp203bTemperatureTemp <<= 8;
	Hp203bTemperatureTemp |= DataBuf[2];
	if(Hp203bTemperatureTemp & 0x800000)
		Hp203bTemperatureTemp |= 0xff000000;
  
	Hp203bPressureTemp = DataBuf[3];
	Hp203bPressureTemp <<= 8;
	Hp203bPressureTemp |= DataBuf[4];
	Hp203bPressureTemp <<= 8;
	Hp203bPressureTemp |= DataBuf[5];
	
	res = hp203b.HP203B_bIICWriteSingle(HP20X_ADDRESSCMD,HP20X_READ_A);
	if(res == 0) return(false);
	res = hp203b.HP203B_bIICReadSeveral(HP20X_ADDRESSCMD,DataBuf,3);
	if(res == 0) return(false);
	Hp203bAltitudeTemp = DataBuf[0];
	Hp203bAltitudeTemp <<= 8;
	Hp203bAltitudeTemp |= DataBuf[1];
	Hp203bAltitudeTemp <<= 8;
	Hp203bAltitudeTemp |= DataBuf[2];
	if(Hp203bAltitudeTemp & 0x800000)
		Hp203bAltitudeTemp |= 0xff000000;
		
		Hp203bTemperature=Hp203bTemperatureTemp;
		Hp203bPressure=Hp203bPressureTemp;
		Hp203bAltitude=Hp203bAltitudeTemp;
		

	return(true);
}
/**********************************************************
**Name:     Hp206cReadPressureTemperature
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static bool Hp206cReadPressureTemperature(void)
{
	byte DataBuf[6];
	bool res;

	res = hp206c.HP206C_bIICWriteSingle(HP20X_ADDRESSCMD,HP20X_WR_CONVERT_CMD|Osr_Cfg);
	if(res == 0) return(false);
	res = Delay_uMs(Osr_ConvertTime*2);
	if(res == 0) return(false);
	
	res = hp206c.HP206C_bIICWriteSingle(HP20X_ADDRESSCMD,HP20X_READ_PT);
	if(res == 0) return(false);
	res = hp206c.HP206C_bIICReadSeveral(HP20X_ADDRESSCMD,DataBuf,6);
	if(res == 0) return(false);
	Hp206cTemperature = DataBuf[0];
	Hp206cTemperature <<= 8;
	Hp206cTemperature |= DataBuf[1];
	Hp206cTemperature <<= 8;
	Hp206cTemperature |= DataBuf[2];
	if(Hp206cTemperature & 0x800000)
		Hp206cTemperature |= 0xff000000;
	
	Hp206cPressure = DataBuf[3];
	Hp206cPressure <<= 8;
	Hp206cPressure |= DataBuf[4];
	Hp206cPressure <<= 8;
	Hp206cPressure |= DataBuf[5];
	
	res = hp206c.HP206C_bIICWriteSingle(HP20X_ADDRESSCMD,HP20X_READ_A);
	if(res == 0) return(false);
	res = hp206c.HP206C_bIICReadSeveral(HP20X_ADDRESSCMD,DataBuf,3);
	if(res == 0) return(false);
	Hp206cAltitude = DataBuf[0];
	Hp206cAltitude <<= 8;
	Hp206cAltitude |= DataBuf[1];
	Hp206cAltitude <<= 8;
	Hp206cAltitude |= DataBuf[2];
	if(Hp206cAltitude & 0x800000)
		Hp206cAltitude |= 0xff000000;
	return(true);
}





/**********************************************************
**Name:     Save_Hp206cAvgAltitudeBuf
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void Save_Hp206cAvgAltitudeBuf(void)
{
	byte i;
	byte j = 1;

	Hp206cAvgAltitude = 0;
	for(i=0;i<32;i++)
	{
		Hp206cAltitudeBuf[i] = Hp206cAltitudeBuf[j];
		j++;	
	}
	Hp206cAltitudeBuf[31] = Hp206cAltitude;
	for(i=0;i<32;i++)
	{
		Hp206cAvgAltitude += Hp206cAltitudeBuf[i];
	}
	Hp206cAvgAltitude = Hp206cAvgAltitude/32;
}

/**********************************************************
**Name:     Save_Hp206cAvgPressureBuf
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void Save_Hp206cAvgPressureBuf(void)
{
	byte i;
	byte j = 1;

	Hp206cAvgPressure = 0;
	for(i=0;i<32;i++)
	{
		Hp206cPressureBuf[i] = Hp206cPressureBuf[j];
		j++;	
	}
	Hp206cPressureBuf[31] = Hp206cPressure;
	for(i=0;i<32;i++)
	{
		Hp206cAvgPressure += Hp206cPressureBuf[i];
	}
	Hp206cAvgPressure = Hp206cAvgPressure/32;
}

/**********************************************************
**Name:     Hex_to_Dec
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void DispDataNop(void)//显示-------
{
	byte i;

	for(i=0;i<8;i++)
	{
		DispBuf[i] = 0x2d;
	}
}
/**********************************************************
**Name:     Hex_to_Dec
**Function: 
**Input:    none
**Output:   none
**********************************************************/
static void Hex_to_Dec(unsigned long hexinput)
{
	byte i;
	byte F_PlusFlag;
	unsigned long Disp_Dec_code;

	Disp_Dec_code = hexinput;
	F_PlusFlag = 0;
	if(Disp_Dec_code & 0x80000000)
	{
		F_PlusFlag = 1;
		Disp_Dec_code = ~Disp_Dec_code+1;
	}
	Disp_Dec_code = Disp_Dec_code&0x00ffffff;
	for(i=0;i<9;i++)
	{
		DispBuf[i] = 0x30;
	}
	DispBuf[8] = 0x00;
	while(Disp_Dec_code > 999999)
	{
		Disp_Dec_code = Disp_Dec_code-1000000;
		DispBuf[0]++;
	}
	while(Disp_Dec_code > 99999)
	{
		Disp_Dec_code = Disp_Dec_code-100000;
		DispBuf[1]++;
	}
	while(Disp_Dec_code > 9999)
	{
		Disp_Dec_code = Disp_Dec_code-10000;
		DispBuf[2]++;
	}
	while(Disp_Dec_code > 999)
	{
		Disp_Dec_code = Disp_Dec_code-1000;
		DispBuf[3]++;
	}
	while(Disp_Dec_code > 99)
	{
		Disp_Dec_code = Disp_Dec_code-100;
		DispBuf[4]++;
	}
	if(Mode==1|Mode==2|Mode==6|Mode==7)
	{
		while(Disp_Dec_code > 9)
		{
			Disp_Dec_code = Disp_Dec_code-10;
			DispBuf[5]++;
		}
		DispBuf[6] += Disp_Dec_code;
		DispBuf[7] = 0x20;
		DispBuf[8] = 0x20;
		if(DispBuf[0] == 0x30)
		{
			DispBuf[0] = 0x20;
			if(DispBuf[1] == 0x30)
			{
				DispBuf[1] = 0x20;
				if(DispBuf[2] == 0x30)
				{
					DispBuf[2] = 0x20;
					if(DispBuf[3] == 0x30)
					{
						DispBuf[3] = 0x20;
					}
				}
			}
		}
	}
	else
	{
		while(Disp_Dec_code > 9)
		{
			Disp_Dec_code = Disp_Dec_code-10;
			DispBuf[6]++;
		}
		DispBuf[7] += Disp_Dec_code;
		DispBuf[5] = 0x2e;        //dot
		if(DispBuf[0] == 0x30)
		{
			DispBuf[0] = 0x20;
			if(DispBuf[1] == 0x30)
			{
				DispBuf[1] = 0x20;
				if(DispBuf[2] == 0x30)
				{
					DispBuf[2] = 0x20;
					if(DispBuf[3] == 0x30)
					{
						DispBuf[3] = 0x20;
						if(F_PlusFlag) DispBuf[3] = 0x2d;
					}
					else
					{
						if(F_PlusFlag)	DispBuf[2] = 0x2d;
					}
				}
				else
				{
					if(F_PlusFlag)	DispBuf[1] = 0x2d;
				}
			}
			else
			{
				if(F_PlusFlag)	DispBuf[0] = 0x2d;      //"-"
			}
		}
	}
}

/***************************************************************************/
static void Hex_to_Ascll(byte HexInput)
{
    byte temp;
    byte i;
    for(i=0;i<10;i++){
        DispBuf[i]=0;
    }
    
    
	temp=HexInput&0x0f;
	if(temp<10){
	    DispBuf[1]=temp+0x30;	    
	}
	else{
	    DispBuf[1]=temp+0x41-10;	    
	}
	temp=(HexInput&0xf0)>>4;
	if(temp<10){
	    DispBuf[0]=temp+0x30;	    
	}
	else{
	    DispBuf[0]=temp+0x41-10;	    
	}
    
    DispBuf[2]='\0';
    
}
	
/********************************************************
------  read press end
********************************************************/



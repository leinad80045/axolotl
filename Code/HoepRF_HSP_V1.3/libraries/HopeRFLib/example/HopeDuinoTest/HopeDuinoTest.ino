/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */

/*! 
 * file       
 * hardware   
 * software   
 * note       
 *
 * version    1.0
 * date       Feb 18 2016
 * author     QY Ruan
 */

#include <HopeDuino_Buzz.h> 
#include <HopeDuino_Disp.h> 

dispClass Lcd;
buzzClass Buzz;

int IO13 = 13;
int IO12 = 12;
int IO11 = 11;
int IO10 = 10;
int IO9  = 9;
int IO8  = 8;
int IO7  = 7;
int IO6  = 6;

int KEY1  = 4;
int KEY2  = 5;
int KEY3  = 18;
int KEY4  = 19;

byte str0[] = {"     Leader      "};
byte str1[] = {" Manufacturer of "};
byte str2[] = {"       IoT       "};
byte str3[] = {" Key Components! "};

byte key1[] = {"KEY1"};
byte key2[] = {"KEY2"};
byte key3[] = {"KEY3"};
byte key4[] = {"KEY4"};
byte keyn[] = {"    "};

byte music[18] = {
                    SM_1, SM_2, SH_3, SM_1, 
                      20,   20,   20,   20
                    };

void setup()
{
 byte i; 
 
 pinMode(IO13, OUTPUT);
 pinMode(IO12, OUTPUT);
 pinMode(IO11, OUTPUT);
 pinMode(IO10, OUTPUT);
 pinMode(IO9, OUTPUT); 
 pinMode(IO8, OUTPUT); 
 pinMode(IO7, OUTPUT); 
 pinMode(IO6, OUTPUT); 
 
 pinMode(KEY1, INPUT);  
 pinMode(KEY2, INPUT);
 pinMode(KEY3, INPUT);
 pinMode(KEY4, INPUT);
 
 Lcd.vDispInit();
 Buzz.vBuzzInit();
 Buzz.vBuzzPlay(music, 4);
 Lcd.vDispLight(1);
 Lcd.vDispFull();
 _delay_ms(500);
 Lcd.vDispClear();
 Lcd.vDisArduinoLogo(false);
 _delay_ms(1000);
 for(i=1; i<=128; i++)
     {
     Lcd.vDispClrYaxis(i);
     _delay_ms(15);
     }
 Lcd.vDisHopeRFLogo(1, false);
 Lcd.vDispString8x8(5, 1, str0);
 Lcd.vDispString8x8(6, 1, str1);
 Lcd.vDispString8x8(7, 1, str2);
 Lcd.vDispString8x8(8, 1, str3);
 _delay_ms(1000);
 for(i=128; i!=0; i--)
     {
     Lcd.vDispClrYaxis(i);
     _delay_ms(15);
     }
}

void loop()
{

 digitalWrite(IO13, HIGH);  
 digitalWrite(IO11, HIGH);  
 digitalWrite(IO9, HIGH);  
 digitalWrite(IO7, HIGH);  

 digitalWrite(IO12, LOW);    
 digitalWrite(IO10, LOW);    
 digitalWrite(IO8, LOW);    
 digitalWrite(IO6, LOW);    
 if(digitalRead(KEY1)==LOW)
     Lcd.vDispString8x8(1, (6*8+1), key1);
 else
     Lcd.vDispString8x8(1, (6*8+1), keyn);
 if(digitalRead(KEY2)==LOW)
     Lcd.vDispString8x8(4, 1, key2);            
 else
     Lcd.vDispString8x8(4, 1, keyn);
 if(digitalRead(KEY3)==LOW)
     Lcd.vDispString8x8(4, (12*8+1), key3);            
 else
     Lcd.vDispString8x8(4, (12*8+1), keyn);
 if(digitalRead(KEY4)==LOW)
     Lcd.vDispString8x8(8, (6*8+1), key4);            
 else
     Lcd.vDispString8x8(8, (6*8+1), keyn);     
     
 delay(500);              

 digitalWrite(IO12, HIGH);  
 digitalWrite(IO10, HIGH);  
 digitalWrite(IO8, HIGH);  
 digitalWrite(IO6, HIGH);  

 digitalWrite(IO13, LOW);    
 digitalWrite(IO11, LOW);    
 digitalWrite(IO9, LOW);    
 digitalWrite(IO7, LOW);    
 if(digitalRead(KEY1)==LOW)
     Lcd.vDispString8x8(1, (6*8+1), key1);
 else
     Lcd.vDispString8x8(1, (6*8+1), keyn);
 if(digitalRead(KEY2)==LOW)
     Lcd.vDispString8x8(4, 1, key2);            
 else
     Lcd.vDispString8x8(4, 1, keyn);        
 if(digitalRead(KEY3)==LOW)
     Lcd.vDispString8x8(4, (12*8+1), key3);            
 else
     Lcd.vDispString8x8(4, (12*8+1), keyn);
 if(digitalRead(KEY4)==LOW)
     Lcd.vDispString8x8(8, (6*8+1), key4);            
 else
     Lcd.vDispString8x8(8, (6*8+1), keyn);     
 delay(500);   
}

/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */

/*! 
 * file       rfm67_tx.ino
 * hardware   HopeDuino
 * software   send message via rfm67
 * note       match for rfm219_RxForRF67          
 *
 * version    1.0
 * date       Apl 1 2016
 * author     QY Ruan
 */

#include <HopeDuino_RFM67.h> 

rf67Class radio;

byte str[21] = {'H','o','p','e','R','F',' ','R','F','M',' ','C','O','B','R','F','M','6','7','S'};

void setup()
{
 radio.Modulation     = FSK;
 radio.Frequency      = 868350;
 radio.OutputPower    = 10+13;          //10dBm OutputPower
 radio.PreambleLength = 16;             //16Byte preamble
 radio.FixedPktLength = false;          //packet length in message which need to be send
 radio.PayloadLength  = 21;

 radio.CrcDisable     = false;
 radio.CrcInverse     = false;
 radio.CrcMode 		  = false;			//CCITT
 radio.CrcSeed		  = 1234;
 
 radio.NodeDisable    = false;
 radio.NodeBefore     = true;			//on account of CMT2219A
 radio.NodeAddr		  = 0x01;

 radio.SymbolTime     = 416000;         //2.4Kbps
 radio.Devation       = 35;             //35KHz for devation
 radio.SyncLength     = 3;              //
 radio.SyncWord[0]    = 0xAA;
 radio.SyncWord[1]    = 0x2D;
 radio.SyncWord[2]    = 0xD4;
 radio.vInitialize();
 radio.vGoStandby();
}

void loop()
{
 radio.bSendMessage(str, 21);
 //radio.vGoStandby();
 radio.vGoSleep();
 delay(1000);
}
 

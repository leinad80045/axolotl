/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */
 
 /*! 
 * file       Interactive_TRx.ino
 * version    1.0
 * date       Jun 10 2017
 * author     QY Ruan
 *
 * hardware   Hopeduino 
 * software   1. basic sample code to show how to use CMT2300A in Tx & Rx mode
 *            2. If set ActiveMode=1, it is Tx-Side, send a packet every about 1s;
 *               If set ActiveMode=0, it is Rx-Side, always in RxMode. If success receive the packet from Tx-Side, then return the same packet to Tx-Side;
 */

#include "RadioCfg.h"
#include <CMT2300A_EasyDriver.h> 
#include <HopeDuino_UART.h>
#include <HopeDuino_Buzz.h> 
#include <HopeDuino_Disp.h> 

#define  ActiveMode    1		//[1]:ActiveTx;  [0]:PassiveRx

#define  LEN1          21
#define  LEN2          26

byte str1[LEN1] = {'H','o','p','e','R','F',' ','R','F','M',' ','C','O','B','R','F','M','3','0','0','A'};		//Payload1
byte str2[LEN2] = {'C','m','o','s','t','e','k',' ','N','e','x','t','G','e','n','R','F',' ','C','M','T','2','3','0','0', 'A'};	//Payload2

int KEY4  = 19;

byte Line1[] = {"2300 Interactive"};

byte Line2T[]= {"Mode: Active--Tx"};
byte Line2R[]= {"Mode: Passive-Rx"};

byte Line5[] = {"TxCount:        "};
byte Line6[] = {"RxCount:        "};
byte Line7[] = {"PktRssi:        "};
byte Line8[] = {"Noise  :        "};


byte music[2] = {
                    SM_4, 
                      10, 
                    };

byte getstr[32];

cmt2300aEasy radio;
uartClass    uart;
dispClass    lcd;
buzzClass    buzz;

bool         change;
word         TxCount;
word         RxCount;

extern word CMTBank[12];
extern word SystemBank[12];
extern word FrequencyBank[8];
extern word DataRateBank[24];
extern word BasebandBank[29];
extern word TXBank[11];
extern byte FreqString[];
extern byte BRnDevString[];
extern byte PowerString[];

void setup()
{
 _delay_ms(20);
 
 //LCD Initial 
 lcd.vDispInit();
 lcd.vDispClear(); 
 lcd.vDispLight(1);
 
 lcd.vDispString8x8(1, 1, Line1);
 if(ActiveMode==1)
    lcd.vDispString8x8(2, 1, Line2T);
 else
    lcd.vDispString8x8(2, 1, Line2R);
 lcd.vDispString8x8(3, 1, FreqString);
 lcd.vDispString8x8(4, 1, BRnDevString);
 lcd.vDispString8x8(5, 1, Line5);
 lcd.vDispString8x8(6, 1, Line6);
 lcd.vDispString8x8(7, 1, Line7);
 lcd.vDispString8x8(8, 1, Line8); 
 
 pinMode(KEY4, INPUT);
 buzz.vBuzzInit();
 uart.vUartInit(9600, _8N1);

 //CMT2300A Initial
 vInitRadio();
 
 if(ActiveMode)
    radio.bGoStandby();
 else
    {
    radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_CRC_OK);	
    //radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_PKT_DONE);
    radio.bGoRx();
    }

 change = false;
 TxCount = 0;
 RxCount = 0;
}

void loop()
{
 byte tmp;
 byte i;
 byte j;
 byte noise;
 
 if(digitalRead(KEY4)==LOW)
    {
    RxCount = 0;
    TxCount = 0;
    }
 
 if(ActiveMode)
    {
    //vInitRadio();
    radio.vIntSrcCfg(INT_TX_FIFO_NMTY, INT_TX_DONE);   //INT2 for TxDone
    if(change) 
        {
        radio.bSendMessageByFlag(str1, LEN1);
        change = false;
        }
    else
        {
        radio.bSendMessageByFlag(str2, LEN2);
        change = true;
        }
    radio.bIntSrcFlagClr();
    radio.vClearFifo(); 
    radio.bGoStandby();
    radio.bReadStatus();

    radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_CRC_OK);	
    //radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_PKT_DONE);
    radio.bGoRx();
	
    TxCount++;
    ShowCount(TxCount, 5);
    _delay_ms(10);
    noise = radio.bReadRssi(true);
    ShowNoise(noise);    
  
    for(i=0;i<100;i++)
        {
        _delay_ms(3);
        tmp = radio.bGetMessageByFlag(getstr);
        if(tmp!=0)
            {
            if(digitalRead(KEY4)==LOW)
                {
                RxCount = 0;
                TxCount = 0;
                } 
            radio.bGoStandby();   
       	    radio.bIntSrcFlagClr();
       	    radio.vClearFifo(); 
       	    radio.bReadStatus();
            if(change)
                {
                for(j=0;j<tmp;j++)  
                    {
                    if(getstr[j]!=str2[j])
                        break;
                    }
                }
            else
                {
                for(j=0;j<tmp;j++)  
                    {
                    if(getstr[j]!=str1[j])
                        break;
                    }
                }      
            if(j==tmp)
                {
                RxCount++;
                ShowCount(RxCount, 6);
                ShowRssi(radio.PktRssi);
                buzz.vBuzzPlay(music, 1);
                uart.vUartPutNByte(getstr, tmp);
                uart.vUartNewLine();
                }
            for(i=0;i<tmp;i++)
                getstr[i] = 0x00; 
            break;             
            }
        }
    radio.bGoSleep();
    for(i=0; i<150; i++)   
        { 
        if(digitalRead(KEY4)==LOW)
            {
            RxCount = 0;
            TxCount = 0;
            }
        _delay_ms(10);	
        }
    }
 else
    {
    tmp = radio.bGetMessageByFlag(getstr);
    if(tmp!=0)
        {
        radio.bGoStandby();   
        radio.bIntSrcFlagClr();
        radio.vClearFifo(); 
        radio.bReadStatus();
 		
        ShowRssi(radio.PktRssi);
        RxCount++;
        ShowCount(RxCount, 6);
        buzz.vBuzzPlay(music, 1);  
        uart.vUartPutNByte(getstr, tmp);
        uart.vUartNewLine();

        _delay_ms(100);
        radio.vIntSrcCfg(INT_TX_FIFO_NMTY, INT_TX_DONE);   //INT2 for TxDone
        radio.bSendMessageByFlag(getstr, tmp);
        radio.bIntSrcFlagClr();
        radio.vClearFifo(); 
        radio.bGoStandby();
        radio.bReadStatus();        

        TxCount++;
        ShowCount(TxCount, 5);
        for(i=0;i<tmp;i++)
            getstr[i] = 0x00;
        _delay_ms(100);
		
        radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_CRC_OK);	
        //radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_PKT_DONE);        
        radio.bGoRx();
        _delay_ms(10);
        noise = radio.bReadRssi(true);
        ShowNoise(noise);
        }
    }	
}
					
					
void vInitRadio(void)					
{
 radio.FixedPktLength    = false;				
 radio.PayloadLength     = 10;	
 radio.vInit();
 radio.vCfgBank(CMTBank, 12);
 radio.vCfgBank(SystemBank, 12);
 radio.vCfgBank(FrequencyBank, 8);
 radio.vCfgBank(DataRateBank, 24);
 radio.vCfgBank(BasebandBank, 29);
 radio.vCfgBank(TXBank, 11);
 radio.vAfterCfg(); 

 radio.vEnableAntSwitch(1);
 radio.vGpioFuncCfg(GPIO1_INT1+GPIO2_INT2+GPIO3_INT2+GPIO4_DOUT);
 //radio.vGpioFuncCfg(GPIO1_INT1+GPIO2_INT2+GPIO3_DOUT+GPIO4_DOUT);   //for test GPO3=Dout
 
 radio.vIntSrcEnable(PKT_DONE_EN+CRC_OK_EN+PREAM_OK_EN+SYNC_OK_EN+TX_DONE_EN); 
 radio.bIntSrcFlagClr();
 radio.vClearFifo();
 radio.bGoSleep();
}


void ShowCount(word count, byte line)
{
 byte i=0;
 byte strcnt[6];
 strcnt[i++] = '0'+(count/10000);
 strcnt[i++] = '0'+(count%10000/1000);
 strcnt[i++] = '0'+(count%1000/100);
 strcnt[i++] = '0'+(count%100/10);
 strcnt[i++] = '0'+(count%10);
 strcnt[i++] = '\0';
 lcd.vDispString8x8(line, (9<<3), strcnt);
}

void ShowRssi(byte rs)
{
 byte i=0;
 byte strcnt[8];
 if(rs&0x80)
    {
    strcnt[i++] = '-';
    rs ^= 0xFF;
    rs += 1;
    }
 else
    strcnt[i++] = '+';
 strcnt[i++] = '0'+(rs/100);
 strcnt[i++] = '0'+(rs%100/10);
 strcnt[i++] = '0'+(rs%10);
 strcnt[i++] = 'd';
 strcnt[i++] = 'B';
 strcnt[i++] = 'm';
 strcnt[i++] = '\0';
 lcd.vDispString8x8(7, (9<<3), strcnt);	 
} 

void ShowNoise(byte noise)
{
 byte i=0;
 byte strcnt[8];
 if(noise&0x80)
    {
    strcnt[i++] = '-';
    noise ^= 0xFF;
    noise += 1;
    }
 else
    strcnt[i++] = '+'; 
 strcnt[i++] = '0'+(noise/100);
 strcnt[i++] = '0'+(noise%100/10);
 strcnt[i++] = '0'+(noise%10);
 strcnt[i++] = 'd';
 strcnt[i++] = 'B';
 strcnt[i++] = 'm';
 strcnt[i++] = '\0';
 lcd.vDispString8x8(8, (9<<3), strcnt);	 
} 

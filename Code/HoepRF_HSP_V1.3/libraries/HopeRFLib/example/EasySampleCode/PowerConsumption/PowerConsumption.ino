/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */
 
 /*! 
 * file       PowerConsumption.ino
 * version    1.0
 * date       Jun 10 2017
 * author     QY Ruan
 *
 * hardware   Hopeduino 
 * software   1. use to test current consumption, just as Tx/Rx/Sleep/Standby
 *            2. In EasyDriver, it will close the DutyCycle Function. So that Sleep mode current will be less than in SLP Mode.
 */

#include "RadioCfg433920K40K18KHz20dBm.h"
#include <CMT2300A_EasyDriver.h> 
#include <HopeDuino_Disp.h> 


cmt2300aEasy radio;			  //define 
dispClass    lcd;
int          KEY1  = 4;
int          KEY2  = 5;
int          KEY3  = 18;
int          KEY4  = 19;
byte mode;
bool KeyTrig;

byte Line1[] = {"  CMT2300A FSK  "};
byte Line2[] = {"PowerConsumption"};
byte Line6[] = {"SW1=STBY SW4=SLP"};
byte Line7[] = {"SW2=RX   SW3=TX "};
byte Line8[] = {"Status:         "};


extern word CMTBank[12];
extern word SystemBank[12];
extern word FrequencyBank[8];
extern word DataRateBank[24];
extern word BasebandBank[29];
extern word TXBank[11];
extern byte FreqString[];
extern byte BRnDevString[];
extern byte PowerString[];	
					

void setup()
{
 _delay_ms(20);
 
 //LCD Initial 
 lcd.vDispInit();
 lcd.vDispClear(); 
 lcd.vDispLight(1);
 lcd.vDispString8x8(1, 1, Line1);
 lcd.vDispString8x8(2, 1, Line2);
 lcd.vDispString8x8(3, 1, FreqString);
 lcd.vDispString8x8(4, 1, BRnDevString);
 lcd.vDispString8x8(5, 1, PowerString);
 lcd.vDispString8x8(6, 1, Line6);
 lcd.vDispString8x8(7, 1, Line7);
 lcd.vDispString8x8(8, 1, Line8);
 
 //KeyInit
 pinMode(KEY1, INPUT);
 pinMode(KEY2, INPUT);
 pinMode(KEY3, INPUT);
 pinMode(KEY4, INPUT);

 //CMT2300A Initial
 radio.FixedPktLength    = false;				
 radio.PayloadLength     = 24;	
 radio.vInit();
 radio.vCfgBank(CMTBank, 12);			//Config 6 banks register	
 radio.vCfgBank(SystemBank, 12);
 radio.vCfgBank(FrequencyBank, 8);
 radio.vCfgBank(DataRateBank, 24);
 radio.vCfgBank(BasebandBank, 29);
 radio.vCfgBank(TXBank, 11);
 radio.vAfterCfg();

 radio.vEnableAntSwitch(1);			//[0]:No switch antenna; [1or2]:Switch Antenna
 radio.vGpioFuncCfg(GPIO1_INT1+GPIO2_INT2+GPIO3_INT2+GPIO4_DOUT);
 radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_CRC_OK);		
 radio.vIntSrcEnable(0x00); 			//all disable
 radio.vClearFifo();
 radio.bGoSleep();
 ShowStatus((const byte*)"SLEEP");
 mode    = 0;
 KeyTrig = false;
}

void loop()
{
 byte tmp;
 	
 if(digitalRead(KEY4)==LOW)
    {
    mode    = 0;                    //Sleep
    KeyTrig = true;
    }
 if(digitalRead(KEY3)==LOW)	
    {
    mode    = 1;                    //TX
    KeyTrig = true;
    }
 if(digitalRead(KEY2)==LOW)        
    {
    mode    = 2;                    //RX
    KeyTrig = true;
    }
 if(digitalRead(KEY1)==LOW) 
    {
    mode    = 3;                    //STBY
    KeyTrig = true;
    }
 
 if((digitalRead(KEY4)==HIGH)&&(digitalRead(KEY3)==HIGH)&&(digitalRead(KEY2)==HIGH)&&(digitalRead(KEY1)==HIGH)&&KeyTrig)
    {
    KeyTrig = false;
    radio.bIntSrcFlagClr();
    radio.vClearFifo(); 
    switch(mode)
        {
        case 1:
            tmp = radio.bReadStatus();
            if(tmp == MODE_STA_RX)
                radio.bGoSwitch();
            else
                radio.bGoTx(); 
        	ShowStatus((const byte*)"TX   ");
        	break;
        case 2:
        	tmp = radio.bReadStatus();
        	if(tmp == MODE_STA_TX)
        	    radio.bGoSwitch();
            else
                radio.bGoRx(); 
            ShowStatus((const byte*)"RX   ");
            break;
        case 3:
            radio.bGoStandby(); 
            ShowStatus((const byte*)"STBY ");
            break;
        case 0:
        default:
        	radio.bGoSleep(); 
        	ShowStatus((const byte*)"SLEEP");
        	break;
        }
    }
}

void ShowStatus(const byte *ptr)
{
 lcd.vDispString8x8(8, (8<<3), (byte *)ptr);
}							
					
					

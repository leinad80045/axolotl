/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */
 
 /*! 
 * file       VariableLongPacket_Tx.ino
 * version    1.0
 * date       Jun 10 2017
 * author     QY Ruan
 *
 * hardware   Hopeduino 
 * software   1. Variable Packet Mode 
 *            2. Packet Length more than FIFO buffer length
 *            3. Tx-Side
 */

#include "RadioCfg.h"
#include <CMT2300A_EasyDriver.h> 
#include <HopeDuino_Disp.h> 


#define MAX      250
#define LEN1     200
#define LEN2     231

byte str1[LEN1] = {"CMOSTEK Microelectronics Co., Ltd.\r\nRoom 202, Honghai Building, Qianhai Road. Nanshan District Shenzhen, Guangdong, China PRC\r\nTel: +86-0755-83235017\r\nFax: +86-0755-82761326\r\nWebsite: www.cmostek.com"};
byte str2[LEN2] = {"Hope Microelectronics Co., Ltd.\r\nAddress: 2F,Building3,Pingshan Private Enterprise science and Technology Park,Xili Town,Nanshan District,Shenzhen,China\r\nTel: +86-755-82973805\r\nFax: +86-755-82973550\r\nWebsite: http://www.hoperf.com"};

cmt2300aEasy radio;					//define 
dispClass    lcd;
word         TxCount;

int          KEY4  = 19;

byte Line1[] = {"  CMT2300A FSK  "};
byte Line2[] = {"Pkt : LongVar Tx"};
byte Line3[] = {"                "};
byte Line7[] = {"                "};
byte Line8[] = {"TxCount:        "};


extern word CMTBank[12];
extern word SystemBank[12];
extern word FrequencyBank[8];
extern word DataRateBank[24];
extern word BasebandBank[29];
extern word TXBank[11];
extern byte FreqString[];
extern byte BRnDevString[];
extern byte PowerString[];	


void setup()
{
 _delay_ms(20);
 
 //LCD Initial 
 lcd.vDispInit();
 lcd.vDispClear(); 
 lcd.vDispLight(1);
 lcd.vDispString8x8(1, 1, Line1);
 lcd.vDispString8x8(2, 1, Line2);
 lcd.vDispString8x8(3, 1, Line3);
 lcd.vDispString8x8(4, 1, FreqString);
 lcd.vDispString8x8(5, 1, BRnDevString);
 lcd.vDispString8x8(6, 1, PowerString);
 lcd.vDispString8x8(7, 1, Line7);
 lcd.vDispString8x8(8, 1, Line8);
 
 pinMode(KEY4, INPUT);
 
 TxCount = 0;    

 //CMT2300A Initial
 radio.FixedPktLength    = false;		//Variable Packet Mode
 radio.PayloadLength     = LEN2;	


 radio.vInit();
 radio.vCfgBank(CMTBank, 12);			//Config 6 banks register	
 radio.vCfgBank(SystemBank, 12);
 radio.vCfgBank(FrequencyBank, 8);
 radio.vCfgBank(DataRateBank, 24);
 radio.vCfgBank(BasebandBank, 29);
 radio.vCfgBank(TXBank, 11);
 radio.vAfterCfg();

 radio.vEnableAntSwitch(1);		        //[0]:No switch antenna; [1or2]:Switch Antenna
 radio.vGpioFuncCfg(GPIO1_INT1+GPIO2_INT2+GPIO3_INT2+GPIO4_DOUT);
 radio.vIntSrcCfg(INT_TX_FIFO_NMTY, INT_TX_DONE);   
 radio.vIntSrcEnable(TX_DONE_EN);                   

 radio.vClearFifo();
 radio.bGoSleep();
}

void loop()
{
 byte tmp;
 if(TxCount%2)
    SendLongPacket(str1, LEN1);
 else
    SendLongPacket(str2, LEN2);
 TxCount++;
 ShowCount(TxCount);
 for(tmp=0; tmp<100; tmp++)
    {
    _delay_ms(10);    
    if(digitalRead(KEY4)==LOW)
        TxCount = 0;
    }
}
					
void ShowCount(word count)
{
 byte i=0;
 byte strcnt[6];
 strcnt[i++] = '0'+(count/10000);
 strcnt[i++] = '0'+(count%10000/1000);
 strcnt[i++] = '0'+(count%1000/100);
 strcnt[i++] = '0'+(count%100/10);
 strcnt[i++] = '0'+(count%10);
 strcnt[i++] = '\0';
 lcd.vDispString8x8(8, (9<<3), strcnt);
}					
					
void SendLongPacket(byte ptr[], byte length)
{
 word flag;
 byte tmp; 
 byte i = 0;
 
 radio.bGoStandby();
 
 radio.vGpioFuncCfg(GPIO1_INT1+GPIO2_INT2+GPIO3_INT2+GPIO4_DCLK);
 radio.vIntSrcCfg(INT_TX_DONE, INT_TX_FIFO_FULL);
 radio.vIntSrcEnable(TX_DONE_EN);

 radio.vClearFifo();
 radio.vSetPayloadLength(true, length);
 radio.vEnableWrFifo();     //Fifo write
 radio.bGoTx();      
 
 for(i=0; i<length; )
    {
    if(GPO3_L())			//Not Full fill in
    	radio.vWriteFifo(ptr[i++]);
    }
 radio.vIntSrcCfg(INT_TX_DONE, INT_TX_DONE);
 do					        //wait send out
    {
    flag = radio.wReadIntFlag();
    }while((flag&(TX_DONE_FLG<<8))==0);
 radio.bIntSrcFlagClr();     
 radio.vClearFifo();
 radio.bGoStandby();
 radio.wReadIntFlag();
 radio.bGoSleep();
}

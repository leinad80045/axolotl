/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */
 
 /*! 
 * file       VariablePacket_Rx1.ino
 * version    1.0
 * date       Jun 10 2017
 * author     QY Ruan
 *
 * hardware   Hopeduino 
 * software   1. Variable Packet Mode 
 *            2. Packet Length less than FIFO buffer length
 *            3. Rx-Side
 *            4. Receive packet by check GPOn, which mapping for interrupt.
 */


//#include "RadioCfg.h"
#include "RadioCfg433920K300K75KHz20dBm.h"
#include <CMT2300A_EasyDriver.h> 
#include <HopeDuino_UART.h>
#include <HopeDuino_Disp.h> 


#define LEN 32                //for Max



cmt2300aEasy radio;					//define 
uartClass    uart;
dispClass    lcd;
word         RxCount;

int          KEY4  = 19;

byte getstr[LEN];

byte Line1[] = {"  CMT2300A FSK  "};
byte Line2[] = {"Pkt : VarPkt Rx1"};
byte Line3[] = {"                "};
byte Line7[] = {"                "};
byte Line8[] = {"RxCount:        "};


extern word CMTBank[12];
extern word SystemBank[12];
extern word FrequencyBank[8];
extern word DataRateBank[24];
extern word BasebandBank[29];
extern word TXBank[11];
extern byte FreqString[];
extern byte BRnDevString[];
extern byte PowerString[];	
					

void setup()
{
 _delay_ms(20);
 
 //LCD Initial 
 lcd.vDispInit();
 lcd.vDispClear(); 
 lcd.vDispLight(1);
 lcd.vDispString8x8(1, 1, Line1);
 lcd.vDispString8x8(2, 1, Line2);
 lcd.vDispString8x8(3, 1, Line3);
 lcd.vDispString8x8(4, 1, FreqString);
 lcd.vDispString8x8(5, 1, BRnDevString);
 lcd.vDispString8x8(6, 1, PowerString);
 lcd.vDispString8x8(7, 1, Line7);
 lcd.vDispString8x8(8, 1, Line8);
 
 pinMode(KEY4, INPUT);
 
 RxCount = 0;

 //CMT2300A Initial
 radio.FixedPktLength    = false;				
 radio.PayloadLength     = LEN;	

 radio.vInit();
 radio.vCfgBank(CMTBank, 12);			//Config 6 banks register	
 radio.vCfgBank(SystemBank, 12);
 radio.vCfgBank(FrequencyBank, 8);
 radio.vCfgBank(DataRateBank, 24);
 radio.vCfgBank(BasebandBank, 29);
 radio.vCfgBank(TXBank, 11);
 radio.vAfterCfg(); 

 radio.vEnableAntSwitch(0);				//[0]:No switch antenna; [1or2]:Switch Antenna
 //radio.vGpioFuncCfg(GPIO1_INT1+GPIO2_DATA+GPIO3_INT2+GPIO4_DOUT);
 radio.vGpioFuncCfg(GPIO1_DCLK+GPIO2_DATA+GPIO3_INT2+GPIO4_DOUT);

 radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_CRC_OK);		//when CRC enable, by RFPDK setting
 //radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_PKT_OK);		//when CRC disable, by RFPDK setting
 radio.vIntSrcEnable(PKT_DONE_EN+CRC_OK_EN+PREAM_OK_EN+SYNC_OK_EN+TX_DONE_EN); 
 
 radio.vClearFifo();
 radio.bGoSleep();
 radio.bGoRx();              			//for Rx
 uart.vUartInit(9600, _8N1);
}

void loop()
{
 byte tmp;
 if(GPO3_H())
    {
    tmp = radio.bGetMessage(getstr);
    radio.bGoStandby();
    radio.bIntSrcFlagClr();
    radio.vClearFifo(); 
    radio.bReadStatus();
    RxCount++;
    ShowCount(RxCount);
    uart.vUartPutNByte(getstr, tmp);
    uart.vUartNewLine();
    radio.bGoRx();    
    if(digitalRead(KEY4)==LOW)
        RxCount = 0;
    }
}

void ShowCount(word count)
{
 byte i=0;
 byte strcnt[6];
 strcnt[i++] = '0'+(count/10000);
 strcnt[i++] = '0'+(count%10000/1000);
 strcnt[i++] = '0'+(count%1000/100);
 strcnt[i++] = '0'+(count%100/10);
 strcnt[i++] = '0'+(count%10);
 strcnt[i++] = '\0';
 lcd.vDispString8x8(8, (9<<3), strcnt);
}							
					
					

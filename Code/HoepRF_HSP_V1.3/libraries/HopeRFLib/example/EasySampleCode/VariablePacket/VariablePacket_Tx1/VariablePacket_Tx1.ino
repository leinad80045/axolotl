/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */
 
 /*! 
 * file       VariablePacket_Tx1.ino
 * version    1.0
 * date       Jun 10 2017
 * author     QY Ruan
 *
 * hardware   Hopeduino 
 * software   1. Variable Packet Mode 
 *            2. Packet Length less than FIFO buffer length
 *            3. Tx-Side
 *            4. Send packet by check GPOn, which mapping for interrupt.
 */


//#include "RadioCfg.h"
#include "RadioCfg433920K300K75KHz20dBm.h"
#include <CMT2300A_EasyDriver.h> 
#include <HopeDuino_Disp.h> 



#define LEN1   21
#define LEN2   26

byte str1[LEN1] = {'H','o','p','e','R','F',' ','R','F','M',' ','C','O','B','R','F','M','3','0','0','A'};		//Payload1
byte str2[LEN2] = {'C','m','o','s','t','e','k',' ','N','e','x','t','G','e','n','R','F',' ','C','M','T', '2', '3', '0', '0', 'A'};		//Payload2

cmt2300aEasy radio;					//define 
dispClass    lcd;
word         TxCount;

int          KEY4  = 19;

byte Line1[] = {"  CMT2300A FSK  "};
byte Line2[] = {"Pkt : VarPkt Tx1"};
byte Line3[] = {"                "};
byte Line7[] = {"                "};
byte Line8[] = {"TxCount:        "};

extern word CMTBank[12];
extern word SystemBank[12];
extern word FrequencyBank[8];
extern word DataRateBank[24];
extern word BasebandBank[29];
extern word TXBank[11];
extern byte FreqString[];
extern byte BRnDevString[];
extern byte PowerString[];	
					

void setup()
{
 _delay_ms(20);
 
 //LCD Initial 
 lcd.vDispInit();
 lcd.vDispClear(); 
 lcd.vDispLight(1);
 lcd.vDispString8x8(1, 1, Line1);
 lcd.vDispString8x8(2, 1, Line2);
 lcd.vDispString8x8(3, 1, Line3);
 lcd.vDispString8x8(4, 1, FreqString);
 lcd.vDispString8x8(5, 1, BRnDevString);
 lcd.vDispString8x8(6, 1, PowerString);
 lcd.vDispString8x8(7, 1, Line7);
 lcd.vDispString8x8(8, 1, Line8);
 
 pinMode(KEY4, INPUT);
 
 TxCount = 0;    

 //CMT2300A Initial
 radio.FixedPktLength    = false;		//Fixed Packet Mode
 radio.PayloadLength     = LEN1;	        

 radio.vInit();
 radio.vCfgBank(CMTBank, 12);			//Config 6 banks register	
 radio.vCfgBank(SystemBank, 12);
 radio.vCfgBank(FrequencyBank, 8);
 radio.vCfgBank(DataRateBank, 24);
 radio.vCfgBank(BasebandBank, 29);
 radio.vCfgBank(TXBank, 11);
 radio.vAfterCfg();  

 radio.vEnableAntSwitch(1);				//[0]:No switch antenna; [1or2]:Switch Antenna
 radio.vGpioFuncCfg(GPIO1_INT1+GPIO2_INT2+GPIO3_INT2+GPIO4_DOUT);
 radio.vIntSrcCfg(INT_TX_FIFO_NMTY, INT_TX_DONE);   
 radio.vIntSrcEnable(TX_DONE_EN);                   

 radio.vClearFifo();
 radio.bGoSleep();

}

void loop()
{
 byte tmp;
 if(TxCount%2)
    radio.vSendMessage(str1, LEN1);
 else
    radio.vSendMessage(str2, LEN2);
 while(GPO3_L());                               //wait TX_DONE    
 radio.bGoStandby();
 radio.bIntSrcFlagClr();			//clear flag
 radio.vClearFifo();
 radio.bGoSleep(); 
 TxCount++;
 ShowCount(TxCount);
 for(tmp=0; tmp<100; tmp++)
    {
    _delay_ms(10);    
    if(digitalRead(KEY4)==LOW)
        TxCount = 0;
    }
}
					
void ShowCount(word count)
{
 byte i=0;
 byte strcnt[6];
 strcnt[i++] = '0'+(count/10000);
 strcnt[i++] = '0'+(count%10000/1000);
 strcnt[i++] = '0'+(count%1000/100);
 strcnt[i++] = '0'+(count%100/10);
 strcnt[i++] = '0'+(count%10);
 strcnt[i++] = '\0';
 lcd.vDispString8x8(8, (9<<3), strcnt);
}					
					

/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 * website: www.HopeRF.com
 *          www.HopeRF.cn
 *
 */
 
 /*! 
 * file       FHSS_Rx.ino
 * version    1.0
 * date       Jun 10 2017
 * author     QY Ruan
 *
 * hardware   Hopeduino 
 * software   1. FHSS Sample Code for Rx-Side;
 *            2. Detect Tx-Side preamble, and set the Rx-Side the same channel with Tx-Side;
 *            3. Note: Tx-Side need have enough preamble length to send, because Rx-Side need to lock the channel during this period;
 */

//#include "RadioCfg.h"
#include "RadioCfg868350K100K35KHz20dBm.h"
#include <CMT2300A_EasyDriver.h> 
#include <HopeDuino_UART.h>
#include <HopeDuino_Disp.h> 

#define RX_INTERVAL     250    //250KHz
#define RX_CHANN_RANGE  16     //16 channel
#define RX_RSSI_THRES   20     //vs AvgRssi, Thresshold   


#define LEN 32                //for Max

cmt2300aEasy radio;			  //define 
uartClass    uart;
dispClass    lcd;
word         RxCount;
byte         Channel;
bool         ChannLock_F;
word         Overtime;
byte         AvgRssiDispCnt;

int          KEY2  = 5;
int          KEY3  = 18;
int          KEY4  = 19;

byte getstr[LEN];

byte Line1[] = {"  CMT2300A FSK  "};
byte Line2[] = {"Pkt : FHSSPkt Rx"};
byte Line6[] = {"Channel:        "};
byte Line7[] = {"Noise  :        "};
byte Line8[] = {"RxCount:        "};

extern word CMTBank[12];
extern word SystemBank[12];
extern word FrequencyBank[8];
extern word DataRateBank[24];
extern word BasebandBank[29];
extern word TXBank[11];
extern byte FreqString[];
extern byte BRnDevString[];
extern byte PowerString[];

void setup()
{
 _delay_ms(20);
 
 //LCD Initial 
 lcd.vDispInit();
 lcd.vDispClear(); 
 lcd.vDispLight(1);
 lcd.vDispString8x8(1, 1, Line1);
 lcd.vDispString8x8(2, 1, Line2);
 lcd.vDispString8x8(3, 1, FreqString);
 lcd.vDispString8x8(4, 1, BRnDevString);
 lcd.vDispString8x8(5, 1, PowerString);
 lcd.vDispString8x8(6, 1, Line6);
 lcd.vDispString8x8(7, 1, Line7);
 lcd.vDispString8x8(8, 1, Line8);
 
 pinMode(KEY2, INPUT);
 pinMode(KEY3, INPUT); 
 pinMode(KEY4, INPUT);
 
 RxCount = 0;

 //CMT2300A Initial
 radio.FixedPktLength    = false;				
 radio.PayloadLength     = LEN;	

 radio.vInit();
 radio.vCfgBank(CMTBank, 12);			//Config 6 banks register	
 radio.vCfgBank(SystemBank, 12);
 radio.vCfgBank(FrequencyBank, 8);
 radio.vCfgBank(DataRateBank, 24);
 radio.vCfgBank(BasebandBank, 29);
 radio.vCfgBank(TXBank, 11);
 radio.vAfterCfg();
 
 radio.FhssChannelRange = RX_CHANN_RANGE;
 radio.FhssChannel      = 0;
 radio.FhssRssiTH       = RX_RSSI_THRES;
 radio.vSetChannelOffset(RX_INTERVAL);
 radio.vSetChannel(radio.FhssChannel); 

 radio.vEnableAntSwitch(1);				//[0]:No switch antenna; [1or2]:Switch Antenna
 radio.vGpioFuncCfg(GPIO1_INT1+GPIO2_INT2+GPIO3_INT2+GPIO4_DOUT);
 radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_CRC_OK);		//when CRC enable, by RFPDK setting
 //radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_PKT_OK);		//when CRC disable, by RFPDK setting
 radio.vIntSrcEnable(0x00);				//Disable All
 
 radio.vClearFifo();
 radio.bGoSleep();
 uart.vUartInit(9600, _8N1);
 ChannLock_F = false;
}

void loop()
{
 byte tmp;
  
 if(!ChannLock_F) 
    {
    Channel = radio.bFHSSDetect();
    if(radio.FhssChannel==0)
        {
        AvgRssiDispCnt++;
        if(AvgRssiDispCnt>=20)
            {
            AvgRssiDispCnt = 0;
            ShowAvgNoise();
            }  
        }
    }
    
 if(Channel<radio.FhssChannelRange)
    {
    ChannLock_F = true;
    radio.bGoStandby();
    radio.vIntSrcEnable(PKT_DONE_EN+CRC_OK_EN+PREAM_OK_EN+SYNC_OK_EN+TX_DONE_EN);     
    radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_CRC_OK);		//when CRC enable, by RFPDK setting
    //radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_PKT_OK);		//when CRC disable, by RFPDK setting    
    radio.vSetChannel(Channel);	
    radio.bGoRx();
    Overtime = 0;
    }
 do
    {
    Overtime++;
    if(Overtime>=8000)                      //1500ms
        {
        ChannLock_F = false;	
        radio.bGoStandby();
        radio.vAckPacket(0);                //Disable AckPacket    
        radio.vIntSrcEnable(0x00);          //Disable All   	
        Overtime = 0;
        break;
        }
    _delay_us(250);
    tmp = radio.bGetMessageByFlag(getstr);
    if(tmp!=0)
        {
        Overtime = 0;
        radio.bGoStandby();
        radio.bIntSrcFlagClr();
        radio.vClearFifo(); 
        RxCount++;

        radio.vAckPacket(1);                //Enable AckPacket
        radio.vIntSrcCfg(INT_TX_FIFO_NMTY, INT_TX_DONE);
        radio.bGoTx();
        while(GPO3_L());
        radio.bGoStandby();
        radio.bIntSrcFlagClr();
        radio.vClearFifo(); 
        radio.vAckPacket(0);                //Disable AckPacket
    
        ShowFreq(Channel);
        ShowPktRssi();
        ShowCount(Channel, 6);
        ShowCount(RxCount, 8);
        uart.vUartPutNByte(getstr, tmp);
        uart.vUartNewLine();
        
        radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_CRC_OK);		//when CRC enable, by RFPDK setting
        //radio.vIntSrcCfg(INT_RX_FIFO_WBYTE, INT_PKT_OK);		//when CRC disable, by RFPDK setting            
        radio.bGoRx();
        }            
    if(digitalRead(KEY4)==LOW)
        RxCount = 0;

    }while(ChannLock_F);
}

void ShowCount(word count, byte line)
{
 byte i=0;
 byte strcnt[6];
 strcnt[i++] = '0'+(count/10000);
 strcnt[i++] = '0'+(count%10000/1000);
 strcnt[i++] = '0'+(count%1000/100);
 strcnt[i++] = '0'+(count%100/10);
 strcnt[i++] = '0'+(count%10);
 strcnt[i++] = '\0';
 lcd.vDispString8x8(line, (9<<3), strcnt);
}						

void ShowAvgNoise(void)
{
 byte i=0;
 byte strcnt[16];
 byte noise = radio.FhssRssiAvg;
 strcnt[i++] = 'N';
 strcnt[i++] = 'o';
 strcnt[i++] = 'i';
 strcnt[i++] = 's';
 strcnt[i++] = 'e'; 
 strcnt[i++] = ' '; 
 strcnt[i++] = ' '; 
 strcnt[i++] = ':';  
 if(noise&0x80)
    {
    strcnt[i++] = '-';
    noise ^= 0xFF;
    noise += 1;
    }
 else
    strcnt[i++] = '+'; 
 strcnt[i++] = '0'+(noise/100);
 strcnt[i++] = '0'+(noise%100/10);
 strcnt[i++] = '0'+(noise%10);
 strcnt[i++] = 'd';
 strcnt[i++] = 'B';
 strcnt[i++] = 'm';
 strcnt[i++] = '\0';
 lcd.vDispString8x8(7, 0, strcnt);	 
} 

void ShowPktRssi(void)
{
 byte i=0;
 byte strcnt[16];
 strcnt[i++] = 'P';
 strcnt[i++] = 'k';
 strcnt[i++] = 't';
 strcnt[i++] = 'R';
 strcnt[i++] = 's'; 
 strcnt[i++] = 's'; 
 strcnt[i++] = 'i'; 
 strcnt[i++] = ':';  
 if(radio.PktRssi&0x80)
    {
    strcnt[i++] = '-';
    radio.PktRssi ^= 0xFF;
    radio.PktRssi += 1;
    }
 else
    strcnt[i++] = '+'; 
 strcnt[i++] = '0'+(radio.PktRssi/100);
 strcnt[i++] = '0'+(radio.PktRssi%100/10);
 strcnt[i++] = '0'+(radio.PktRssi%10);
 strcnt[i++] = 'd';
 strcnt[i++] = 'B';
 strcnt[i++] = 'm';
 strcnt[i++] = '\0';
 lcd.vDispString8x8(7, 0, strcnt);	 
} 


void ShowFreq(byte channel)	
{
 lword freq;
 byte strcnt[8];
 byte i = 0;
 
 freq = GetFreq();
 freq+= RX_INTERVAL*channel;
 
 strcnt[i++] = '0'+(freq%1000000/100000);
 strcnt[i++] = '0'+(freq%100000/10000);
 strcnt[i++] = '0'+(freq%10000/1000);
 strcnt[i++] = '.';
 strcnt[i++] = '0'+(freq%1000/100);
 strcnt[i++] = '0'+(freq%100/10);
 strcnt[i++] = '0'+(freq%10);
 strcnt[i++] = '\0';
 lcd.vDispString8x8(3, (6<<3), strcnt);  
}
					
lword GetFreq(void)
{
 lword i;	
 i = (FreqString[6] -'0')*100000;
 i+= (lword)(FreqString[7] -'0')*10000;
 i+= (lword)(FreqString[8] -'0')*1000;
 i+= (lword)(FreqString[10]-'0')*100;
 i+= (lword)(FreqString[11]-'0')*10;
 i+= (lword)(FreqString[12]-'0');
 return(i);
}

/*
 * THE FOLLOWING FIRMWARE IS PROVIDED: 
 *  (1) "AS IS" WITH NO WARRANTY; 
 *  (2) TO ENABLE ACCESS TO CODING INFORMATION TO GUIDE AND FACILITATE CUSTOMER.
 * CONSEQUENTLY, HopeRF SHALL NOT BE HELD LIABLE FOR ANY DIRECT, INDIRECT OR
 * CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE CONTENT
 * OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING INFORMATION
 * CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
 * 
 * Copyright (C) HopeRF
 *
 * website: www.HopeRF.com
 *          www.HopeRF.cn     
 */

/*! 
 * file       RFduino_RFM67.cpp
 * brief      driver for RFM67 Tx
 * hardware   HopeRF's RFduino TRx & HopeRF RFM67 COB rf-module   
 *            
 *
 * version    1.0
 * date       May 31 2016
 * author     QY Ruan
 */

#ifndef HopeDuino_RFM67_h
	#define HopeDuino_RFM67_h
	
	#ifndef F_CPU
		#define F_CPU 16000000UL		//for delay.h to use, & RFduino use 16MHz Xo
	#endif	

	#ifndef	byte
		typedef unsigned char byte;
	#endif
	
	#ifndef word
		typedef unsigned int  word;
	#endif
	
	#ifndef lword
		typedef unsigned long lword;
	#endif
	
	#ifndef __DELAY_BACKWARD_COMPATIBLE__
		#define __DELAY_BACKWARD_COMPATIBLE__
	#endif 
	
	#include <avr/io.h>
	#include <util/delay.h>
	#include "HopeDuino_SPI.h"
	
	/** Hardware brief **/
	/** POR & DIO0 is must be connect **/
	//PORTB						//DDRx		PORTx		PINx
	#define DCLK_67	0x01		// 0		  0          1
	#define	POR_67	0x02		// 0          0          1

	//PORTD
	/** DIO1~DIO4 can be select to connect **/
	#define	PLLLOCK	0x20		// 0          0          1
	#define	DATA_67	0x40		// 0          0          1
	#define EOL		0x80		// 0          0          1
	
	//Output
	#define RF67_POROut()	(DDRB |= POR_67)
	#define	RF67_PORIn()	(DDRB &= (~POR_67))
	#define RF67_SetPOR()	(PORTB |= POR_67)
	#define	RF67_ClrPOR()	(PORTB &= (~POR_67))	
	
	#define RF67_DatOut()	(DDRD |= DATA_67)
	#define	RF67_DatIn()	(DDRD &= (~DATA_67))
	#define	RF67_SetDat()	(PORTD |= DATA_67)
	#define RF67_ClrDat()	(PORTD &= (~DATA_67))
	
	
	//Input	
	#define	RF67_DclkIn()	(DDRB &= (~DCLK_67))
	#define	RF67_PllLockIn()	(DDRD &= (~PLLLOCK))
	#define	RF67_EolIn()	(DDRD &= (~EOL))
	
	#define RF67_Dclk_H()	((PINB&DCLK_67)==DCLK_67)
	#define RF67_Dclk_L()	((PINB&DCLK_67)==0)
	
	#define RF67_PLL_H()	((PIND&PLLLOCK)==PLLLOCK)
	#define RF67_PLL_L()	((PIND&PLLLOCK)==0)
	
	#define RF67_Data_H()	((PIND&DATA_67)==DATA_67)
	#define RF67_Data_L()	((PIND&DATA_67)==0)
	
	#define RF67_Eol_H()	((PIND&EOL)==EOL)
	#define RF67_Eol_L()	((PIND&EOL)==0)
	
	
	typedef union  
	{
	 struct
	 	{
	  	byte FreqL: 8;
	  	byte FreqM: 8;
	  	byte FreqH: 8;
	  	byte FreqX: 8;
	 	}freq;
	 unsigned long Freq;
	}FreqStruct;

	enum modulationType {OOK, FSK, GFSK};
	
	class rf67Class
	{
	 public:	
	 	modulationType Modulation;					//OOK/FSK/GFSK
	 	lword Frequency;							//unit: KHz
 		lword SymbolTime;							//unit: ns
 		lword Devation;								//unit: KHz
 		byte  OutputPower;							//unit: dBm   range: 0-31 [-13dBm~+17dBm] for RFM67
 		byte  PreambleLength;						//unit: byte  range: 0-31
	
		bool  CrcDisable;							//false: CRC enable�� & use CCITT 16bit 
													//true : CRC disable
		bool  CrcMode;								//false : CCITT-16 
													//ture: IBM-16
		word  CrcSeed;								//
		bool  CrcInverse;							//true : CRC result inverse
		                                            //false: CRC result do not inverse
		
		bool  NodeDisable;							//false: Node enable
		                                            //true : Node disable
		bool  NodeBefore;							//true : Preamble+Sync+Node+Length+Payload+CRC, on account for variable package
													//false: Preamble+Sync+Length+Node+Payload+CRC, [Length=Node+Payload+CRC]
		
		bool  FixedPktLength;						//false: for contain packet length in Tx message, the same mean with variable lenth
 		                                            //true : for doesn't include packet length in Tx message, the same mean with fixed length
 		byte  NodeAddr;								//
 		
 		byte  SyncLength;							//unit: none, range: 1-8[Byte], value '0' is not allowed!
 		byte  SyncWord[8];
		byte  PayloadLength;						//PayloadLength is need to be set a value, when FixedPktLength is true.
		
		void vInitialize(void);
		void vConfig(void);
		void vGoStandby(void);
		void vGoFs(void);
		void vGoTx(void);
		void vGoSleep(void);
		void vChangeFreq(lword freq);
		bool bSendMessage(byte msg[], byte length);
		
	 private:
	 	spiClass Spi;
	 	FreqStruct FrequencyValue;
	 	byte SampleRate;
	 	word BitRateValue;
		word DevationValue;
		void vRF67Reset(void);
		byte bSelectRamping(lword symbol);
		byte bAssembleMsg(byte inptr[], byte length, byte outptr[]);
		word wCRC16Bit(byte CRCData, word CRCResult);
		bool bTxPolling(byte dat);
	};
#else
	#warning "HopeDuino_RFM67.h have been defined!"

#endif
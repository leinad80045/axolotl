//==============================================================
// HopeRF HopeDuino
//
// Ardunio 3rd-party hardware package for HopeDuino
// Installation Guide
// 
// 2016/03/01
//  	Initial version for V1.0
//
// 2016/04/06
//  	Initial version for V1.1
//==============================================================

1. Features
	> Core for LGT8F328D/E 
	> Support arduino 1.0.5
	> Bootloader baudrate: 57600bps
	> Support board: Larduino w/ LGT8F328E and LGT8F168E

2. Package contents
	HopeRF_HSP_vX.X : package root directory
	|
	+ hardware : 3rd party hardware support package
	|    |
	|    + LGT : package for support arduino 1.5.x/1.6.x
	|    |
	|    + LGT8F : package for support arduino 1.0.x (for HopeDuino)
	|
	+ libraries : 3rd party hardware library
	     |
	     + LGTE2PROM : e2prom library & samples
	     |
	     + LGTSoftUART : Software UART library & samples
	     |
	     + LGTPMU: TPMU library & samples
	     |
	     + LGTSPI: SPI library & samples
	     |
	     + LGTWire: Wire library & samples
	     |
	     + HopeRFLib: HopeRF RF-Module & Sensor library & samples (for HopeDuino)

3. Installation:
	> Unzip HopeDuino_HSP_vX.X.rar
	> Copy [hardware] and [libraries] directories to arduino's sketchbook direcotry
	> Restart Arduino, you will see new board from [Tools]->[Border] menu.

4. about arduino's sketchbook directory:
	You can always find this directory from [File]->[Preferences] menu.
	Here is the default sketchbook directory for most popluar system:
	> Windows: C:\Users\<Username>\Documents\Arduino
	> Mac OSX: /Users/user/Documents/Arduino
	> LINUX: /home/<Username>/sketchbook

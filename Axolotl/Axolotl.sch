<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.0.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC" urn="urn:adsk.eagle:symbol:13874/1" library_version="1">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" urn="urn:adsk.eagle:component:13926/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Axolotl">
<packages>
<package name="TRISOLX_SOLAR_WING">
<wire x1="-14" y1="-4" x2="12.3" y2="-4" width="0.127" layer="21"/>
<wire x1="12.3" y1="-4" x2="12.3" y2="4" width="0.127" layer="21"/>
<wire x1="12.3" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-14" y1="-4" x2="-4" y2="4" width="0.127" layer="21" curve="-80"/>
<smd name="-" x="12.4" y="-0.7" dx="6" dy="1" layer="1" rot="R90"/>
<smd name="+" x="0" y="0" dx="20" dy="3" layer="1"/>
</package>
<package name="TQFP-32A-(7X7MM)">
<wire x1="-3.5" y1="3.2" x2="-3.5" y2="-3" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3" x2="-3.1" y2="-3.4" width="0.127" layer="21"/>
<wire x1="-3.1" y1="-3.4" x2="3.1" y2="-3.4" width="0.127" layer="21"/>
<wire x1="3.1" y1="-3.4" x2="3.5" y2="-3" width="0.127" layer="21"/>
<wire x1="3.5" y1="-3" x2="3.5" y2="3.2" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.2" x2="3.1" y2="3.6" width="0.127" layer="21"/>
<wire x1="3.1" y1="3.6" x2="-3.1" y2="3.6" width="0.127" layer="21"/>
<wire x1="-3.1" y1="3.6" x2="-3.5" y2="3.2" width="0.127" layer="21"/>
<smd name="13" x="0.4" y="-4.35" dx="0.4" dy="1.9" layer="1"/>
<smd name="12" x="-0.4" y="-4.35" dx="0.4" dy="1.9" layer="1"/>
<smd name="14" x="1.2" y="-4.35" dx="0.4" dy="1.9" layer="1"/>
<smd name="15" x="2" y="-4.35" dx="0.4" dy="1.9" layer="1"/>
<smd name="16" x="2.8" y="-4.35" dx="0.4" dy="1.9" layer="1"/>
<smd name="11" x="-1.2" y="-4.35" dx="0.4" dy="1.9" layer="1"/>
<smd name="10" x="-2" y="-4.35" dx="0.4" dy="1.9" layer="1"/>
<smd name="9" x="-2.8" y="-4.35" dx="0.4" dy="1.9" layer="1"/>
<smd name="4" x="-4.45" y="0.5" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="5" x="-4.45" y="-0.3" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="3" x="-4.45" y="1.3" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="2" x="-4.45" y="2.1" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="1" x="-4.45" y="2.9" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="6" x="-4.45" y="-1.1" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="7" x="-4.45" y="-1.9" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="8" x="-4.45" y="-2.7" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="21" x="4.45" y="0.5" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="22" x="4.45" y="1.3" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="23" x="4.45" y="2.1" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="24" x="4.45" y="2.9" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="20" x="4.45" y="-0.3" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="19" x="4.45" y="-1.1" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="18" x="4.45" y="-1.9" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="17" x="4.45" y="-2.7" dx="0.4" dy="1.9" layer="1" rot="R90"/>
<smd name="28" x="0.4" y="4.55" dx="0.4" dy="1.9" layer="1"/>
<smd name="27" x="1.2" y="4.55" dx="0.4" dy="1.9" layer="1"/>
<smd name="26" x="2" y="4.55" dx="0.4" dy="1.9" layer="1"/>
<smd name="25" x="2.8" y="4.55" dx="0.4" dy="1.9" layer="1"/>
<smd name="29" x="-0.4" y="4.55" dx="0.4" dy="1.9" layer="1"/>
<smd name="30" x="-1.2" y="4.55" dx="0.4" dy="1.9" layer="1"/>
<smd name="31" x="-2" y="4.55" dx="0.4" dy="1.9" layer="1"/>
<smd name="32" x="-2.8" y="4.55" dx="0.4" dy="1.9" layer="1"/>
<circle x="-2.6" y="2.7" radius="0.4472125" width="0.127" layer="21"/>
<text x="-3.2" y="5.8" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="S-PVQFN-N20">
<smd name="3" x="0.05" y="-1.7" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="18" x="-1.7" y="0.05" dx="0.8" dy="0.28" layer="1"/>
<smd name="13" x="0.05" y="1.8" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="8" x="1.8" y="0.05" dx="0.8" dy="0.28" layer="1"/>
<smd name="4" x="0.55" y="-1.7" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="5" x="1.05" y="-1.7" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="2" x="-0.45" y="-1.7" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="1" x="-0.95" y="-1.7" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="19" x="-1.7" y="-0.45" dx="0.8" dy="0.28" layer="1"/>
<smd name="20" x="-1.7" y="-0.95" dx="0.8" dy="0.28" layer="1"/>
<smd name="17" x="-1.7" y="0.55" dx="0.8" dy="0.28" layer="1"/>
<smd name="16" x="-1.7" y="1.05" dx="0.8" dy="0.28" layer="1"/>
<smd name="14" x="-0.45" y="1.8" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="15" x="-0.95" y="1.8" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="12" x="0.55" y="1.8" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="11" x="1.05" y="1.8" dx="0.8" dy="0.28" layer="1" rot="R90"/>
<smd name="9" x="1.8" y="0.55" dx="0.8" dy="0.28" layer="1"/>
<smd name="10" x="1.8" y="1.05" dx="0.8" dy="0.28" layer="1"/>
<smd name="7" x="1.8" y="-0.45" dx="0.8" dy="0.28" layer="1"/>
<smd name="6" x="1.8" y="-0.95" dx="0.8" dy="0.28" layer="1"/>
<smd name="THERMAL" x="0.05" y="0.05" dx="1.6" dy="1.6" layer="1"/>
<wire x1="-1.7" y1="-1.7" x2="1.8" y2="-1.7" width="0.05" layer="21"/>
<wire x1="1.8" y1="-1.7" x2="1.8" y2="1.8" width="0.05" layer="21"/>
<wire x1="1.8" y1="1.8" x2="-1.7" y2="1.8" width="0.05" layer="21"/>
<wire x1="-1.7" y1="1.8" x2="-1.7" y2="-1.7" width="0.05" layer="21"/>
<text x="-1.7" y="2.4" size="0.7" layer="25">&gt;NAME</text>
<circle x="-1.4" y="-1.421" radius="0.2" width="0.05" layer="21"/>
</package>
<package name="SI1145/46/47">
<smd name="1" x="-0.95" y="0.75" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="-0.95" y="0.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="3" x="-0.95" y="-0.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="4" x="-0.95" y="-0.75" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="9" x="0.95" y="0.75" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="8" x="0.95" y="0.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="7" x="0.95" y="-0.25" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="6" x="0.95" y="-0.75" dx="0.3" dy="0.8" layer="1" rot="R90"/>
<smd name="10" x="0" y="0.95" dx="0.3" dy="0.8" layer="1" rot="R180"/>
<smd name="5" x="0" y="-0.95" dx="0.3" dy="0.8" layer="1" rot="R180"/>
<wire x1="-1" y1="1" x2="1" y2="1" width="0.05" layer="21"/>
<wire x1="1" y1="1" x2="1" y2="-1" width="0.05" layer="21"/>
<wire x1="1" y1="-1" x2="-1" y2="-1" width="0.05" layer="21"/>
<wire x1="-1" y1="-1" x2="-1" y2="1" width="0.05" layer="21"/>
<circle x="-1.25" y="1.25" radius="0.180275" width="0.05" layer="21"/>
<text x="2" y="-1.05" size="0.5" layer="21" rot="R90">&gt;NAME</text>
</package>
<package name="DRL0006A">
<smd name="3" x="-0.762" y="-0.508" dx="0.3" dy="0.67" layer="1" rot="R90"/>
<smd name="2" x="-0.762" y="-0.008" dx="0.3" dy="0.67" layer="1" rot="R90"/>
<smd name="1" x="-0.762" y="0.492" dx="0.3" dy="0.67" layer="1" rot="R90"/>
<smd name="4" x="0.718" y="-0.508" dx="0.3" dy="0.67" layer="1" rot="R90"/>
<smd name="5" x="0.718" y="-0.008" dx="0.3" dy="0.67" layer="1" rot="R90"/>
<smd name="6" x="0.718" y="0.492" dx="0.3" dy="0.67" layer="1" rot="R90"/>
<wire x1="-0.762" y1="0.792" x2="0.738" y2="0.792" width="0.05" layer="21"/>
<wire x1="0.738" y1="0.792" x2="0.738" y2="-0.808" width="0.05" layer="21"/>
<wire x1="0.738" y1="-0.808" x2="-0.762" y2="-0.808" width="0.05" layer="21"/>
<wire x1="-0.762" y1="-0.808" x2="-0.762" y2="0.792" width="0.05" layer="21"/>
<circle x="-0.962" y="0.992" radius="0.14141875" width="0.05" layer="21"/>
<text x="-1.308" y="-1.438" size="0.5" layer="21">&gt;NAME</text>
</package>
<package name="LGA-24-LEAD">
<wire x1="-1.8" y1="-1.5" x2="1.7" y2="-1.5" width="0.05" layer="21"/>
<wire x1="-1.8" y1="-1.5" x2="-1.8" y2="1.5" width="0.05" layer="21"/>
<wire x1="-1.8" y1="1.5" x2="1.7" y2="1.5" width="0.05" layer="21"/>
<wire x1="1.7" y1="1.5" x2="1.7" y2="-1.5" width="0.05" layer="21"/>
<smd name="9" x="-0.265" y="-1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="10" x="0.165" y="-1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="11" x="0.595" y="-1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="12" x="1.025" y="-1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="13" x="1.455" y="-1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="8" x="-0.695" y="-1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="7" x="-1.125" y="-1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="6" x="-1.555" y="-1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="21" x="0.165" y="1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="20" x="0.595" y="1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="19" x="1.025" y="1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="18" x="1.455" y="1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="22" x="-0.265" y="1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="23" x="-0.695" y="1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="24" x="-1.125" y="1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="1" x="-1.555" y="1.225" dx="0.23" dy="0.35" layer="1"/>
<smd name="3" x="-1.525" y="0.215" dx="0.23" dy="0.35" layer="1" rot="R90"/>
<smd name="4" x="-1.525" y="-0.215" dx="0.23" dy="0.35" layer="1" rot="R90"/>
<smd name="2" x="-1.525" y="0.645" dx="0.23" dy="0.35" layer="1" rot="R90"/>
<smd name="5" x="-1.525" y="-0.645" dx="0.23" dy="0.35" layer="1" rot="R90"/>
<smd name="16" x="1.425" y="0.215" dx="0.23" dy="0.35" layer="1" rot="R90"/>
<smd name="15" x="1.425" y="-0.215" dx="0.23" dy="0.35" layer="1" rot="R90"/>
<smd name="17" x="1.425" y="0.645" dx="0.23" dy="0.35" layer="1" rot="R90"/>
<smd name="14" x="1.425" y="-0.645" dx="0.23" dy="0.35" layer="1" rot="R90"/>
<circle x="-1.6" y="1.8" radius="0.14141875" width="0.05" layer="21"/>
<text x="-1.8" y="-2.1" size="0.5" layer="25">&gt;NAME</text>
</package>
<package name="DPT301120">
<wire x1="-11" y1="-5" x2="9" y2="-5" width="0.127" layer="21"/>
<wire x1="9" y1="-5" x2="9" y2="6" width="0.127" layer="21"/>
<wire x1="9" y1="6" x2="-11" y2="6" width="0.127" layer="21"/>
<wire x1="-11" y1="6" x2="-11" y2="-5" width="0.127" layer="21"/>
<pad name="P$1" x="10.746" y="5.254" drill="0.7" shape="octagon"/>
<pad name="P$2" x="10.746" y="3.381" drill="0.7" shape="square" rot="R180"/>
<text x="12.241" y="4.779" size="1.27" layer="21">+</text>
<text x="-4" y="0" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="C0805" urn="urn:adsk.eagle:footprint:23124/1">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="R0603" urn="urn:adsk.eagle:footprint:23044/1" locally_modified="yes">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="RFM95/95/97">
<wire x1="-8" y1="-8" x2="8" y2="-8" width="0.127" layer="21"/>
<wire x1="8" y1="8" x2="-8" y2="8" width="0.127" layer="21"/>
<smd name="P$18" x="-8" y="-7" dx="2" dy="1" layer="1"/>
<smd name="P$1" x="-8" y="-5" dx="2" dy="1" layer="1"/>
<smd name="P$2" x="-8" y="-3" dx="2" dy="1" layer="1"/>
<smd name="P$3" x="-8" y="-1" dx="2" dy="1" layer="1"/>
<smd name="P$4" x="-8" y="1" dx="2" dy="1" layer="1"/>
<smd name="P$5" x="-8" y="3" dx="2" dy="1" layer="1"/>
<smd name="P$6" x="-8" y="5" dx="2" dy="1" layer="1"/>
<smd name="P$7" x="-8" y="7" dx="2" dy="1" layer="1"/>
<smd name="P$8" x="8" y="-7" dx="2" dy="1" layer="1"/>
<smd name="P$9" x="8" y="-5" dx="2" dy="1" layer="1"/>
<smd name="P$10" x="8" y="-3" dx="2" dy="1" layer="1"/>
<smd name="P$11" x="8" y="-1" dx="2" dy="1" layer="1"/>
<smd name="P$12" x="8" y="1" dx="2" dy="1" layer="1"/>
<smd name="P$13" x="8" y="3" dx="2" dy="1" layer="1"/>
<smd name="P$14" x="8" y="5" dx="2" dy="1" layer="1"/>
<smd name="P$15" x="8" y="7" dx="2" dy="1" layer="1"/>
<wire x1="-8" y1="8" x2="-8" y2="-8" width="0.127" layer="21"/>
<wire x1="8" y1="8" x2="8" y2="-8" width="0.127" layer="21"/>
<text x="-8" y="9" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="ECS-SR-B">
<description>Murata CSTCR_G resonator series</description>
<smd name="P$1" x="-1.5" y="0" dx="0.8" dy="2.6" layer="1"/>
<smd name="P$2" x="0" y="0" dx="0.8" dy="2.6" layer="1"/>
<smd name="P$3" x="1.5" y="0" dx="0.8" dy="2.6" layer="1"/>
<wire x1="-2" y1="-1.3" x2="2" y2="-1.3" width="0.127" layer="21"/>
<wire x1="2" y1="-1.3" x2="2" y2="1.3" width="0.127" layer="21"/>
<wire x1="2" y1="1.3" x2="-2" y2="1.3" width="0.127" layer="21"/>
<wire x1="-2" y1="1.3" x2="-2" y2="-1.3" width="0.127" layer="21"/>
<text x="-2.1" y="1.6" size="0.5" layer="25">&gt;NAME</text>
</package>
<package name="V-DIPOLE">
<pad name="P$1" x="-2" y="1" drill="0.6" shape="offset" rot="R315"/>
<pad name="P$2" x="2" y="1" drill="0.6" shape="offset" rot="R225"/>
<text x="-3" y="-2" size="1.27" layer="25">&gt;NAME</text>
</package>
<package name="AXOLOTL_LOGO">
<wire x1="0.03193125" y1="-3.40125" x2="-0.04861875" y2="-3.39975" width="0.05" layer="21"/>
<wire x1="-0.04861875" y1="-3.39975" x2="-0.0467" y2="-3.19111875" width="0.05" layer="21"/>
<wire x1="-0.0467" y1="-3.19111875" x2="-0.044490625" y2="-3.02136875" width="0.05" layer="21"/>
<wire x1="-0.044490625" y1="-3.02136875" x2="-0.041859375" y2="-2.906240625" width="0.05" layer="21"/>
<wire x1="-0.041859375" y1="-2.906240625" x2="-0.03893125" y2="-2.829990625" width="0.05" layer="21"/>
<wire x1="-0.03893125" y1="-2.829990625" x2="0.26" y2="-2.829990625" width="0.05" layer="21"/>
<wire x1="0.26" y1="-2.829990625" x2="0.26" y2="-3.258959375" width="0.05" layer="21"/>
<wire x1="0.26" y1="-3.258959375" x2="0.32875" y2="-3.26268125" width="0.05" layer="21"/>
<wire x1="0.32875" y1="-3.26268125" x2="0.38448125" y2="-3.263840625" width="0.05" layer="21"/>
<wire x1="0.38448125" y1="-3.263840625" x2="0.485409375" y2="-3.264259375" width="0.05" layer="21"/>
<wire x1="0.485409375" y1="-3.264259375" x2="0.76553125" y2="-3.26281875" width="0.05" layer="21"/>
<wire x1="0.76553125" y1="-3.26281875" x2="1.13356875" y2="-3.25923125" width="0.05" layer="21"/>
<wire x1="1.13356875" y1="-3.25923125" x2="1.13673125" y2="-3.278359375" width="0.05" layer="21"/>
<wire x1="1.13673125" y1="-3.278359375" x2="1.13898125" y2="-3.3067" width="0.05" layer="21"/>
<wire x1="1.13898125" y1="-3.3067" x2="1.139940625" y2="-3.347959375" width="0.05" layer="21"/>
<wire x1="1.139940625" y1="-3.347959375" x2="1.14" y2="-3.39841875" width="0.05" layer="21"/>
<wire x1="1.14" y1="-3.39841875" x2="1.06125" y2="-3.401690625" width="0.05" layer="21"/>
<wire x1="1.06125" y1="-3.401690625" x2="1.00261875" y2="-3.402690625" width="0.05" layer="21"/>
<wire x1="1.00261875" y1="-3.402690625" x2="0.89305" y2="-3.40338125" width="0.05" layer="21"/>
<wire x1="0.89305" y1="-3.40338125" x2="0.582940625" y2="-3.40383125" width="0.05" layer="21"/>
<wire x1="0.582940625" y1="-3.40383125" x2="0.254659375" y2="-3.403109375" width="0.05" layer="21"/>
<wire x1="0.254659375" y1="-3.403109375" x2="0.03195" y2="-3.40125" width="0.05" layer="21"/>
<wire x1="0.03193125" y1="-3.40125" x2="0.03195" y2="-3.40125" width="0.05" layer="21"/>
<wire x1="1.43293125" y1="-3.389809375" x2="1.408440625" y2="-3.384459375" width="0.05" layer="21"/>
<wire x1="1.408440625" y1="-3.384459375" x2="1.385340625" y2="-3.37713125" width="0.05" layer="21"/>
<wire x1="1.385340625" y1="-3.37713125" x2="1.36385" y2="-3.36798125" width="0.05" layer="21"/>
<wire x1="1.36385" y1="-3.36798125" x2="1.34421875" y2="-3.35715" width="0.05" layer="21"/>
<wire x1="1.34421875" y1="-3.35715" x2="1.326659375" y2="-3.34476875" width="0.05" layer="21"/>
<wire x1="1.326659375" y1="-3.34476875" x2="1.3114" y2="-3.331" width="0.05" layer="21"/>
<wire x1="1.3114" y1="-3.331" x2="1.29866875" y2="-3.31596875" width="0.05" layer="21"/>
<wire x1="1.29866875" y1="-3.31596875" x2="1.29331875" y2="-3.30803125" width="0.05" layer="21"/>
<wire x1="1.29331875" y1="-3.30803125" x2="1.2887" y2="-3.29983125" width="0.05" layer="21"/>
<wire x1="1.2887" y1="-3.29983125" x2="1.2747" y2="-3.27245" width="0.05" layer="21"/>
<wire x1="1.2747" y1="-3.27245" x2="1.27156875" y2="-3.12171875" width="0.05" layer="21"/>
<wire x1="1.27156875" y1="-3.12171875" x2="1.270359375" y2="-3.0477" width="0.05" layer="21"/>
<wire x1="1.270359375" y1="-3.0477" x2="1.270459375" y2="-2.99758125" width="0.05" layer="21"/>
<wire x1="1.270459375" y1="-2.99758125" x2="1.27198125" y2="-2.96598125" width="0.05" layer="21"/>
<wire x1="1.27198125" y1="-2.96598125" x2="1.273290625" y2="-2.955459375" width="0.05" layer="21"/>
<wire x1="1.273290625" y1="-2.955459375" x2="1.275" y2="-2.94755" width="0.05" layer="21"/>
<wire x1="1.275" y1="-2.94755" x2="1.279540625" y2="-2.9355" width="0.05" layer="21"/>
<wire x1="1.279540625" y1="-2.9355" x2="1.2864" y2="-2.92298125" width="0.05" layer="21"/>
<wire x1="1.2864" y1="-2.92298125" x2="1.29523125" y2="-2.91033125" width="0.05" layer="21"/>
<wire x1="1.29523125" y1="-2.91033125" x2="1.305690625" y2="-2.89795" width="0.05" layer="21"/>
<wire x1="1.305690625" y1="-2.89795" x2="1.317440625" y2="-2.886190625" width="0.05" layer="21"/>
<wire x1="1.317440625" y1="-2.886190625" x2="1.330140625" y2="-2.87543125" width="0.05" layer="21"/>
<wire x1="1.330140625" y1="-2.87543125" x2="1.34345" y2="-2.866040625" width="0.05" layer="21"/>
<wire x1="1.34345" y1="-2.866040625" x2="1.35703125" y2="-2.858390625" width="0.05" layer="21"/>
<wire x1="1.35703125" y1="-2.858390625" x2="1.376659375" y2="-2.849309375" width="0.05" layer="21"/>
<wire x1="1.376659375" y1="-2.849309375" x2="1.386490625" y2="-2.8456" width="0.05" layer="21"/>
<wire x1="1.386490625" y1="-2.8456" x2="1.39725" y2="-2.842390625" width="0.05" layer="21"/>
<wire x1="1.39725" y1="-2.842390625" x2="1.40963125" y2="-2.83963125" width="0.05" layer="21"/>
<wire x1="1.40963125" y1="-2.83963125" x2="1.42433125" y2="-2.837259375" width="0.05" layer="21"/>
<wire x1="1.42433125" y1="-2.837259375" x2="1.463440625" y2="-2.83356875" width="0.05" layer="21"/>
<wire x1="1.463440625" y1="-2.83356875" x2="1.52011875" y2="-2.83093125" width="0.05" layer="21"/>
<wire x1="1.52011875" y1="-2.83093125" x2="1.599890625" y2="-2.829" width="0.05" layer="21"/>
<wire x1="1.599890625" y1="-2.829" x2="1.85088125" y2="-2.82575" width="0.05" layer="21"/>
<wire x1="1.85088125" y1="-2.82575" x2="2.13981875" y2="-2.82385" width="0.05" layer="21"/>
<wire x1="2.13981875" y1="-2.82385" x2="2.233640625" y2="-2.824209375" width="0.05" layer="21"/>
<wire x1="2.233640625" y1="-2.824209375" x2="2.27513125" y2="-2.82545" width="0.05" layer="21"/>
<wire x1="2.27513125" y1="-2.82545" x2="2.30233125" y2="-2.831009375" width="0.05" layer="21"/>
<wire x1="2.30233125" y1="-2.831009375" x2="2.3281" y2="-2.838790625" width="0.05" layer="21"/>
<wire x1="2.3281" y1="-2.838790625" x2="2.352140625" y2="-2.8486" width="0.05" layer="21"/>
<wire x1="2.352140625" y1="-2.8486" x2="2.374140625" y2="-2.860259375" width="0.05" layer="21"/>
<wire x1="2.374140625" y1="-2.860259375" x2="2.393809375" y2="-2.8736" width="0.05" layer="21"/>
<wire x1="2.393809375" y1="-2.8736" x2="2.410859375" y2="-2.888440625" width="0.05" layer="21"/>
<wire x1="2.410859375" y1="-2.888440625" x2="2.418309375" y2="-2.896359375" width="0.05" layer="21"/>
<wire x1="2.418309375" y1="-2.896359375" x2="2.42498125" y2="-2.9046" width="0.05" layer="21"/>
<wire x1="2.42498125" y1="-2.9046" x2="2.43085" y2="-2.913109375" width="0.05" layer="21"/>
<wire x1="2.43085" y1="-2.913109375" x2="2.43588125" y2="-2.921890625" width="0.05" layer="21"/>
<wire x1="2.43588125" y1="-2.921890625" x2="2.4389" y2="-2.928609375" width="0.05" layer="21"/>
<wire x1="2.4389" y1="-2.928609375" x2="2.441359375" y2="-2.936659375" width="0.05" layer="21"/>
<wire x1="2.441359375" y1="-2.936659375" x2="2.44338125" y2="-2.947309375" width="0.05" layer="21"/>
<wire x1="2.44338125" y1="-2.947309375" x2="2.44508125" y2="-2.961840625" width="0.05" layer="21"/>
<wire x1="2.44508125" y1="-2.961840625" x2="2.44798125" y2="-3.007540625" width="0.05" layer="21"/>
<wire x1="2.44798125" y1="-3.007540625" x2="2.45101875" y2="-3.0839" width="0.05" layer="21"/>
<wire x1="2.45101875" y1="-3.0839" x2="2.453109375" y2="-3.15125" width="0.05" layer="21"/>
<wire x1="2.453109375" y1="-3.15125" x2="2.45373125" y2="-3.1986" width="0.05" layer="21"/>
<wire x1="2.45373125" y1="-3.1986" x2="2.45286875" y2="-3.22895" width="0.05" layer="21"/>
<wire x1="2.45286875" y1="-3.22895" x2="2.45188125" y2="-3.23866875" width="0.05" layer="21"/>
<wire x1="2.45188125" y1="-3.23866875" x2="2.45053125" y2="-3.24526875" width="0.05" layer="21"/>
<wire x1="2.45053125" y1="-3.24526875" x2="2.44716875" y2="-3.25433125" width="0.05" layer="21"/>
<wire x1="2.44716875" y1="-3.25433125" x2="2.44256875" y2="-3.263359375" width="0.05" layer="21"/>
<wire x1="2.44256875" y1="-3.263359375" x2="2.4368" y2="-3.272309375" width="0.05" layer="21"/>
<wire x1="2.4368" y1="-3.272309375" x2="2.42995" y2="-3.28113125" width="0.05" layer="21"/>
<wire x1="2.42995" y1="-3.28113125" x2="2.42208125" y2="-3.289759375" width="0.05" layer="21"/>
<wire x1="2.42208125" y1="-3.289759375" x2="2.41326875" y2="-3.29815" width="0.05" layer="21"/>
<wire x1="2.41326875" y1="-3.29815" x2="2.4036" y2="-3.30625" width="0.05" layer="21"/>
<wire x1="2.4036" y1="-3.30625" x2="2.39313125" y2="-3.314009375" width="0.05" layer="21"/>
<wire x1="2.39313125" y1="-3.314009375" x2="2.370140625" y2="-3.32828125" width="0.05" layer="21"/>
<wire x1="2.370140625" y1="-3.32828125" x2="2.344890625" y2="-3.34055" width="0.05" layer="21"/>
<wire x1="2.344890625" y1="-3.34055" x2="2.31798125" y2="-3.3504" width="0.05" layer="21"/>
<wire x1="2.31798125" y1="-3.3504" x2="2.304090625" y2="-3.35428125" width="0.05" layer="21"/>
<wire x1="2.304090625" y1="-3.35428125" x2="2.29001875" y2="-3.3574" width="0.05" layer="21"/>
<wire x1="2.29001875" y1="-3.3574" x2="2.268890625" y2="-3.360159375" width="0.05" layer="21"/>
<wire x1="2.268890625" y1="-3.360159375" x2="2.233190625" y2="-3.36316875" width="0.05" layer="21"/>
<wire x1="2.233190625" y1="-3.36316875" x2="2.12036875" y2="-3.369809375" width="0.05" layer="21"/>
<wire x1="2.12036875" y1="-3.369809375" x2="1.95611875" y2="-3.377109375" width="0.05" layer="21"/>
<wire x1="1.95611875" y1="-3.377109375" x2="1.74501875" y2="-3.38483125" width="0.05" layer="21"/>
<wire x1="1.74501875" y1="-3.38483125" x2="1.588290625" y2="-3.38996875" width="0.05" layer="21"/>
<wire x1="1.588290625" y1="-3.38996875" x2="1.49923125" y2="-3.39226875" width="0.05" layer="21"/>
<wire x1="1.49923125" y1="-3.39226875" x2="1.455040625" y2="-3.3921" width="0.05" layer="21"/>
<wire x1="1.455040625" y1="-3.3921" x2="1.44265" y2="-3.3912" width="0.05" layer="21"/>
<wire x1="1.44265" y1="-3.3912" x2="1.43291875" y2="-3.389809375" width="0.05" layer="21"/>
<wire x1="1.43291875" y1="-3.389809375" x2="1.43293125" y2="-3.389809375" width="0.05" layer="21"/>
<wire x1="1.97251875" y1="-3.23926875" x2="2.09655" y2="-3.234909375" width="0.05" layer="21"/>
<wire x1="2.09655" y1="-3.234909375" x2="2.149009375" y2="-3.23238125" width="0.05" layer="21"/>
<wire x1="2.149009375" y1="-3.23238125" x2="2.14903125" y2="-3.19155" width="0.05" layer="21"/>
<wire x1="2.14903125" y1="-3.19155" x2="2.14696875" y2="-3.09506875" width="0.05" layer="21"/>
<wire x1="2.14696875" y1="-3.09506875" x2="2.143440625" y2="-2.95896875" width="0.05" layer="21"/>
<wire x1="2.143440625" y1="-2.95896875" x2="1.93548125" y2="-2.96205" width="0.05" layer="21"/>
<wire x1="1.93548125" y1="-2.96205" x2="1.766209375" y2="-2.9651" width="0.05" layer="21"/>
<wire x1="1.766209375" y1="-2.9651" x2="1.65126875" y2="-2.968090625" width="0.05" layer="21"/>
<wire x1="1.65126875" y1="-2.968090625" x2="1.57501875" y2="-2.971040625" width="0.05" layer="21"/>
<wire x1="1.57501875" y1="-2.971040625" x2="1.57501875" y2="-3.08731875" width="0.05" layer="21"/>
<wire x1="1.57501875" y1="-3.08731875" x2="1.57598125" y2="-3.17646875" width="0.05" layer="21"/>
<wire x1="1.57598125" y1="-3.17646875" x2="1.57703125" y2="-3.209059375" width="0.05" layer="21"/>
<wire x1="1.57703125" y1="-3.209059375" x2="1.5783" y2="-3.22753125" width="0.05" layer="21"/>
<wire x1="1.5783" y1="-3.22753125" x2="1.58158125" y2="-3.251459375" width="0.05" layer="21"/>
<wire x1="1.58158125" y1="-3.251459375" x2="1.68955" y2="-3.248209375" width="0.05" layer="21"/>
<wire x1="1.68955" y1="-3.248209375" x2="1.97251875" y2="-3.23926875" width="0.05" layer="21"/>
<wire x1="-0.69748125" y1="-3.38206875" x2="-0.906690625" y2="-3.37318125" width="0.05" layer="21"/>
<wire x1="-0.906690625" y1="-3.37318125" x2="-1.06183125" y2="-3.365309375" width="0.05" layer="21"/>
<wire x1="-1.06183125" y1="-3.365309375" x2="-1.1638" y2="-3.358409375" width="0.05" layer="21"/>
<wire x1="-1.1638" y1="-3.358409375" x2="-1.19513125" y2="-3.355309375" width="0.05" layer="21"/>
<wire x1="-1.19513125" y1="-3.355309375" x2="-1.213490625" y2="-3.35243125" width="0.05" layer="21"/>
<wire x1="-1.213490625" y1="-3.35243125" x2="-1.22731875" y2="-3.348709375" width="0.05" layer="21"/>
<wire x1="-1.22731875" y1="-3.348709375" x2="-1.24176875" y2="-3.343890625" width="0.05" layer="21"/>
<wire x1="-1.24176875" y1="-3.343890625" x2="-1.27096875" y2="-3.331659375" width="0.05" layer="21"/>
<wire x1="-1.27096875" y1="-3.331659375" x2="-1.28491875" y2="-3.32465" width="0.05" layer="21"/>
<wire x1="-1.28491875" y1="-3.32465" x2="-1.2979" y2="-3.31728125" width="0.05" layer="21"/>
<wire x1="-1.2979" y1="-3.31728125" x2="-1.3095" y2="-3.309759375" width="0.05" layer="21"/>
<wire x1="-1.3095" y1="-3.309759375" x2="-1.31933125" y2="-3.302259375" width="0.05" layer="21"/>
<wire x1="-1.31933125" y1="-3.302259375" x2="-1.326559375" y2="-3.295259375" width="0.05" layer="21"/>
<wire x1="-1.326559375" y1="-3.295259375" x2="-1.33453125" y2="-3.28603125" width="0.05" layer="21"/>
<wire x1="-1.33453125" y1="-3.28603125" x2="-1.34228125" y2="-3.27578125" width="0.05" layer="21"/>
<wire x1="-1.34228125" y1="-3.27578125" x2="-1.34883125" y2="-3.265709375" width="0.05" layer="21"/>
<wire x1="-1.34883125" y1="-3.265709375" x2="-1.3625" y2="-3.2425" width="0.05" layer="21"/>
<wire x1="-1.3625" y1="-3.2425" x2="-1.36091875" y2="-3.1375" width="0.05" layer="21"/>
<wire x1="-1.36091875" y1="-3.1375" x2="-1.35866875" y2="-3.0475" width="0.05" layer="21"/>
<wire x1="-1.35866875" y1="-3.0475" x2="-1.355490625" y2="-2.97856875" width="0.05" layer="21"/>
<wire x1="-1.355490625" y1="-2.97856875" x2="-1.352990625" y2="-2.94916875" width="0.05" layer="21"/>
<wire x1="-1.352990625" y1="-2.94916875" x2="-1.34988125" y2="-2.930109375" width="0.05" layer="21"/>
<wire x1="-1.34988125" y1="-2.930109375" x2="-1.34771875" y2="-2.92296875" width="0.05" layer="21"/>
<wire x1="-1.34771875" y1="-2.92296875" x2="-1.344959375" y2="-2.91661875" width="0.05" layer="21"/>
<wire x1="-1.344959375" y1="-2.91661875" x2="-1.33706875" y2="-2.903909375" width="0.05" layer="21"/>
<wire x1="-1.33706875" y1="-2.903909375" x2="-1.3255" y2="-2.88966875" width="0.05" layer="21"/>
<wire x1="-1.3255" y1="-2.88966875" x2="-1.311609375" y2="-2.87645" width="0.05" layer="21"/>
<wire x1="-1.311609375" y1="-2.87645" x2="-1.295590625" y2="-2.86436875" width="0.05" layer="21"/>
<wire x1="-1.295590625" y1="-2.86436875" x2="-1.27765" y2="-2.85351875" width="0.05" layer="21"/>
<wire x1="-1.27765" y1="-2.85351875" x2="-1.258" y2="-2.84401875" width="0.05" layer="21"/>
<wire x1="-1.258" y1="-2.84401875" x2="-1.23683125" y2="-2.83598125" width="0.05" layer="21"/>
<wire x1="-1.23683125" y1="-2.83598125" x2="-1.21435" y2="-2.829509375" width="0.05" layer="21"/>
<wire x1="-1.21435" y1="-2.829509375" x2="-1.19075" y2="-2.82471875" width="0.05" layer="21"/>
<wire x1="-1.19075" y1="-2.82471875" x2="-1.177359375" y2="-2.823890625" width="0.05" layer="21"/>
<wire x1="-1.177359375" y1="-2.823890625" x2="-1.149759375" y2="-2.82333125" width="0.05" layer="21"/>
<wire x1="-1.149759375" y1="-2.82333125" x2="-1.05728125" y2="-2.82298125" width="0.05" layer="21"/>
<wire x1="-1.05728125" y1="-2.82298125" x2="-0.92401875" y2="-2.823640625" width="0.05" layer="21"/>
<wire x1="-0.92401875" y1="-2.823640625" x2="-0.760659375" y2="-2.8253" width="0.05" layer="21"/>
<wire x1="-0.760659375" y1="-2.8253" x2="-0.50931875" y2="-2.8288" width="0.05" layer="21"/>
<wire x1="-0.50931875" y1="-2.8288" x2="-0.430290625" y2="-2.83075" width="0.05" layer="21"/>
<wire x1="-0.430290625" y1="-2.83075" x2="-0.374690625" y2="-2.833340625" width="0.05" layer="21"/>
<wire x1="-0.374690625" y1="-2.833340625" x2="-0.3368" y2="-2.83695" width="0.05" layer="21"/>
<wire x1="-0.3368" y1="-2.83695" x2="-0.322690625" y2="-2.83925" width="0.05" layer="21"/>
<wire x1="-0.322690625" y1="-2.83925" x2="-0.310859375" y2="-2.84195" width="0.05" layer="21"/>
<wire x1="-0.310859375" y1="-2.84195" x2="-0.30058125" y2="-2.8451" width="0.05" layer="21"/>
<wire x1="-0.30058125" y1="-2.8451" x2="-0.291140625" y2="-2.848740625" width="0.05" layer="21"/>
<wire x1="-0.291140625" y1="-2.848740625" x2="-0.271890625" y2="-2.85768125" width="0.05" layer="21"/>
<wire x1="-0.271890625" y1="-2.85768125" x2="-0.25865" y2="-2.86476875" width="0.05" layer="21"/>
<wire x1="-0.25865" y1="-2.86476875" x2="-0.24586875" y2="-2.87291875" width="0.05" layer="21"/>
<wire x1="-0.24586875" y1="-2.87291875" x2="-0.23375" y2="-2.88193125" width="0.05" layer="21"/>
<wire x1="-0.23375" y1="-2.88193125" x2="-0.222490625" y2="-2.89161875" width="0.05" layer="21"/>
<wire x1="-0.222490625" y1="-2.89161875" x2="-0.212290625" y2="-2.901809375" width="0.05" layer="21"/>
<wire x1="-0.212290625" y1="-2.901809375" x2="-0.20335" y2="-2.912309375" width="0.05" layer="21"/>
<wire x1="-0.20335" y1="-2.912309375" x2="-0.19586875" y2="-2.922940625" width="0.05" layer="21"/>
<wire x1="-0.19586875" y1="-2.922940625" x2="-0.19005" y2="-2.93351875" width="0.05" layer="21"/>
<wire x1="-0.19005" y1="-2.93351875" x2="-0.18643125" y2="-2.941640625" width="0.05" layer="21"/>
<wire x1="-0.18643125" y1="-2.941640625" x2="-0.18373125" y2="-2.94988125" width="0.05" layer="21"/>
<wire x1="-0.18373125" y1="-2.94988125" x2="-0.18183125" y2="-2.960209375" width="0.05" layer="21"/>
<wire x1="-0.18183125" y1="-2.960209375" x2="-0.180640625" y2="-2.9746" width="0.05" layer="21"/>
<wire x1="-0.180640625" y1="-2.9746" x2="-0.17998125" y2="-3.0235" width="0.05" layer="21"/>
<wire x1="-0.17998125" y1="-3.0235" x2="-0.180940625" y2="-3.11236875" width="0.05" layer="21"/>
<wire x1="-0.180940625" y1="-3.11236875" x2="-0.18231875" y2="-3.1976" width="0.05" layer="21"/>
<wire x1="-0.18231875" y1="-3.1976" x2="-0.184240625" y2="-3.247090625" width="0.05" layer="21"/>
<wire x1="-0.184240625" y1="-3.247090625" x2="-0.18566875" y2="-3.26241875" width="0.05" layer="21"/>
<wire x1="-0.18566875" y1="-3.26241875" x2="-0.18756875" y2="-3.27361875" width="0.05" layer="21"/>
<wire x1="-0.18756875" y1="-3.27361875" x2="-0.19005" y2="-3.28228125" width="0.05" layer="21"/>
<wire x1="-0.19005" y1="-3.28228125" x2="-0.193209375" y2="-3.29" width="0.05" layer="21"/>
<wire x1="-0.193209375" y1="-3.29" x2="-0.198890625" y2="-3.300390625" width="0.05" layer="21"/>
<wire x1="-0.198890625" y1="-3.300390625" x2="-0.206509375" y2="-3.31123125" width="0.05" layer="21"/>
<wire x1="-0.206509375" y1="-3.31123125" x2="-0.215709375" y2="-3.32215" width="0.05" layer="21"/>
<wire x1="-0.215709375" y1="-3.32215" x2="-0.2261" y2="-3.332790625" width="0.05" layer="21"/>
<wire x1="-0.2261" y1="-3.332790625" x2="-0.237290625" y2="-3.342790625" width="0.05" layer="21"/>
<wire x1="-0.237290625" y1="-3.342790625" x2="-0.24891875" y2="-3.35176875" width="0.05" layer="21"/>
<wire x1="-0.24891875" y1="-3.35176875" x2="-0.260590625" y2="-3.359390625" width="0.05" layer="21"/>
<wire x1="-0.260590625" y1="-3.359390625" x2="-0.27193125" y2="-3.365259375" width="0.05" layer="21"/>
<wire x1="-0.27193125" y1="-3.365259375" x2="-0.2951" y2="-3.374359375" width="0.05" layer="21"/>
<wire x1="-0.2951" y1="-3.374359375" x2="-0.307259375" y2="-3.37805" width="0.05" layer="21"/>
<wire x1="-0.307259375" y1="-3.37805" x2="-0.320359375" y2="-3.3812" width="0.05" layer="21"/>
<wire x1="-0.320359375" y1="-3.3812" x2="-0.334790625" y2="-3.383809375" width="0.05" layer="21"/>
<wire x1="-0.334790625" y1="-3.383809375" x2="-0.35095" y2="-3.385909375" width="0.05" layer="21"/>
<wire x1="-0.35095" y1="-3.385909375" x2="-0.39013125" y2="-3.38863125" width="0.05" layer="21"/>
<wire x1="-0.39013125" y1="-3.38863125" x2="-0.44113125" y2="-3.38946875" width="0.05" layer="21"/>
<wire x1="-0.44113125" y1="-3.38946875" x2="-0.507190625" y2="-3.38858125" width="0.05" layer="21"/>
<wire x1="-0.507190625" y1="-3.38858125" x2="-0.59156875" y2="-3.38606875" width="0.05" layer="21"/>
<wire x1="-0.59156875" y1="-3.38606875" x2="-0.6975" y2="-3.38208125" width="0.05" layer="21"/>
<wire x1="-0.6975" y1="-3.38208125" x2="-0.69748125" y2="-3.38206875" width="0.05" layer="21"/>
<wire x1="-0.48826875" y1="-3.193740625" x2="-0.48596875" y2="-3.1295" width="0.05" layer="21"/>
<wire x1="-0.48596875" y1="-3.1295" x2="-0.485" y2="-3.054040625" width="0.05" layer="21"/>
<wire x1="-0.485" y1="-3.054040625" x2="-0.485" y2="-2.9706" width="0.05" layer="21"/>
<wire x1="-0.485" y1="-2.9706" x2="-0.50625" y2="-2.96795" width="0.05" layer="21"/>
<wire x1="-0.50625" y1="-2.96795" x2="-0.53526875" y2="-2.96666875" width="0.05" layer="21"/>
<wire x1="-0.53526875" y1="-2.96666875" x2="-0.59855" y2="-2.965140625" width="0.05" layer="21"/>
<wire x1="-0.59855" y1="-2.965140625" x2="-0.79061875" y2="-2.962109375" width="0.05" layer="21"/>
<wire x1="-0.79061875" y1="-2.962109375" x2="-1.05375" y2="-2.95891875" width="0.05" layer="21"/>
<wire x1="-1.05375" y1="-2.95891875" x2="-1.05745" y2="-3.092209375" width="0.05" layer="21"/>
<wire x1="-1.05745" y1="-3.092209375" x2="-1.059290625" y2="-3.18713125" width="0.05" layer="21"/>
<wire x1="-1.059290625" y1="-3.18713125" x2="-1.059240625" y2="-3.2166" width="0.05" layer="21"/>
<wire x1="-1.059240625" y1="-3.2166" x2="-1.058509375" y2="-3.22815" width="0.05" layer="21"/>
<wire x1="-1.058509375" y1="-3.22815" x2="-1.04525" y2="-3.22941875" width="0.05" layer="21"/>
<wire x1="-1.04525" y1="-3.22941875" x2="-1.0104" y2="-3.23138125" width="0.05" layer="21"/>
<wire x1="-1.0104" y1="-3.23138125" x2="-0.89518125" y2="-3.236559375" width="0.05" layer="21"/>
<wire x1="-0.89518125" y1="-3.236559375" x2="-0.75138125" y2="-3.242090625" width="0.05" layer="21"/>
<wire x1="-0.75138125" y1="-3.242090625" x2="-0.6175" y2="-3.24638125" width="0.05" layer="21"/>
<wire x1="-0.6175" y1="-3.24638125" x2="-0.52701875" y2="-3.24893125" width="0.05" layer="21"/>
<wire x1="-0.52701875" y1="-3.24893125" x2="-0.49153125" y2="-3.249990625" width="0.05" layer="21"/>
<wire x1="-0.49153125" y1="-3.249990625" x2="-0.488290625" y2="-3.193740625" width="0.05" layer="21"/>
<wire x1="-0.488290625" y1="-3.193740625" x2="-0.48826875" y2="-3.193740625" width="0.05" layer="21"/>
<wire x1="-1.65095" y1="-3.332090625" x2="-1.819409375" y2="-3.32031875" width="0.05" layer="21"/>
<wire x1="-1.819409375" y1="-3.32031875" x2="-1.934859375" y2="-3.23996875" width="0.05" layer="21"/>
<wire x1="-1.934859375" y1="-3.23996875" x2="-2.0503" y2="-3.159609375" width="0.05" layer="21"/>
<wire x1="-2.0503" y1="-3.159609375" x2="-2.16751875" y2="-3.2198" width="0.05" layer="21"/>
<wire x1="-2.16751875" y1="-3.2198" x2="-2.226390625" y2="-3.249559375" width="0.05" layer="21"/>
<wire x1="-2.226390625" y1="-3.249559375" x2="-2.265490625" y2="-3.267990625" width="0.05" layer="21"/>
<wire x1="-2.265490625" y1="-3.267990625" x2="-2.27926875" y2="-3.27366875" width="0.05" layer="21"/>
<wire x1="-2.27926875" y1="-3.27366875" x2="-2.29008125" y2="-3.27736875" width="0.05" layer="21"/>
<wire x1="-2.29008125" y1="-3.27736875" x2="-2.298559375" y2="-3.27938125" width="0.05" layer="21"/>
<wire x1="-2.298559375" y1="-3.27938125" x2="-2.30538125" y2="-3.279990625" width="0.05" layer="21"/>
<wire x1="-2.30538125" y1="-3.279990625" x2="-2.36026875" y2="-3.276240625" width="0.05" layer="21"/>
<wire x1="-2.36026875" y1="-3.276240625" x2="-2.460840625" y2="-3.26766875" width="0.05" layer="21"/>
<wire x1="-2.460840625" y1="-3.26766875" x2="-2.56223125" y2="-3.25826875" width="0.05" layer="21"/>
<wire x1="-2.56223125" y1="-3.25826875" x2="-2.619559375" y2="-3.25201875" width="0.05" layer="21"/>
<wire x1="-2.619559375" y1="-3.25201875" x2="-2.62478125" y2="-3.251040625" width="0.05" layer="21"/>
<wire x1="-2.62478125" y1="-3.251040625" x2="-2.627940625" y2="-3.24991875" width="0.05" layer="21"/>
<wire x1="-2.627940625" y1="-3.24991875" x2="-2.628640625" y2="-3.24836875" width="0.05" layer="21"/>
<wire x1="-2.628640625" y1="-3.24836875" x2="-2.62651875" y2="-3.246109375" width="0.05" layer="21"/>
<wire x1="-2.62651875" y1="-3.246109375" x2="-2.61225" y2="-3.23835" width="0.05" layer="21"/>
<wire x1="-2.61225" y1="-3.23835" x2="-2.582059375" y2="-3.22436875" width="0.05" layer="21"/>
<wire x1="-2.582059375" y1="-3.22436875" x2="-2.49128125" y2="-3.181" width="0.05" layer="21"/>
<wire x1="-2.49128125" y1="-3.181" x2="-2.37243125" y2="-3.12211875" width="0.05" layer="21"/>
<wire x1="-2.37243125" y1="-3.12211875" x2="-2.26913125" y2="-3.069609375" width="0.05" layer="21"/>
<wire x1="-2.26913125" y1="-3.069609375" x2="-2.236940625" y2="-3.05258125" width="0.05" layer="21"/>
<wire x1="-2.236940625" y1="-3.05258125" x2="-2.225" y2="-3.04535" width="0.05" layer="21"/>
<wire x1="-2.225" y1="-3.04535" x2="-2.230559375" y2="-3.040290625" width="0.05" layer="21"/>
<wire x1="-2.230559375" y1="-3.040290625" x2="-2.24641875" y2="-3.02891875" width="0.05" layer="21"/>
<wire x1="-2.24641875" y1="-3.02891875" x2="-2.303990625" y2="-2.990540625" width="0.05" layer="21"/>
<wire x1="-2.303990625" y1="-2.990540625" x2="-2.387659375" y2="-2.936759375" width="0.05" layer="21"/>
<wire x1="-2.387659375" y1="-2.936759375" x2="-2.487409375" y2="-2.87413125" width="0.05" layer="21"/>
<wire x1="-2.487409375" y1="-2.87413125" x2="-2.5701" y2="-2.82263125" width="0.05" layer="21"/>
<wire x1="-2.5701" y1="-2.82263125" x2="-2.593090625" y2="-2.807590625" width="0.05" layer="21"/>
<wire x1="-2.593090625" y1="-2.807590625" x2="-2.605540625" y2="-2.79811875" width="0.05" layer="21"/>
<wire x1="-2.605540625" y1="-2.79811875" x2="-2.6082" y2="-2.7951" width="0.05" layer="21"/>
<wire x1="-2.6082" y1="-2.7951" x2="-2.608690625" y2="-2.793040625" width="0.05" layer="21"/>
<wire x1="-2.608690625" y1="-2.793040625" x2="-2.60718125" y2="-2.79178125" width="0.05" layer="21"/>
<wire x1="-2.60718125" y1="-2.79178125" x2="-2.60381875" y2="-2.79118125" width="0.05" layer="21"/>
<wire x1="-2.60381875" y1="-2.79118125" x2="-2.592159375" y2="-2.791359375" width="0.05" layer="21"/>
<wire x1="-2.592159375" y1="-2.791359375" x2="-2.575" y2="-2.79241875" width="0.05" layer="21"/>
<wire x1="-2.575" y1="-2.79241875" x2="-2.5068" y2="-2.79495" width="0.05" layer="21"/>
<wire x1="-2.5068" y1="-2.79495" x2="-2.4025" y2="-2.79768125" width="0.05" layer="21"/>
<wire x1="-2.4025" y1="-2.79768125" x2="-2.2725" y2="-2.800540625" width="0.05" layer="21"/>
<wire x1="-2.2725" y1="-2.800540625" x2="-2.215" y2="-2.83696875" width="0.05" layer="21"/>
<wire x1="-2.215" y1="-2.83696875" x2="-2.0995" y2="-2.911190625" width="0.05" layer="21"/>
<wire x1="-2.0995" y1="-2.911190625" x2="-2.0415" y2="-2.948990625" width="0.05" layer="21"/>
<wire x1="-2.0415" y1="-2.948990625" x2="-1.92005" y2="-2.87923125" width="0.05" layer="21"/>
<wire x1="-1.92005" y1="-2.87923125" x2="-1.7986" y2="-2.80946875" width="0.05" layer="21"/>
<wire x1="-1.7986" y1="-2.80946875" x2="-1.625609375" y2="-2.8135" width="0.05" layer="21"/>
<wire x1="-1.625609375" y1="-2.8135" x2="-1.45261875" y2="-2.81751875" width="0.05" layer="21"/>
<wire x1="-1.45261875" y1="-2.81751875" x2="-1.592559375" y2="-2.90078125" width="0.05" layer="21"/>
<wire x1="-1.592559375" y1="-2.90078125" x2="-1.711590625" y2="-2.9709" width="0.05" layer="21"/>
<wire x1="-1.711590625" y1="-2.9709" x2="-1.80125" y2="-3.02256875" width="0.05" layer="21"/>
<wire x1="-1.80125" y1="-3.02256875" x2="-1.849809375" y2="-3.0502" width="0.05" layer="21"/>
<wire x1="-1.849809375" y1="-3.0502" x2="-1.87" y2="-3.0625" width="0.05" layer="21"/>
<wire x1="-1.87" y1="-3.0625" x2="-1.83438125" y2="-3.08815" width="0.05" layer="21"/>
<wire x1="-1.83438125" y1="-3.08815" x2="-1.74875" y2="-3.14785" width="0.05" layer="21"/>
<wire x1="-1.74875" y1="-3.14785" x2="-1.554009375" y2="-3.28421875" width="0.05" layer="21"/>
<wire x1="-1.554009375" y1="-3.28421875" x2="-1.49606875" y2="-3.32601875" width="0.05" layer="21"/>
<wire x1="-1.49606875" y1="-3.32601875" x2="-1.48043125" y2="-3.33785" width="0.05" layer="21"/>
<wire x1="-1.48043125" y1="-3.33785" x2="-1.475" y2="-3.3427" width="0.05" layer="21"/>
<wire x1="-1.475" y1="-3.3427" x2="-1.4761" y2="-3.34416875" width="0.05" layer="21"/>
<wire x1="-1.4761" y1="-3.34416875" x2="-1.47875" y2="-3.344459375" width="0.05" layer="21"/>
<wire x1="-1.47875" y1="-3.344459375" x2="-1.65096875" y2="-3.33213125" width="0.05" layer="21"/>
<wire x1="-1.65096875" y1="-3.33213125" x2="-1.65095" y2="-3.332090625" width="0.05" layer="21"/>
<wire x1="3.02806875" y1="-3.28438125" x2="3.02436875" y2="-3.215590625" width="0.05" layer="21"/>
<wire x1="3.02436875" y1="-3.215590625" x2="3.0195" y2="-3.094990625" width="0.05" layer="21"/>
<wire x1="3.0195" y1="-3.094990625" x2="3.015090625" y2="-2.983309375" width="0.05" layer="21"/>
<wire x1="3.015090625" y1="-2.983309375" x2="3.012490625" y2="-2.93598125" width="0.05" layer="21"/>
<wire x1="3.012490625" y1="-2.93598125" x2="2.966959375" y2="-2.9364" width="0.05" layer="21"/>
<wire x1="2.966959375" y1="-2.9364" x2="2.859340625" y2="-2.939540625" width="0.05" layer="21"/>
<wire x1="2.859340625" y1="-2.939540625" x2="2.64501875" y2="-2.945940625" width="0.05" layer="21"/>
<wire x1="2.64501875" y1="-2.945940625" x2="2.58251875" y2="-2.94726875" width="0.05" layer="21"/>
<wire x1="2.58251875" y1="-2.94726875" x2="2.57958125" y2="-2.88488125" width="0.05" layer="21"/>
<wire x1="2.57958125" y1="-2.88488125" x2="2.57836875" y2="-2.839909375" width="0.05" layer="21"/>
<wire x1="2.57836875" y1="-2.839909375" x2="2.57866875" y2="-2.82553125" width="0.05" layer="21"/>
<wire x1="2.57866875" y1="-2.82553125" x2="2.57958125" y2="-2.819409375" width="0.05" layer="21"/>
<wire x1="2.57958125" y1="-2.819409375" x2="2.60578125" y2="-2.817759375" width="0.05" layer="21"/>
<wire x1="2.60578125" y1="-2.817759375" x2="2.67491875" y2="-2.815540625" width="0.05" layer="21"/>
<wire x1="2.67491875" y1="-2.815540625" x2="2.90001875" y2="-2.81058125" width="0.05" layer="21"/>
<wire x1="2.90001875" y1="-2.81058125" x2="3.40285" y2="-2.80063125" width="0.05" layer="21"/>
<wire x1="3.40285" y1="-2.80063125" x2="3.69876875" y2="-2.793390625" width="0.05" layer="21"/>
<wire x1="3.69876875" y1="-2.793390625" x2="3.74001875" y2="-2.79178125" width="0.05" layer="21"/>
<wire x1="3.74001875" y1="-2.79178125" x2="3.74001875" y2="-2.825190625" width="0.05" layer="21"/>
<wire x1="3.74001875" y1="-2.825190625" x2="3.740940625" y2="-2.855490625" width="0.05" layer="21"/>
<wire x1="3.740940625" y1="-2.855490625" x2="3.74315" y2="-2.88145" width="0.05" layer="21"/>
<wire x1="3.74315" y1="-2.88145" x2="3.74628125" y2="-2.9043" width="0.05" layer="21"/>
<wire x1="3.74628125" y1="-2.9043" x2="3.7294" y2="-2.9069" width="0.05" layer="21"/>
<wire x1="3.7294" y1="-2.9069" x2="3.65968125" y2="-2.91091875" width="0.05" layer="21"/>
<wire x1="3.65968125" y1="-2.91091875" x2="3.51575" y2="-2.91693125" width="0.05" layer="21"/>
<wire x1="3.51575" y1="-2.91693125" x2="3.376109375" y2="-2.92285" width="0.05" layer="21"/>
<wire x1="3.376109375" y1="-2.92285" x2="3.3331" y2="-2.92518125" width="0.05" layer="21"/>
<wire x1="3.3331" y1="-2.92518125" x2="3.31668125" y2="-2.926659375" width="0.05" layer="21"/>
<wire x1="3.31668125" y1="-2.926659375" x2="3.316409375" y2="-2.94166875" width="0.05" layer="21"/>
<wire x1="3.316409375" y1="-2.94166875" x2="3.317359375" y2="-2.98085" width="0.05" layer="21"/>
<wire x1="3.317359375" y1="-2.98085" x2="3.32221875" y2="-3.107909375" width="0.05" layer="21"/>
<wire x1="3.32221875" y1="-3.107909375" x2="3.32535" y2="-3.18658125" width="0.05" layer="21"/>
<wire x1="3.32535" y1="-3.18658125" x2="3.32685" y2="-3.24306875" width="0.05" layer="21"/>
<wire x1="3.32685" y1="-3.24306875" x2="3.326740625" y2="-3.2775" width="0.05" layer="21"/>
<wire x1="3.326740625" y1="-3.2775" x2="3.32608125" y2="-3.28648125" width="0.05" layer="21"/>
<wire x1="3.32608125" y1="-3.28648125" x2="3.32501875" y2="-3.289990625" width="0.05" layer="21"/>
<wire x1="3.32501875" y1="-3.289990625" x2="3.31408125" y2="-3.29188125" width="0.05" layer="21"/>
<wire x1="3.31408125" y1="-3.29188125" x2="3.288040625" y2="-3.29473125" width="0.05" layer="21"/>
<wire x1="3.288040625" y1="-3.29473125" x2="3.206240625" y2="-3.301759375" width="0.05" layer="21"/>
<wire x1="3.206240625" y1="-3.301759375" x2="3.061740625" y2="-3.313340625" width="0.05" layer="21"/>
<wire x1="3.061740625" y1="-3.313340625" x2="3.03095" y2="-3.31626875" width="0.05" layer="21"/>
<wire x1="3.03095" y1="-3.31626875" x2="3.02806875" y2="-3.28438125" width="0.05" layer="21"/>
<wire x1="-2.8925" y1="-3.2248" x2="-3.026890625" y2="-3.210340625" width="0.05" layer="21"/>
<wire x1="-3.026890625" y1="-3.210340625" x2="-3.02983125" y2="-3.209959375" width="0.05" layer="21"/>
<wire x1="-3.02983125" y1="-3.209959375" x2="-3.03201875" y2="-3.208640625" width="0.05" layer="21"/>
<wire x1="-3.03201875" y1="-3.208640625" x2="-3.03351875" y2="-3.20573125" width="0.05" layer="21"/>
<wire x1="-3.03351875" y1="-3.20573125" x2="-3.0344" y2="-3.2006" width="0.05" layer="21"/>
<wire x1="-3.0344" y1="-3.2006" x2="-3.03446875" y2="-3.181090625" width="0.05" layer="21"/>
<wire x1="-3.03446875" y1="-3.181090625" x2="-3.032690625" y2="-3.145" width="0.05" layer="21"/>
<wire x1="-3.032690625" y1="-3.145" x2="-3.03091875" y2="-3.10906875" width="0.05" layer="21"/>
<wire x1="-3.03091875" y1="-3.10906875" x2="-3.03098125" y2="-3.08958125" width="0.05" layer="21"/>
<wire x1="-3.03098125" y1="-3.08958125" x2="-3.03183125" y2="-3.084440625" width="0.05" layer="21"/>
<wire x1="-3.03183125" y1="-3.084440625" x2="-3.0333" y2="-3.081540625" width="0.05" layer="21"/>
<wire x1="-3.0333" y1="-3.081540625" x2="-3.03543125" y2="-3.08025" width="0.05" layer="21"/>
<wire x1="-3.03543125" y1="-3.08025" x2="-3.0383" y2="-3.07995" width="0.05" layer="21"/>
<wire x1="-3.0383" y1="-3.07995" x2="-3.235" y2="-3.062459375" width="0.05" layer="21"/>
<wire x1="-3.235" y1="-3.062459375" x2="-3.42375" y2="-3.045009375" width="0.05" layer="21"/>
<wire x1="-3.42375" y1="-3.045009375" x2="-3.42463125" y2="-3.03178125" width="0.05" layer="21"/>
<wire x1="-3.42463125" y1="-3.03178125" x2="-3.425" y2="-3" width="0.05" layer="21"/>
<wire x1="-3.425" y1="-3" x2="-3.425" y2="-2.955" width="0.05" layer="21"/>
<wire x1="-3.425" y1="-2.955" x2="-3.41125" y2="-2.955" width="0.05" layer="21"/>
<wire x1="-3.41125" y1="-2.955" x2="-3.35086875" y2="-2.958659375" width="0.05" layer="21"/>
<wire x1="-3.35086875" y1="-2.958659375" x2="-3.225" y2="-2.96745" width="0.05" layer="21"/>
<wire x1="-3.225" y1="-2.96745" x2="-3.03875" y2="-2.979940625" width="0.05" layer="21"/>
<wire x1="-3.03875" y1="-2.979940625" x2="-3.025" y2="-2.979990625" width="0.05" layer="21"/>
<wire x1="-3.025" y1="-2.979990625" x2="-3.025" y2="-2.88646875" width="0.05" layer="21"/>
<wire x1="-3.025" y1="-2.88646875" x2="-3.10125" y2="-2.88305" width="0.05" layer="21"/>
<wire x1="-3.10125" y1="-2.88305" x2="-3.355" y2="-2.87001875" width="0.05" layer="21"/>
<wire x1="-3.355" y1="-2.87001875" x2="-3.56" y2="-2.859159375" width="0.05" layer="21"/>
<wire x1="-3.56" y1="-2.859159375" x2="-3.5875" y2="-2.85793125" width="0.05" layer="21"/>
<wire x1="-3.5875" y1="-2.85793125" x2="-3.59051875" y2="-2.996559375" width="0.05" layer="21"/>
<wire x1="-3.59051875" y1="-2.996559375" x2="-3.59351875" y2="-3.09533125" width="0.05" layer="21"/>
<wire x1="-3.59351875" y1="-3.09533125" x2="-3.595090625" y2="-3.126040625" width="0.05" layer="21"/>
<wire x1="-3.595090625" y1="-3.126040625" x2="-3.596459375" y2="-3.138109375" width="0.05" layer="21"/>
<wire x1="-3.596459375" y1="-3.138109375" x2="-3.608859375" y2="-3.137740625" width="0.05" layer="21"/>
<wire x1="-3.608859375" y1="-3.137740625" x2="-3.64046875" y2="-3.13456875" width="0.05" layer="21"/>
<wire x1="-3.64046875" y1="-3.13456875" x2="-3.74218125" y2="-3.121959375" width="0.05" layer="21"/>
<wire x1="-3.74218125" y1="-3.121959375" x2="-3.885" y2="-3.1029" width="0.05" layer="21"/>
<wire x1="-3.885" y1="-3.1029" x2="-3.885" y2="-2.828809375" width="0.05" layer="21"/>
<wire x1="-3.885" y1="-2.828809375" x2="-3.86625" y2="-2.810159375" width="0.05" layer="21"/>
<wire x1="-3.86625" y1="-2.810159375" x2="-3.856609375" y2="-2.801940625" width="0.05" layer="21"/>
<wire x1="-3.856609375" y1="-2.801940625" x2="-3.844609375" y2="-2.79378125" width="0.05" layer="21"/>
<wire x1="-3.844609375" y1="-2.79378125" x2="-3.83153125" y2="-2.786459375" width="0.05" layer="21"/>
<wire x1="-3.83153125" y1="-2.786459375" x2="-3.818640625" y2="-2.78078125" width="0.05" layer="21"/>
<wire x1="-3.818640625" y1="-2.78078125" x2="-3.798209375" y2="-2.77373125" width="0.05" layer="21"/>
<wire x1="-3.798209375" y1="-2.77373125" x2="-3.778109375" y2="-2.76836875" width="0.05" layer="21"/>
<wire x1="-3.778109375" y1="-2.76836875" x2="-3.75601875" y2="-2.76456875" width="0.05" layer="21"/>
<wire x1="-3.75601875" y1="-2.76456875" x2="-3.72968125" y2="-2.7622" width="0.05" layer="21"/>
<wire x1="-3.72968125" y1="-2.7622" x2="-3.69676875" y2="-2.76111875" width="0.05" layer="21"/>
<wire x1="-3.69676875" y1="-2.76111875" x2="-3.655009375" y2="-2.761209375" width="0.05" layer="21"/>
<wire x1="-3.655009375" y1="-2.761209375" x2="-3.535740625" y2="-2.76431875" width="0.05" layer="21"/>
<wire x1="-3.535740625" y1="-2.76431875" x2="-3.12" y2="-2.777559375" width="0.05" layer="21"/>
<wire x1="-3.12" y1="-2.777559375" x2="-2.97468125" y2="-2.782690625" width="0.05" layer="21"/>
<wire x1="-2.97468125" y1="-2.782690625" x2="-2.92615" y2="-2.785290625" width="0.05" layer="21"/>
<wire x1="-2.92615" y1="-2.785290625" x2="-2.88963125" y2="-2.788359375" width="0.05" layer="21"/>
<wire x1="-2.88963125" y1="-2.788359375" x2="-2.86205" y2="-2.792240625" width="0.05" layer="21"/>
<wire x1="-2.86205" y1="-2.792240625" x2="-2.840340625" y2="-2.797240625" width="0.05" layer="21"/>
<wire x1="-2.840340625" y1="-2.797240625" x2="-2.82143125" y2="-2.803709375" width="0.05" layer="21"/>
<wire x1="-2.82143125" y1="-2.803709375" x2="-2.80226875" y2="-2.81198125" width="0.05" layer="21"/>
<wire x1="-2.80226875" y1="-2.81198125" x2="-2.790959375" y2="-2.81778125" width="0.05" layer="21"/>
<wire x1="-2.790959375" y1="-2.81778125" x2="-2.778790625" y2="-2.82508125" width="0.05" layer="21"/>
<wire x1="-2.778790625" y1="-2.82508125" x2="-2.76725" y2="-2.83293125" width="0.05" layer="21"/>
<wire x1="-2.76725" y1="-2.83293125" x2="-2.75778125" y2="-2.840409375" width="0.05" layer="21"/>
<wire x1="-2.75778125" y1="-2.840409375" x2="-2.74655" y2="-2.8507" width="0.05" layer="21"/>
<wire x1="-2.74655" y1="-2.8507" x2="-2.738159375" y2="-2.860659375" width="0.05" layer="21"/>
<wire x1="-2.738159375" y1="-2.860659375" x2="-2.73493125" y2="-2.86623125" width="0.05" layer="21"/>
<wire x1="-2.73493125" y1="-2.86623125" x2="-2.7323" y2="-2.87255" width="0.05" layer="21"/>
<wire x1="-2.7323" y1="-2.87255" x2="-2.730209375" y2="-2.87991875" width="0.05" layer="21"/>
<wire x1="-2.730209375" y1="-2.87991875" x2="-2.728640625" y2="-2.888609375" width="0.05" layer="21"/>
<wire x1="-2.728640625" y1="-2.888609375" x2="-2.72686875" y2="-2.9111" width="0.05" layer="21"/>
<wire x1="-2.72686875" y1="-2.9111" x2="-2.72668125" y2="-2.942259375" width="0.05" layer="21"/>
<wire x1="-2.72668125" y1="-2.942259375" x2="-2.72975" y2="-3.039590625" width="0.05" layer="21"/>
<wire x1="-2.72975" y1="-3.039590625" x2="-2.7345" y2="-3.15175" width="0.05" layer="21"/>
<wire x1="-2.7345" y1="-3.15175" x2="-2.738040625" y2="-3.21373125" width="0.05" layer="21"/>
<wire x1="-2.738040625" y1="-3.21373125" x2="-2.73866875" y2="-3.224" width="0.05" layer="21"/>
<wire x1="-2.73866875" y1="-3.224" x2="-2.73976875" y2="-3.231390625" width="0.05" layer="21"/>
<wire x1="-2.73976875" y1="-3.231390625" x2="-2.741259375" y2="-3.23405" width="0.05" layer="21"/>
<wire x1="-2.741259375" y1="-3.23405" x2="-2.74378125" y2="-3.236059375" width="0.05" layer="21"/>
<wire x1="-2.74378125" y1="-3.236059375" x2="-2.747640625" y2="-3.23743125" width="0.05" layer="21"/>
<wire x1="-2.747640625" y1="-3.23743125" x2="-2.75313125" y2="-3.238190625" width="0.05" layer="21"/>
<wire x1="-2.75313125" y1="-3.238190625" x2="-2.770240625" y2="-3.23795" width="0.05" layer="21"/>
<wire x1="-2.770240625" y1="-3.23795" x2="-2.79755" y2="-3.23551875" width="0.05" layer="21"/>
<wire x1="-2.79755" y1="-3.23551875" x2="-2.8925" y2="-3.224790625" width="0.05" layer="21"/>
<wire x1="-2.8925" y1="-3.224790625" x2="-2.8925" y2="-3.2248" width="0.05" layer="21"/>
<wire x1="3.8899" y1="-3.211609375" x2="3.88768125" y2="-3.141840625" width="0.05" layer="21"/>
<wire x1="3.88768125" y1="-3.141840625" x2="3.882509375" y2="-3.015" width="0.05" layer="21"/>
<wire x1="3.882509375" y1="-3.015" x2="3.877340625" y2="-2.888390625" width="0.05" layer="21"/>
<wire x1="3.877340625" y1="-2.888390625" x2="3.87511875" y2="-2.8192" width="0.05" layer="21"/>
<wire x1="3.87511875" y1="-2.8192" x2="3.875009375" y2="-2.790890625" width="0.05" layer="21"/>
<wire x1="3.875009375" y1="-2.790890625" x2="3.916259375" y2="-2.788109375" width="0.05" layer="21"/>
<wire x1="3.916259375" y1="-2.788109375" x2="3.97713125" y2="-2.78533125" width="0.05" layer="21"/>
<wire x1="3.97713125" y1="-2.78533125" x2="4.06555" y2="-2.782540625" width="0.05" layer="21"/>
<wire x1="4.06555" y1="-2.782540625" x2="4.173590625" y2="-2.779740625" width="0.05" layer="21"/>
<wire x1="4.173590625" y1="-2.779740625" x2="4.176859375" y2="-2.87861875" width="0.05" layer="21"/>
<wire x1="4.176859375" y1="-2.87861875" x2="4.183290625" y2="-3.03938125" width="0.05" layer="21"/>
<wire x1="4.183290625" y1="-3.03938125" x2="4.186440625" y2="-3.101259375" width="0.05" layer="21"/>
<wire x1="4.186440625" y1="-3.101259375" x2="4.22948125" y2="-3.09781875" width="0.05" layer="21"/>
<wire x1="4.22948125" y1="-3.09781875" x2="4.39015" y2="-3.08273125" width="0.05" layer="21"/>
<wire x1="4.39015" y1="-3.08273125" x2="4.64186875" y2="-3.05738125" width="0.05" layer="21"/>
<wire x1="4.64186875" y1="-3.05738125" x2="4.884709375" y2="-3.03201875" width="0.05" layer="21"/>
<wire x1="4.884709375" y1="-3.03201875" x2="5.018759375" y2="-3.016890625" width="0.05" layer="21"/>
<wire x1="5.018759375" y1="-3.016890625" x2="5.040009375" y2="-3.013859375" width="0.05" layer="21"/>
<wire x1="5.040009375" y1="-3.013859375" x2="5.040009375" y2="-3.05683125" width="0.05" layer="21"/>
<wire x1="5.040009375" y1="-3.05683125" x2="5.039740625" y2="-3.080690625" width="0.05" layer="21"/>
<wire x1="5.039740625" y1="-3.080690625" x2="5.038359375" y2="-3.09386875" width="0.05" layer="21"/>
<wire x1="5.038359375" y1="-3.09386875" x2="5.03698125" y2="-3.097559375" width="0.05" layer="21"/>
<wire x1="5.03698125" y1="-3.097559375" x2="5.034990625" y2="-3.099909375" width="0.05" layer="21"/>
<wire x1="5.034990625" y1="-3.099909375" x2="5.032290625" y2="-3.10135" width="0.05" layer="21"/>
<wire x1="5.032290625" y1="-3.10135" x2="5.028759375" y2="-3.10231875" width="0.05" layer="21"/>
<wire x1="5.028759375" y1="-3.10231875" x2="4.99113125" y2="-3.108440625" width="0.05" layer="21"/>
<wire x1="4.99113125" y1="-3.108440625" x2="4.91666875" y2="-3.118759375" width="0.05" layer="21"/>
<wire x1="4.91666875" y1="-3.118759375" x2="4.69093125" y2="-3.147790625" width="0.05" layer="21"/>
<wire x1="4.69093125" y1="-3.147790625" x2="4.4188" y2="-3.181" width="0.05" layer="21"/>
<wire x1="4.4188" y1="-3.181" x2="4.167509375" y2="-3.209990625" width="0.05" layer="21"/>
<wire x1="4.167509375" y1="-3.209990625" x2="3.916259375" y2="-3.237759375" width="0.05" layer="21"/>
<wire x1="3.916259375" y1="-3.237759375" x2="3.890009375" y2="-3.24071875" width="0.05" layer="21"/>
<wire x1="3.890009375" y1="-3.24071875" x2="3.8899" y2="-3.211609375" width="0.05" layer="21"/>
<wire x1="-1.910209375" y1="-2.22035" x2="-1.92563125" y2="-2.21638125" width="0.05" layer="21"/>
<wire x1="-1.92563125" y1="-2.21638125" x2="-1.94838125" y2="-2.209009375" width="0.05" layer="21"/>
<wire x1="-1.94838125" y1="-2.209009375" x2="-2.01053125" y2="-2.18611875" width="0.05" layer="21"/>
<wire x1="-2.01053125" y1="-2.18611875" x2="-2.08595" y2="-2.1558" width="0.05" layer="21"/>
<wire x1="-2.08595" y1="-2.1558" x2="-2.16396875" y2="-2.122209375" width="0.05" layer="21"/>
<wire x1="-2.16396875" y1="-2.122209375" x2="-2.23838125" y2="-2.0875" width="0.05" layer="21"/>
<wire x1="-2.23838125" y1="-2.0875" x2="-2.272559375" y2="-2.070340625" width="0.05" layer="21"/>
<wire x1="-2.272559375" y1="-2.070340625" x2="-2.3053" y2="-2.05301875" width="0.05" layer="21"/>
<wire x1="-2.3053" y1="-2.05301875" x2="-2.33701875" y2="-2.035290625" width="0.05" layer="21"/>
<wire x1="-2.33701875" y1="-2.035290625" x2="-2.36813125" y2="-2.016940625" width="0.05" layer="21"/>
<wire x1="-2.36813125" y1="-2.016940625" x2="-2.399059375" y2="-1.99771875" width="0.05" layer="21"/>
<wire x1="-2.399059375" y1="-1.99771875" x2="-2.430240625" y2="-1.977409375" width="0.05" layer="21"/>
<wire x1="-2.430240625" y1="-1.977409375" x2="-2.482340625" y2="-1.941459375" width="0.05" layer="21"/>
<wire x1="-2.482340625" y1="-1.941459375" x2="-2.52731875" y2="-1.90761875" width="0.05" layer="21"/>
<wire x1="-2.52731875" y1="-1.90761875" x2="-2.5473" y2="-1.89131875" width="0.05" layer="21"/>
<wire x1="-2.5473" y1="-1.89131875" x2="-2.56566875" y2="-1.875340625" width="0.05" layer="21"/>
<wire x1="-2.56566875" y1="-1.875340625" x2="-2.58251875" y2="-1.85963125" width="0.05" layer="21"/>
<wire x1="-2.58251875" y1="-1.85963125" x2="-2.597890625" y2="-1.84411875" width="0.05" layer="21"/>
<wire x1="-2.597890625" y1="-1.84411875" x2="-2.611859375" y2="-1.828740625" width="0.05" layer="21"/>
<wire x1="-2.611859375" y1="-1.828740625" x2="-2.624490625" y2="-1.81341875" width="0.05" layer="21"/>
<wire x1="-2.624490625" y1="-1.81341875" x2="-2.63583125" y2="-1.798090625" width="0.05" layer="21"/>
<wire x1="-2.63583125" y1="-1.798090625" x2="-2.64595" y2="-1.782709375" width="0.05" layer="21"/>
<wire x1="-2.64595" y1="-1.782709375" x2="-2.65491875" y2="-1.76718125" width="0.05" layer="21"/>
<wire x1="-2.65491875" y1="-1.76718125" x2="-2.662790625" y2="-1.751459375" width="0.05" layer="21"/>
<wire x1="-2.662790625" y1="-1.751459375" x2="-2.66963125" y2="-1.73548125" width="0.05" layer="21"/>
<wire x1="-2.66963125" y1="-1.73548125" x2="-2.6755" y2="-1.719159375" width="0.05" layer="21"/>
<wire x1="-2.6755" y1="-1.719159375" x2="-2.68006875" y2="-1.70293125" width="0.05" layer="21"/>
<wire x1="-2.68006875" y1="-1.70293125" x2="-2.68353125" y2="-1.684859375" width="0.05" layer="21"/>
<wire x1="-2.68353125" y1="-1.684859375" x2="-2.68591875" y2="-1.66388125" width="0.05" layer="21"/>
<wire x1="-2.68591875" y1="-1.66388125" x2="-2.68726875" y2="-1.638890625" width="0.05" layer="21"/>
<wire x1="-2.68726875" y1="-1.638890625" x2="-2.687609375" y2="-1.60881875" width="0.05" layer="21"/>
<wire x1="-2.687609375" y1="-1.60881875" x2="-2.68698125" y2="-1.572590625" width="0.05" layer="21"/>
<wire x1="-2.68698125" y1="-1.572590625" x2="-2.682940625" y2="-1.477340625" width="0.05" layer="21"/>
<wire x1="-2.682940625" y1="-1.477340625" x2="-2.68113125" y2="-1.4356" width="0.05" layer="21"/>
<wire x1="-2.68113125" y1="-1.4356" x2="-2.680809375" y2="-1.40621875" width="0.05" layer="21"/>
<wire x1="-2.680809375" y1="-1.40621875" x2="-2.68203125" y2="-1.386309375" width="0.05" layer="21"/>
<wire x1="-2.68203125" y1="-1.386309375" x2="-2.68485" y2="-1.372990625" width="0.05" layer="21"/>
<wire x1="-2.68485" y1="-1.372990625" x2="-2.69016875" y2="-1.36225" width="0.05" layer="21"/>
<wire x1="-2.69016875" y1="-1.36225" x2="-2.69991875" y2="-1.34781875" width="0.05" layer="21"/>
<wire x1="-2.69991875" y1="-1.34781875" x2="-2.71311875" y2="-1.33101875" width="0.05" layer="21"/>
<wire x1="-2.71311875" y1="-1.33101875" x2="-2.7288" y2="-1.31316875" width="0.05" layer="21"/>
<wire x1="-2.7288" y1="-1.31316875" x2="-2.751390625" y2="-1.28723125" width="0.05" layer="21"/>
<wire x1="-2.751390625" y1="-1.28723125" x2="-2.760890625" y2="-1.27486875" width="0.05" layer="21"/>
<wire x1="-2.760890625" y1="-1.27486875" x2="-2.76925" y2="-1.26268125" width="0.05" layer="21"/>
<wire x1="-2.76925" y1="-1.26268125" x2="-2.776540625" y2="-1.25048125" width="0.05" layer="21"/>
<wire x1="-2.776540625" y1="-1.25048125" x2="-2.7828" y2="-1.238090625" width="0.05" layer="21"/>
<wire x1="-2.7828" y1="-1.238090625" x2="-2.78806875" y2="-1.22533125" width="0.05" layer="21"/>
<wire x1="-2.78806875" y1="-1.22533125" x2="-2.79241875" y2="-1.212040625" width="0.05" layer="21"/>
<wire x1="-2.79241875" y1="-1.212040625" x2="-2.795890625" y2="-1.19803125" width="0.05" layer="21"/>
<wire x1="-2.795890625" y1="-1.19803125" x2="-2.798540625" y2="-1.18311875" width="0.05" layer="21"/>
<wire x1="-2.798540625" y1="-1.18311875" x2="-2.800409375" y2="-1.167140625" width="0.05" layer="21"/>
<wire x1="-2.800409375" y1="-1.167140625" x2="-2.801559375" y2="-1.149909375" width="0.05" layer="21"/>
<wire x1="-2.801559375" y1="-1.149909375" x2="-2.80188125" y2="-1.111" width="0.05" layer="21"/>
<wire x1="-2.80188125" y1="-1.111" x2="-2.799909375" y2="-1.064959375" width="0.05" layer="21"/>
<wire x1="-2.799909375" y1="-1.064959375" x2="-2.796209375" y2="-1.011" width="0.05" layer="21"/>
<wire x1="-2.796209375" y1="-1.011" x2="-2.79165" y2="-0.963809375" width="0.05" layer="21"/>
<wire x1="-2.79165" y1="-0.963809375" x2="-2.78563125" y2="-0.92066875" width="0.05" layer="21"/>
<wire x1="-2.78563125" y1="-0.92066875" x2="-2.77753125" y2="-0.878840625" width="0.05" layer="21"/>
<wire x1="-2.77753125" y1="-0.878840625" x2="-2.76675" y2="-0.835590625" width="0.05" layer="21"/>
<wire x1="-2.76675" y1="-0.835590625" x2="-2.75268125" y2="-0.788190625" width="0.05" layer="21"/>
<wire x1="-2.75268125" y1="-0.788190625" x2="-2.734709375" y2="-0.733909375" width="0.05" layer="21"/>
<wire x1="-2.734709375" y1="-0.733909375" x2="-2.71225" y2="-0.67001875" width="0.05" layer="21"/>
<wire x1="-2.71225" y1="-0.67001875" x2="-2.677690625" y2="-0.572409375" width="0.05" layer="21"/>
<wire x1="-2.677690625" y1="-0.572409375" x2="-2.6508" y2="-0.493509375" width="0.05" layer="21"/>
<wire x1="-2.6508" y1="-0.493509375" x2="-2.63088125" y2="-0.43048125" width="0.05" layer="21"/>
<wire x1="-2.63088125" y1="-0.43048125" x2="-2.617259375" y2="-0.380459375" width="0.05" layer="21"/>
<wire x1="-2.617259375" y1="-0.380459375" x2="-2.6126" y2="-0.35943125" width="0.05" layer="21"/>
<wire x1="-2.6126" y1="-0.35943125" x2="-2.60925" y2="-0.340590625" width="0.05" layer="21"/>
<wire x1="-2.60925" y1="-0.340590625" x2="-2.607140625" y2="-0.32358125" width="0.05" layer="21"/>
<wire x1="-2.607140625" y1="-0.32358125" x2="-2.60616875" y2="-0.308040625" width="0.05" layer="21"/>
<wire x1="-2.60616875" y1="-0.308040625" x2="-2.60625" y2="-0.293609375" width="0.05" layer="21"/>
<wire x1="-2.60625" y1="-0.293609375" x2="-2.607309375" y2="-0.279940625" width="0.05" layer="21"/>
<wire x1="-2.607309375" y1="-0.279940625" x2="-2.609259375" y2="-0.26668125" width="0.05" layer="21"/>
<wire x1="-2.609259375" y1="-0.26668125" x2="-2.612009375" y2="-0.25345" width="0.05" layer="21"/>
<wire x1="-2.612009375" y1="-0.25345" x2="-2.61713125" y2="-0.234009375" width="0.05" layer="21"/>
<wire x1="-2.61713125" y1="-0.234009375" x2="-2.623109375" y2="-0.21616875" width="0.05" layer="21"/>
<wire x1="-2.623109375" y1="-0.21616875" x2="-2.6303" y2="-0.19936875" width="0.05" layer="21"/>
<wire x1="-2.6303" y1="-0.19936875" x2="-2.63905" y2="-0.183009375" width="0.05" layer="21"/>
<wire x1="-2.63905" y1="-0.183009375" x2="-2.649709375" y2="-0.166509375" width="0.05" layer="21"/>
<wire x1="-2.649709375" y1="-0.166509375" x2="-2.662609375" y2="-0.14928125" width="0.05" layer="21"/>
<wire x1="-2.662609375" y1="-0.14928125" x2="-2.678090625" y2="-0.130759375" width="0.05" layer="21"/>
<wire x1="-2.678090625" y1="-0.130759375" x2="-2.69651875" y2="-0.110340625" width="0.05" layer="21"/>
<wire x1="-2.69651875" y1="-0.110340625" x2="-2.716190625" y2="-0.08868125" width="0.05" layer="21"/>
<wire x1="-2.716190625" y1="-0.08868125" x2="-2.73286875" y2="-0.0692" width="0.05" layer="21"/>
<wire x1="-2.73286875" y1="-0.0692" x2="-2.74696875" y2="-0.051259375" width="0.05" layer="21"/>
<wire x1="-2.74696875" y1="-0.051259375" x2="-2.75891875" y2="-0.03423125" width="0.05" layer="21"/>
<wire x1="-2.75891875" y1="-0.03423125" x2="-2.769109375" y2="-0.01746875" width="0.05" layer="21"/>
<wire x1="-2.769109375" y1="-0.01746875" x2="-2.77798125" y2="-0.000340625" width="0.05" layer="21"/>
<wire x1="-2.77798125" y1="-0.000340625" x2="-2.78593125" y2="0.01778125" width="0.05" layer="21"/>
<wire x1="-2.78593125" y1="0.01778125" x2="-2.79338125" y2="0.03753125" width="0.05" layer="21"/>
<wire x1="-2.79338125" y1="0.03753125" x2="-2.8075" y2="0.0775" width="0.05" layer="21"/>
<wire x1="-2.8075" y1="0.0775" x2="-2.80891875" y2="0.411259375" width="0.05" layer="21"/>
<wire x1="-2.80891875" y1="0.411259375" x2="-2.810340625" y2="0.74501875" width="0.05" layer="21"/>
<wire x1="-2.810340625" y1="0.74501875" x2="-2.79806875" y2="0.780290625" width="0.05" layer="21"/>
<wire x1="-2.79806875" y1="0.780290625" x2="-2.79071875" y2="0.7996" width="0.05" layer="21"/>
<wire x1="-2.79071875" y1="0.7996" x2="-2.782290625" y2="0.81831875" width="0.05" layer="21"/>
<wire x1="-2.782290625" y1="0.81831875" x2="-2.772840625" y2="0.836409375" width="0.05" layer="21"/>
<wire x1="-2.772840625" y1="0.836409375" x2="-2.76238125" y2="0.853809375" width="0.05" layer="21"/>
<wire x1="-2.76238125" y1="0.853809375" x2="-2.75096875" y2="0.870459375" width="0.05" layer="21"/>
<wire x1="-2.75096875" y1="0.870459375" x2="-2.73863125" y2="0.886309375" width="0.05" layer="21"/>
<wire x1="-2.73863125" y1="0.886309375" x2="-2.7254" y2="0.901290625" width="0.05" layer="21"/>
<wire x1="-2.7254" y1="0.901290625" x2="-2.71133125" y2="0.91535" width="0.05" layer="21"/>
<wire x1="-2.71133125" y1="0.91535" x2="-2.69441875" y2="0.92955" width="0.05" layer="21"/>
<wire x1="-2.69441875" y1="0.92955" x2="-2.667240625" y2="0.950609375" width="0.05" layer="21"/>
<wire x1="-2.667240625" y1="0.950609375" x2="-2.596959375" y2="1.002540625" width="0.05" layer="21"/>
<wire x1="-2.596959375" y1="1.002540625" x2="-2.53036875" y2="1.04953125" width="0.05" layer="21"/>
<wire x1="-2.53036875" y1="1.04953125" x2="-2.507809375" y2="1.06443125" width="0.05" layer="21"/>
<wire x1="-2.507809375" y1="1.06443125" x2="-2.49736875" y2="1.07" width="0.05" layer="21"/>
<wire x1="-2.49736875" y1="1.07" x2="-2.49645" y2="1.06876875" width="0.05" layer="21"/>
<wire x1="-2.49645" y1="1.06876875" x2="-2.4957" y2="1.065409375" width="0.05" layer="21"/>
<wire x1="-2.4957" y1="1.065409375" x2="-2.495" y2="1.05438125" width="0.05" layer="21"/>
<wire x1="-2.495" y1="1.05438125" x2="-2.49318125" y2="1.03256875" width="0.05" layer="21"/>
<wire x1="-2.49318125" y1="1.03256875" x2="-2.48818125" y2="1.00286875" width="0.05" layer="21"/>
<wire x1="-2.48818125" y1="1.00286875" x2="-2.48068125" y2="0.967859375" width="0.05" layer="21"/>
<wire x1="-2.48068125" y1="0.967859375" x2="-2.47135" y2="0.93013125" width="0.05" layer="21"/>
<wire x1="-2.47135" y1="0.93013125" x2="-2.46086875" y2="0.892259375" width="0.05" layer="21"/>
<wire x1="-2.46086875" y1="0.892259375" x2="-2.44993125" y2="0.85685" width="0.05" layer="21"/>
<wire x1="-2.44993125" y1="0.85685" x2="-2.439190625" y2="0.82648125" width="0.05" layer="21"/>
<wire x1="-2.439190625" y1="0.82648125" x2="-2.42933125" y2="0.80375" width="0.05" layer="21"/>
<wire x1="-2.42933125" y1="0.80375" x2="-2.423609375" y2="0.79331875" width="0.05" layer="21"/>
<wire x1="-2.423609375" y1="0.79331875" x2="-2.419040625" y2="0.787040625" width="0.05" layer="21"/>
<wire x1="-2.419040625" y1="0.787040625" x2="-2.41718125" y2="0.785509375" width="0.05" layer="21"/>
<wire x1="-2.41718125" y1="0.785509375" x2="-2.41556875" y2="0.785090625" width="0.05" layer="21"/>
<wire x1="-2.41556875" y1="0.785090625" x2="-2.41423125" y2="0.785790625" width="0.05" layer="21"/>
<wire x1="-2.41423125" y1="0.785790625" x2="-2.41313125" y2="0.78763125" width="0.05" layer="21"/>
<wire x1="-2.41313125" y1="0.78763125" x2="-2.41165" y2="0.79485" width="0.05" layer="21"/>
<wire x1="-2.41165" y1="0.79485" x2="-2.41105" y2="0.806909375" width="0.05" layer="21"/>
<wire x1="-2.41105" y1="0.806909375" x2="-2.41223125" y2="0.84625" width="0.05" layer="21"/>
<wire x1="-2.41223125" y1="0.84625" x2="-2.413209375" y2="0.87195" width="0.05" layer="21"/>
<wire x1="-2.413209375" y1="0.87195" x2="-2.41325" y2="0.89271875" width="0.05" layer="21"/>
<wire x1="-2.41325" y1="0.89271875" x2="-2.41236875" y2="0.90736875" width="0.05" layer="21"/>
<wire x1="-2.41236875" y1="0.90736875" x2="-2.410609375" y2="0.91468125" width="0.05" layer="21"/>
<wire x1="-2.410609375" y1="0.91468125" x2="-2.406509375" y2="0.918540625" width="0.05" layer="21"/>
<wire x1="-2.406509375" y1="0.918540625" x2="-2.40408125" y2="0.91926875" width="0.05" layer="21"/>
<wire x1="-2.40408125" y1="0.91926875" x2="-2.40135" y2="0.91916875" width="0.05" layer="21"/>
<wire x1="-2.40135" y1="0.91916875" x2="-2.3949" y2="0.91633125" width="0.05" layer="21"/>
<wire x1="-2.3949" y1="0.91633125" x2="-2.38691875" y2="0.909809375" width="0.05" layer="21"/>
<wire x1="-2.38691875" y1="0.909809375" x2="-2.37718125" y2="0.89938125" width="0.05" layer="21"/>
<wire x1="-2.37718125" y1="0.89938125" x2="-2.36543125" y2="0.884809375" width="0.05" layer="21"/>
<wire x1="-2.36543125" y1="0.884809375" x2="-2.335" y2="0.84236875" width="0.05" layer="21"/>
<wire x1="-2.335" y1="0.84236875" x2="-2.2994" y2="0.792840625" width="0.05" layer="21"/>
<wire x1="-2.2994" y1="0.792840625" x2="-2.28696875" y2="0.77725" width="0.05" layer="21"/>
<wire x1="-2.28696875" y1="0.77725" x2="-2.2805" y2="0.77091875" width="0.05" layer="21"/>
<wire x1="-2.2805" y1="0.77091875" x2="-2.27885" y2="0.7707" width="0.05" layer="21"/>
<wire x1="-2.27885" y1="0.7707" x2="-2.277540625" y2="0.77125" width="0.05" layer="21"/>
<wire x1="-2.277540625" y1="0.77125" x2="-2.275990625" y2="0.77468125" width="0.05" layer="21"/>
<wire x1="-2.275990625" y1="0.77468125" x2="-2.27588125" y2="0.78136875" width="0.05" layer="21"/>
<wire x1="-2.27588125" y1="0.78136875" x2="-2.277240625" y2="0.79148125" width="0.05" layer="21"/>
<wire x1="-2.277240625" y1="0.79148125" x2="-2.28446875" y2="0.822559375" width="0.05" layer="21"/>
<wire x1="-2.28446875" y1="0.822559375" x2="-2.297890625" y2="0.869140625" width="0.05" layer="21"/>
<wire x1="-2.297890625" y1="0.869140625" x2="-2.30998125" y2="0.91166875" width="0.05" layer="21"/>
<wire x1="-2.30998125" y1="0.91166875" x2="-2.317259375" y2="0.94336875" width="0.05" layer="21"/>
<wire x1="-2.317259375" y1="0.94336875" x2="-2.319040625" y2="0.95481875" width="0.05" layer="21"/>
<wire x1="-2.319040625" y1="0.95481875" x2="-2.319540625" y2="0.963159375" width="0.05" layer="21"/>
<wire x1="-2.319540625" y1="0.963159375" x2="-2.318740625" y2="0.96826875" width="0.05" layer="21"/>
<wire x1="-2.318740625" y1="0.96826875" x2="-2.317840625" y2="0.969559375" width="0.05" layer="21"/>
<wire x1="-2.317840625" y1="0.969559375" x2="-2.316609375" y2="0.97" width="0.05" layer="21"/>
<wire x1="-2.316609375" y1="0.97" x2="-2.31196875" y2="0.9684" width="0.05" layer="21"/>
<wire x1="-2.31196875" y1="0.9684" x2="-2.305340625" y2="0.96295" width="0.05" layer="21"/>
<wire x1="-2.305340625" y1="0.96295" x2="-2.296" y2="0.9527" width="0.05" layer="21"/>
<wire x1="-2.296" y1="0.9527" x2="-2.2832" y2="0.936659375" width="0.05" layer="21"/>
<wire x1="-2.2832" y1="0.936659375" x2="-2.24423125" y2="0.88338125" width="0.05" layer="21"/>
<wire x1="-2.24423125" y1="0.88338125" x2="-2.1825" y2="0.79536875" width="0.05" layer="21"/>
<wire x1="-2.1825" y1="0.79536875" x2="-2.16606875" y2="0.772340625" width="0.05" layer="21"/>
<wire x1="-2.16606875" y1="0.772340625" x2="-2.15358125" y2="0.7562" width="0.05" layer="21"/>
<wire x1="-2.15358125" y1="0.7562" x2="-2.14501875" y2="0.746959375" width="0.05" layer="21"/>
<wire x1="-2.14501875" y1="0.746959375" x2="-2.1422" y2="0.744940625" width="0.05" layer="21"/>
<wire x1="-2.1422" y1="0.744940625" x2="-2.140359375" y2="0.744640625" width="0.05" layer="21"/>
<wire x1="-2.140359375" y1="0.744640625" x2="-2.13948125" y2="0.74608125" width="0.05" layer="21"/>
<wire x1="-2.13948125" y1="0.74608125" x2="-2.139559375" y2="0.74925" width="0.05" layer="21"/>
<wire x1="-2.139559375" y1="0.74925" x2="-2.142609375" y2="0.760790625" width="0.05" layer="21"/>
<wire x1="-2.142609375" y1="0.760790625" x2="-2.14948125" y2="0.77926875" width="0.05" layer="21"/>
<wire x1="-2.14948125" y1="0.77926875" x2="-2.160140625" y2="0.80471875" width="0.05" layer="21"/>
<wire x1="-2.160140625" y1="0.80471875" x2="-2.17663125" y2="0.845790625" width="0.05" layer="21"/>
<wire x1="-2.17663125" y1="0.845790625" x2="-2.189559375" y2="0.88535" width="0.05" layer="21"/>
<wire x1="-2.189559375" y1="0.88535" x2="-2.199659375" y2="0.92591875" width="0.05" layer="21"/>
<wire x1="-2.199659375" y1="0.92591875" x2="-2.207640625" y2="0.97" width="0.05" layer="21"/>
<wire x1="-2.207640625" y1="0.97" x2="-2.21025" y2="0.9921" width="0.05" layer="21"/>
<wire x1="-2.21025" y1="0.9921" x2="-2.212340625" y2="1.019540625" width="0.05" layer="21"/>
<wire x1="-2.212340625" y1="1.019540625" x2="-2.21475" y2="1.080459375" width="0.05" layer="21"/>
<wire x1="-2.21475" y1="1.080459375" x2="-2.21498125" y2="1.108959375" width="0.05" layer="21"/>
<wire x1="-2.21498125" y1="1.108959375" x2="-2.214490625" y2="1.132840625" width="0.05" layer="21"/>
<wire x1="-2.214490625" y1="1.132840625" x2="-2.213240625" y2="1.1496" width="0.05" layer="21"/>
<wire x1="-2.213240625" y1="1.1496" x2="-2.21231875" y2="1.154540625" width="0.05" layer="21"/>
<wire x1="-2.21231875" y1="1.154540625" x2="-2.211190625" y2="1.15676875" width="0.05" layer="21"/>
<wire x1="-2.211190625" y1="1.15676875" x2="-2.20616875" y2="1.154740625" width="0.05" layer="21"/>
<wire x1="-2.20616875" y1="1.154740625" x2="-2.19533125" y2="1.147490625" width="0.05" layer="21"/>
<wire x1="-2.19533125" y1="1.147490625" x2="-2.16256875" y2="1.12186875" width="0.05" layer="21"/>
<wire x1="-2.16256875" y1="1.12186875" x2="-2.1294" y2="1.09531875" width="0.05" layer="21"/>
<wire x1="-2.1294" y1="1.09531875" x2="-2.09511875" y2="1.069940625" width="0.05" layer="21"/>
<wire x1="-2.09511875" y1="1.069940625" x2="-2.059759375" y2="1.04573125" width="0.05" layer="21"/>
<wire x1="-2.059759375" y1="1.04573125" x2="-2.023340625" y2="1.022709375" width="0.05" layer="21"/>
<wire x1="-2.023340625" y1="1.022709375" x2="-1.98586875" y2="1.00088125" width="0.05" layer="21"/>
<wire x1="-1.98586875" y1="1.00088125" x2="-1.94738125" y2="0.98025" width="0.05" layer="21"/>
<wire x1="-1.94738125" y1="0.98025" x2="-1.90788125" y2="0.96081875" width="0.05" layer="21"/>
<wire x1="-1.90788125" y1="0.96081875" x2="-1.86738125" y2="0.9426" width="0.05" layer="21"/>
<wire x1="-1.86738125" y1="0.9426" x2="-1.82591875" y2="0.925609375" width="0.05" layer="21"/>
<wire x1="-1.82591875" y1="0.925609375" x2="-1.7835" y2="0.90985" width="0.05" layer="21"/>
<wire x1="-1.7835" y1="0.90985" x2="-1.74015" y2="0.89533125" width="0.05" layer="21"/>
<wire x1="-1.74015" y1="0.89533125" x2="-1.69588125" y2="0.88205" width="0.05" layer="21"/>
<wire x1="-1.69588125" y1="0.88205" x2="-1.650709375" y2="0.87003125" width="0.05" layer="21"/>
<wire x1="-1.650709375" y1="0.87003125" x2="-1.604659375" y2="0.859259375" width="0.05" layer="21"/>
<wire x1="-1.604659375" y1="0.859259375" x2="-1.55775" y2="0.84976875" width="0.05" layer="21"/>
<wire x1="-1.55775" y1="0.84976875" x2="-1.51" y2="0.841559375" width="0.05" layer="21"/>
<wire x1="-1.51" y1="0.841559375" x2="-1.4732" y2="0.837309375" width="0.05" layer="21"/>
<wire x1="-1.4732" y1="0.837309375" x2="-1.426659375" y2="0.8343" width="0.05" layer="21"/>
<wire x1="-1.426659375" y1="0.8343" x2="-1.37365" y2="0.8325" width="0.05" layer="21"/>
<wire x1="-1.37365" y1="0.8325" x2="-1.317390625" y2="0.831909375" width="0.05" layer="21"/>
<wire x1="-1.317390625" y1="0.831909375" x2="-1.26115" y2="0.83255" width="0.05" layer="21"/>
<wire x1="-1.26115" y1="0.83255" x2="-1.20816875" y2="0.8344" width="0.05" layer="21"/>
<wire x1="-1.20816875" y1="0.8344" x2="-1.161709375" y2="0.837459375" width="0.05" layer="21"/>
<wire x1="-1.161709375" y1="0.837459375" x2="-1.125" y2="0.841740625" width="0.05" layer="21"/>
<wire x1="-1.125" y1="0.841740625" x2="-1.06845" y2="0.851940625" width="0.05" layer="21"/>
<wire x1="-1.06845" y1="0.851940625" x2="-1.01336875" y2="0.86436875" width="0.05" layer="21"/>
<wire x1="-1.01336875" y1="0.86436875" x2="-0.95983125" y2="0.87898125" width="0.05" layer="21"/>
<wire x1="-0.95983125" y1="0.87898125" x2="-0.907890625" y2="0.89573125" width="0.05" layer="21"/>
<wire x1="-0.907890625" y1="0.89573125" x2="-0.857609375" y2="0.91458125" width="0.05" layer="21"/>
<wire x1="-0.857609375" y1="0.91458125" x2="-0.809059375" y2="0.9355" width="0.05" layer="21"/>
<wire x1="-0.809059375" y1="0.9355" x2="-0.7623" y2="0.958440625" width="0.05" layer="21"/>
<wire x1="-0.7623" y1="0.958440625" x2="-0.7174" y2="0.98338125" width="0.05" layer="21"/>
<wire x1="-0.7174" y1="0.98338125" x2="-0.67441875" y2="1.01026875" width="0.05" layer="21"/>
<wire x1="-0.67441875" y1="1.01026875" x2="-0.63341875" y2="1.03908125" width="0.05" layer="21"/>
<wire x1="-0.63341875" y1="1.03908125" x2="-0.59446875" y2="1.069759375" width="0.05" layer="21"/>
<wire x1="-0.59446875" y1="1.069759375" x2="-0.557640625" y2="1.10228125" width="0.05" layer="21"/>
<wire x1="-0.557640625" y1="1.10228125" x2="-0.52296875" y2="1.1366" width="0.05" layer="21"/>
<wire x1="-0.52296875" y1="1.1366" x2="-0.49055" y2="1.172690625" width="0.05" layer="21"/>
<wire x1="-0.49055" y1="1.172690625" x2="-0.460440625" y2="1.2105" width="0.05" layer="21"/>
<wire x1="-0.460440625" y1="1.2105" x2="-0.432690625" y2="1.25" width="0.05" layer="21"/>
<wire x1="-0.432690625" y1="1.25" x2="-0.415459375" y2="1.27763125" width="0.05" layer="21"/>
<wire x1="-0.415459375" y1="1.27763125" x2="-0.39958125" y2="1.306059375" width="0.05" layer="21"/>
<wire x1="-0.39958125" y1="1.306059375" x2="-0.38505" y2="1.33521875" width="0.05" layer="21"/>
<wire x1="-0.38505" y1="1.33521875" x2="-0.371909375" y2="1.36503125" width="0.05" layer="21"/>
<wire x1="-0.371909375" y1="1.36503125" x2="-0.360159375" y2="1.39543125" width="0.05" layer="21"/>
<wire x1="-0.360159375" y1="1.39543125" x2="-0.34981875" y2="1.426340625" width="0.05" layer="21"/>
<wire x1="-0.34981875" y1="1.426340625" x2="-0.340909375" y2="1.457690625" width="0.05" layer="21"/>
<wire x1="-0.340909375" y1="1.457690625" x2="-0.33343125" y2="1.4894" width="0.05" layer="21"/>
<wire x1="-0.33343125" y1="1.4894" x2="-0.327409375" y2="1.5214" width="0.05" layer="21"/>
<wire x1="-0.327409375" y1="1.5214" x2="-0.322859375" y2="1.55361875" width="0.05" layer="21"/>
<wire x1="-0.322859375" y1="1.55361875" x2="-0.3198" y2="1.585990625" width="0.05" layer="21"/>
<wire x1="-0.3198" y1="1.585990625" x2="-0.318240625" y2="1.618440625" width="0.05" layer="21"/>
<wire x1="-0.318240625" y1="1.618440625" x2="-0.318190625" y2="1.65088125" width="0.05" layer="21"/>
<wire x1="-0.318190625" y1="1.65088125" x2="-0.31968125" y2="1.683259375" width="0.05" layer="21"/>
<wire x1="-0.31968125" y1="1.683259375" x2="-0.32271875" y2="1.715490625" width="0.05" layer="21"/>
<wire x1="-0.32271875" y1="1.715490625" x2="-0.32731875" y2="1.7475" width="0.05" layer="21"/>
<wire x1="-0.32731875" y1="1.7475" x2="-0.335090625" y2="1.7855" width="0.05" layer="21"/>
<wire x1="-0.335090625" y1="1.7855" x2="-0.34573125" y2="1.825190625" width="0.05" layer="21"/>
<wire x1="-0.34573125" y1="1.825190625" x2="-0.358959375" y2="1.86586875" width="0.05" layer="21"/>
<wire x1="-0.358959375" y1="1.86586875" x2="-0.3745" y2="1.906859375" width="0.05" layer="21"/>
<wire x1="-0.3745" y1="1.906859375" x2="-0.392059375" y2="1.94746875" width="0.05" layer="21"/>
<wire x1="-0.392059375" y1="1.94746875" x2="-0.41138125" y2="1.98698125" width="0.05" layer="21"/>
<wire x1="-0.41138125" y1="1.98698125" x2="-0.432159375" y2="2.02473125" width="0.05" layer="21"/>
<wire x1="-0.432159375" y1="2.02473125" x2="-0.45411875" y2="2.06" width="0.05" layer="21"/>
<wire x1="-0.45411875" y1="2.06" x2="-0.471040625" y2="2.087590625" width="0.05" layer="21"/>
<wire x1="-0.471040625" y1="2.087590625" x2="-0.47565" y2="2.096840625" width="0.05" layer="21"/>
<wire x1="-0.47565" y1="2.096840625" x2="-0.47671875" y2="2.10125" width="0.05" layer="21"/>
<wire x1="-0.47671875" y1="2.10125" x2="-0.47475" y2="2.102709375" width="0.05" layer="21"/>
<wire x1="-0.47475" y1="2.102709375" x2="-0.4711" y2="2.1039" width="0.05" layer="21"/>
<wire x1="-0.4711" y1="2.1039" x2="-0.46083125" y2="2.105" width="0.05" layer="21"/>
<wire x1="-0.46083125" y1="2.105" x2="-0.44961875" y2="2.106190625" width="0.05" layer="21"/>
<wire x1="-0.44961875" y1="2.106190625" x2="-0.43191875" y2="2.109509375" width="0.05" layer="21"/>
<wire x1="-0.43191875" y1="2.109509375" x2="-0.38326875" y2="2.12093125" width="0.05" layer="21"/>
<wire x1="-0.38326875" y1="2.12093125" x2="-0.32725" y2="2.13611875" width="0.05" layer="21"/>
<wire x1="-0.32725" y1="2.13611875" x2="-0.27625" y2="2.15193125" width="0.05" layer="21"/>
<wire x1="-0.27625" y1="2.15193125" x2="-0.24005" y2="2.16341875" width="0.05" layer="21"/>
<wire x1="-0.24005" y1="2.16341875" x2="-0.229040625" y2="2.166290625" width="0.05" layer="21"/>
<wire x1="-0.229040625" y1="2.166290625" x2="-0.225" y2="2.1666" width="0.05" layer="21"/>
<wire x1="-0.225" y1="2.1666" x2="-0.22398125" y2="2.1662" width="0.05" layer="21"/>
<wire x1="-0.22398125" y1="2.1662" x2="-0.221209375" y2="2.167140625" width="0.05" layer="21"/>
<wire x1="-0.221209375" y1="2.167140625" x2="-0.212090625" y2="2.172309375" width="0.05" layer="21"/>
<wire x1="-0.212090625" y1="2.172309375" x2="-0.20655" y2="2.17538125" width="0.05" layer="21"/>
<wire x1="-0.20655" y1="2.17538125" x2="-0.200990625" y2="2.177509375" width="0.05" layer="21"/>
<wire x1="-0.200990625" y1="2.177509375" x2="-0.196059375" y2="2.1785" width="0.05" layer="21"/>
<wire x1="-0.196059375" y1="2.1785" x2="-0.19241875" y2="2.178159375" width="0.05" layer="21"/>
<wire x1="-0.19241875" y1="2.178159375" x2="-0.18716875" y2="2.1774" width="0.05" layer="21"/>
<wire x1="-0.18716875" y1="2.1774" x2="-0.185209375" y2="2.178009375" width="0.05" layer="21"/>
<wire x1="-0.185209375" y1="2.178009375" x2="-0.18406875" y2="2.17921875" width="0.05" layer="21"/>
<wire x1="-0.18406875" y1="2.17921875" x2="-0.177740625" y2="2.18355" width="0.05" layer="21"/>
<wire x1="-0.177740625" y1="2.18355" x2="-0.161659375" y2="2.19263125" width="0.05" layer="21"/>
<wire x1="-0.161659375" y1="2.19263125" x2="-0.11" y2="2.21973125" width="0.05" layer="21"/>
<wire x1="-0.11" y1="2.21973125" x2="-0.05378125" y2="2.249240625" width="0.05" layer="21"/>
<wire x1="-0.05378125" y1="2.249240625" x2="-0.0059" y2="2.27651875" width="0.05" layer="21"/>
<wire x1="-0.0059" y1="2.27651875" x2="0.03531875" y2="2.30255" width="0.05" layer="21"/>
<wire x1="0.03531875" y1="2.30255" x2="0.07156875" y2="2.328359375" width="0.05" layer="21"/>
<wire x1="0.07156875" y1="2.328359375" x2="0.10985" y2="2.35921875" width="0.05" layer="21"/>
<wire x1="0.10985" y1="2.35921875" x2="0.14838125" y2="2.393509375" width="0.05" layer="21"/>
<wire x1="0.14838125" y1="2.393509375" x2="0.183740625" y2="2.428059375" width="0.05" layer="21"/>
<wire x1="0.183740625" y1="2.428059375" x2="0.199159375" y2="2.44443125" width="0.05" layer="21"/>
<wire x1="0.199159375" y1="2.44443125" x2="0.2125" y2="2.459659375" width="0.05" layer="21"/>
<wire x1="0.2125" y1="2.459659375" x2="0.23013125" y2="2.48026875" width="0.05" layer="21"/>
<wire x1="0.23013125" y1="2.48026875" x2="0.23628125" y2="2.486559375" width="0.05" layer="21"/>
<wire x1="0.23628125" y1="2.486559375" x2="0.24143125" y2="2.490690625" width="0.05" layer="21"/>
<wire x1="0.24143125" y1="2.490690625" x2="0.246190625" y2="2.493040625" width="0.05" layer="21"/>
<wire x1="0.246190625" y1="2.493040625" x2="0.251159375" y2="2.494009375" width="0.05" layer="21"/>
<wire x1="0.251159375" y1="2.494009375" x2="0.26406875" y2="2.493340625" width="0.05" layer="21"/>
<wire x1="0.26406875" y1="2.493340625" x2="0.278190625" y2="2.490590625" width="0.05" layer="21"/>
<wire x1="0.278190625" y1="2.490590625" x2="0.284390625" y2="2.48831875" width="0.05" layer="21"/>
<wire x1="0.284390625" y1="2.48831875" x2="0.290009375" y2="2.485459375" width="0.05" layer="21"/>
<wire x1="0.290009375" y1="2.485459375" x2="0.29505" y2="2.48201875" width="0.05" layer="21"/>
<wire x1="0.29505" y1="2.48201875" x2="0.2995" y2="2.478" width="0.05" layer="21"/>
<wire x1="0.2995" y1="2.478" x2="0.303359375" y2="2.47341875" width="0.05" layer="21"/>
<wire x1="0.303359375" y1="2.47341875" x2="0.30661875" y2="2.46826875" width="0.05" layer="21"/>
<wire x1="0.30661875" y1="2.46826875" x2="0.309290625" y2="2.46256875" width="0.05" layer="21"/>
<wire x1="0.309290625" y1="2.46256875" x2="0.311359375" y2="2.45633125" width="0.05" layer="21"/>
<wire x1="0.311359375" y1="2.45633125" x2="0.31366875" y2="2.44221875" width="0.05" layer="21"/>
<wire x1="0.31366875" y1="2.44221875" x2="0.31353125" y2="2.426" width="0.05" layer="21"/>
<wire x1="0.31353125" y1="2.426" x2="0.310909375" y2="2.40773125" width="0.05" layer="21"/>
<wire x1="0.310909375" y1="2.40773125" x2="0.306290625" y2="2.388809375" width="0.05" layer="21"/>
<wire x1="0.306290625" y1="2.388809375" x2="0.29971875" y2="2.368" width="0.05" layer="21"/>
<wire x1="0.29971875" y1="2.368" x2="0.291340625" y2="2.345540625" width="0.05" layer="21"/>
<wire x1="0.291340625" y1="2.345540625" x2="0.28128125" y2="2.32166875" width="0.05" layer="21"/>
<wire x1="0.28128125" y1="2.32166875" x2="0.2697" y2="2.29663125" width="0.05" layer="21"/>
<wire x1="0.2697" y1="2.29663125" x2="0.256709375" y2="2.270659375" width="0.05" layer="21"/>
<wire x1="0.256709375" y1="2.270659375" x2="0.24246875" y2="2.244" width="0.05" layer="21"/>
<wire x1="0.24246875" y1="2.244" x2="0.22711875" y2="2.216890625" width="0.05" layer="21"/>
<wire x1="0.22711875" y1="2.216890625" x2="0.19363125" y2="2.16226875" width="0.05" layer="21"/>
<wire x1="0.19363125" y1="2.16226875" x2="0.175759375" y2="2.135240625" width="0.05" layer="21"/>
<wire x1="0.175759375" y1="2.135240625" x2="0.157340625" y2="2.10871875" width="0.05" layer="21"/>
<wire x1="0.157340625" y1="2.10871875" x2="0.138509375" y2="2.082940625" width="0.05" layer="21"/>
<wire x1="0.138509375" y1="2.082940625" x2="0.119390625" y2="2.058159375" width="0.05" layer="21"/>
<wire x1="0.119390625" y1="2.058159375" x2="0.100140625" y2="2.0346" width="0.05" layer="21"/>
<wire x1="0.100140625" y1="2.0346" x2="0.08088125" y2="2.0125" width="0.05" layer="21"/>
<wire x1="0.08088125" y1="2.0125" x2="0.05403125" y2="1.98393125" width="0.05" layer="21"/>
<wire x1="0.05403125" y1="1.98393125" x2="0.02331875" y2="1.9533" width="0.05" layer="21"/>
<wire x1="0.02331875" y1="1.9533" x2="-0.0103" y2="1.92146875" width="0.05" layer="21"/>
<wire x1="-0.0103" y1="1.92146875" x2="-0.04586875" y2="1.88926875" width="0.05" layer="21"/>
<wire x1="-0.04586875" y1="1.88926875" x2="-0.082459375" y2="1.85753125" width="0.05" layer="21"/>
<wire x1="-0.082459375" y1="1.85753125" x2="-0.119109375" y2="1.827109375" width="0.05" layer="21"/>
<wire x1="-0.119109375" y1="1.827109375" x2="-0.154890625" y2="1.79881875" width="0.05" layer="21"/>
<wire x1="-0.154890625" y1="1.79881875" x2="-0.18883125" y2="1.77351875" width="0.05" layer="21"/>
<wire x1="-0.18883125" y1="1.77351875" x2="-0.19703125" y2="1.767059375" width="0.05" layer="21"/>
<wire x1="-0.19703125" y1="1.767059375" x2="-0.20286875" y2="1.76128125" width="0.05" layer="21"/>
<wire x1="-0.20286875" y1="1.76128125" x2="-0.20593125" y2="1.756690625" width="0.05" layer="21"/>
<wire x1="-0.20593125" y1="1.756690625" x2="-0.20626875" y2="1.754990625" width="0.05" layer="21"/>
<wire x1="-0.20626875" y1="1.754990625" x2="-0.20576875" y2="1.75376875" width="0.05" layer="21"/>
<wire x1="-0.20576875" y1="1.75376875" x2="-0.199559375" y2="1.751290625" width="0.05" layer="21"/>
<wire x1="-0.199559375" y1="1.751290625" x2="-0.18593125" y2="1.74776875" width="0.05" layer="21"/>
<wire x1="-0.18593125" y1="1.74776875" x2="-0.14443125" y2="1.739540625" width="0.05" layer="21"/>
<wire x1="-0.14443125" y1="1.739540625" x2="-0.11236875" y2="1.735059375" width="0.05" layer="21"/>
<wire x1="-0.11236875" y1="1.735059375" x2="-0.07383125" y2="1.73138125" width="0.05" layer="21"/>
<wire x1="-0.07383125" y1="1.73138125" x2="-0.0309" y2="1.72855" width="0.05" layer="21"/>
<wire x1="-0.0309" y1="1.72855" x2="0.01428125" y2="1.72665" width="0.05" layer="21"/>
<wire x1="0.01428125" y1="1.72665" x2="0.05961875" y2="1.725740625" width="0.05" layer="21"/>
<wire x1="0.05961875" y1="1.725740625" x2="0.103" y2="1.72588125" width="0.05" layer="21"/>
<wire x1="0.103" y1="1.72588125" x2="0.1423" y2="1.72715" width="0.05" layer="21"/>
<wire x1="0.1423" y1="1.72715" x2="0.175409375" y2="1.7296" width="0.05" layer="21"/>
<wire x1="0.175409375" y1="1.7296" x2="0.2036" y2="1.73295" width="0.05" layer="21"/>
<wire x1="0.2036" y1="1.73295" x2="0.23208125" y2="1.73711875" width="0.05" layer="21"/>
<wire x1="0.23208125" y1="1.73711875" x2="0.28956875" y2="1.74781875" width="0.05" layer="21"/>
<wire x1="0.28956875" y1="1.74781875" x2="0.347259375" y2="1.7615" width="0.05" layer="21"/>
<wire x1="0.347259375" y1="1.7615" x2="0.404509375" y2="1.777959375" width="0.05" layer="21"/>
<wire x1="0.404509375" y1="1.777959375" x2="0.460690625" y2="1.797" width="0.05" layer="21"/>
<wire x1="0.460690625" y1="1.797" x2="0.515159375" y2="1.81843125" width="0.05" layer="21"/>
<wire x1="0.515159375" y1="1.81843125" x2="0.54155" y2="1.82998125" width="0.05" layer="21"/>
<wire x1="0.54155" y1="1.82998125" x2="0.56728125" y2="1.84205" width="0.05" layer="21"/>
<wire x1="0.56728125" y1="1.84205" x2="0.59226875" y2="1.854609375" width="0.05" layer="21"/>
<wire x1="0.59226875" y1="1.854609375" x2="0.61643125" y2="1.86765" width="0.05" layer="21"/>
<wire x1="0.61643125" y1="1.86765" x2="0.63486875" y2="1.877559375" width="0.05" layer="21"/>
<wire x1="0.63486875" y1="1.877559375" x2="0.65078125" y2="1.885190625" width="0.05" layer="21"/>
<wire x1="0.65078125" y1="1.885190625" x2="0.664390625" y2="1.890590625" width="0.05" layer="21"/>
<wire x1="0.664390625" y1="1.890590625" x2="0.675959375" y2="1.893809375" width="0.05" layer="21"/>
<wire x1="0.675959375" y1="1.893809375" x2="0.68573125" y2="1.894890625" width="0.05" layer="21"/>
<wire x1="0.68573125" y1="1.894890625" x2="0.69395" y2="1.8939" width="0.05" layer="21"/>
<wire x1="0.69395" y1="1.8939" x2="0.70086875" y2="1.89086875" width="0.05" layer="21"/>
<wire x1="0.70086875" y1="1.89086875" x2="0.70673125" y2="1.885859375" width="0.05" layer="21"/>
<wire x1="0.70673125" y1="1.885859375" x2="0.709940625" y2="1.880959375" width="0.05" layer="21"/>
<wire x1="0.709940625" y1="1.880959375" x2="0.71255" y2="1.87438125" width="0.05" layer="21"/>
<wire x1="0.71255" y1="1.87438125" x2="0.714309375" y2="1.86696875" width="0.05" layer="21"/>
<wire x1="0.714309375" y1="1.86696875" x2="0.71493125" y2="1.859609375" width="0.05" layer="21"/>
<wire x1="0.71493125" y1="1.859609375" x2="0.71328125" y2="1.84678125" width="0.05" layer="21"/>
<wire x1="0.71328125" y1="1.84678125" x2="0.70875" y2="1.828040625" width="0.05" layer="21"/>
<wire x1="0.70875" y1="1.828040625" x2="0.701840625" y2="1.804790625" width="0.05" layer="21"/>
<wire x1="0.701840625" y1="1.804790625" x2="0.69301875" y2="1.778459375" width="0.05" layer="21"/>
<wire x1="0.69301875" y1="1.778459375" x2="0.682809375" y2="1.75045" width="0.05" layer="21"/>
<wire x1="0.682809375" y1="1.75045" x2="0.671690625" y2="1.72216875" width="0.05" layer="21"/>
<wire x1="0.671690625" y1="1.72216875" x2="0.660159375" y2="1.69501875" width="0.05" layer="21"/>
<wire x1="0.660159375" y1="1.69501875" x2="0.6487" y2="1.670440625" width="0.05" layer="21"/>
<wire x1="0.6487" y1="1.670440625" x2="0.62065" y2="1.6125" width="0.05" layer="21"/>
<wire x1="0.62065" y1="1.6125" x2="0.62065" y2="1.61186875" width="0.05" layer="21"/>
<wire x1="0.62065" y1="1.61186875" x2="0.621409375" y2="1.612" width="0.05" layer="21"/>
<wire x1="0.621409375" y1="1.612" x2="0.62495" y2="1.614340625" width="0.05" layer="21"/>
<wire x1="0.62495" y1="1.614340625" x2="0.63808125" y2="1.625790625" width="0.05" layer="21"/>
<wire x1="0.63808125" y1="1.625790625" x2="0.680109375" y2="1.66283125" width="0.05" layer="21"/>
<wire x1="0.680109375" y1="1.66283125" x2="0.731809375" y2="1.705390625" width="0.05" layer="21"/>
<wire x1="0.731809375" y1="1.705390625" x2="0.7769" y2="1.74041875" width="0.05" layer="21"/>
<wire x1="0.7769" y1="1.74041875" x2="0.79186875" y2="1.751040625" width="0.05" layer="21"/>
<wire x1="0.79186875" y1="1.751040625" x2="0.79906875" y2="1.754859375" width="0.05" layer="21"/>
<wire x1="0.79906875" y1="1.754859375" x2="0.801390625" y2="1.75343125" width="0.05" layer="21"/>
<wire x1="0.801390625" y1="1.75343125" x2="0.80516875" y2="1.74961875" width="0.05" layer="21"/>
<wire x1="0.80516875" y1="1.74961875" x2="0.815" y2="1.737209375" width="0.05" layer="21"/>
<wire x1="0.815" y1="1.737209375" x2="0.82241875" y2="1.72845" width="0.05" layer="21"/>
<wire x1="0.82241875" y1="1.72845" x2="0.83338125" y2="1.717540625" width="0.05" layer="21"/>
<wire x1="0.83338125" y1="1.717540625" x2="0.8464" y2="1.70586875" width="0.05" layer="21"/>
<wire x1="0.8464" y1="1.70586875" x2="0.86" y2="1.694840625" width="0.05" layer="21"/>
<wire x1="0.86" y1="1.694840625" x2="0.8727" y2="1.68563125" width="0.05" layer="21"/>
<wire x1="0.8727" y1="1.68563125" x2="0.88558125" y2="1.6773" width="0.05" layer="21"/>
<wire x1="0.88558125" y1="1.6773" x2="0.898709375" y2="1.66985" width="0.05" layer="21"/>
<wire x1="0.898709375" y1="1.66985" x2="0.912140625" y2="1.663259375" width="0.05" layer="21"/>
<wire x1="0.912140625" y1="1.663259375" x2="0.92593125" y2="1.657509375" width="0.05" layer="21"/>
<wire x1="0.92593125" y1="1.657509375" x2="0.94013125" y2="1.652609375" width="0.05" layer="21"/>
<wire x1="0.94013125" y1="1.652609375" x2="0.9548" y2="1.648540625" width="0.05" layer="21"/>
<wire x1="0.9548" y1="1.648540625" x2="0.969990625" y2="1.64528125" width="0.05" layer="21"/>
<wire x1="0.969990625" y1="1.64528125" x2="0.985759375" y2="1.642840625" width="0.05" layer="21"/>
<wire x1="0.985759375" y1="1.642840625" x2="1.00216875" y2="1.641190625" width="0.05" layer="21"/>
<wire x1="1.00216875" y1="1.641190625" x2="1.019259375" y2="1.640340625" width="0.05" layer="21"/>
<wire x1="1.019259375" y1="1.640340625" x2="1.037109375" y2="1.640259375" width="0.05" layer="21"/>
<wire x1="1.037109375" y1="1.640259375" x2="1.075259375" y2="1.6424" width="0.05" layer="21"/>
<wire x1="1.075259375" y1="1.6424" x2="1.11706875" y2="1.64753125" width="0.05" layer="21"/>
<wire x1="1.11706875" y1="1.64753125" x2="1.156690625" y2="1.65313125" width="0.05" layer="21"/>
<wire x1="1.156690625" y1="1.65313125" x2="1.178559375" y2="1.65483125" width="0.05" layer="21"/>
<wire x1="1.178559375" y1="1.65483125" x2="1.18413125" y2="1.6542" width="0.05" layer="21"/>
<wire x1="1.18413125" y1="1.6542" x2="1.18683125" y2="1.65256875" width="0.05" layer="21"/>
<wire x1="1.18683125" y1="1.65256875" x2="1.18716875" y2="1.64991875" width="0.05" layer="21"/>
<wire x1="1.18716875" y1="1.64991875" x2="1.18566875" y2="1.64625" width="0.05" layer="21"/>
<wire x1="1.18566875" y1="1.64625" x2="1.18038125" y2="1.640259375" width="0.05" layer="21"/>
<wire x1="1.18038125" y1="1.640259375" x2="1.16985" y2="1.632159375" width="0.05" layer="21"/>
<wire x1="1.16985" y1="1.632159375" x2="1.154209375" y2="1.622009375" width="0.05" layer="21"/>
<wire x1="1.154209375" y1="1.622009375" x2="1.13356875" y2="1.60988125" width="0.05" layer="21"/>
<wire x1="1.13356875" y1="1.60988125" x2="1.077859375" y2="1.58" width="0.05" layer="21"/>
<wire x1="1.077859375" y1="1.58" x2="1.00375" y2="1.5431" width="0.05" layer="21"/>
<wire x1="1.00375" y1="1.5431" x2="0.965409375" y2="1.5241" width="0.05" layer="21"/>
<wire x1="0.965409375" y1="1.5241" x2="0.934009375" y2="1.507759375" width="0.05" layer="21"/>
<wire x1="0.934009375" y1="1.507759375" x2="0.9128" y2="1.49581875" width="0.05" layer="21"/>
<wire x1="0.9128" y1="1.49581875" x2="0.90701875" y2="1.492040625" width="0.05" layer="21"/>
<wire x1="0.90701875" y1="1.492040625" x2="0.905" y2="1.49001875" width="0.05" layer="21"/>
<wire x1="0.905" y1="1.49001875" x2="0.905290625" y2="1.48801875" width="0.05" layer="21"/>
<wire x1="0.905290625" y1="1.48801875" x2="0.907090625" y2="1.486959375" width="0.05" layer="21"/>
<wire x1="0.907090625" y1="1.486959375" x2="0.91178125" y2="1.487159375" width="0.05" layer="21"/>
<wire x1="0.91178125" y1="1.487159375" x2="0.92076875" y2="1.48893125" width="0.05" layer="21"/>
<wire x1="0.92076875" y1="1.48893125" x2="0.95716875" y2="1.498490625" width="0.05" layer="21"/>
<wire x1="0.95716875" y1="1.498490625" x2="1.027440625" y2="1.518209375" width="0.05" layer="21"/>
<wire x1="1.027440625" y1="1.518209375" x2="1.098909375" y2="1.537090625" width="0.05" layer="21"/>
<wire x1="1.098909375" y1="1.537090625" x2="1.1241" y2="1.542790625" width="0.05" layer="21"/>
<wire x1="1.1241" y1="1.542790625" x2="1.137440625" y2="1.544840625" width="0.05" layer="21"/>
<wire x1="1.137440625" y1="1.544840625" x2="1.1525" y2="1.54468125" width="0.05" layer="21"/>
<wire x1="1.1525" y1="1.54468125" x2="1.13955" y2="1.53326875" width="0.05" layer="21"/>
<wire x1="1.13955" y1="1.53326875" x2="1.12858125" y2="1.525390625" width="0.05" layer="21"/>
<wire x1="1.12858125" y1="1.525390625" x2="1.10826875" y2="1.512390625" width="0.05" layer="21"/>
<wire x1="1.10826875" y1="1.512390625" x2="1.051240625" y2="1.47821875" width="0.05" layer="21"/>
<wire x1="1.051240625" y1="1.47821875" x2="1.00743125" y2="1.4523" width="0.05" layer="21"/>
<wire x1="1.00743125" y1="1.4523" x2="0.980090625" y2="1.43461875" width="0.05" layer="21"/>
<wire x1="0.980090625" y1="1.43461875" x2="0.9719" y2="1.428359375" width="0.05" layer="21"/>
<wire x1="0.9719" y1="1.428359375" x2="0.96696875" y2="1.42356875" width="0.05" layer="21"/>
<wire x1="0.96696875" y1="1.42356875" x2="0.96503125" y2="1.42003125" width="0.05" layer="21"/>
<wire x1="0.96503125" y1="1.42003125" x2="0.965790625" y2="1.417540625" width="0.05" layer="21"/>
<wire x1="0.965790625" y1="1.417540625" x2="0.973140625" y2="1.41883125" width="0.05" layer="21"/>
<wire x1="0.973140625" y1="1.41883125" x2="0.99151875" y2="1.42398125" width="0.05" layer="21"/>
<wire x1="0.99151875" y1="1.42398125" x2="1.050259375" y2="1.44255" width="0.05" layer="21"/>
<wire x1="1.050259375" y1="1.44255" x2="1.08213125" y2="1.452740625" width="0.05" layer="21"/>
<wire x1="1.08213125" y1="1.452740625" x2="1.10916875" y2="1.460590625" width="0.05" layer="21"/>
<wire x1="1.10916875" y1="1.460590625" x2="1.131559375" y2="1.46611875" width="0.05" layer="21"/>
<wire x1="1.131559375" y1="1.46611875" x2="1.14948125" y2="1.46935" width="0.05" layer="21"/>
<wire x1="1.14948125" y1="1.46935" x2="1.1631" y2="1.4703" width="0.05" layer="21"/>
<wire x1="1.1631" y1="1.4703" x2="1.172609375" y2="1.469009375" width="0.05" layer="21"/>
<wire x1="1.172609375" y1="1.469009375" x2="1.17588125" y2="1.46753125" width="0.05" layer="21"/>
<wire x1="1.17588125" y1="1.46753125" x2="1.178190625" y2="1.465490625" width="0.05" layer="21"/>
<wire x1="1.178190625" y1="1.465490625" x2="1.17955" y2="1.462909375" width="0.05" layer="21"/>
<wire x1="1.17955" y1="1.462909375" x2="1.18" y2="1.45978125" width="0.05" layer="21"/>
<wire x1="1.18" y1="1.45978125" x2="1.17916875" y2="1.45671875" width="0.05" layer="21"/>
<wire x1="1.17916875" y1="1.45671875" x2="1.17675" y2="1.452240625" width="0.05" layer="21"/>
<wire x1="1.17675" y1="1.452240625" x2="1.16756875" y2="1.43951875" width="0.05" layer="21"/>
<wire x1="1.16756875" y1="1.43951875" x2="1.15335" y2="1.422740625" width="0.05" layer="21"/>
<wire x1="1.15335" y1="1.422740625" x2="1.135" y2="1.40303125" width="0.05" layer="21"/>
<wire x1="1.135" y1="1.40303125" x2="1.10321875" y2="1.368490625" width="0.05" layer="21"/>
<wire x1="1.10321875" y1="1.368490625" x2="1.09355" y2="1.35666875" width="0.05" layer="21"/>
<wire x1="1.09355" y1="1.35666875" x2="1.09" y2="1.350759375" width="0.05" layer="21"/>
<wire x1="1.09" y1="1.350759375" x2="1.090440625" y2="1.348659375" width="0.05" layer="21"/>
<wire x1="1.090440625" y1="1.348659375" x2="1.09171875" y2="1.34706875" width="0.05" layer="21"/>
<wire x1="1.09171875" y1="1.34706875" x2="1.09666875" y2="1.34535" width="0.05" layer="21"/>
<wire x1="1.09666875" y1="1.34535" x2="1.10458125" y2="1.34546875" width="0.05" layer="21"/>
<wire x1="1.10458125" y1="1.34546875" x2="1.115159375" y2="1.347309375" width="0.05" layer="21"/>
<wire x1="1.115159375" y1="1.347309375" x2="1.128140625" y2="1.35076875" width="0.05" layer="21"/>
<wire x1="1.128140625" y1="1.35076875" x2="1.143209375" y2="1.3557" width="0.05" layer="21"/>
<wire x1="1.143209375" y1="1.3557" x2="1.17855" y2="1.36955" width="0.05" layer="21"/>
<wire x1="1.17855" y1="1.36955" x2="1.21891875" y2="1.38788125" width="0.05" layer="21"/>
<wire x1="1.21891875" y1="1.38788125" x2="1.26205" y2="1.409740625" width="0.05" layer="21"/>
<wire x1="1.26205" y1="1.409740625" x2="1.305659375" y2="1.43416875" width="0.05" layer="21"/>
<wire x1="1.305659375" y1="1.43416875" x2="1.3475" y2="1.4602" width="0.05" layer="21"/>
<wire x1="1.3475" y1="1.4602" x2="1.36873125" y2="1.47481875" width="0.05" layer="21"/>
<wire x1="1.36873125" y1="1.47481875" x2="1.389659375" y2="1.49036875" width="0.05" layer="21"/>
<wire x1="1.389659375" y1="1.49036875" x2="1.40978125" y2="1.50643125" width="0.05" layer="21"/>
<wire x1="1.40978125" y1="1.50643125" x2="1.428590625" y2="1.52251875" width="0.05" layer="21"/>
<wire x1="1.428590625" y1="1.52251875" x2="1.44558125" y2="1.53821875" width="0.05" layer="21"/>
<wire x1="1.44558125" y1="1.53821875" x2="1.46023125" y2="1.553059375" width="0.05" layer="21"/>
<wire x1="1.46023125" y1="1.553059375" x2="1.472059375" y2="1.5666" width="0.05" layer="21"/>
<wire x1="1.472059375" y1="1.5666" x2="1.48055" y2="1.57838125" width="0.05" layer="21"/>
<wire x1="1.48055" y1="1.57838125" x2="1.48883125" y2="1.59335" width="0.05" layer="21"/>
<wire x1="1.48883125" y1="1.59335" x2="1.494240625" y2="1.60708125" width="0.05" layer="21"/>
<wire x1="1.494240625" y1="1.60708125" x2="1.49671875" y2="1.619909375" width="0.05" layer="21"/>
<wire x1="1.49671875" y1="1.619909375" x2="1.496840625" y2="1.6261" width="0.05" layer="21"/>
<wire x1="1.496840625" y1="1.6261" x2="1.4962" y2="1.632190625" width="0.05" layer="21"/>
<wire x1="1.4962" y1="1.632190625" x2="1.492640625" y2="1.64425" width="0.05" layer="21"/>
<wire x1="1.492640625" y1="1.64425" x2="1.48596875" y2="1.65645" width="0.05" layer="21"/>
<wire x1="1.48596875" y1="1.65645" x2="1.476140625" y2="1.669109375" width="0.05" layer="21"/>
<wire x1="1.476140625" y1="1.669109375" x2="1.463090625" y2="1.68258125" width="0.05" layer="21"/>
<wire x1="1.463090625" y1="1.68258125" x2="1.44658125" y2="1.70011875" width="0.05" layer="21"/>
<wire x1="1.44658125" y1="1.70011875" x2="1.43265" y2="1.718840625" width="0.05" layer="21"/>
<wire x1="1.43265" y1="1.718840625" x2="1.421259375" y2="1.7388" width="0.05" layer="21"/>
<wire x1="1.421259375" y1="1.7388" x2="1.4124" y2="1.760040625" width="0.05" layer="21"/>
<wire x1="1.4124" y1="1.760040625" x2="1.406059375" y2="1.782609375" width="0.05" layer="21"/>
<wire x1="1.406059375" y1="1.782609375" x2="1.40221875" y2="1.806559375" width="0.05" layer="21"/>
<wire x1="1.40221875" y1="1.806559375" x2="1.400859375" y2="1.83193125" width="0.05" layer="21"/>
<wire x1="1.400859375" y1="1.83193125" x2="1.40196875" y2="1.85878125" width="0.05" layer="21"/>
<wire x1="1.40196875" y1="1.85878125" x2="1.40516875" y2="1.90551875" width="0.05" layer="21"/>
<wire x1="1.40516875" y1="1.90551875" x2="1.40826875" y2="1.96353125" width="0.05" layer="21"/>
<wire x1="1.40826875" y1="1.96353125" x2="1.411290625" y2="2.029559375" width="0.05" layer="21"/>
<wire x1="1.411290625" y1="2.029559375" x2="1.4369" y2="2.03466875" width="0.05" layer="21"/>
<wire x1="1.4369" y1="2.03466875" x2="1.47026875" y2="2.040590625" width="0.05" layer="21"/>
<wire x1="1.47026875" y1="2.040590625" x2="1.503809375" y2="2.045140625" width="0.05" layer="21"/>
<wire x1="1.503809375" y1="2.045140625" x2="1.53688125" y2="2.04828125" width="0.05" layer="21"/>
<wire x1="1.53688125" y1="2.04828125" x2="1.56881875" y2="2.05001875" width="0.05" layer="21"/>
<wire x1="1.56881875" y1="2.05001875" x2="1.59898125" y2="2.050309375" width="0.05" layer="21"/>
<wire x1="1.59898125" y1="2.050309375" x2="1.62671875" y2="2.049140625" width="0.05" layer="21"/>
<wire x1="1.62671875" y1="2.049140625" x2="1.65136875" y2="2.046490625" width="0.05" layer="21"/>
<wire x1="1.65136875" y1="2.046490625" x2="1.6723" y2="2.042340625" width="0.05" layer="21"/>
<wire x1="1.6723" y1="2.042340625" x2="1.689009375" y2="2.03743125" width="0.05" layer="21"/>
<wire x1="1.689009375" y1="2.03743125" x2="1.70488125" y2="2.0316" width="0.05" layer="21"/>
<wire x1="1.70488125" y1="2.0316" x2="1.7199" y2="2.02486875" width="0.05" layer="21"/>
<wire x1="1.7199" y1="2.02486875" x2="1.73408125" y2="2.01723125" width="0.05" layer="21"/>
<wire x1="1.73408125" y1="2.01723125" x2="1.747409375" y2="2.00868125" width="0.05" layer="21"/>
<wire x1="1.747409375" y1="2.00868125" x2="1.7599" y2="1.99923125" width="0.05" layer="21"/>
<wire x1="1.7599" y1="1.99923125" x2="1.771540625" y2="1.98886875" width="0.05" layer="21"/>
<wire x1="1.771540625" y1="1.98886875" x2="1.78233125" y2="1.977609375" width="0.05" layer="21"/>
<wire x1="1.78233125" y1="1.977609375" x2="1.79228125" y2="1.96545" width="0.05" layer="21"/>
<wire x1="1.79228125" y1="1.96545" x2="1.80138125" y2="1.95236875" width="0.05" layer="21"/>
<wire x1="1.80138125" y1="1.95236875" x2="1.80963125" y2="1.9384" width="0.05" layer="21"/>
<wire x1="1.80963125" y1="1.9384" x2="1.81703125" y2="1.92351875" width="0.05" layer="21"/>
<wire x1="1.81703125" y1="1.92351875" x2="1.82358125" y2="1.90775" width="0.05" layer="21"/>
<wire x1="1.82358125" y1="1.90775" x2="1.82928125" y2="1.891059375" width="0.05" layer="21"/>
<wire x1="1.82928125" y1="1.891059375" x2="1.834140625" y2="1.87348125" width="0.05" layer="21"/>
<wire x1="1.834140625" y1="1.87348125" x2="1.838140625" y2="1.855" width="0.05" layer="21"/>
<wire x1="1.838140625" y1="1.855" x2="1.84188125" y2="1.83058125" width="0.05" layer="21"/>
<wire x1="1.84188125" y1="1.83058125" x2="1.84391875" y2="1.804090625" width="0.05" layer="21"/>
<wire x1="1.84391875" y1="1.804090625" x2="1.844409375" y2="1.77181875" width="0.05" layer="21"/>
<wire x1="1.844409375" y1="1.77181875" x2="1.8435" y2="1.73" width="0.05" layer="21"/>
<wire x1="1.8435" y1="1.73" x2="1.842190625" y2="1.6984" width="0.05" layer="21"/>
<wire x1="1.842190625" y1="1.6984" x2="1.84028125" y2="1.670959375" width="0.05" layer="21"/>
<wire x1="1.84028125" y1="1.670959375" x2="1.83753125" y2="1.64643125" width="0.05" layer="21"/>
<wire x1="1.83753125" y1="1.64643125" x2="1.833690625" y2="1.623590625" width="0.05" layer="21"/>
<wire x1="1.833690625" y1="1.623590625" x2="1.82851875" y2="1.6012" width="0.05" layer="21"/>
<wire x1="1.82851875" y1="1.6012" x2="1.82176875" y2="1.57803125" width="0.05" layer="21"/>
<wire x1="1.82176875" y1="1.57803125" x2="1.8132" y2="1.55283125" width="0.05" layer="21"/>
<wire x1="1.8132" y1="1.55283125" x2="1.80255" y2="1.52438125" width="0.05" layer="21"/>
<wire x1="1.80255" y1="1.52438125" x2="1.78845" y2="1.49036875" width="0.05" layer="21"/>
<wire x1="1.78845" y1="1.49036875" x2="1.77161875" y2="1.45433125" width="0.05" layer="21"/>
<wire x1="1.77161875" y1="1.45433125" x2="1.752159375" y2="1.4164" width="0.05" layer="21"/>
<wire x1="1.752159375" y1="1.4164" x2="1.73018125" y2="1.3767" width="0.05" layer="21"/>
<wire x1="1.73018125" y1="1.3767" x2="1.705759375" y2="1.3354" width="0.05" layer="21"/>
<wire x1="1.705759375" y1="1.3354" x2="1.67901875" y2="1.292640625" width="0.05" layer="21"/>
<wire x1="1.67901875" y1="1.292640625" x2="1.650040625" y2="1.24855" width="0.05" layer="21"/>
<wire x1="1.650040625" y1="1.24855" x2="1.61895" y2="1.20328125" width="0.05" layer="21"/>
<wire x1="1.61895" y1="1.20328125" x2="1.58581875" y2="1.15696875" width="0.05" layer="21"/>
<wire x1="1.58581875" y1="1.15696875" x2="1.55076875" y2="1.10976875" width="0.05" layer="21"/>
<wire x1="1.55076875" y1="1.10976875" x2="1.5139" y2="1.06181875" width="0.05" layer="21"/>
<wire x1="1.5139" y1="1.06181875" x2="1.4753" y2="1.013259375" width="0.05" layer="21"/>
<wire x1="1.4753" y1="1.013259375" x2="1.43506875" y2="0.964240625" width="0.05" layer="21"/>
<wire x1="1.43506875" y1="0.964240625" x2="1.39331875" y2="0.9149" width="0.05" layer="21"/>
<wire x1="1.39331875" y1="0.9149" x2="1.35015" y2="0.86538125" width="0.05" layer="21"/>
<wire x1="1.35015" y1="0.86538125" x2="1.305659375" y2="0.81583125" width="0.05" layer="21"/>
<wire x1="1.305659375" y1="0.81583125" x2="1.26121875" y2="0.76588125" width="0.05" layer="21"/>
<wire x1="1.26121875" y1="0.76588125" x2="1.2465" y2="0.74845" width="0.05" layer="21"/>
<wire x1="1.2465" y1="0.74845" x2="1.23968125" y2="0.739390625" width="0.05" layer="21"/>
<wire x1="1.23968125" y1="0.739390625" x2="1.236509375" y2="0.73111875" width="0.05" layer="21"/>
<wire x1="1.236509375" y1="0.73111875" x2="1.23623125" y2="0.72373125" width="0.05" layer="21"/>
<wire x1="1.23623125" y1="0.72373125" x2="1.23875" y2="0.71721875" width="0.05" layer="21"/>
<wire x1="1.23875" y1="0.71721875" x2="1.243940625" y2="0.711609375" width="0.05" layer="21"/>
<wire x1="1.243940625" y1="0.711609375" x2="1.2517" y2="0.706909375" width="0.05" layer="21"/>
<wire x1="1.2517" y1="0.706909375" x2="1.261909375" y2="0.703140625" width="0.05" layer="21"/>
<wire x1="1.261909375" y1="0.703140625" x2="1.274459375" y2="0.700290625" width="0.05" layer="21"/>
<wire x1="1.274459375" y1="0.700290625" x2="1.289240625" y2="0.69838125" width="0.05" layer="21"/>
<wire x1="1.289240625" y1="0.69838125" x2="1.30613125" y2="0.697409375" width="0.05" layer="21"/>
<wire x1="1.30613125" y1="0.697409375" x2="1.32503125" y2="0.697409375" width="0.05" layer="21"/>
<wire x1="1.32503125" y1="0.697409375" x2="1.34581875" y2="0.69838125" width="0.05" layer="21"/>
<wire x1="1.34581875" y1="0.69838125" x2="1.3684" y2="0.70033125" width="0.05" layer="21"/>
<wire x1="1.3684" y1="0.70033125" x2="1.392640625" y2="0.70326875" width="0.05" layer="21"/>
<wire x1="1.392640625" y1="0.70326875" x2="1.41843125" y2="0.707209375" width="0.05" layer="21"/>
<wire x1="1.41843125" y1="0.707209375" x2="1.44566875" y2="0.71215" width="0.05" layer="21"/>
<wire x1="1.44566875" y1="0.71215" x2="1.47425" y2="0.71811875" width="0.05" layer="21"/>
<wire x1="1.47425" y1="0.71811875" x2="1.5305" y2="0.731940625" width="0.05" layer="21"/>
<wire x1="1.5305" y1="0.731940625" x2="1.58678125" y2="0.748359375" width="0.05" layer="21"/>
<wire x1="1.58678125" y1="0.748359375" x2="1.642909375" y2="0.76728125" width="0.05" layer="21"/>
<wire x1="1.642909375" y1="0.76728125" x2="1.69866875" y2="0.78858125" width="0.05" layer="21"/>
<wire x1="1.69866875" y1="0.78858125" x2="1.75388125" y2="0.81213125" width="0.05" layer="21"/>
<wire x1="1.75388125" y1="0.81213125" x2="1.80835" y2="0.837840625" width="0.05" layer="21"/>
<wire x1="1.80835" y1="0.837840625" x2="1.861859375" y2="0.86556875" width="0.05" layer="21"/>
<wire x1="1.861859375" y1="0.86556875" x2="1.91423125" y2="0.89521875" width="0.05" layer="21"/>
<wire x1="1.91423125" y1="0.89521875" x2="1.965259375" y2="0.92666875" width="0.05" layer="21"/>
<wire x1="1.965259375" y1="0.92666875" x2="2.014759375" y2="0.9598" width="0.05" layer="21"/>
<wire x1="2.014759375" y1="0.9598" x2="2.06251875" y2="0.994490625" width="0.05" layer="21"/>
<wire x1="2.06251875" y1="0.994490625" x2="2.10835" y2="1.03063125" width="0.05" layer="21"/>
<wire x1="2.10835" y1="1.03063125" x2="2.15205" y2="1.068109375" width="0.05" layer="21"/>
<wire x1="2.15205" y1="1.068109375" x2="2.193440625" y2="1.1068" width="0.05" layer="21"/>
<wire x1="2.193440625" y1="1.1068" x2="2.2323" y2="1.1466" width="0.05" layer="21"/>
<wire x1="2.2323" y1="1.1466" x2="2.26845" y2="1.18738125" width="0.05" layer="21"/>
<wire x1="2.26845" y1="1.18738125" x2="2.28916875" y2="1.21268125" width="0.05" layer="21"/>
<wire x1="2.28916875" y1="1.21268125" x2="2.30908125" y2="1.238390625" width="0.05" layer="21"/>
<wire x1="2.30908125" y1="1.238390625" x2="2.32816875" y2="1.2645" width="0.05" layer="21"/>
<wire x1="2.32816875" y1="1.2645" x2="2.346440625" y2="1.291" width="0.05" layer="21"/>
<wire x1="2.346440625" y1="1.291" x2="2.363890625" y2="1.31788125" width="0.05" layer="21"/>
<wire x1="2.363890625" y1="1.31788125" x2="2.380509375" y2="1.34511875" width="0.05" layer="21"/>
<wire x1="2.380509375" y1="1.34511875" x2="2.3963" y2="1.372709375" width="0.05" layer="21"/>
<wire x1="2.3963" y1="1.372709375" x2="2.41126875" y2="1.40063125" width="0.05" layer="21"/>
<wire x1="2.41126875" y1="1.40063125" x2="2.425390625" y2="1.428890625" width="0.05" layer="21"/>
<wire x1="2.425390625" y1="1.428890625" x2="2.43868125" y2="1.45745" width="0.05" layer="21"/>
<wire x1="2.43868125" y1="1.45745" x2="2.45111875" y2="1.48631875" width="0.05" layer="21"/>
<wire x1="2.45111875" y1="1.48631875" x2="2.46271875" y2="1.51546875" width="0.05" layer="21"/>
<wire x1="2.46271875" y1="1.51546875" x2="2.47346875" y2="1.544890625" width="0.05" layer="21"/>
<wire x1="2.47346875" y1="1.544890625" x2="2.483359375" y2="1.57458125" width="0.05" layer="21"/>
<wire x1="2.483359375" y1="1.57458125" x2="2.492409375" y2="1.60451875" width="0.05" layer="21"/>
<wire x1="2.492409375" y1="1.60451875" x2="2.500590625" y2="1.634690625" width="0.05" layer="21"/>
<wire x1="2.500590625" y1="1.634690625" x2="2.507909375" y2="1.665090625" width="0.05" layer="21"/>
<wire x1="2.507909375" y1="1.665090625" x2="2.51436875" y2="1.6957" width="0.05" layer="21"/>
<wire x1="2.51436875" y1="1.6957" x2="2.51995" y2="1.7265" width="0.05" layer="21"/>
<wire x1="2.51995" y1="1.7265" x2="2.52466875" y2="1.757490625" width="0.05" layer="21"/>
<wire x1="2.52466875" y1="1.757490625" x2="2.528509375" y2="1.788659375" width="0.05" layer="21"/>
<wire x1="2.528509375" y1="1.788659375" x2="2.53148125" y2="1.81998125" width="0.05" layer="21"/>
<wire x1="2.53148125" y1="1.81998125" x2="2.533559375" y2="1.85145" width="0.05" layer="21"/>
<wire x1="2.533559375" y1="1.85145" x2="2.534759375" y2="1.883059375" width="0.05" layer="21"/>
<wire x1="2.534759375" y1="1.883059375" x2="2.53506875" y2="1.91478125" width="0.05" layer="21"/>
<wire x1="2.53506875" y1="1.91478125" x2="2.53448125" y2="1.94661875" width="0.05" layer="21"/>
<wire x1="2.53448125" y1="1.94661875" x2="2.533009375" y2="1.978559375" width="0.05" layer="21"/>
<wire x1="2.533009375" y1="1.978559375" x2="2.530640625" y2="2.01056875" width="0.05" layer="21"/>
<wire x1="2.530640625" y1="2.01056875" x2="2.527359375" y2="2.042659375" width="0.05" layer="21"/>
<wire x1="2.527359375" y1="2.042659375" x2="2.52318125" y2="2.074809375" width="0.05" layer="21"/>
<wire x1="2.52318125" y1="2.074809375" x2="2.5181" y2="2.107009375" width="0.05" layer="21"/>
<wire x1="2.5181" y1="2.107009375" x2="2.5121" y2="2.13923125" width="0.05" layer="21"/>
<wire x1="2.5121" y1="2.13923125" x2="2.49988125" y2="2.19376875" width="0.05" layer="21"/>
<wire x1="2.49988125" y1="2.19376875" x2="2.485340625" y2="2.2462" width="0.05" layer="21"/>
<wire x1="2.485340625" y1="2.2462" x2="2.46816875" y2="2.297290625" width="0.05" layer="21"/>
<wire x1="2.46816875" y1="2.297290625" x2="2.44801875" y2="2.3478" width="0.05" layer="21"/>
<wire x1="2.44801875" y1="2.3478" x2="2.424559375" y2="2.398490625" width="0.05" layer="21"/>
<wire x1="2.424559375" y1="2.398490625" x2="2.39748125" y2="2.45015" width="0.05" layer="21"/>
<wire x1="2.39748125" y1="2.45015" x2="2.36643125" y2="2.50351875" width="0.05" layer="21"/>
<wire x1="2.36643125" y1="2.50351875" x2="2.33108125" y2="2.559390625" width="0.05" layer="21"/>
<wire x1="2.33108125" y1="2.559390625" x2="2.293790625" y2="2.61628125" width="0.05" layer="21"/>
<wire x1="2.293790625" y1="2.61628125" x2="3.33315" y2="3.655790625" width="0.05" layer="21"/>
<wire x1="3.33315" y1="3.655790625" x2="3.855909375" y2="4.178090625" width="0.05" layer="21"/>
<wire x1="3.855909375" y1="4.178090625" x2="4.17721875" y2="4.497440625" width="0.05" layer="21"/>
<wire x1="4.17721875" y1="4.497440625" x2="4.27663125" y2="4.595140625" width="0.05" layer="21"/>
<wire x1="4.27663125" y1="4.595140625" x2="4.342840625" y2="4.65905" width="0.05" layer="21"/>
<wire x1="4.342840625" y1="4.65905" x2="4.38156875" y2="4.69483125" width="0.05" layer="21"/>
<wire x1="4.38156875" y1="4.69483125" x2="4.39241875" y2="4.703940625" width="0.05" layer="21"/>
<wire x1="4.39241875" y1="4.703940625" x2="4.39855" y2="4.70813125" width="0.05" layer="21"/>
<wire x1="4.39855" y1="4.70813125" x2="4.40888125" y2="4.71261875" width="0.05" layer="21"/>
<wire x1="4.40888125" y1="4.71261875" x2="4.419809375" y2="4.716259375" width="0.05" layer="21"/>
<wire x1="4.419809375" y1="4.716259375" x2="4.43111875" y2="4.71901875" width="0.05" layer="21"/>
<wire x1="4.43111875" y1="4.71901875" x2="4.442590625" y2="4.72086875" width="0.05" layer="21"/>
<wire x1="4.442590625" y1="4.72086875" x2="4.454" y2="4.72178125" width="0.05" layer="21"/>
<wire x1="4.454" y1="4.72178125" x2="4.46511875" y2="4.721740625" width="0.05" layer="21"/>
<wire x1="4.46511875" y1="4.721740625" x2="4.475740625" y2="4.720709375" width="0.05" layer="21"/>
<wire x1="4.475740625" y1="4.720709375" x2="4.485640625" y2="4.71866875" width="0.05" layer="21"/>
<wire x1="4.485640625" y1="4.71866875" x2="4.491740625" y2="4.71638125" width="0.05" layer="21"/>
<wire x1="4.491740625" y1="4.71638125" x2="4.497209375" y2="4.71303125" width="0.05" layer="21"/>
<wire x1="4.497209375" y1="4.71303125" x2="4.502059375" y2="4.7087" width="0.05" layer="21"/>
<wire x1="4.502059375" y1="4.7087" x2="4.506259375" y2="4.703459375" width="0.05" layer="21"/>
<wire x1="4.506259375" y1="4.703459375" x2="4.509809375" y2="4.697390625" width="0.05" layer="21"/>
<wire x1="4.509809375" y1="4.697390625" x2="4.5127" y2="4.690559375" width="0.05" layer="21"/>
<wire x1="4.5127" y1="4.690559375" x2="4.514909375" y2="4.683059375" width="0.05" layer="21"/>
<wire x1="4.514909375" y1="4.683059375" x2="4.516440625" y2="4.67495" width="0.05" layer="21"/>
<wire x1="4.516440625" y1="4.67495" x2="4.5174" y2="4.65723125" width="0.05" layer="21"/>
<wire x1="4.5174" y1="4.65723125" x2="4.51548125" y2="4.638009375" width="0.05" layer="21"/>
<wire x1="4.51548125" y1="4.638009375" x2="4.51061875" y2="4.617890625" width="0.05" layer="21"/>
<wire x1="4.51061875" y1="4.617890625" x2="4.502709375" y2="4.597490625" width="0.05" layer="21"/>
<wire x1="4.502709375" y1="4.597490625" x2="4.49916875" y2="4.59136875" width="0.05" layer="21"/>
<wire x1="4.49916875" y1="4.59136875" x2="4.49255" y2="4.58245" width="0.05" layer="21"/>
<wire x1="4.49255" y1="4.58245" x2="4.465590625" y2="4.551690625" width="0.05" layer="21"/>
<wire x1="4.465590625" y1="4.551690625" x2="4.412859375" y2="4.4961" width="0.05" layer="21"/>
<wire x1="4.412859375" y1="4.4961" x2="4.32536875" y2="4.406559375" width="0.05" layer="21"/>
<wire x1="4.32536875" y1="4.406559375" x2="4.01025" y2="4.08913125" width="0.05" layer="21"/>
<wire x1="4.01025" y1="4.08913125" x2="3.448390625" y2="3.526459375" width="0.05" layer="21"/>
<wire x1="3.448390625" y1="3.526459375" x2="2.40801875" y2="2.48543125" width="0.05" layer="21"/>
<wire x1="2.40801875" y1="2.48543125" x2="2.45791875" y2="2.433959375" width="0.05" layer="21"/>
<wire x1="2.45791875" y1="2.433959375" x2="2.491609375" y2="2.39783125" width="0.05" layer="21"/>
<wire x1="2.491609375" y1="2.39783125" x2="2.52345" y2="2.36088125" width="0.05" layer="21"/>
<wire x1="2.52345" y1="2.36088125" x2="2.55341875" y2="2.323140625" width="0.05" layer="21"/>
<wire x1="2.55341875" y1="2.323140625" x2="2.581490625" y2="2.284659375" width="0.05" layer="21"/>
<wire x1="2.581490625" y1="2.284659375" x2="2.607659375" y2="2.245459375" width="0.05" layer="21"/>
<wire x1="2.607659375" y1="2.245459375" x2="2.631909375" y2="2.205590625" width="0.05" layer="21"/>
<wire x1="2.631909375" y1="2.205590625" x2="2.654209375" y2="2.16508125" width="0.05" layer="21"/>
<wire x1="2.654209375" y1="2.16508125" x2="2.674559375" y2="2.123959375" width="0.05" layer="21"/>
<wire x1="2.674559375" y1="2.123959375" x2="2.69293125" y2="2.08226875" width="0.05" layer="21"/>
<wire x1="2.69293125" y1="2.08226875" x2="2.70931875" y2="2.04005" width="0.05" layer="21"/>
<wire x1="2.70931875" y1="2.04005" x2="2.7237" y2="1.99733125" width="0.05" layer="21"/>
<wire x1="2.7237" y1="1.99733125" x2="2.73605" y2="1.95415" width="0.05" layer="21"/>
<wire x1="2.73605" y1="1.95415" x2="2.74636875" y2="1.910540625" width="0.05" layer="21"/>
<wire x1="2.74636875" y1="1.910540625" x2="2.75463125" y2="1.86653125" width="0.05" layer="21"/>
<wire x1="2.75463125" y1="1.86653125" x2="2.76081875" y2="1.82216875" width="0.05" layer="21"/>
<wire x1="2.76081875" y1="1.82216875" x2="2.764909375" y2="1.777490625" width="0.05" layer="21"/>
<wire x1="2.764909375" y1="1.777490625" x2="2.7669" y2="1.73306875" width="0.05" layer="21"/>
<wire x1="2.7669" y1="1.73306875" x2="2.76685" y2="1.688509375" width="0.05" layer="21"/>
<wire x1="2.76685" y1="1.688509375" x2="2.764790625" y2="1.64401875" width="0.05" layer="21"/>
<wire x1="2.764790625" y1="1.64401875" x2="2.76075" y2="1.599790625" width="0.05" layer="21"/>
<wire x1="2.76075" y1="1.599790625" x2="2.754740625" y2="1.556040625" width="0.05" layer="21"/>
<wire x1="2.754740625" y1="1.556040625" x2="2.746790625" y2="1.512959375" width="0.05" layer="21"/>
<wire x1="2.746790625" y1="1.512959375" x2="2.73691875" y2="1.470759375" width="0.05" layer="21"/>
<wire x1="2.73691875" y1="1.470759375" x2="2.72515" y2="1.42961875" width="0.05" layer="21"/>
<wire x1="2.72515" y1="1.42961875" x2="2.71085" y2="1.384" width="0.05" layer="21"/>
<wire x1="2.71085" y1="1.384" x2="2.72575" y2="1.35871875" width="0.05" layer="21"/>
<wire x1="2.72575" y1="1.35871875" x2="2.7438" y2="1.326490625" width="0.05" layer="21"/>
<wire x1="2.7438" y1="1.326490625" x2="2.760440625" y2="1.293659375" width="0.05" layer="21"/>
<wire x1="2.760440625" y1="1.293659375" x2="2.77466875" y2="1.2623" width="0.05" layer="21"/>
<wire x1="2.77466875" y1="1.2623" x2="2.78551875" y2="1.234440625" width="0.05" layer="21"/>
<wire x1="2.78551875" y1="1.234440625" x2="2.79123125" y2="1.21483125" width="0.05" layer="21"/>
<wire x1="2.79123125" y1="1.21483125" x2="2.795059375" y2="1.192" width="0.05" layer="21"/>
<wire x1="2.795059375" y1="1.192" x2="2.79763125" y2="1.16066875" width="0.05" layer="21"/>
<wire x1="2.79763125" y1="1.16066875" x2="2.799559375" y2="1.11556875" width="0.05" layer="21"/>
<wire x1="2.799559375" y1="1.11556875" x2="2.8025" y2="1.02865" width="0.05" layer="21"/>
<wire x1="2.8025" y1="1.02865" x2="3.18" y2="1.40273125" width="0.05" layer="21"/>
<wire x1="3.18" y1="1.40273125" x2="3.6205" y2="1.839209375" width="0.05" layer="21"/>
<wire x1="3.6205" y1="1.839209375" x2="3.74843125" y2="1.965509375" width="0.05" layer="21"/>
<wire x1="3.74843125" y1="1.965509375" x2="3.83023125" y2="2.04535" width="0.05" layer="21"/>
<wire x1="3.83023125" y1="2.04535" x2="3.87741875" y2="2.08986875" width="0.05" layer="21"/>
<wire x1="3.87741875" y1="2.08986875" x2="3.891659375" y2="2.10236875" width="0.05" layer="21"/>
<wire x1="3.891659375" y1="2.10236875" x2="3.90156875" y2="2.110209375" width="0.05" layer="21"/>
<wire x1="3.90156875" y1="2.110209375" x2="3.908609375" y2="2.11478125" width="0.05" layer="21"/>
<wire x1="3.908609375" y1="2.11478125" x2="3.91421875" y2="2.117490625" width="0.05" layer="21"/>
<wire x1="3.91421875" y1="2.117490625" x2="3.926909375" y2="2.122859375" width="0.05" layer="21"/>
<wire x1="3.926909375" y1="2.122859375" x2="3.94783125" y2="2.1325" width="0.05" layer="21"/>
<wire x1="3.94783125" y1="2.1325" x2="3.964440625" y2="2.13843125" width="0.05" layer="21"/>
<wire x1="3.964440625" y1="2.13843125" x2="3.979559375" y2="2.141440625" width="0.05" layer="21"/>
<wire x1="3.979559375" y1="2.141440625" x2="3.996059375" y2="2.14233125" width="0.05" layer="21"/>
<wire x1="3.996059375" y1="2.14233125" x2="4.01275" y2="2.14198125" width="0.05" layer="21"/>
<wire x1="4.01275" y1="2.14198125" x2="4.0187" y2="2.14123125" width="0.05" layer="21"/>
<wire x1="4.0187" y1="2.14123125" x2="4.0236" y2="2.13988125" width="0.05" layer="21"/>
<wire x1="4.0236" y1="2.13988125" x2="4.02786875" y2="2.137790625" width="0.05" layer="21"/>
<wire x1="4.02786875" y1="2.137790625" x2="4.03191875" y2="2.134790625" width="0.05" layer="21"/>
<wire x1="4.03191875" y1="2.134790625" x2="4.04103125" y2="2.12546875" width="0.05" layer="21"/>
<wire x1="4.04103125" y1="2.12546875" x2="4.04938125" y2="2.115309375" width="0.05" layer="21"/>
<wire x1="4.04938125" y1="2.115309375" x2="4.051959375" y2="2.11091875" width="0.05" layer="21"/>
<wire x1="4.051959375" y1="2.11091875" x2="4.05363125" y2="2.10636875" width="0.05" layer="21"/>
<wire x1="4.05363125" y1="2.10636875" x2="4.05453125" y2="2.101209375" width="0.05" layer="21"/>
<wire x1="4.05453125" y1="2.101209375" x2="4.05476875" y2="2.09496875" width="0.05" layer="21"/>
<wire x1="4.05476875" y1="2.09496875" x2="4.053759375" y2="2.0774" width="0.05" layer="21"/>
<wire x1="4.053759375" y1="2.0774" x2="4.051590625" y2="2.060990625" width="0.05" layer="21"/>
<wire x1="4.051590625" y1="2.060990625" x2="4.047559375" y2="2.04503125" width="0.05" layer="21"/>
<wire x1="4.047559375" y1="2.04503125" x2="4.04126875" y2="2.028259375" width="0.05" layer="21"/>
<wire x1="4.04126875" y1="2.028259375" x2="4.032309375" y2="2.00941875" width="0.05" layer="21"/>
<wire x1="4.032309375" y1="2.00941875" x2="4.02845" y2="2.002540625" width="0.05" layer="21"/>
<wire x1="4.02845" y1="2.002540625" x2="4.022859375" y2="1.994290625" width="0.05" layer="21"/>
<wire x1="4.022859375" y1="1.994290625" x2="4.014459375" y2="1.983559375" width="0.05" layer="21"/>
<wire x1="4.014459375" y1="1.983559375" x2="4.00215" y2="1.969209375" width="0.05" layer="21"/>
<wire x1="4.00215" y1="1.969209375" x2="3.9614" y2="1.92521875" width="0.05" layer="21"/>
<wire x1="3.9614" y1="1.92521875" x2="3.89181875" y2="1.853340625" width="0.05" layer="21"/>
<wire x1="3.89181875" y1="1.853340625" x2="3.6311" y2="1.59001875" width="0.05" layer="21"/>
<wire x1="3.6311" y1="1.59001875" x2="3.14975" y2="1.107490625" width="0.05" layer="21"/>
<wire x1="3.14975" y1="1.107490625" x2="2.286140625" y2="0.242490625" width="0.05" layer="21"/>
<wire x1="2.286140625" y1="0.242490625" x2="2.24133125" y2="0.174990625" width="0.05" layer="21"/>
<wire x1="2.24133125" y1="0.174990625" x2="2.203209375" y2="0.11675" width="0.05" layer="21"/>
<wire x1="2.203209375" y1="0.11675" x2="2.16528125" y2="0.057159375" width="0.05" layer="21"/>
<wire x1="2.16528125" y1="0.057159375" x2="2.12783125" y2="-0.00328125" width="0.05" layer="21"/>
<wire x1="2.12783125" y1="-0.00328125" x2="2.09118125" y2="-0.064040625" width="0.05" layer="21"/>
<wire x1="2.09118125" y1="-0.064040625" x2="2.055640625" y2="-0.124609375" width="0.05" layer="21"/>
<wire x1="2.055640625" y1="-0.124609375" x2="2.0215" y2="-0.18446875" width="0.05" layer="21"/>
<wire x1="2.0215" y1="-0.18446875" x2="1.98908125" y2="-0.243109375" width="0.05" layer="21"/>
<wire x1="1.98908125" y1="-0.243109375" x2="1.95868125" y2="-0.300009375" width="0.05" layer="21"/>
<wire x1="1.95868125" y1="-0.300009375" x2="1.94023125" y2="-0.33633125" width="0.05" layer="21"/>
<wire x1="1.94023125" y1="-0.33633125" x2="1.92753125" y2="-0.36438125" width="0.05" layer="21"/>
<wire x1="1.92753125" y1="-0.36438125" x2="1.919409375" y2="-0.38701875" width="0.05" layer="21"/>
<wire x1="1.919409375" y1="-0.38701875" x2="1.91468125" y2="-0.407109375" width="0.05" layer="21"/>
<wire x1="1.91468125" y1="-0.407109375" x2="1.912559375" y2="-0.42213125" width="0.05" layer="21"/>
<wire x1="1.912559375" y1="-0.42213125" x2="1.911309375" y2="-0.43758125" width="0.05" layer="21"/>
<wire x1="1.911309375" y1="-0.43758125" x2="1.91091875" y2="-0.45336875" width="0.05" layer="21"/>
<wire x1="1.91091875" y1="-0.45336875" x2="1.911359375" y2="-0.469440625" width="0.05" layer="21"/>
<wire x1="1.911359375" y1="-0.469440625" x2="1.91263125" y2="-0.48571875" width="0.05" layer="21"/>
<wire x1="1.91263125" y1="-0.48571875" x2="1.9147" y2="-0.502140625" width="0.05" layer="21"/>
<wire x1="1.9147" y1="-0.502140625" x2="1.917559375" y2="-0.51861875" width="0.05" layer="21"/>
<wire x1="1.917559375" y1="-0.51861875" x2="1.921190625" y2="-0.535109375" width="0.05" layer="21"/>
<wire x1="1.921190625" y1="-0.535109375" x2="1.9307" y2="-0.567809375" width="0.05" layer="21"/>
<wire x1="1.9307" y1="-0.567809375" x2="1.936540625" y2="-0.58388125" width="0.05" layer="21"/>
<wire x1="1.936540625" y1="-0.58388125" x2="1.94308125" y2="-0.59966875" width="0.05" layer="21"/>
<wire x1="1.94308125" y1="-0.59966875" x2="1.950309375" y2="-0.615109375" width="0.05" layer="21"/>
<wire x1="1.950309375" y1="-0.615109375" x2="1.958209375" y2="-0.630140625" width="0.05" layer="21"/>
<wire x1="1.958209375" y1="-0.630140625" x2="1.966759375" y2="-0.64466875" width="0.05" layer="21"/>
<wire x1="1.966759375" y1="-0.64466875" x2="1.97595" y2="-0.65865" width="0.05" layer="21"/>
<wire x1="1.97595" y1="-0.65865" x2="1.992509375" y2="-0.682509375" width="0.05" layer="21"/>
<wire x1="1.992509375" y1="-0.682509375" x2="1.99191875" y2="-0.752509375" width="0.05" layer="21"/>
<wire x1="1.99191875" y1="-0.752509375" x2="1.99115" y2="-0.79038125" width="0.05" layer="21"/>
<wire x1="1.99115" y1="-0.79038125" x2="1.989090625" y2="-0.81786875" width="0.05" layer="21"/>
<wire x1="1.989090625" y1="-0.81786875" x2="1.985009375" y2="-0.84116875" width="0.05" layer="21"/>
<wire x1="1.985009375" y1="-0.84116875" x2="1.978140625" y2="-0.86645" width="0.05" layer="21"/>
<wire x1="1.978140625" y1="-0.86645" x2="1.972190625" y2="-0.88488125" width="0.05" layer="21"/>
<wire x1="1.972190625" y1="-0.88488125" x2="1.965390625" y2="-0.90341875" width="0.05" layer="21"/>
<wire x1="1.965390625" y1="-0.90341875" x2="1.94926875" y2="-0.94075" width="0.05" layer="21"/>
<wire x1="1.94926875" y1="-0.94075" x2="1.92985" y2="-0.978309375" width="0.05" layer="21"/>
<wire x1="1.92985" y1="-0.978309375" x2="1.90718125" y2="-1.015940625" width="0.05" layer="21"/>
<wire x1="1.90718125" y1="-1.015940625" x2="1.85711875" y2="-1.08868125" width="0.05" layer="21"/>
<wire x1="1.85711875" y1="-1.08868125" x2="1.775740625" y2="-1.203190625" width="0.05" layer="21"/>
<wire x1="1.775740625" y1="-1.203190625" x2="1.691259375" y2="-1.32005" width="0.05" layer="21"/>
<wire x1="1.691259375" y1="-1.32005" x2="1.63195" y2="-1.39986875" width="0.05" layer="21"/>
<wire x1="1.63195" y1="-1.39986875" x2="1.60905" y2="-1.428759375" width="0.05" layer="21"/>
<wire x1="1.60905" y1="-1.428759375" x2="1.586859375" y2="-1.45526875" width="0.05" layer="21"/>
<wire x1="1.586859375" y1="-1.45526875" x2="1.5651" y2="-1.479559375" width="0.05" layer="21"/>
<wire x1="1.5651" y1="-1.479559375" x2="1.543509375" y2="-1.50181875" width="0.05" layer="21"/>
<wire x1="1.543509375" y1="-1.50181875" x2="1.521809375" y2="-1.522209375" width="0.05" layer="21"/>
<wire x1="1.521809375" y1="-1.522209375" x2="1.49973125" y2="-1.540909375" width="0.05" layer="21"/>
<wire x1="1.49973125" y1="-1.540909375" x2="1.47698125" y2="-1.55808125" width="0.05" layer="21"/>
<wire x1="1.47698125" y1="-1.55808125" x2="1.453309375" y2="-1.5739" width="0.05" layer="21"/>
<wire x1="1.453309375" y1="-1.5739" x2="1.42843125" y2="-1.588540625" width="0.05" layer="21"/>
<wire x1="1.42843125" y1="-1.588540625" x2="1.40206875" y2="-1.60216875" width="0.05" layer="21"/>
<wire x1="1.40206875" y1="-1.60216875" x2="1.373959375" y2="-1.61496875" width="0.05" layer="21"/>
<wire x1="1.373959375" y1="-1.61496875" x2="1.34381875" y2="-1.6271" width="0.05" layer="21"/>
<wire x1="1.34381875" y1="-1.6271" x2="1.31138125" y2="-1.63873125" width="0.05" layer="21"/>
<wire x1="1.31138125" y1="-1.63873125" x2="1.276359375" y2="-1.65005" width="0.05" layer="21"/>
<wire x1="1.276359375" y1="-1.65005" x2="1.238490625" y2="-1.66121875" width="0.05" layer="21"/>
<wire x1="1.238490625" y1="-1.66121875" x2="1.197509375" y2="-1.672409375" width="0.05" layer="21"/>
<wire x1="1.197509375" y1="-1.672409375" x2="1.12706875" y2="-1.689859375" width="0.05" layer="21"/>
<wire x1="1.12706875" y1="-1.689859375" x2="1.05818125" y2="-1.70448125" width="0.05" layer="21"/>
<wire x1="1.05818125" y1="-1.70448125" x2="0.989359375" y2="-1.71641875" width="0.05" layer="21"/>
<wire x1="0.989359375" y1="-1.71641875" x2="0.91915" y2="-1.72586875" width="0.05" layer="21"/>
<wire x1="0.91915" y1="-1.72586875" x2="0.84608125" y2="-1.732990625" width="0.05" layer="21"/>
<wire x1="0.84608125" y1="-1.732990625" x2="0.76866875" y2="-1.737940625" width="0.05" layer="21"/>
<wire x1="0.76866875" y1="-1.737940625" x2="0.68546875" y2="-1.7409" width="0.05" layer="21"/>
<wire x1="0.68546875" y1="-1.7409" x2="0.595009375" y2="-1.742040625" width="0.05" layer="21"/>
<wire x1="0.595009375" y1="-1.742040625" x2="0.49153125" y2="-1.74146875" width="0.05" layer="21"/>
<wire x1="0.49153125" y1="-1.74146875" x2="0.4543" y2="-1.74006875" width="0.05" layer="21"/>
<wire x1="0.4543" y1="-1.74006875" x2="0.42358125" y2="-1.73758125" width="0.05" layer="21"/>
<wire x1="0.42358125" y1="-1.73758125" x2="0.396959375" y2="-1.73371875" width="0.05" layer="21"/>
<wire x1="0.396959375" y1="-1.73371875" x2="0.372090625" y2="-1.72825" width="0.05" layer="21"/>
<wire x1="0.372090625" y1="-1.72825" x2="0.34658125" y2="-1.720890625" width="0.05" layer="21"/>
<wire x1="0.34658125" y1="-1.720890625" x2="0.318040625" y2="-1.71138125" width="0.05" layer="21"/>
<wire x1="0.318040625" y1="-1.71138125" x2="0.29023125" y2="-1.7024" width="0.05" layer="21"/>
<wire x1="0.29023125" y1="-1.7024" x2="0.26666875" y2="-1.696709375" width="0.05" layer="21"/>
<wire x1="0.26666875" y1="-1.696709375" x2="0.24241875" y2="-1.693359375" width="0.05" layer="21"/>
<wire x1="0.24241875" y1="-1.693359375" x2="0.212509375" y2="-1.69143125" width="0.05" layer="21"/>
<wire x1="0.212509375" y1="-1.69143125" x2="0.18218125" y2="-1.690940625" width="0.05" layer="21"/>
<wire x1="0.18218125" y1="-1.690940625" x2="0.154809375" y2="-1.692390625" width="0.05" layer="21"/>
<wire x1="0.154809375" y1="-1.692390625" x2="0.130090625" y2="-1.6959" width="0.05" layer="21"/>
<wire x1="0.130090625" y1="-1.6959" x2="0.1077" y2="-1.70155" width="0.05" layer="21"/>
<wire x1="0.1077" y1="-1.70155" x2="0.08731875" y2="-1.709459375" width="0.05" layer="21"/>
<wire x1="0.08731875" y1="-1.709459375" x2="0.06861875" y2="-1.71971875" width="0.05" layer="21"/>
<wire x1="0.06861875" y1="-1.71971875" x2="0.051290625" y2="-1.73243125" width="0.05" layer="21"/>
<wire x1="0.051290625" y1="-1.73243125" x2="0.035009375" y2="-1.7477" width="0.05" layer="21"/>
<wire x1="0.035009375" y1="-1.7477" x2="0.020759375" y2="-1.76188125" width="0.05" layer="21"/>
<wire x1="0.020759375" y1="-1.76188125" x2="0.00388125" y2="-1.777509375" width="0.05" layer="21"/>
<wire x1="0.00388125" y1="-1.777509375" x2="-0.03511875" y2="-1.810909375" width="0.05" layer="21"/>
<wire x1="-0.03511875" y1="-1.810909375" x2="-0.076640625" y2="-1.843509375" width="0.05" layer="21"/>
<wire x1="-0.076640625" y1="-1.843509375" x2="-0.11533125" y2="-1.87093125" width="0.05" layer="21"/>
<wire x1="-0.11533125" y1="-1.87093125" x2="-0.14305" y2="-1.888390625" width="0.05" layer="21"/>
<wire x1="-0.14305" y1="-1.888390625" x2="-0.17158125" y2="-1.904840625" width="0.05" layer="21"/>
<wire x1="-0.17158125" y1="-1.904840625" x2="-0.20088125" y2="-1.92025" width="0.05" layer="21"/>
<wire x1="-0.20088125" y1="-1.92025" x2="-0.2309" y2="-1.93461875" width="0.05" layer="21"/>
<wire x1="-0.2309" y1="-1.93461875" x2="-0.26156875" y2="-1.94793125" width="0.05" layer="21"/>
<wire x1="-0.26156875" y1="-1.94793125" x2="-0.292859375" y2="-1.960159375" width="0.05" layer="21"/>
<wire x1="-0.292859375" y1="-1.960159375" x2="-0.324690625" y2="-1.971309375" width="0.05" layer="21"/>
<wire x1="-0.324690625" y1="-1.971309375" x2="-0.35703125" y2="-1.981340625" width="0.05" layer="21"/>
<wire x1="-0.35703125" y1="-1.981340625" x2="-0.38981875" y2="-1.990259375" width="0.05" layer="21"/>
<wire x1="-0.38981875" y1="-1.990259375" x2="-0.423009375" y2="-1.99803125" width="0.05" layer="21"/>
<wire x1="-0.423009375" y1="-1.99803125" x2="-0.456540625" y2="-2.00465" width="0.05" layer="21"/>
<wire x1="-0.456540625" y1="-2.00465" x2="-0.490359375" y2="-2.010109375" width="0.05" layer="21"/>
<wire x1="-0.490359375" y1="-2.010109375" x2="-0.52441875" y2="-2.01438125" width="0.05" layer="21"/>
<wire x1="-0.52441875" y1="-2.01438125" x2="-0.558659375" y2="-2.01745" width="0.05" layer="21"/>
<wire x1="-0.558659375" y1="-2.01745" x2="-0.593040625" y2="-2.019309375" width="0.05" layer="21"/>
<wire x1="-0.593040625" y1="-2.019309375" x2="-0.627490625" y2="-2.019940625" width="0.05" layer="21"/>
<wire x1="-0.627490625" y1="-2.019940625" x2="-0.65436875" y2="-2.01945" width="0.05" layer="21"/>
<wire x1="-0.65436875" y1="-2.01945" x2="-0.68098125" y2="-2.01793125" width="0.05" layer="21"/>
<wire x1="-0.68098125" y1="-2.01793125" x2="-0.70783125" y2="-2.0153" width="0.05" layer="21"/>
<wire x1="-0.70783125" y1="-2.0153" x2="-0.735490625" y2="-2.01146875" width="0.05" layer="21"/>
<wire x1="-0.735490625" y1="-2.01146875" x2="-0.76448125" y2="-2.00635" width="0.05" layer="21"/>
<wire x1="-0.76448125" y1="-2.00635" x2="-0.795340625" y2="-1.99985" width="0.05" layer="21"/>
<wire x1="-0.795340625" y1="-1.99985" x2="-0.82861875" y2="-1.9919" width="0.05" layer="21"/>
<wire x1="-0.82861875" y1="-1.9919" x2="-0.86485" y2="-1.9824" width="0.05" layer="21"/>
<wire x1="-0.86485" y1="-1.9824" x2="-0.93226875" y2="-1.96501875" width="0.05" layer="21"/>
<wire x1="-0.93226875" y1="-1.96501875" x2="-0.977709375" y2="-1.954940625" width="0.05" layer="21"/>
<wire x1="-0.977709375" y1="-1.954940625" x2="-1.00705" y2="-1.9514" width="0.05" layer="21"/>
<wire x1="-1.00705" y1="-1.9514" x2="-1.03815" y2="-1.95003125" width="0.05" layer="21"/>
<wire x1="-1.03815" y1="-1.95003125" x2="-1.07083125" y2="-1.95078125" width="0.05" layer="21"/>
<wire x1="-1.07083125" y1="-1.95078125" x2="-1.1049" y2="-1.953590625" width="0.05" layer="21"/>
<wire x1="-1.1049" y1="-1.953590625" x2="-1.14018125" y2="-1.958409375" width="0.05" layer="21"/>
<wire x1="-1.14018125" y1="-1.958409375" x2="-1.17648125" y2="-1.96518125" width="0.05" layer="21"/>
<wire x1="-1.17648125" y1="-1.96518125" x2="-1.21361875" y2="-1.97385" width="0.05" layer="21"/>
<wire x1="-1.21361875" y1="-1.97385" x2="-1.251409375" y2="-1.984359375" width="0.05" layer="21"/>
<wire x1="-1.251409375" y1="-1.984359375" x2="-1.28966875" y2="-1.996659375" width="0.05" layer="21"/>
<wire x1="-1.28966875" y1="-1.996659375" x2="-1.328209375" y2="-2.01068125" width="0.05" layer="21"/>
<wire x1="-1.328209375" y1="-2.01068125" x2="-1.366840625" y2="-2.026390625" width="0.05" layer="21"/>
<wire x1="-1.366840625" y1="-2.026390625" x2="-1.405390625" y2="-2.043709375" width="0.05" layer="21"/>
<wire x1="-1.405390625" y1="-2.043709375" x2="-1.443659375" y2="-2.062590625" width="0.05" layer="21"/>
<wire x1="-1.443659375" y1="-2.062590625" x2="-1.48148125" y2="-2.08298125" width="0.05" layer="21"/>
<wire x1="-1.48148125" y1="-2.08298125" x2="-1.51865" y2="-2.10483125" width="0.05" layer="21"/>
<wire x1="-1.51865" y1="-2.10483125" x2="-1.554990625" y2="-2.12806875" width="0.05" layer="21"/>
<wire x1="-1.554990625" y1="-2.12806875" x2="-1.591659375" y2="-2.151490625" width="0.05" layer="21"/>
<wire x1="-1.591659375" y1="-2.151490625" x2="-1.62588125" y2="-2.17103125" width="0.05" layer="21"/>
<wire x1="-1.62588125" y1="-2.17103125" x2="-1.65846875" y2="-2.18696875" width="0.05" layer="21"/>
<wire x1="-1.65846875" y1="-2.18696875" x2="-1.69021875" y2="-2.199590625" width="0.05" layer="21"/>
<wire x1="-1.69021875" y1="-2.199590625" x2="-1.72195" y2="-2.209190625" width="0.05" layer="21"/>
<wire x1="-1.72195" y1="-2.209190625" x2="-1.75445" y2="-2.21603125" width="0.05" layer="21"/>
<wire x1="-1.75445" y1="-2.21603125" x2="-1.78853125" y2="-2.2204" width="0.05" layer="21"/>
<wire x1="-1.78853125" y1="-2.2204" x2="-1.824990625" y2="-2.222590625" width="0.05" layer="21"/>
<wire x1="-1.824990625" y1="-2.222590625" x2="-1.875809375" y2="-2.22273125" width="0.05" layer="21"/>
<wire x1="-1.875809375" y1="-2.22273125" x2="-1.89645" y2="-2.221809375" width="0.05" layer="21"/>
<wire x1="-1.89645" y1="-2.221809375" x2="-1.9102" y2="-2.22033125" width="0.05" layer="21"/>
<wire x1="-1.9102" y1="-2.22033125" x2="-1.910209375" y2="-2.22035" width="0.05" layer="21"/>
<wire x1="-1.82" y1="-1.40215" x2="-1.801459375" y2="-1.39746875" width="0.05" layer="21"/>
<wire x1="-1.801459375" y1="-1.39746875" x2="-1.782890625" y2="-1.39195" width="0.05" layer="21"/>
<wire x1="-1.782890625" y1="-1.39195" x2="-1.764309375" y2="-1.3856" width="0.05" layer="21"/>
<wire x1="-1.764309375" y1="-1.3856" x2="-1.74575" y2="-1.378440625" width="0.05" layer="21"/>
<wire x1="-1.74575" y1="-1.378440625" x2="-1.70873125" y2="-1.361759375" width="0.05" layer="21"/>
<wire x1="-1.70873125" y1="-1.361759375" x2="-1.671959375" y2="-1.342040625" width="0.05" layer="21"/>
<wire x1="-1.671959375" y1="-1.342040625" x2="-1.63558125" y2="-1.319409375" width="0.05" layer="21"/>
<wire x1="-1.63558125" y1="-1.319409375" x2="-1.599740625" y2="-1.294" width="0.05" layer="21"/>
<wire x1="-1.599740625" y1="-1.294" x2="-1.564559375" y2="-1.26595" width="0.05" layer="21"/>
<wire x1="-1.564559375" y1="-1.26595" x2="-1.530209375" y2="-1.23536875" width="0.05" layer="21"/>
<wire x1="-1.530209375" y1="-1.23536875" x2="-1.4968" y2="-1.20241875" width="0.05" layer="21"/>
<wire x1="-1.4968" y1="-1.20241875" x2="-1.464490625" y2="-1.1672" width="0.05" layer="21"/>
<wire x1="-1.464490625" y1="-1.1672" x2="-1.433409375" y2="-1.12986875" width="0.05" layer="21"/>
<wire x1="-1.433409375" y1="-1.12986875" x2="-1.4037" y2="-1.090540625" width="0.05" layer="21"/>
<wire x1="-1.4037" y1="-1.090540625" x2="-1.3755" y2="-1.049340625" width="0.05" layer="21"/>
<wire x1="-1.3755" y1="-1.049340625" x2="-1.34895" y2="-1.00641875" width="0.05" layer="21"/>
<wire x1="-1.34895" y1="-1.00641875" x2="-1.3242" y2="-0.9619" width="0.05" layer="21"/>
<wire x1="-1.3242" y1="-0.9619" x2="-1.30136875" y2="-0.915909375" width="0.05" layer="21"/>
<wire x1="-1.30136875" y1="-0.915909375" x2="-1.290490625" y2="-0.891309375" width="0.05" layer="21"/>
<wire x1="-1.290490625" y1="-0.891309375" x2="-1.27938125" y2="-0.863740625" width="0.05" layer="21"/>
<wire x1="-1.27938125" y1="-0.863740625" x2="-1.26833125" y2="-0.83408125" width="0.05" layer="21"/>
<wire x1="-1.26833125" y1="-0.83408125" x2="-1.25763125" y2="-0.8032" width="0.05" layer="21"/>
<wire x1="-1.25763125" y1="-0.8032" x2="-1.23846875" y2="-0.741290625" width="0.05" layer="21"/>
<wire x1="-1.23846875" y1="-0.741290625" x2="-1.23058125" y2="-0.712009375" width="0.05" layer="21"/>
<wire x1="-1.23058125" y1="-0.712009375" x2="-1.22421875" y2="-0.68501875" width="0.05" layer="21"/>
<wire x1="-1.22421875" y1="-0.68501875" x2="-1.218359375" y2="-0.65398125" width="0.05" layer="21"/>
<wire x1="-1.218359375" y1="-0.65398125" x2="-1.2149" y2="-0.62271875" width="0.05" layer="21"/>
<wire x1="-1.2149" y1="-0.62271875" x2="-1.21321875" y2="-0.582609375" width="0.05" layer="21"/>
<wire x1="-1.21321875" y1="-0.582609375" x2="-1.212709375" y2="-0.52501875" width="0.05" layer="21"/>
<wire x1="-1.212709375" y1="-0.52501875" x2="-1.21295" y2="-0.4661" width="0.05" layer="21"/>
<wire x1="-1.21295" y1="-0.4661" x2="-1.21443125" y2="-0.427690625" width="0.05" layer="21"/>
<wire x1="-1.21443125" y1="-0.427690625" x2="-1.21586875" y2="-0.413059375" width="0.05" layer="21"/>
<wire x1="-1.21586875" y1="-0.413059375" x2="-1.2179" y2="-0.39981875" width="0.05" layer="21"/>
<wire x1="-1.2179" y1="-0.39981875" x2="-1.22411875" y2="-0.37251875" width="0.05" layer="21"/>
<wire x1="-1.22411875" y1="-0.37251875" x2="-1.23063125" y2="-0.3509" width="0.05" layer="21"/>
<wire x1="-1.23063125" y1="-0.3509" x2="-1.239759375" y2="-0.325309375" width="0.05" layer="21"/>
<wire x1="-1.239759375" y1="-0.325309375" x2="-1.2503" y2="-0.29895" width="0.05" layer="21"/>
<wire x1="-1.2503" y1="-0.29895" x2="-1.26103125" y2="-0.27501875" width="0.05" layer="21"/>
<wire x1="-1.26103125" y1="-0.27501875" x2="-1.27556875" y2="-0.24641875" width="0.05" layer="21"/>
<wire x1="-1.27556875" y1="-0.24641875" x2="-1.28906875" y2="-0.22466875" width="0.05" layer="21"/>
<wire x1="-1.28906875" y1="-0.22466875" x2="-1.30501875" y2="-0.204859375" width="0.05" layer="21"/>
<wire x1="-1.30501875" y1="-0.204859375" x2="-1.326909375" y2="-0.18205" width="0.05" layer="21"/>
<wire x1="-1.326909375" y1="-0.18205" x2="-1.33971875" y2="-0.16981875" width="0.05" layer="21"/>
<wire x1="-1.33971875" y1="-0.16981875" x2="-1.3527" y2="-0.158509375" width="0.05" layer="21"/>
<wire x1="-1.3527" y1="-0.158509375" x2="-1.36586875" y2="-0.148109375" width="0.05" layer="21"/>
<wire x1="-1.36586875" y1="-0.148109375" x2="-1.379259375" y2="-0.138609375" width="0.05" layer="21"/>
<wire x1="-1.379259375" y1="-0.138609375" x2="-1.392890625" y2="-0.13" width="0.05" layer="21"/>
<wire x1="-1.392890625" y1="-0.13" x2="-1.406790625" y2="-0.12228125" width="0.05" layer="21"/>
<wire x1="-1.406790625" y1="-0.12228125" x2="-1.42096875" y2="-0.11543125" width="0.05" layer="21"/>
<wire x1="-1.42096875" y1="-0.11543125" x2="-1.43545" y2="-0.109440625" width="0.05" layer="21"/>
<wire x1="-1.43545" y1="-0.109440625" x2="-1.45026875" y2="-0.10431875" width="0.05" layer="21"/>
<wire x1="-1.45026875" y1="-0.10431875" x2="-1.46543125" y2="-0.100040625" width="0.05" layer="21"/>
<wire x1="-1.46543125" y1="-0.100040625" x2="-1.48096875" y2="-0.096609375" width="0.05" layer="21"/>
<wire x1="-1.48096875" y1="-0.096609375" x2="-1.4969" y2="-0.094009375" width="0.05" layer="21"/>
<wire x1="-1.4969" y1="-0.094009375" x2="-1.513240625" y2="-0.09223125" width="0.05" layer="21"/>
<wire x1="-1.513240625" y1="-0.09223125" x2="-1.53003125" y2="-0.09126875" width="0.05" layer="21"/>
<wire x1="-1.53003125" y1="-0.09126875" x2="-1.54726875" y2="-0.09111875" width="0.05" layer="21"/>
<wire x1="-1.54726875" y1="-0.09111875" x2="-1.565" y2="-0.09176875" width="0.05" layer="21"/>
<wire x1="-1.565" y1="-0.09176875" x2="-1.59163125" y2="-0.09421875" width="0.05" layer="21"/>
<wire x1="-1.59163125" y1="-0.09421875" x2="-1.61831875" y2="-0.098440625" width="0.05" layer="21"/>
<wire x1="-1.61831875" y1="-0.098440625" x2="-1.64501875" y2="-0.1044" width="0.05" layer="21"/>
<wire x1="-1.64501875" y1="-0.1044" x2="-1.671690625" y2="-0.112040625" width="0.05" layer="21"/>
<wire x1="-1.671690625" y1="-0.112040625" x2="-1.69828125" y2="-0.12131875" width="0.05" layer="21"/>
<wire x1="-1.69828125" y1="-0.12131875" x2="-1.724759375" y2="-0.13218125" width="0.05" layer="21"/>
<wire x1="-1.724759375" y1="-0.13218125" x2="-1.75106875" y2="-0.14458125" width="0.05" layer="21"/>
<wire x1="-1.75106875" y1="-0.14458125" x2="-1.77718125" y2="-0.15848125" width="0.05" layer="21"/>
<wire x1="-1.77718125" y1="-0.15848125" x2="-1.803040625" y2="-0.17383125" width="0.05" layer="21"/>
<wire x1="-1.803040625" y1="-0.17383125" x2="-1.828609375" y2="-0.19056875" width="0.05" layer="21"/>
<wire x1="-1.828609375" y1="-0.19056875" x2="-1.85385" y2="-0.20866875" width="0.05" layer="21"/>
<wire x1="-1.85385" y1="-0.20866875" x2="-1.878709375" y2="-0.22806875" width="0.05" layer="21"/>
<wire x1="-1.878709375" y1="-0.22806875" x2="-1.90315" y2="-0.24873125" width="0.05" layer="21"/>
<wire x1="-1.90315" y1="-0.24873125" x2="-1.92711875" y2="-0.2706" width="0.05" layer="21"/>
<wire x1="-1.92711875" y1="-0.2706" x2="-1.950590625" y2="-0.29363125" width="0.05" layer="21"/>
<wire x1="-1.950590625" y1="-0.29363125" x2="-1.973509375" y2="-0.31778125" width="0.05" layer="21"/>
<wire x1="-1.973509375" y1="-0.31778125" x2="-1.99583125" y2="-0.343" width="0.05" layer="21"/>
<wire x1="-1.99583125" y1="-0.343" x2="-2.01751875" y2="-0.36925" width="0.05" layer="21"/>
<wire x1="-2.01751875" y1="-0.36925" x2="-2.03853125" y2="-0.396459375" width="0.05" layer="21"/>
<wire x1="-2.03853125" y1="-0.396459375" x2="-2.058809375" y2="-0.424609375" width="0.05" layer="21"/>
<wire x1="-2.058809375" y1="-0.424609375" x2="-2.07833125" y2="-0.45363125" width="0.05" layer="21"/>
<wire x1="-2.07833125" y1="-0.45363125" x2="-2.097040625" y2="-0.483490625" width="0.05" layer="21"/>
<wire x1="-2.097040625" y1="-0.483490625" x2="-2.114890625" y2="-0.51413125" width="0.05" layer="21"/>
<wire x1="-2.114890625" y1="-0.51413125" x2="-2.13185" y2="-0.54551875" width="0.05" layer="21"/>
<wire x1="-2.13185" y1="-0.54551875" x2="-2.14786875" y2="-0.577590625" width="0.05" layer="21"/>
<wire x1="-2.14786875" y1="-0.577590625" x2="-2.1629" y2="-0.610309375" width="0.05" layer="21"/>
<wire x1="-2.1629" y1="-0.610309375" x2="-2.176909375" y2="-0.64363125" width="0.05" layer="21"/>
<wire x1="-2.176909375" y1="-0.64363125" x2="-2.189859375" y2="-0.677490625" width="0.05" layer="21"/>
<wire x1="-2.189859375" y1="-0.677490625" x2="-2.201690625" y2="-0.711859375" width="0.05" layer="21"/>
<wire x1="-2.201690625" y1="-0.711859375" x2="-2.212359375" y2="-0.746690625" width="0.05" layer="21"/>
<wire x1="-2.212359375" y1="-0.746690625" x2="-2.22183125" y2="-0.78191875" width="0.05" layer="21"/>
<wire x1="-2.22183125" y1="-0.78191875" x2="-2.23006875" y2="-0.81751875" width="0.05" layer="21"/>
<wire x1="-2.23006875" y1="-0.81751875" x2="-2.23531875" y2="-0.847359375" width="0.05" layer="21"/>
<wire x1="-2.23531875" y1="-0.847359375" x2="-2.23941875" y2="-0.88121875" width="0.05" layer="21"/>
<wire x1="-2.23941875" y1="-0.88121875" x2="-2.24233125" y2="-0.917709375" width="0.05" layer="21"/>
<wire x1="-2.24233125" y1="-0.917709375" x2="-2.24401875" y2="-0.95541875" width="0.05" layer="21"/>
<wire x1="-2.24401875" y1="-0.95541875" x2="-2.24443125" y2="-0.99293125" width="0.05" layer="21"/>
<wire x1="-2.24443125" y1="-0.99293125" x2="-2.24355" y2="-1.028859375" width="0.05" layer="21"/>
<wire x1="-2.24355" y1="-1.028859375" x2="-2.241309375" y2="-1.061790625" width="0.05" layer="21"/>
<wire x1="-2.241309375" y1="-1.061790625" x2="-2.2377" y2="-1.090309375" width="0.05" layer="21"/>
<wire x1="-2.2377" y1="-1.090309375" x2="-2.233609375" y2="-1.112240625" width="0.05" layer="21"/>
<wire x1="-2.233609375" y1="-1.112240625" x2="-2.2288" y2="-1.133490625" width="0.05" layer="21"/>
<wire x1="-2.2288" y1="-1.133490625" x2="-2.22328125" y2="-1.15405" width="0.05" layer="21"/>
<wire x1="-2.22328125" y1="-1.15405" x2="-2.21708125" y2="-1.173909375" width="0.05" layer="21"/>
<wire x1="-2.21708125" y1="-1.173909375" x2="-2.210190625" y2="-1.19305" width="0.05" layer="21"/>
<wire x1="-2.210190625" y1="-1.19305" x2="-2.20265" y2="-1.21146875" width="0.05" layer="21"/>
<wire x1="-2.20265" y1="-1.21146875" x2="-2.19445" y2="-1.229159375" width="0.05" layer="21"/>
<wire x1="-2.19445" y1="-1.229159375" x2="-2.18561875" y2="-1.246109375" width="0.05" layer="21"/>
<wire x1="-2.18561875" y1="-1.246109375" x2="-2.176159375" y2="-1.2623" width="0.05" layer="21"/>
<wire x1="-2.176159375" y1="-1.2623" x2="-2.166109375" y2="-1.27773125" width="0.05" layer="21"/>
<wire x1="-2.166109375" y1="-1.27773125" x2="-2.15545" y2="-1.29238125" width="0.05" layer="21"/>
<wire x1="-2.15545" y1="-1.29238125" x2="-2.14421875" y2="-1.306240625" width="0.05" layer="21"/>
<wire x1="-2.14421875" y1="-1.306240625" x2="-2.13243125" y2="-1.319309375" width="0.05" layer="21"/>
<wire x1="-2.13243125" y1="-1.319309375" x2="-2.12008125" y2="-1.33156875" width="0.05" layer="21"/>
<wire x1="-2.12008125" y1="-1.33156875" x2="-2.1072" y2="-1.343009375" width="0.05" layer="21"/>
<wire x1="-2.1072" y1="-1.343009375" x2="-2.0938" y2="-1.35363125" width="0.05" layer="21"/>
<wire x1="-2.0938" y1="-1.35363125" x2="-2.079890625" y2="-1.363409375" width="0.05" layer="21"/>
<wire x1="-2.079890625" y1="-1.363409375" x2="-2.065490625" y2="-1.37233125" width="0.05" layer="21"/>
<wire x1="-2.065490625" y1="-1.37233125" x2="-2.0506" y2="-1.3804" width="0.05" layer="21"/>
<wire x1="-2.0506" y1="-1.3804" x2="-2.035259375" y2="-1.387590625" width="0.05" layer="21"/>
<wire x1="-2.035259375" y1="-1.387590625" x2="-2.019459375" y2="-1.393909375" width="0.05" layer="21"/>
<wire x1="-2.019459375" y1="-1.393909375" x2="-2.00321875" y2="-1.39933125" width="0.05" layer="21"/>
<wire x1="-2.00321875" y1="-1.39933125" x2="-1.98656875" y2="-1.40385" width="0.05" layer="21"/>
<wire x1="-1.98656875" y1="-1.40385" x2="-1.9695" y2="-1.40745" width="0.05" layer="21"/>
<wire x1="-1.9695" y1="-1.40745" x2="-1.95205" y2="-1.41013125" width="0.05" layer="21"/>
<wire x1="-1.95205" y1="-1.41013125" x2="-1.934209375" y2="-1.41188125" width="0.05" layer="21"/>
<wire x1="-1.934209375" y1="-1.41188125" x2="-1.916" y2="-1.41268125" width="0.05" layer="21"/>
<wire x1="-1.916" y1="-1.41268125" x2="-1.89745" y2="-1.41253125" width="0.05" layer="21"/>
<wire x1="-1.89745" y1="-1.41253125" x2="-1.878559375" y2="-1.411409375" width="0.05" layer="21"/>
<wire x1="-1.878559375" y1="-1.411409375" x2="-1.859340625" y2="-1.409309375" width="0.05" layer="21"/>
<wire x1="-1.859340625" y1="-1.409309375" x2="-1.83981875" y2="-1.40623125" width="0.05" layer="21"/>
<wire x1="-1.83981875" y1="-1.40623125" x2="-1.82" y2="-1.40215" width="0.05" layer="21"/>
<wire x1="0.88295" y1="-1.31718125" x2="0.91588125" y2="-1.310409375" width="0.05" layer="21"/>
<wire x1="0.91588125" y1="-1.310409375" x2="0.94978125" y2="-1.3013" width="0.05" layer="21"/>
<wire x1="0.94978125" y1="-1.3013" x2="0.98426875" y2="-1.289990625" width="0.05" layer="21"/>
<wire x1="0.98426875" y1="-1.289990625" x2="1.01896875" y2="-1.276640625" width="0.05" layer="21"/>
<wire x1="1.01896875" y1="-1.276640625" x2="1.05351875" y2="-1.2614" width="0.05" layer="21"/>
<wire x1="1.05351875" y1="-1.2614" x2="1.087540625" y2="-1.244440625" width="0.05" layer="21"/>
<wire x1="1.087540625" y1="-1.244440625" x2="1.120659375" y2="-1.2259" width="0.05" layer="21"/>
<wire x1="1.120659375" y1="-1.2259" x2="1.1525" y2="-1.205940625" width="0.05" layer="21"/>
<wire x1="1.1525" y1="-1.205940625" x2="1.184609375" y2="-1.1835" width="0.05" layer="21"/>
<wire x1="1.184609375" y1="-1.1835" x2="1.21541875" y2="-1.159709375" width="0.05" layer="21"/>
<wire x1="1.21541875" y1="-1.159709375" x2="1.24488125" y2="-1.13461875" width="0.05" layer="21"/>
<wire x1="1.24488125" y1="-1.13461875" x2="1.272940625" y2="-1.1083" width="0.05" layer="21"/>
<wire x1="1.272940625" y1="-1.1083" x2="1.29958125" y2="-1.080809375" width="0.05" layer="21"/>
<wire x1="1.29958125" y1="-1.080809375" x2="1.32473125" y2="-1.05221875" width="0.05" layer="21"/>
<wire x1="1.32473125" y1="-1.05221875" x2="1.34836875" y2="-1.02258125" width="0.05" layer="21"/>
<wire x1="1.34836875" y1="-1.02258125" x2="1.370440625" y2="-0.991959375" width="0.05" layer="21"/>
<wire x1="1.370440625" y1="-0.991959375" x2="1.3909" y2="-0.96041875" width="0.05" layer="21"/>
<wire x1="1.3909" y1="-0.96041875" x2="1.409709375" y2="-0.92803125" width="0.05" layer="21"/>
<wire x1="1.409709375" y1="-0.92803125" x2="1.42683125" y2="-0.89485" width="0.05" layer="21"/>
<wire x1="1.42683125" y1="-0.89485" x2="1.442209375" y2="-0.860940625" width="0.05" layer="21"/>
<wire x1="1.442209375" y1="-0.860940625" x2="1.455809375" y2="-0.82636875" width="0.05" layer="21"/>
<wire x1="1.455809375" y1="-0.82636875" x2="1.467590625" y2="-0.7912" width="0.05" layer="21"/>
<wire x1="1.467590625" y1="-0.7912" x2="1.4775" y2="-0.755490625" width="0.05" layer="21"/>
<wire x1="1.4775" y1="-0.755490625" x2="1.485509375" y2="-0.7193" width="0.05" layer="21"/>
<wire x1="1.485509375" y1="-0.7193" x2="1.488309375" y2="-0.699640625" width="0.05" layer="21"/>
<wire x1="1.488309375" y1="-0.699640625" x2="1.4902" y2="-0.67551875" width="0.05" layer="21"/>
<wire x1="1.4902" y1="-0.67551875" x2="1.49118125" y2="-0.64845" width="0.05" layer="21"/>
<wire x1="1.49118125" y1="-0.64845" x2="1.49128125" y2="-0.61993125" width="0.05" layer="21"/>
<wire x1="1.49128125" y1="-0.61993125" x2="1.490509375" y2="-0.59145" width="0.05" layer="21"/>
<wire x1="1.490509375" y1="-0.59145" x2="1.488890625" y2="-0.56451875" width="0.05" layer="21"/>
<wire x1="1.488890625" y1="-0.56451875" x2="1.486440625" y2="-0.540640625" width="0.05" layer="21"/>
<wire x1="1.486440625" y1="-0.540640625" x2="1.48316875" y2="-0.5213" width="0.05" layer="21"/>
<wire x1="1.48316875" y1="-0.5213" x2="1.47666875" y2="-0.49533125" width="0.05" layer="21"/>
<wire x1="1.47666875" y1="-0.49533125" x2="1.468890625" y2="-0.47023125" width="0.05" layer="21"/>
<wire x1="1.468890625" y1="-0.47023125" x2="1.45986875" y2="-0.44603125" width="0.05" layer="21"/>
<wire x1="1.45986875" y1="-0.44603125" x2="1.449640625" y2="-0.42275" width="0.05" layer="21"/>
<wire x1="1.449640625" y1="-0.42275" x2="1.438240625" y2="-0.4004" width="0.05" layer="21"/>
<wire x1="1.438240625" y1="-0.4004" x2="1.425709375" y2="-0.379009375" width="0.05" layer="21"/>
<wire x1="1.425709375" y1="-0.379009375" x2="1.412090625" y2="-0.35858125" width="0.05" layer="21"/>
<wire x1="1.412090625" y1="-0.35858125" x2="1.3974" y2="-0.33913125" width="0.05" layer="21"/>
<wire x1="1.3974" y1="-0.33913125" x2="1.3817" y2="-0.320690625" width="0.05" layer="21"/>
<wire x1="1.3817" y1="-0.320690625" x2="1.36501875" y2="-0.30326875" width="0.05" layer="21"/>
<wire x1="1.36501875" y1="-0.30326875" x2="1.34738125" y2="-0.286890625" width="0.05" layer="21"/>
<wire x1="1.34738125" y1="-0.286890625" x2="1.328840625" y2="-0.271559375" width="0.05" layer="21"/>
<wire x1="1.328840625" y1="-0.271559375" x2="1.30943125" y2="-0.2573" width="0.05" layer="21"/>
<wire x1="1.30943125" y1="-0.2573" x2="1.28918125" y2="-0.24413125" width="0.05" layer="21"/>
<wire x1="1.28918125" y1="-0.24413125" x2="1.26813125" y2="-0.23206875" width="0.05" layer="21"/>
<wire x1="1.26813125" y1="-0.23206875" x2="1.24631875" y2="-0.22113125" width="0.05" layer="21"/>
<wire x1="1.24631875" y1="-0.22113125" x2="1.22378125" y2="-0.211340625" width="0.05" layer="21"/>
<wire x1="1.22378125" y1="-0.211340625" x2="1.200559375" y2="-0.2027" width="0.05" layer="21"/>
<wire x1="1.200559375" y1="-0.2027" x2="1.176690625" y2="-0.195240625" width="0.05" layer="21"/>
<wire x1="1.176690625" y1="-0.195240625" x2="1.1522" y2="-0.18896875" width="0.05" layer="21"/>
<wire x1="1.1522" y1="-0.18896875" x2="1.127140625" y2="-0.183909375" width="0.05" layer="21"/>
<wire x1="1.127140625" y1="-0.183909375" x2="1.101540625" y2="-0.18008125" width="0.05" layer="21"/>
<wire x1="1.101540625" y1="-0.18008125" x2="1.075440625" y2="-0.177490625" width="0.05" layer="21"/>
<wire x1="1.075440625" y1="-0.177490625" x2="1.04886875" y2="-0.17616875" width="0.05" layer="21"/>
<wire x1="1.04886875" y1="-0.17616875" x2="1.02186875" y2="-0.17611875" width="0.05" layer="21"/>
<wire x1="1.02186875" y1="-0.17611875" x2="0.99448125" y2="-0.17738125" width="0.05" layer="21"/>
<wire x1="0.99448125" y1="-0.17738125" x2="0.966740625" y2="-0.179940625" width="0.05" layer="21"/>
<wire x1="0.966740625" y1="-0.179940625" x2="0.93868125" y2="-0.183840625" width="0.05" layer="21"/>
<wire x1="0.93868125" y1="-0.183840625" x2="0.910340625" y2="-0.189090625" width="0.05" layer="21"/>
<wire x1="0.910340625" y1="-0.189090625" x2="0.881759375" y2="-0.1957" width="0.05" layer="21"/>
<wire x1="0.881759375" y1="-0.1957" x2="0.85296875" y2="-0.2037" width="0.05" layer="21"/>
<wire x1="0.85296875" y1="-0.2037" x2="0.82401875" y2="-0.2131" width="0.05" layer="21"/>
<wire x1="0.82401875" y1="-0.2131" x2="0.7879" y2="-0.226709375" width="0.05" layer="21"/>
<wire x1="0.7879" y1="-0.226709375" x2="0.75276875" y2="-0.24201875" width="0.05" layer="21"/>
<wire x1="0.75276875" y1="-0.24201875" x2="0.718659375" y2="-0.25895" width="0.05" layer="21"/>
<wire x1="0.718659375" y1="-0.25895" x2="0.68561875" y2="-0.27741875" width="0.05" layer="21"/>
<wire x1="0.68561875" y1="-0.27741875" x2="0.6537" y2="-0.29735" width="0.05" layer="21"/>
<wire x1="0.6537" y1="-0.29735" x2="0.62291875" y2="-0.31865" width="0.05" layer="21"/>
<wire x1="0.62291875" y1="-0.31865" x2="0.593340625" y2="-0.34125" width="0.05" layer="21"/>
<wire x1="0.593340625" y1="-0.34125" x2="0.564990625" y2="-0.36508125" width="0.05" layer="21"/>
<wire x1="0.564990625" y1="-0.36508125" x2="0.537909375" y2="-0.390040625" width="0.05" layer="21"/>
<wire x1="0.537909375" y1="-0.390040625" x2="0.512159375" y2="-0.41606875" width="0.05" layer="21"/>
<wire x1="0.512159375" y1="-0.41606875" x2="0.487759375" y2="-0.44306875" width="0.05" layer="21"/>
<wire x1="0.487759375" y1="-0.44306875" x2="0.46476875" y2="-0.47098125" width="0.05" layer="21"/>
<wire x1="0.46476875" y1="-0.47098125" x2="0.443209375" y2="-0.499709375" width="0.05" layer="21"/>
<wire x1="0.443209375" y1="-0.499709375" x2="0.423140625" y2="-0.52918125" width="0.05" layer="21"/>
<wire x1="0.423140625" y1="-0.52918125" x2="0.4046" y2="-0.559309375" width="0.05" layer="21"/>
<wire x1="0.4046" y1="-0.559309375" x2="0.38761875" y2="-0.59001875" width="0.05" layer="21"/>
<wire x1="0.38761875" y1="-0.59001875" x2="0.37225" y2="-0.621240625" width="0.05" layer="21"/>
<wire x1="0.37225" y1="-0.621240625" x2="0.358540625" y2="-0.65288125" width="0.05" layer="21"/>
<wire x1="0.358540625" y1="-0.65288125" x2="0.346509375" y2="-0.684859375" width="0.05" layer="21"/>
<wire x1="0.346509375" y1="-0.684859375" x2="0.33621875" y2="-0.717109375" width="0.05" layer="21"/>
<wire x1="0.33621875" y1="-0.717109375" x2="0.327709375" y2="-0.749540625" width="0.05" layer="21"/>
<wire x1="0.327709375" y1="-0.749540625" x2="0.321009375" y2="-0.78206875" width="0.05" layer="21"/>
<wire x1="0.321009375" y1="-0.78206875" x2="0.316159375" y2="-0.81463125" width="0.05" layer="21"/>
<wire x1="0.316159375" y1="-0.81463125" x2="0.31321875" y2="-0.847140625" width="0.05" layer="21"/>
<wire x1="0.31321875" y1="-0.847140625" x2="0.31221875" y2="-0.879509375" width="0.05" layer="21"/>
<wire x1="0.31221875" y1="-0.879509375" x2="0.313209375" y2="-0.911659375" width="0.05" layer="21"/>
<wire x1="0.313209375" y1="-0.911659375" x2="0.31621875" y2="-0.94353125" width="0.05" layer="21"/>
<wire x1="0.31621875" y1="-0.94353125" x2="0.321290625" y2="-0.97501875" width="0.05" layer="21"/>
<wire x1="0.321290625" y1="-0.97501875" x2="0.32848125" y2="-1.00605" width="0.05" layer="21"/>
<wire x1="0.32848125" y1="-1.00605" x2="0.337809375" y2="-1.036559375" width="0.05" layer="21"/>
<wire x1="0.337809375" y1="-1.036559375" x2="0.34933125" y2="-1.06645" width="0.05" layer="21"/>
<wire x1="0.34933125" y1="-1.06645" x2="0.363090625" y2="-1.095640625" width="0.05" layer="21"/>
<wire x1="0.363090625" y1="-1.095640625" x2="0.37623125" y2="-1.119190625" width="0.05" layer="21"/>
<wire x1="0.37623125" y1="-1.119190625" x2="0.39075" y2="-1.14165" width="0.05" layer="21"/>
<wire x1="0.39075" y1="-1.14165" x2="0.4066" y2="-1.163" width="0.05" layer="21"/>
<wire x1="0.4066" y1="-1.163" x2="0.42373125" y2="-1.183209375" width="0.05" layer="21"/>
<wire x1="0.42373125" y1="-1.183209375" x2="0.442109375" y2="-1.202240625" width="0.05" layer="21"/>
<wire x1="0.442109375" y1="-1.202240625" x2="0.46166875" y2="-1.22006875" width="0.05" layer="21"/>
<wire x1="0.46166875" y1="-1.22006875" x2="0.48236875" y2="-1.236659375" width="0.05" layer="21"/>
<wire x1="0.48236875" y1="-1.236659375" x2="0.50416875" y2="-1.25198125" width="0.05" layer="21"/>
<wire x1="0.50416875" y1="-1.25198125" x2="0.527009375" y2="-1.266009375" width="0.05" layer="21"/>
<wire x1="0.527009375" y1="-1.266009375" x2="0.550859375" y2="-1.2787" width="0.05" layer="21"/>
<wire x1="0.550859375" y1="-1.2787" x2="0.57565" y2="-1.29003125" width="0.05" layer="21"/>
<wire x1="0.57565" y1="-1.29003125" x2="0.601359375" y2="-1.29996875" width="0.05" layer="21"/>
<wire x1="0.601359375" y1="-1.29996875" x2="0.62791875" y2="-1.308490625" width="0.05" layer="21"/>
<wire x1="0.62791875" y1="-1.308490625" x2="0.655290625" y2="-1.31555" width="0.05" layer="21"/>
<wire x1="0.655290625" y1="-1.31555" x2="0.68343125" y2="-1.32113125" width="0.05" layer="21"/>
<wire x1="0.68343125" y1="-1.32113125" x2="0.71228125" y2="-1.325190625" width="0.05" layer="21"/>
<wire x1="0.71228125" y1="-1.325190625" x2="0.72963125" y2="-1.326609375" width="0.05" layer="21"/>
<wire x1="0.72963125" y1="-1.326609375" x2="0.74928125" y2="-1.327290625" width="0.05" layer="21"/>
<wire x1="0.74928125" y1="-1.327290625" x2="0.79311875" y2="-1.326509375" width="0.05" layer="21"/>
<wire x1="0.79311875" y1="-1.326509375" x2="0.839209375" y2="-1.32306875" width="0.05" layer="21"/>
<wire x1="0.839209375" y1="-1.32306875" x2="0.861659375" y2="-1.32041875" width="0.05" layer="21"/>
<wire x1="0.861659375" y1="-1.32041875" x2="0.88295" y2="-1.31718125" width="0.05" layer="21"/>
<wire x1="-0.2509" y1="0.365359375" x2="-0.212809375" y2="0.38945" width="0.05" layer="21"/>
<wire x1="-0.212809375" y1="0.38945" x2="-0.17363125" y2="0.417359375" width="0.05" layer="21"/>
<wire x1="-0.17363125" y1="0.417359375" x2="-0.134209375" y2="0.448309375" width="0.05" layer="21"/>
<wire x1="-0.134209375" y1="0.448309375" x2="-0.095440625" y2="0.48151875" width="0.05" layer="21"/>
<wire x1="-0.095440625" y1="0.48151875" x2="-0.058190625" y2="0.516209375" width="0.05" layer="21"/>
<wire x1="-0.058190625" y1="0.516209375" x2="-0.023340625" y2="0.551590625" width="0.05" layer="21"/>
<wire x1="-0.023340625" y1="0.551590625" x2="0.00825" y2="0.5869" width="0.05" layer="21"/>
<wire x1="0.00825" y1="0.5869" x2="0.035690625" y2="0.621340625" width="0.05" layer="21"/>
<wire x1="0.035690625" y1="0.621340625" x2="0.06428125" y2="0.66225" width="0.05" layer="21"/>
<wire x1="0.06428125" y1="0.66225" x2="0.09335" y2="0.70743125" width="0.05" layer="21"/>
<wire x1="0.09335" y1="0.70743125" x2="0.11591875" y2="0.74561875" width="0.05" layer="21"/>
<wire x1="0.11591875" y1="0.74561875" x2="0.122590625" y2="0.758559375" width="0.05" layer="21"/>
<wire x1="0.122590625" y1="0.758559375" x2="0.125" y2="0.76551875" width="0.05" layer="21"/>
<wire x1="0.125" y1="0.76551875" x2="0.124090625" y2="0.76723125" width="0.05" layer="21"/>
<wire x1="0.124090625" y1="0.76723125" x2="0.12136875" y2="0.768309375" width="0.05" layer="21"/>
<wire x1="0.12136875" y1="0.768309375" x2="0.1105" y2="0.768590625" width="0.05" layer="21"/>
<wire x1="0.1105" y1="0.768590625" x2="0.092390625" y2="0.76635" width="0.05" layer="21"/>
<wire x1="0.092390625" y1="0.76635" x2="0.067040625" y2="0.76158125" width="0.05" layer="21"/>
<wire x1="0.067040625" y1="0.76158125" x2="0.05488125" y2="0.760390625" width="0.05" layer="21"/>
<wire x1="0.05488125" y1="0.760390625" x2="0.032540625" y2="0.75938125" width="0.05" layer="21"/>
<wire x1="0.032540625" y1="0.75938125" x2="-0.03" y2="0.758309375" width="0.05" layer="21"/>
<wire x1="-0.03" y1="0.758309375" x2="-0.07945" y2="0.758909375" width="0.05" layer="21"/>
<wire x1="-0.07945" y1="0.758909375" x2="-0.12211875" y2="0.761259375" width="0.05" layer="21"/>
<wire x1="-0.12211875" y1="0.761259375" x2="-0.16185" y2="0.765690625" width="0.05" layer="21"/>
<wire x1="-0.16185" y1="0.765690625" x2="-0.2025" y2="0.7725" width="0.05" layer="21"/>
<wire x1="-0.2025" y1="0.7725" x2="-0.207290625" y2="0.77323125" width="0.05" layer="21"/>
<wire x1="-0.207290625" y1="0.77323125" x2="-0.21138125" y2="0.773190625" width="0.05" layer="21"/>
<wire x1="-0.21138125" y1="0.773190625" x2="-0.215159375" y2="0.77206875" width="0.05" layer="21"/>
<wire x1="-0.215159375" y1="0.77206875" x2="-0.218990625" y2="0.769559375" width="0.05" layer="21"/>
<wire x1="-0.218990625" y1="0.769559375" x2="-0.22326875" y2="0.76535" width="0.05" layer="21"/>
<wire x1="-0.22326875" y1="0.76535" x2="-0.228359375" y2="0.759109375" width="0.05" layer="21"/>
<wire x1="-0.228359375" y1="0.759109375" x2="-0.2425" y2="0.739340625" width="0.05" layer="21"/>
<wire x1="-0.2425" y1="0.739340625" x2="-0.257740625" y2="0.71893125" width="0.05" layer="21"/>
<wire x1="-0.257740625" y1="0.71893125" x2="-0.27655" y2="0.696209375" width="0.05" layer="21"/>
<wire x1="-0.27655" y1="0.696209375" x2="-0.29793125" y2="0.672209375" width="0.05" layer="21"/>
<wire x1="-0.29793125" y1="0.672209375" x2="-0.32086875" y2="0.64796875" width="0.05" layer="21"/>
<wire x1="-0.32086875" y1="0.64796875" x2="-0.344359375" y2="0.624509375" width="0.05" layer="21"/>
<wire x1="-0.344359375" y1="0.624509375" x2="-0.3674" y2="0.60288125" width="0.05" layer="21"/>
<wire x1="-0.3674" y1="0.60288125" x2="-0.388990625" y2="0.5841" width="0.05" layer="21"/>
<wire x1="-0.388990625" y1="0.5841" x2="-0.408109375" y2="0.5692" width="0.05" layer="21"/>
<wire x1="-0.408109375" y1="0.5692" x2="-0.42695" y2="0.556440625" width="0.05" layer="21"/>
<wire x1="-0.42695" y1="0.556440625" x2="-0.44915" y2="0.542590625" width="0.05" layer="21"/>
<wire x1="-0.44915" y1="0.542590625" x2="-0.47191875" y2="0.52931875" width="0.05" layer="21"/>
<wire x1="-0.47191875" y1="0.52931875" x2="-0.4925" y2="0.5183" width="0.05" layer="21"/>
<wire x1="-0.4925" y1="0.5183" x2="-0.52721875" y2="0.50006875" width="0.05" layer="21"/>
<wire x1="-0.52721875" y1="0.50006875" x2="-0.5475" y2="0.487990625" width="0.05" layer="21"/>
<wire x1="-0.5475" y1="0.487990625" x2="-0.553" y2="0.4836" width="0.05" layer="21"/>
<wire x1="-0.553" y1="0.4836" x2="-0.55461875" y2="0.481290625" width="0.05" layer="21"/>
<wire x1="-0.55461875" y1="0.481290625" x2="-0.552190625" y2="0.48036875" width="0.05" layer="21"/>
<wire x1="-0.552190625" y1="0.48036875" x2="-0.54553125" y2="0.480140625" width="0.05" layer="21"/>
<wire x1="-0.54553125" y1="0.480140625" x2="-0.53306875" y2="0.48226875" width="0.05" layer="21"/>
<wire x1="-0.53306875" y1="0.48226875" x2="-0.50796875" y2="0.488190625" width="0.05" layer="21"/>
<wire x1="-0.50796875" y1="0.488190625" x2="-0.43446875" y2="0.507759375" width="0.05" layer="21"/>
<wire x1="-0.43446875" y1="0.507759375" x2="-0.366540625" y2="0.5262" width="0.05" layer="21"/>
<wire x1="-0.366540625" y1="0.5262" x2="-0.320509375" y2="0.53715" width="0.05" layer="21"/>
<wire x1="-0.320509375" y1="0.53715" x2="-0.304540625" y2="0.54005" width="0.05" layer="21"/>
<wire x1="-0.304540625" y1="0.54005" x2="-0.29266875" y2="0.54135" width="0.05" layer="21"/>
<wire x1="-0.29266875" y1="0.54135" x2="-0.28441875" y2="0.54116875" width="0.05" layer="21"/>
<wire x1="-0.28441875" y1="0.54116875" x2="-0.279340625" y2="0.53958125" width="0.05" layer="21"/>
<wire x1="-0.279340625" y1="0.53958125" x2="-0.2767" y2="0.537" width="0.05" layer="21"/>
<wire x1="-0.2767" y1="0.537" x2="-0.276459375" y2="0.533690625" width="0.05" layer="21"/>
<wire x1="-0.276459375" y1="0.533690625" x2="-0.27898125" y2="0.52933125" width="0.05" layer="21"/>
<wire x1="-0.27898125" y1="0.52933125" x2="-0.284590625" y2="0.52361875" width="0.05" layer="21"/>
<wire x1="-0.284590625" y1="0.52361875" x2="-0.30645" y2="0.50695" width="0.05" layer="21"/>
<wire x1="-0.30645" y1="0.50695" x2="-0.34476875" y2="0.481240625" width="0.05" layer="21"/>
<wire x1="-0.34476875" y1="0.481240625" x2="-0.392159375" y2="0.44903125" width="0.05" layer="21"/>
<wire x1="-0.392159375" y1="0.44903125" x2="-0.40856875" y2="0.436740625" width="0.05" layer="21"/>
<wire x1="-0.40856875" y1="0.436740625" x2="-0.420259375" y2="0.42688125" width="0.05" layer="21"/>
<wire x1="-0.420259375" y1="0.42688125" x2="-0.427309375" y2="0.419359375" width="0.05" layer="21"/>
<wire x1="-0.427309375" y1="0.419359375" x2="-0.42911875" y2="0.41645" width="0.05" layer="21"/>
<wire x1="-0.42911875" y1="0.41645" x2="-0.429809375" y2="0.414090625" width="0.05" layer="21"/>
<wire x1="-0.429809375" y1="0.414090625" x2="-0.429390625" y2="0.41228125" width="0.05" layer="21"/>
<wire x1="-0.429390625" y1="0.41228125" x2="-0.427859375" y2="0.411" width="0.05" layer="21"/>
<wire x1="-0.427859375" y1="0.411" x2="-0.42155" y2="0.40998125" width="0.05" layer="21"/>
<wire x1="-0.42155" y1="0.40998125" x2="-0.41311875" y2="0.41153125" width="0.05" layer="21"/>
<wire x1="-0.41311875" y1="0.41153125" x2="-0.3964" y2="0.41573125" width="0.05" layer="21"/>
<wire x1="-0.3964" y1="0.41573125" x2="-0.3478" y2="0.42953125" width="0.05" layer="21"/>
<wire x1="-0.3478" y1="0.42953125" x2="-0.30018125" y2="0.443290625" width="0.05" layer="21"/>
<wire x1="-0.30018125" y1="0.443290625" x2="-0.26911875" y2="0.4508" width="0.05" layer="21"/>
<wire x1="-0.26911875" y1="0.4508" x2="-0.258509375" y2="0.45246875" width="0.05" layer="21"/>
<wire x1="-0.258509375" y1="0.45246875" x2="-0.250509375" y2="0.452859375" width="0.05" layer="21"/>
<wire x1="-0.250509375" y1="0.452859375" x2="-0.244609375" y2="0.4521" width="0.05" layer="21"/>
<wire x1="-0.244609375" y1="0.4521" x2="-0.24028125" y2="0.450259375" width="0.05" layer="21"/>
<wire x1="-0.24028125" y1="0.450259375" x2="-0.237090625" y2="0.44705" width="0.05" layer="21"/>
<wire x1="-0.237090625" y1="0.44705" x2="-0.23608125" y2="0.44266875" width="0.05" layer="21"/>
<wire x1="-0.23608125" y1="0.44266875" x2="-0.23736875" y2="0.436909375" width="0.05" layer="21"/>
<wire x1="-0.23736875" y1="0.436909375" x2="-0.24106875" y2="0.429590625" width="0.05" layer="21"/>
<wire x1="-0.24106875" y1="0.429590625" x2="-0.2473" y2="0.420509375" width="0.05" layer="21"/>
<wire x1="-0.2473" y1="0.420509375" x2="-0.25618125" y2="0.40948125" width="0.05" layer="21"/>
<wire x1="-0.25618125" y1="0.40948125" x2="-0.282340625" y2="0.380790625" width="0.05" layer="21"/>
<wire x1="-0.282340625" y1="0.380790625" x2="-0.308940625" y2="0.35185" width="0.05" layer="21"/>
<wire x1="-0.308940625" y1="0.35185" x2="-0.31703125" y2="0.3422" width="0.05" layer="21"/>
<wire x1="-0.31703125" y1="0.3422" x2="-0.32" y2="0.337640625" width="0.05" layer="21"/>
<wire x1="-0.32" y1="0.337640625" x2="-0.31966875" y2="0.33643125" width="0.05" layer="21"/>
<wire x1="-0.31966875" y1="0.33643125" x2="-0.3187" y2="0.335659375" width="0.05" layer="21"/>
<wire x1="-0.3187" y1="0.335659375" x2="-0.31491875" y2="0.335390625" width="0.05" layer="21"/>
<wire x1="-0.31491875" y1="0.335390625" x2="-0.30885" y2="0.336759375" width="0.05" layer="21"/>
<wire x1="-0.30885" y1="0.336759375" x2="-0.30068125" y2="0.33968125" width="0.05" layer="21"/>
<wire x1="-0.30068125" y1="0.33968125" x2="-0.27883125" y2="0.34986875" width="0.05" layer="21"/>
<wire x1="-0.27883125" y1="0.34986875" x2="-0.2509" y2="0.365359375" width="0.05" layer="21"/>
<wire x1="0.272309375" y1="1.122890625" x2="0.27538125" y2="1.128440625" width="0.05" layer="21"/>
<wire x1="0.27538125" y1="1.128440625" x2="0.277490625" y2="1.134040625" width="0.05" layer="21"/>
<wire x1="0.277490625" y1="1.134040625" x2="0.278459375" y2="1.13901875" width="0.05" layer="21"/>
<wire x1="0.278459375" y1="1.13901875" x2="0.2781" y2="1.14273125" width="0.05" layer="21"/>
<wire x1="0.2781" y1="1.14273125" x2="0.27748125" y2="1.14568125" width="0.05" layer="21"/>
<wire x1="0.27748125" y1="1.14568125" x2="0.277759375" y2="1.14858125" width="0.05" layer="21"/>
<wire x1="0.277759375" y1="1.14858125" x2="0.278859375" y2="1.1511" width="0.05" layer="21"/>
<wire x1="0.278859375" y1="1.1511" x2="0.280690625" y2="1.152909375" width="0.05" layer="21"/>
<wire x1="0.280690625" y1="1.152909375" x2="0.28255" y2="1.15511875" width="0.05" layer="21"/>
<wire x1="0.28255" y1="1.15511875" x2="0.28371875" y2="1.158740625" width="0.05" layer="21"/>
<wire x1="0.28371875" y1="1.158740625" x2="0.28411875" y2="1.163259375" width="0.05" layer="21"/>
<wire x1="0.28411875" y1="1.163259375" x2="0.283640625" y2="1.168190625" width="0.05" layer="21"/>
<wire x1="0.283640625" y1="1.168190625" x2="0.283009375" y2="1.1773" width="0.05" layer="21"/>
<wire x1="0.283009375" y1="1.1773" x2="0.283590625" y2="1.18055" width="0.05" layer="21"/>
<wire x1="0.283590625" y1="1.18055" x2="0.28473125" y2="1.182309375" width="0.05" layer="21"/>
<wire x1="0.28473125" y1="1.182309375" x2="0.285959375" y2="1.18473125" width="0.05" layer="21"/>
<wire x1="0.285959375" y1="1.18473125" x2="0.286809375" y2="1.18975" width="0.05" layer="21"/>
<wire x1="0.286809375" y1="1.18975" x2="0.28708125" y2="1.204690625" width="0.05" layer="21"/>
<wire x1="0.28708125" y1="1.204690625" x2="0.28703125" y2="1.21256875" width="0.05" layer="21"/>
<wire x1="0.28703125" y1="1.21256875" x2="0.287659375" y2="1.21901875" width="0.05" layer="21"/>
<wire x1="0.287659375" y1="1.21901875" x2="0.288859375" y2="1.22338125" width="0.05" layer="21"/>
<wire x1="0.288859375" y1="1.22338125" x2="0.29051875" y2="1.22498125" width="0.05" layer="21"/>
<wire x1="0.29051875" y1="1.22498125" x2="0.29221875" y2="1.22648125" width="0.05" layer="21"/>
<wire x1="0.29221875" y1="1.22648125" x2="0.293509375" y2="1.23065" width="0.05" layer="21"/>
<wire x1="0.293509375" y1="1.23065" x2="0.294790625" y2="1.245140625" width="0.05" layer="21"/>
<wire x1="0.294790625" y1="1.245140625" x2="0.29428125" y2="1.2647" width="0.05" layer="21"/>
<wire x1="0.29428125" y1="1.2647" x2="0.2919" y2="1.28558125" width="0.05" layer="21"/>
<wire x1="0.2919" y1="1.28558125" x2="0.2889" y2="1.30366875" width="0.05" layer="21"/>
<wire x1="0.2889" y1="1.30366875" x2="0.2357" y2="1.279859375" width="0.05" layer="21"/>
<wire x1="0.2357" y1="1.279859375" x2="0.188640625" y2="1.2604" width="0.05" layer="21"/>
<wire x1="0.188640625" y1="1.2604" x2="0.138759375" y2="1.242809375" width="0.05" layer="21"/>
<wire x1="0.138759375" y1="1.242809375" x2="0.087209375" y2="1.227459375" width="0.05" layer="21"/>
<wire x1="0.087209375" y1="1.227459375" x2="0.03513125" y2="1.21471875" width="0.05" layer="21"/>
<wire x1="0.03513125" y1="1.21471875" x2="0.00113125" y2="1.206890625" width="0.05" layer="21"/>
<wire x1="0.00113125" y1="1.206890625" x2="-0.014090625" y2="1.202559375" width="0.05" layer="21"/>
<wire x1="-0.014090625" y1="1.202559375" x2="-0.01323125" y2="1.200840625" width="0.05" layer="21"/>
<wire x1="-0.01323125" y1="1.200840625" x2="-0.009509375" y2="1.19753125" width="0.05" layer="21"/>
<wire x1="-0.009509375" y1="1.19753125" x2="0.004109375" y2="1.18805" width="0.05" layer="21"/>
<wire x1="0.004109375" y1="1.18805" x2="0.01915" y2="1.17768125" width="0.05" layer="21"/>
<wire x1="0.01915" y1="1.17768125" x2="0.02715" y2="1.17055" width="0.05" layer="21"/>
<wire x1="0.02715" y1="1.17055" x2="0.03288125" y2="1.166290625" width="0.05" layer="21"/>
<wire x1="0.03288125" y1="1.166290625" x2="0.04505" y2="1.160359375" width="0.05" layer="21"/>
<wire x1="0.04505" y1="1.160359375" x2="0.082640625" y2="1.145640625" width="0.05" layer="21"/>
<wire x1="0.082640625" y1="1.145640625" x2="0.127759375" y2="1.130740625" width="0.05" layer="21"/>
<wire x1="0.127759375" y1="1.130740625" x2="0.149340625" y2="1.12458125" width="0.05" layer="21"/>
<wire x1="0.149340625" y1="1.12458125" x2="0.16825" y2="1.120009375" width="0.05" layer="21"/>
<wire x1="0.16825" y1="1.120009375" x2="0.207040625" y2="1.11331875" width="0.05" layer="21"/>
<wire x1="0.207040625" y1="1.11331875" x2="0.224709375" y2="1.111209375" width="0.05" layer="21"/>
<wire x1="0.224709375" y1="1.111209375" x2="0.23818125" y2="1.11033125" width="0.05" layer="21"/>
<wire x1="0.23818125" y1="1.11033125" x2="0.25188125" y2="1.110590625" width="0.05" layer="21"/>
<wire x1="0.25188125" y1="1.110590625" x2="0.260840625" y2="1.1123" width="0.05" layer="21"/>
<wire x1="0.260840625" y1="1.1123" x2="0.264159375" y2="1.113909375" width="0.05" layer="21"/>
<wire x1="0.264159375" y1="1.113909375" x2="0.267009375" y2="1.116159375" width="0.05" layer="21"/>
<wire x1="0.267009375" y1="1.116159375" x2="0.272309375" y2="1.122890625" width="0.05" layer="21"/>
<wire x1="-0.94838125" y1="1.29218125" x2="-0.964040625" y2="1.298040625" width="0.05" layer="21"/>
<wire x1="-0.964040625" y1="1.298040625" x2="-0.979109375" y2="1.305590625" width="0.05" layer="21"/>
<wire x1="-0.979109375" y1="1.305590625" x2="-0.99346875" y2="1.314740625" width="0.05" layer="21"/>
<wire x1="-0.99346875" y1="1.314740625" x2="-1.00703125" y2="1.325390625" width="0.05" layer="21"/>
<wire x1="-1.00703125" y1="1.325390625" x2="-1.01965" y2="1.337440625" width="0.05" layer="21"/>
<wire x1="-1.01965" y1="1.337440625" x2="-1.031240625" y2="1.350790625" width="0.05" layer="21"/>
<wire x1="-1.031240625" y1="1.350790625" x2="-1.04166875" y2="1.365340625" width="0.05" layer="21"/>
<wire x1="-1.04166875" y1="1.365340625" x2="-1.050840625" y2="1.381009375" width="0.05" layer="21"/>
<wire x1="-1.050840625" y1="1.381009375" x2="-1.0562" y2="1.39298125" width="0.05" layer="21"/>
<wire x1="-1.0562" y1="1.39298125" x2="-1.059709375" y2="1.405490625" width="0.05" layer="21"/>
<wire x1="-1.059709375" y1="1.405490625" x2="-1.06165" y2="1.419959375" width="0.05" layer="21"/>
<wire x1="-1.06165" y1="1.419959375" x2="-1.062309375" y2="1.43783125" width="0.05" layer="21"/>
<wire x1="-1.062309375" y1="1.43783125" x2="-1.06191875" y2="1.45698125" width="0.05" layer="21"/>
<wire x1="-1.06191875" y1="1.45698125" x2="-1.059990625" y2="1.4707" width="0.05" layer="21"/>
<wire x1="-1.059990625" y1="1.4707" x2="-1.05575" y2="1.48215" width="0.05" layer="21"/>
<wire x1="-1.05575" y1="1.48215" x2="-1.04841875" y2="1.49446875" width="0.05" layer="21"/>
<wire x1="-1.04841875" y1="1.49446875" x2="-1.042740625" y2="1.502059375" width="0.05" layer="21"/>
<wire x1="-1.042740625" y1="1.502059375" x2="-1.036259375" y2="1.508909375" width="0.05" layer="21"/>
<wire x1="-1.036259375" y1="1.508909375" x2="-1.029040625" y2="1.515009375" width="0.05" layer="21"/>
<wire x1="-1.029040625" y1="1.515009375" x2="-1.021159375" y2="1.520340625" width="0.05" layer="21"/>
<wire x1="-1.021159375" y1="1.520340625" x2="-1.012659375" y2="1.5249" width="0.05" layer="21"/>
<wire x1="-1.012659375" y1="1.5249" x2="-1.00363125" y2="1.52866875" width="0.05" layer="21"/>
<wire x1="-1.00363125" y1="1.52866875" x2="-0.994109375" y2="1.53165" width="0.05" layer="21"/>
<wire x1="-0.994109375" y1="1.53165" x2="-0.98418125" y2="1.53381875" width="0.05" layer="21"/>
<wire x1="-0.98418125" y1="1.53381875" x2="-0.9739" y2="1.53516875" width="0.05" layer="21"/>
<wire x1="-0.9739" y1="1.53516875" x2="-0.96333125" y2="1.5357" width="0.05" layer="21"/>
<wire x1="-0.96333125" y1="1.5357" x2="-0.95253125" y2="1.53538125" width="0.05" layer="21"/>
<wire x1="-0.95253125" y1="1.53538125" x2="-0.94158125" y2="1.5342" width="0.05" layer="21"/>
<wire x1="-0.94158125" y1="1.5342" x2="-0.930540625" y2="1.53216875" width="0.05" layer="21"/>
<wire x1="-0.930540625" y1="1.53216875" x2="-0.919459375" y2="1.529259375" width="0.05" layer="21"/>
<wire x1="-0.919459375" y1="1.529259375" x2="-0.90841875" y2="1.525459375" width="0.05" layer="21"/>
<wire x1="-0.90841875" y1="1.525459375" x2="-0.89748125" y2="1.52076875" width="0.05" layer="21"/>
<wire x1="-0.89748125" y1="1.52076875" x2="-0.88341875" y2="1.51305" width="0.05" layer="21"/>
<wire x1="-0.88341875" y1="1.51305" x2="-0.86941875" y2="1.50336875" width="0.05" layer="21"/>
<wire x1="-0.86941875" y1="1.50336875" x2="-0.855840625" y2="1.49211875" width="0.05" layer="21"/>
<wire x1="-0.855840625" y1="1.49211875" x2="-0.843090625" y2="1.47973125" width="0.05" layer="21"/>
<wire x1="-0.843090625" y1="1.47973125" x2="-0.831540625" y2="1.466590625" width="0.05" layer="21"/>
<wire x1="-0.831540625" y1="1.466590625" x2="-0.821590625" y2="1.453109375" width="0.05" layer="21"/>
<wire x1="-0.821590625" y1="1.453109375" x2="-0.81361875" y2="1.4397" width="0.05" layer="21"/>
<wire x1="-0.81361875" y1="1.4397" x2="-0.80803125" y2="1.42676875" width="0.05" layer="21"/>
<wire x1="-0.80803125" y1="1.42676875" x2="-0.803309375" y2="1.40956875" width="0.05" layer="21"/>
<wire x1="-0.803309375" y1="1.40956875" x2="-0.800959375" y2="1.393" width="0.05" layer="21"/>
<wire x1="-0.800959375" y1="1.393" x2="-0.80086875" y2="1.377159375" width="0.05" layer="21"/>
<wire x1="-0.80086875" y1="1.377159375" x2="-0.8029" y2="1.36216875" width="0.05" layer="21"/>
<wire x1="-0.8029" y1="1.36216875" x2="-0.806940625" y2="1.348159375" width="0.05" layer="21"/>
<wire x1="-0.806940625" y1="1.348159375" x2="-0.81286875" y2="1.335240625" width="0.05" layer="21"/>
<wire x1="-0.81286875" y1="1.335240625" x2="-0.820540625" y2="1.32353125" width="0.05" layer="21"/>
<wire x1="-0.820540625" y1="1.32353125" x2="-0.829859375" y2="1.31315" width="0.05" layer="21"/>
<wire x1="-0.829859375" y1="1.31315" x2="-0.84068125" y2="1.30421875" width="0.05" layer="21"/>
<wire x1="-0.84068125" y1="1.30421875" x2="-0.8529" y2="1.29685" width="0.05" layer="21"/>
<wire x1="-0.8529" y1="1.29685" x2="-0.86636875" y2="1.29116875" width="0.05" layer="21"/>
<wire x1="-0.86636875" y1="1.29116875" x2="-0.880990625" y2="1.287290625" width="0.05" layer="21"/>
<wire x1="-0.880990625" y1="1.287290625" x2="-0.89661875" y2="1.28533125" width="0.05" layer="21"/>
<wire x1="-0.89661875" y1="1.28533125" x2="-0.913140625" y2="1.28541875" width="0.05" layer="21"/>
<wire x1="-0.913140625" y1="1.28541875" x2="-0.930440625" y2="1.287659375" width="0.05" layer="21"/>
<wire x1="-0.930440625" y1="1.287659375" x2="-0.94838125" y2="1.29218125" width="0.05" layer="21"/>
<wire x1="-2.5475" y1="1.45065" x2="-2.58886875" y2="1.455509375" width="0.05" layer="21"/>
<wire x1="-2.58886875" y1="1.455509375" x2="-2.630440625" y2="1.4622" width="0.05" layer="21"/>
<wire x1="-2.630440625" y1="1.4622" x2="-2.672040625" y2="1.470640625" width="0.05" layer="21"/>
<wire x1="-2.672040625" y1="1.470640625" x2="-2.71351875" y2="1.48078125" width="0.05" layer="21"/>
<wire x1="-2.71351875" y1="1.48078125" x2="-2.754740625" y2="1.492540625" width="0.05" layer="21"/>
<wire x1="-2.754740625" y1="1.492540625" x2="-2.79553125" y2="1.505859375" width="0.05" layer="21"/>
<wire x1="-2.79553125" y1="1.505859375" x2="-2.83575" y2="1.52068125" width="0.05" layer="21"/>
<wire x1="-2.83575" y1="1.52068125" x2="-2.875240625" y2="1.53691875" width="0.05" layer="21"/>
<wire x1="-2.875240625" y1="1.53691875" x2="-2.913859375" y2="1.55451875" width="0.05" layer="21"/>
<wire x1="-2.913859375" y1="1.55451875" x2="-2.951440625" y2="1.57341875" width="0.05" layer="21"/>
<wire x1="-2.951440625" y1="1.57341875" x2="-2.987840625" y2="1.593540625" width="0.05" layer="21"/>
<wire x1="-2.987840625" y1="1.593540625" x2="-3.0229" y2="1.61483125" width="0.05" layer="21"/>
<wire x1="-3.0229" y1="1.61483125" x2="-3.05646875" y2="1.63721875" width="0.05" layer="21"/>
<wire x1="-3.05646875" y1="1.63721875" x2="-3.0884" y2="1.66063125" width="0.05" layer="21"/>
<wire x1="-3.0884" y1="1.66063125" x2="-3.11853125" y2="1.685009375" width="0.05" layer="21"/>
<wire x1="-3.11853125" y1="1.685009375" x2="-3.146709375" y2="1.710290625" width="0.05" layer="21"/>
<wire x1="-3.146709375" y1="1.710290625" x2="-3.166790625" y2="1.730040625" width="0.05" layer="21"/>
<wire x1="-3.166790625" y1="1.730040625" x2="-3.185959375" y2="1.75041875" width="0.05" layer="21"/>
<wire x1="-3.185959375" y1="1.75041875" x2="-3.20423125" y2="1.77143125" width="0.05" layer="21"/>
<wire x1="-3.20423125" y1="1.77143125" x2="-3.22156875" y2="1.79303125" width="0.05" layer="21"/>
<wire x1="-3.22156875" y1="1.79303125" x2="-3.23798125" y2="1.8152" width="0.05" layer="21"/>
<wire x1="-3.23798125" y1="1.8152" x2="-3.25343125" y2="1.8379" width="0.05" layer="21"/>
<wire x1="-3.25343125" y1="1.8379" x2="-3.26791875" y2="1.86113125" width="0.05" layer="21"/>
<wire x1="-3.26791875" y1="1.86113125" x2="-3.28143125" y2="1.884840625" width="0.05" layer="21"/>
<wire x1="-3.28143125" y1="1.884840625" x2="-3.29395" y2="1.90901875" width="0.05" layer="21"/>
<wire x1="-3.29395" y1="1.90901875" x2="-3.305459375" y2="1.933640625" width="0.05" layer="21"/>
<wire x1="-3.305459375" y1="1.933640625" x2="-3.31595" y2="1.95866875" width="0.05" layer="21"/>
<wire x1="-3.31595" y1="1.95866875" x2="-3.325409375" y2="1.9841" width="0.05" layer="21"/>
<wire x1="-3.325409375" y1="1.9841" x2="-3.33381875" y2="2.00988125" width="0.05" layer="21"/>
<wire x1="-3.33381875" y1="2.00988125" x2="-3.34116875" y2="2.036009375" width="0.05" layer="21"/>
<wire x1="-3.34116875" y1="2.036009375" x2="-3.347440625" y2="2.06245" width="0.05" layer="21"/>
<wire x1="-3.347440625" y1="2.06245" x2="-3.35261875" y2="2.08916875" width="0.05" layer="21"/>
<wire x1="-3.35261875" y1="2.08916875" x2="-3.35611875" y2="2.1144" width="0.05" layer="21"/>
<wire x1="-3.35611875" y1="2.1144" x2="-3.358790625" y2="2.143490625" width="0.05" layer="21"/>
<wire x1="-3.358790625" y1="2.143490625" x2="-3.360590625" y2="2.17458125" width="0.05" layer="21"/>
<wire x1="-3.360590625" y1="2.17458125" x2="-3.361490625" y2="2.20575" width="0.05" layer="21"/>
<wire x1="-3.361490625" y1="2.20575" x2="-3.36148125" y2="2.23513125" width="0.05" layer="21"/>
<wire x1="-3.36148125" y1="2.23513125" x2="-3.360509375" y2="2.26083125" width="0.05" layer="21"/>
<wire x1="-3.360509375" y1="2.26083125" x2="-3.35856875" y2="2.280940625" width="0.05" layer="21"/>
<wire x1="-3.35856875" y1="2.280940625" x2="-3.35563125" y2="2.293590625" width="0.05" layer="21"/>
<wire x1="-3.35563125" y1="2.293590625" x2="-3.35268125" y2="2.29986875" width="0.05" layer="21"/>
<wire x1="-3.35268125" y1="2.29986875" x2="-3.3495" y2="2.305009375" width="0.05" layer="21"/>
<wire x1="-3.3495" y1="2.305009375" x2="-3.34603125" y2="2.30898125" width="0.05" layer="21"/>
<wire x1="-3.34603125" y1="2.30898125" x2="-3.34221875" y2="2.31178125" width="0.05" layer="21"/>
<wire x1="-3.34221875" y1="2.31178125" x2="-3.338009375" y2="2.313359375" width="0.05" layer="21"/>
<wire x1="-3.338009375" y1="2.313359375" x2="-3.33336875" y2="2.313709375" width="0.05" layer="21"/>
<wire x1="-3.33336875" y1="2.313709375" x2="-3.32821875" y2="2.3128" width="0.05" layer="21"/>
<wire x1="-3.32821875" y1="2.3128" x2="-3.32253125" y2="2.3106" width="0.05" layer="21"/>
<wire x1="-3.32253125" y1="2.3106" x2="-3.31623125" y2="2.3071" width="0.05" layer="21"/>
<wire x1="-3.31623125" y1="2.3071" x2="-3.309290625" y2="2.302259375" width="0.05" layer="21"/>
<wire x1="-3.309290625" y1="2.302259375" x2="-3.29321875" y2="2.28846875" width="0.05" layer="21"/>
<wire x1="-3.29321875" y1="2.28846875" x2="-3.273909375" y2="2.269059375" width="0.05" layer="21"/>
<wire x1="-3.273909375" y1="2.269059375" x2="-3.25095" y2="2.2438" width="0.05" layer="21"/>
<wire x1="-3.25095" y1="2.2438" x2="-3.2075" y2="2.196309375" width="0.05" layer="21"/>
<wire x1="-3.2075" y1="2.196309375" x2="-3.165759375" y2="2.153890625" width="0.05" layer="21"/>
<wire x1="-3.165759375" y1="2.153890625" x2="-3.128140625" y2="2.118909375" width="0.05" layer="21"/>
<wire x1="-3.128140625" y1="2.118909375" x2="-3.111640625" y2="2.10495" width="0.05" layer="21"/>
<wire x1="-3.111640625" y1="2.10495" x2="-3.097090625" y2="2.093740625" width="0.05" layer="21"/>
<wire x1="-3.097090625" y1="2.093740625" x2="-3.072840625" y2="2.077809375" width="0.05" layer="21"/>
<wire x1="-3.072840625" y1="2.077809375" x2="-3.045709375" y2="2.06278125" width="0.05" layer="21"/>
<wire x1="-3.045709375" y1="2.06278125" x2="-3.016409375" y2="2.04891875" width="0.05" layer="21"/>
<wire x1="-3.016409375" y1="2.04891875" x2="-2.98566875" y2="2.036509375" width="0.05" layer="21"/>
<wire x1="-2.98566875" y1="2.036509375" x2="-2.9542" y2="2.025809375" width="0.05" layer="21"/>
<wire x1="-2.9542" y1="2.025809375" x2="-2.92271875" y2="2.01708125" width="0.05" layer="21"/>
<wire x1="-2.92271875" y1="2.01708125" x2="-2.89195" y2="2.010590625" width="0.05" layer="21"/>
<wire x1="-2.89195" y1="2.010590625" x2="-2.8626" y2="2.0066" width="0.05" layer="21"/>
<wire x1="-2.8626" y1="2.0066" x2="-2.827709375" y2="2.003359375" width="0.05" layer="21"/>
<wire x1="-2.827709375" y1="2.003359375" x2="-2.8441" y2="2.022940625" width="0.05" layer="21"/>
<wire x1="-2.8441" y1="2.022940625" x2="-2.89703125" y2="2.08768125" width="0.05" layer="21"/>
<wire x1="-2.89703125" y1="2.08768125" x2="-2.91743125" y2="2.114309375" width="0.05" layer="21"/>
<wire x1="-2.91743125" y1="2.114309375" x2="-2.93491875" y2="2.138759375" width="0.05" layer="21"/>
<wire x1="-2.93491875" y1="2.138759375" x2="-2.95033125" y2="2.162259375" width="0.05" layer="21"/>
<wire x1="-2.95033125" y1="2.162259375" x2="-2.96453125" y2="2.18603125" width="0.05" layer="21"/>
<wire x1="-2.96453125" y1="2.18603125" x2="-2.97835" y2="2.211309375" width="0.05" layer="21"/>
<wire x1="-2.97835" y1="2.211309375" x2="-2.99263125" y2="2.23933125" width="0.05" layer="21"/>
<wire x1="-2.99263125" y1="2.23933125" x2="-3.01103125" y2="2.278490625" width="0.05" layer="21"/>
<wire x1="-3.01103125" y1="2.278490625" x2="-3.02683125" y2="2.31676875" width="0.05" layer="21"/>
<wire x1="-3.02683125" y1="2.31676875" x2="-3.04015" y2="2.354590625" width="0.05" layer="21"/>
<wire x1="-3.04015" y1="2.354590625" x2="-3.051090625" y2="2.39241875" width="0.05" layer="21"/>
<wire x1="-3.051090625" y1="2.39241875" x2="-3.05976875" y2="2.43068125" width="0.05" layer="21"/>
<wire x1="-3.05976875" y1="2.43068125" x2="-3.066309375" y2="2.46981875" width="0.05" layer="21"/>
<wire x1="-3.066309375" y1="2.46981875" x2="-3.07081875" y2="2.51028125" width="0.05" layer="21"/>
<wire x1="-3.07081875" y1="2.51028125" x2="-3.073409375" y2="2.552509375" width="0.05" layer="21"/>
<wire x1="-3.073409375" y1="2.552509375" x2="-3.074090625" y2="2.59331875" width="0.05" layer="21"/>
<wire x1="-3.074090625" y1="2.59331875" x2="-3.07288125" y2="2.63283125" width="0.05" layer="21"/>
<wire x1="-3.07288125" y1="2.63283125" x2="-3.069740625" y2="2.67131875" width="0.05" layer="21"/>
<wire x1="-3.069740625" y1="2.67131875" x2="-3.064609375" y2="2.709090625" width="0.05" layer="21"/>
<wire x1="-3.064609375" y1="2.709090625" x2="-3.05745" y2="2.74643125" width="0.05" layer="21"/>
<wire x1="-3.05745" y1="2.74643125" x2="-3.0482" y2="2.783640625" width="0.05" layer="21"/>
<wire x1="-3.0482" y1="2.783640625" x2="-3.036840625" y2="2.821" width="0.05" layer="21"/>
<wire x1="-3.036840625" y1="2.821" x2="-3.023290625" y2="2.858809375" width="0.05" layer="21"/>
<wire x1="-3.023290625" y1="2.858809375" x2="-3.016409375" y2="2.875640625" width="0.05" layer="21"/>
<wire x1="-3.016409375" y1="2.875640625" x2="-3.00986875" y2="2.889209375" width="0.05" layer="21"/>
<wire x1="-3.00986875" y1="2.889209375" x2="-3.00348125" y2="2.8997" width="0.05" layer="21"/>
<wire x1="-3.00348125" y1="2.8997" x2="-2.99705" y2="2.907290625" width="0.05" layer="21"/>
<wire x1="-2.99705" y1="2.907290625" x2="-2.990390625" y2="2.91216875" width="0.05" layer="21"/>
<wire x1="-2.990390625" y1="2.91216875" x2="-2.9833" y2="2.91451875" width="0.05" layer="21"/>
<wire x1="-2.9833" y1="2.91451875" x2="-2.9756" y2="2.91451875" width="0.05" layer="21"/>
<wire x1="-2.9756" y1="2.91451875" x2="-2.9671" y2="2.91235" width="0.05" layer="21"/>
<wire x1="-2.9671" y1="2.91235" x2="-2.963259375" y2="2.91061875" width="0.05" layer="21"/>
<wire x1="-2.963259375" y1="2.91061875" x2="-2.9597" y2="2.908190625" width="0.05" layer="21"/>
<wire x1="-2.9597" y1="2.908190625" x2="-2.952909375" y2="2.90045" width="0.05" layer="21"/>
<wire x1="-2.952909375" y1="2.90045" x2="-2.94566875" y2="2.887590625" width="0.05" layer="21"/>
<wire x1="-2.94566875" y1="2.887590625" x2="-2.936959375" y2="2.868109375" width="0.05" layer="21"/>
<wire x1="-2.936959375" y1="2.868109375" x2="-2.92073125" y2="2.831309375" width="0.05" layer="21"/>
<wire x1="-2.92073125" y1="2.831309375" x2="-2.90333125" y2="2.795340625" width="0.05" layer="21"/>
<wire x1="-2.90333125" y1="2.795340625" x2="-2.88475" y2="2.760209375" width="0.05" layer="21"/>
<wire x1="-2.88475" y1="2.760209375" x2="-2.86501875" y2="2.725940625" width="0.05" layer="21"/>
<wire x1="-2.86501875" y1="2.725940625" x2="-2.84415" y2="2.69255" width="0.05" layer="21"/>
<wire x1="-2.84415" y1="2.69255" x2="-2.82215" y2="2.660059375" width="0.05" layer="21"/>
<wire x1="-2.82215" y1="2.660059375" x2="-2.799040625" y2="2.62848125" width="0.05" layer="21"/>
<wire x1="-2.799040625" y1="2.62848125" x2="-2.77483125" y2="2.59783125" width="0.05" layer="21"/>
<wire x1="-2.77483125" y1="2.59783125" x2="-2.74955" y2="2.56813125" width="0.05" layer="21"/>
<wire x1="-2.74955" y1="2.56813125" x2="-2.723190625" y2="2.5394" width="0.05" layer="21"/>
<wire x1="-2.723190625" y1="2.5394" x2="-2.69578125" y2="2.511640625" width="0.05" layer="21"/>
<wire x1="-2.69578125" y1="2.511640625" x2="-2.66733125" y2="2.484890625" width="0.05" layer="21"/>
<wire x1="-2.66733125" y1="2.484890625" x2="-2.63785" y2="2.459159375" width="0.05" layer="21"/>
<wire x1="-2.63785" y1="2.459159375" x2="-2.60736875" y2="2.43446875" width="0.05" layer="21"/>
<wire x1="-2.60736875" y1="2.43446875" x2="-2.575890625" y2="2.41081875" width="0.05" layer="21"/>
<wire x1="-2.575890625" y1="2.41081875" x2="-2.54341875" y2="2.38825" width="0.05" layer="21"/>
<wire x1="-2.54341875" y1="2.38825" x2="-2.50685" y2="2.36523125" width="0.05" layer="21"/>
<wire x1="-2.50685" y1="2.36523125" x2="-2.46583125" y2="2.341340625" width="0.05" layer="21"/>
<wire x1="-2.46583125" y1="2.341340625" x2="-2.43105" y2="2.3226" width="0.05" layer="21"/>
<wire x1="-2.43105" y1="2.3226" x2="-2.419340625" y2="2.31703125" width="0.05" layer="21"/>
<wire x1="-2.419340625" y1="2.31703125" x2="-2.413209375" y2="2.315009375" width="0.05" layer="21"/>
<wire x1="-2.413209375" y1="2.315009375" x2="-2.412690625" y2="2.31656875" width="0.05" layer="21"/>
<wire x1="-2.412690625" y1="2.31656875" x2="-2.413309375" y2="2.32081875" width="0.05" layer="21"/>
<wire x1="-2.413309375" y1="2.32081875" x2="-2.41741875" y2="2.33478125" width="0.05" layer="21"/>
<wire x1="-2.41741875" y1="2.33478125" x2="-2.42658125" y2="2.366940625" width="0.05" layer="21"/>
<wire x1="-2.42658125" y1="2.366940625" x2="-2.43781875" y2="2.415040625" width="0.05" layer="21"/>
<wire x1="-2.43781875" y1="2.415040625" x2="-2.449140625" y2="2.46995" width="0.05" layer="21"/>
<wire x1="-2.449140625" y1="2.46995" x2="-2.45853125" y2="2.522509375" width="0.05" layer="21"/>
<wire x1="-2.45853125" y1="2.522509375" x2="-2.46286875" y2="2.55856875" width="0.05" layer="21"/>
<wire x1="-2.46286875" y1="2.55856875" x2="-2.465840625" y2="2.60236875" width="0.05" layer="21"/>
<wire x1="-2.465840625" y1="2.60236875" x2="-2.467459375" y2="2.651509375" width="0.05" layer="21"/>
<wire x1="-2.467459375" y1="2.651509375" x2="-2.467740625" y2="2.703590625" width="0.05" layer="21"/>
<wire x1="-2.467740625" y1="2.703590625" x2="-2.4667" y2="2.75621875" width="0.05" layer="21"/>
<wire x1="-2.4667" y1="2.75621875" x2="-2.46435" y2="2.807" width="0.05" layer="21"/>
<wire x1="-2.46435" y1="2.807" x2="-2.46073125" y2="2.85353125" width="0.05" layer="21"/>
<wire x1="-2.46073125" y1="2.85353125" x2="-2.45583125" y2="2.89341875" width="0.05" layer="21"/>
<wire x1="-2.45583125" y1="2.89341875" x2="-2.45055" y2="2.9253" width="0.05" layer="21"/>
<wire x1="-2.45055" y1="2.9253" x2="-2.44496875" y2="2.954590625" width="0.05" layer="21"/>
<wire x1="-2.44496875" y1="2.954590625" x2="-2.43906875" y2="2.9813" width="0.05" layer="21"/>
<wire x1="-2.43906875" y1="2.9813" x2="-2.43285" y2="3.005459375" width="0.05" layer="21"/>
<wire x1="-2.43285" y1="3.005459375" x2="-2.4263" y2="3.02708125" width="0.05" layer="21"/>
<wire x1="-2.4263" y1="3.02708125" x2="-2.4194" y2="3.04616875" width="0.05" layer="21"/>
<wire x1="-2.4194" y1="3.04616875" x2="-2.41215" y2="3.06275" width="0.05" layer="21"/>
<wire x1="-2.41215" y1="3.06275" x2="-2.40453125" y2="3.07683125" width="0.05" layer="21"/>
<wire x1="-2.40453125" y1="3.07683125" x2="-2.39655" y2="3.088440625" width="0.05" layer="21"/>
<wire x1="-2.39655" y1="3.088440625" x2="-2.388190625" y2="3.09756875" width="0.05" layer="21"/>
<wire x1="-2.388190625" y1="3.09756875" x2="-2.379440625" y2="3.104259375" width="0.05" layer="21"/>
<wire x1="-2.379440625" y1="3.104259375" x2="-2.370290625" y2="3.1085" width="0.05" layer="21"/>
<wire x1="-2.370290625" y1="3.1085" x2="-2.36073125" y2="3.11033125" width="0.05" layer="21"/>
<wire x1="-2.36073125" y1="3.11033125" x2="-2.350759375" y2="3.10975" width="0.05" layer="21"/>
<wire x1="-2.350759375" y1="3.10975" x2="-2.340359375" y2="3.10678125" width="0.05" layer="21"/>
<wire x1="-2.340359375" y1="3.10678125" x2="-2.32951875" y2="3.10143125" width="0.05" layer="21"/>
<wire x1="-2.32951875" y1="3.10143125" x2="-2.32156875" y2="3.095959375" width="0.05" layer="21"/>
<wire x1="-2.32156875" y1="3.095959375" x2="-2.319009375" y2="3.09305" width="0.05" layer="21"/>
<wire x1="-2.319009375" y1="3.09305" x2="-2.3172" y2="3.089559375" width="0.05" layer="21"/>
<wire x1="-2.3172" y1="3.089559375" x2="-2.315340625" y2="3.079409375" width="0.05" layer="21"/>
<wire x1="-2.315340625" y1="3.079409375" x2="-2.314909375" y2="3.06268125" width="0.05" layer="21"/>
<wire x1="-2.314909375" y1="3.06268125" x2="-2.312709375" y2="3.026359375" width="0.05" layer="21"/>
<wire x1="-2.312709375" y1="3.026359375" x2="-2.30755" y2="2.9814" width="0.05" layer="21"/>
<wire x1="-2.30755" y1="2.9814" x2="-2.30195" y2="2.94681875" width="0.05" layer="21"/>
<wire x1="-2.30195" y1="2.94681875" x2="-2.29503125" y2="2.912740625" width="0.05" layer="21"/>
<wire x1="-2.29503125" y1="2.912740625" x2="-2.286809375" y2="2.87916875" width="0.05" layer="21"/>
<wire x1="-2.286809375" y1="2.87916875" x2="-2.27728125" y2="2.8461" width="0.05" layer="21"/>
<wire x1="-2.27728125" y1="2.8461" x2="-2.266440625" y2="2.81353125" width="0.05" layer="21"/>
<wire x1="-2.266440625" y1="2.81353125" x2="-2.2543" y2="2.78148125" width="0.05" layer="21"/>
<wire x1="-2.2543" y1="2.78148125" x2="-2.24085" y2="2.749940625" width="0.05" layer="21"/>
<wire x1="-2.24085" y1="2.749940625" x2="-2.2261" y2="2.718909375" width="0.05" layer="21"/>
<wire x1="-2.2261" y1="2.718909375" x2="-2.21005" y2="2.688390625" width="0.05" layer="21"/>
<wire x1="-2.21005" y1="2.688390625" x2="-2.192690625" y2="2.658390625" width="0.05" layer="21"/>
<wire x1="-2.192690625" y1="2.658390625" x2="-2.174040625" y2="2.628909375" width="0.05" layer="21"/>
<wire x1="-2.174040625" y1="2.628909375" x2="-2.15408125" y2="2.59995" width="0.05" layer="21"/>
<wire x1="-2.15408125" y1="2.59995" x2="-2.13283125" y2="2.571509375" width="0.05" layer="21"/>
<wire x1="-2.13283125" y1="2.571509375" x2="-2.11026875" y2="2.543590625" width="0.05" layer="21"/>
<wire x1="-2.11026875" y1="2.543590625" x2="-2.08641875" y2="2.5162" width="0.05" layer="21"/>
<wire x1="-2.08641875" y1="2.5162" x2="-2.06128125" y2="2.489340625" width="0.05" layer="21"/>
<wire x1="-2.06128125" y1="2.489340625" x2="-2.03395" y2="2.461159375" width="0.05" layer="21"/>
<wire x1="-2.03395" y1="2.461159375" x2="-2.08331875" y2="2.43345" width="0.05" layer="21"/>
<wire x1="-2.08331875" y1="2.43345" x2="-2.111640625" y2="2.416909375" width="0.05" layer="21"/>
<wire x1="-2.111640625" y1="2.416909375" x2="-2.13926875" y2="2.399459375" width="0.05" layer="21"/>
<wire x1="-2.13926875" y1="2.399459375" x2="-2.16616875" y2="2.38115" width="0.05" layer="21"/>
<wire x1="-2.16616875" y1="2.38115" x2="-2.1923" y2="2.362009375" width="0.05" layer="21"/>
<wire x1="-2.1923" y1="2.362009375" x2="-2.21761875" y2="2.3421" width="0.05" layer="21"/>
<wire x1="-2.21761875" y1="2.3421" x2="-2.242090625" y2="2.32145" width="0.05" layer="21"/>
<wire x1="-2.242090625" y1="2.32145" x2="-2.265659375" y2="2.3001" width="0.05" layer="21"/>
<wire x1="-2.265659375" y1="2.3001" x2="-2.2883" y2="2.2781" width="0.05" layer="21"/>
<wire x1="-2.2883" y1="2.2781" x2="-2.309959375" y2="2.25548125" width="0.05" layer="21"/>
<wire x1="-2.309959375" y1="2.25548125" x2="-2.3306" y2="2.232290625" width="0.05" layer="21"/>
<wire x1="-2.3306" y1="2.232290625" x2="-2.350190625" y2="2.20858125" width="0.05" layer="21"/>
<wire x1="-2.350190625" y1="2.20858125" x2="-2.36866875" y2="2.18438125" width="0.05" layer="21"/>
<wire x1="-2.36866875" y1="2.18438125" x2="-2.386009375" y2="2.15973125" width="0.05" layer="21"/>
<wire x1="-2.386009375" y1="2.15973125" x2="-2.402159375" y2="2.13468125" width="0.05" layer="21"/>
<wire x1="-2.402159375" y1="2.13468125" x2="-2.417090625" y2="2.10926875" width="0.05" layer="21"/>
<wire x1="-2.417090625" y1="2.10926875" x2="-2.430740625" y2="2.08353125" width="0.05" layer="21"/>
<wire x1="-2.430740625" y1="2.08353125" x2="-2.44543125" y2="2.05228125" width="0.05" layer="21"/>
<wire x1="-2.44543125" y1="2.05228125" x2="-2.45843125" y2="2.020290625" width="0.05" layer="21"/>
<wire x1="-2.45843125" y1="2.020290625" x2="-2.469759375" y2="1.987659375" width="0.05" layer="21"/>
<wire x1="-2.469759375" y1="1.987659375" x2="-2.479390625" y2="1.95445" width="0.05" layer="21"/>
<wire x1="-2.479390625" y1="1.95445" x2="-2.487340625" y2="1.920740625" width="0.05" layer="21"/>
<wire x1="-2.487340625" y1="1.920740625" x2="-2.493590625" y2="1.88661875" width="0.05" layer="21"/>
<wire x1="-2.493590625" y1="1.88661875" x2="-2.49815" y2="1.85215" width="0.05" layer="21"/>
<wire x1="-2.49815" y1="1.85215" x2="-2.500990625" y2="1.817409375" width="0.05" layer="21"/>
<wire x1="-2.500990625" y1="1.817409375" x2="-2.502140625" y2="1.78248125" width="0.05" layer="21"/>
<wire x1="-2.502140625" y1="1.78248125" x2="-2.501559375" y2="1.747440625" width="0.05" layer="21"/>
<wire x1="-2.501559375" y1="1.747440625" x2="-2.49926875" y2="1.71235" width="0.05" layer="21"/>
<wire x1="-2.49926875" y1="1.71235" x2="-2.49526875" y2="1.677309375" width="0.05" layer="21"/>
<wire x1="-2.49526875" y1="1.677309375" x2="-2.48953125" y2="1.64238125" width="0.05" layer="21"/>
<wire x1="-2.48953125" y1="1.64238125" x2="-2.482059375" y2="1.607640625" width="0.05" layer="21"/>
<wire x1="-2.482059375" y1="1.607640625" x2="-2.472859375" y2="1.57316875" width="0.05" layer="21"/>
<wire x1="-2.472859375" y1="1.57316875" x2="-2.46193125" y2="1.53905" width="0.05" layer="21"/>
<wire x1="-2.46193125" y1="1.53905" x2="-2.4466" y2="1.49753125" width="0.05" layer="21"/>
<wire x1="-2.4466" y1="1.49753125" x2="-2.43478125" y2="1.469590625" width="0.05" layer="21"/>
<wire x1="-2.43478125" y1="1.469590625" x2="-2.425459375" y2="1.451309375" width="0.05" layer="21"/>
<wire x1="-2.425459375" y1="1.451309375" x2="-2.44843125" y2="1.448159375" width="0.05" layer="21"/>
<wire x1="-2.44843125" y1="1.448159375" x2="-2.46646875" y2="1.446190625" width="0.05" layer="21"/>
<wire x1="-2.46646875" y1="1.446190625" x2="-2.4849" y2="1.44585" width="0.05" layer="21"/>
<wire x1="-2.4849" y1="1.44585" x2="-2.509859375" y2="1.44728125" width="0.05" layer="21"/>
<wire x1="-2.509859375" y1="1.44728125" x2="-2.5475" y2="1.45065" width="0.05" layer="21"/>
<wire x1="-2.13705" y1="1.64288125" x2="-2.14756875" y2="1.64723125" width="0.05" layer="21"/>
<wire x1="-2.14756875" y1="1.64723125" x2="-2.15738125" y2="1.6523" width="0.05" layer="21"/>
<wire x1="-2.15738125" y1="1.6523" x2="-2.166440625" y2="1.65806875" width="0.05" layer="21"/>
<wire x1="-2.166440625" y1="1.65806875" x2="-2.17471875" y2="1.6645" width="0.05" layer="21"/>
<wire x1="-2.17471875" y1="1.6645" x2="-2.182190625" y2="1.67158125" width="0.05" layer="21"/>
<wire x1="-2.182190625" y1="1.67158125" x2="-2.18883125" y2="1.67928125" width="0.05" layer="21"/>
<wire x1="-2.18883125" y1="1.67928125" x2="-2.1946" y2="1.68758125" width="0.05" layer="21"/>
<wire x1="-2.1946" y1="1.68758125" x2="-2.199490625" y2="1.69645" width="0.05" layer="21"/>
<wire x1="-2.199490625" y1="1.69645" x2="-2.20585" y2="1.71268125" width="0.05" layer="21"/>
<wire x1="-2.20585" y1="1.71268125" x2="-2.209159375" y2="1.72876875" width="0.05" layer="21"/>
<wire x1="-2.209159375" y1="1.72876875" x2="-2.20966875" y2="1.736759375" width="0.05" layer="21"/>
<wire x1="-2.20966875" y1="1.736759375" x2="-2.20941875" y2="1.744709375" width="0.05" layer="21"/>
<wire x1="-2.20941875" y1="1.744709375" x2="-2.208409375" y2="1.752609375" width="0.05" layer="21"/>
<wire x1="-2.208409375" y1="1.752609375" x2="-2.206640625" y2="1.760459375" width="0.05" layer="21"/>
<wire x1="-2.206640625" y1="1.760459375" x2="-2.20081875" y2="1.77603125" width="0.05" layer="21"/>
<wire x1="-2.20081875" y1="1.77603125" x2="-2.19196875" y2="1.79138125" width="0.05" layer="21"/>
<wire x1="-2.19196875" y1="1.79138125" x2="-2.180090625" y2="1.806509375" width="0.05" layer="21"/>
<wire x1="-2.180090625" y1="1.806509375" x2="-2.16518125" y2="1.8214" width="0.05" layer="21"/>
<wire x1="-2.16518125" y1="1.8214" x2="-2.150540625" y2="1.83306875" width="0.05" layer="21"/>
<wire x1="-2.150540625" y1="1.83306875" x2="-2.134890625" y2="1.84281875" width="0.05" layer="21"/>
<wire x1="-2.134890625" y1="1.84281875" x2="-2.118459375" y2="1.85068125" width="0.05" layer="21"/>
<wire x1="-2.118459375" y1="1.85068125" x2="-2.10145" y2="1.85665" width="0.05" layer="21"/>
<wire x1="-2.10145" y1="1.85665" x2="-2.0841" y2="1.860790625" width="0.05" layer="21"/>
<wire x1="-2.0841" y1="1.860790625" x2="-2.0666" y2="1.863109375" width="0.05" layer="21"/>
<wire x1="-2.0666" y1="1.863109375" x2="-2.0492" y2="1.863640625" width="0.05" layer="21"/>
<wire x1="-2.0492" y1="1.863640625" x2="-2.032090625" y2="1.8624" width="0.05" layer="21"/>
<wire x1="-2.032090625" y1="1.8624" x2="-2.0155" y2="1.85943125" width="0.05" layer="21"/>
<wire x1="-2.0155" y1="1.85943125" x2="-1.99965" y2="1.85475" width="0.05" layer="21"/>
<wire x1="-1.99965" y1="1.85475" x2="-1.98475" y2="1.848390625" width="0.05" layer="21"/>
<wire x1="-1.98475" y1="1.848390625" x2="-1.97101875" y2="1.84038125" width="0.05" layer="21"/>
<wire x1="-1.97101875" y1="1.84038125" x2="-1.95868125" y2="1.830740625" width="0.05" layer="21"/>
<wire x1="-1.95868125" y1="1.830740625" x2="-1.947940625" y2="1.819490625" width="0.05" layer="21"/>
<wire x1="-1.947940625" y1="1.819490625" x2="-1.93903125" y2="1.80668125" width="0.05" layer="21"/>
<wire x1="-1.93903125" y1="1.80668125" x2="-1.932159375" y2="1.79231875" width="0.05" layer="21"/>
<wire x1="-1.932159375" y1="1.79231875" x2="-1.92883125" y2="1.78161875" width="0.05" layer="21"/>
<wire x1="-1.92883125" y1="1.78161875" x2="-1.926909375" y2="1.770790625" width="0.05" layer="21"/>
<wire x1="-1.926909375" y1="1.770790625" x2="-1.92635" y2="1.759890625" width="0.05" layer="21"/>
<wire x1="-1.92635" y1="1.759890625" x2="-1.92711875" y2="1.749" width="0.05" layer="21"/>
<wire x1="-1.92711875" y1="1.749" x2="-1.92916875" y2="1.738190625" width="0.05" layer="21"/>
<wire x1="-1.92916875" y1="1.738190625" x2="-1.932459375" y2="1.727509375" width="0.05" layer="21"/>
<wire x1="-1.932459375" y1="1.727509375" x2="-1.936940625" y2="1.71705" width="0.05" layer="21"/>
<wire x1="-1.936940625" y1="1.71705" x2="-1.94256875" y2="1.70688125" width="0.05" layer="21"/>
<wire x1="-1.94256875" y1="1.70688125" x2="-1.94931875" y2="1.69705" width="0.05" layer="21"/>
<wire x1="-1.94931875" y1="1.69705" x2="-1.95713125" y2="1.68765" width="0.05" layer="21"/>
<wire x1="-1.95713125" y1="1.68765" x2="-1.965959375" y2="1.678740625" width="0.05" layer="21"/>
<wire x1="-1.965959375" y1="1.678740625" x2="-1.97578125" y2="1.670390625" width="0.05" layer="21"/>
<wire x1="-1.97578125" y1="1.670390625" x2="-1.98653125" y2="1.66266875" width="0.05" layer="21"/>
<wire x1="-1.98653125" y1="1.66266875" x2="-1.99818125" y2="1.655640625" width="0.05" layer="21"/>
<wire x1="-1.99818125" y1="1.655640625" x2="-2.01068125" y2="1.649390625" width="0.05" layer="21"/>
<wire x1="-2.01068125" y1="1.649390625" x2="-2.023990625" y2="1.64396875" width="0.05" layer="21"/>
<wire x1="-2.023990625" y1="1.64396875" x2="-2.036" y2="1.640509375" width="0.05" layer="21"/>
<wire x1="-2.036" y1="1.640509375" x2="-2.049990625" y2="1.637990625" width="0.05" layer="21"/>
<wire x1="-2.049990625" y1="1.637990625" x2="-2.06526875" y2="1.636409375" width="0.05" layer="21"/>
<wire x1="-2.06526875" y1="1.636409375" x2="-2.081159375" y2="1.63578125" width="0.05" layer="21"/>
<wire x1="-2.081159375" y1="1.63578125" x2="-2.09696875" y2="1.636109375" width="0.05" layer="21"/>
<wire x1="-2.09696875" y1="1.636109375" x2="-2.112009375" y2="1.637390625" width="0.05" layer="21"/>
<wire x1="-2.112009375" y1="1.637390625" x2="-2.1256" y2="1.63965" width="0.05" layer="21"/>
<wire x1="-2.1256" y1="1.63965" x2="-2.13705" y2="1.64288125" width="0.05" layer="21"/>
<wire x1="0.955" y1="3.2249" x2="1.00441875" y2="3.275290625" width="0.05" layer="21"/>
<wire x1="1.00441875" y1="3.275290625" x2="1.138959375" y2="3.41051875" width="0.05" layer="21"/>
<wire x1="1.138959375" y1="3.41051875" x2="1.58125" y2="3.85303125" width="0.05" layer="21"/>
<wire x1="1.58125" y1="3.85303125" x2="1.865590625" y2="4.13638125" width="0.05" layer="21"/>
<wire x1="1.865590625" y1="4.13638125" x2="2.06381875" y2="4.33241875" width="0.05" layer="21"/>
<wire x1="2.06381875" y1="4.33241875" x2="2.18228125" y2="4.44738125" width="0.05" layer="21"/>
<wire x1="2.18228125" y1="4.44738125" x2="2.21356875" y2="4.476390625" width="0.05" layer="21"/>
<wire x1="2.21356875" y1="4.476390625" x2="2.2273" y2="4.48746875" width="0.05" layer="21"/>
<wire x1="2.2273" y1="4.48746875" x2="2.236659375" y2="4.491140625" width="0.05" layer="21"/>
<wire x1="2.236659375" y1="4.491140625" x2="2.247490625" y2="4.494509375" width="0.05" layer="21"/>
<wire x1="2.247490625" y1="4.494509375" x2="2.25846875" y2="4.4972" width="0.05" layer="21"/>
<wire x1="2.25846875" y1="4.4972" x2="2.268240625" y2="4.49883125" width="0.05" layer="21"/>
<wire x1="2.268240625" y1="4.49883125" x2="2.28788125" y2="4.49996875" width="0.05" layer="21"/>
<wire x1="2.28788125" y1="4.49996875" x2="2.304409375" y2="4.49855" width="0.05" layer="21"/>
<wire x1="2.304409375" y1="4.49855" x2="2.311540625" y2="4.496840625" width="0.05" layer="21"/>
<wire x1="2.311540625" y1="4.496840625" x2="2.31791875" y2="4.49446875" width="0.05" layer="21"/>
<wire x1="2.31791875" y1="4.49446875" x2="2.32356875" y2="4.491409375" width="0.05" layer="21"/>
<wire x1="2.32356875" y1="4.491409375" x2="2.3285" y2="4.487659375" width="0.05" layer="21"/>
<wire x1="2.3285" y1="4.487659375" x2="2.33271875" y2="4.483209375" width="0.05" layer="21"/>
<wire x1="2.33271875" y1="4.483209375" x2="2.33625" y2="4.478040625" width="0.05" layer="21"/>
<wire x1="2.33625" y1="4.478040625" x2="2.33908125" y2="4.472140625" width="0.05" layer="21"/>
<wire x1="2.33908125" y1="4.472140625" x2="2.34125" y2="4.465509375" width="0.05" layer="21"/>
<wire x1="2.34125" y1="4.465509375" x2="2.34275" y2="4.458140625" width="0.05" layer="21"/>
<wire x1="2.34275" y1="4.458140625" x2="2.343609375" y2="4.450009375" width="0.05" layer="21"/>
<wire x1="2.343609375" y1="4.450009375" x2="2.343409375" y2="4.431440625" width="0.05" layer="21"/>
<wire x1="2.343409375" y1="4.431440625" x2="2.341659375" y2="4.41638125" width="0.05" layer="21"/>
<wire x1="2.341659375" y1="4.41638125" x2="2.338640625" y2="4.402740625" width="0.05" layer="21"/>
<wire x1="2.338640625" y1="4.402740625" x2="2.334309375" y2="4.39031875" width="0.05" layer="21"/>
<wire x1="2.334309375" y1="4.39031875" x2="2.328590625" y2="4.378940625" width="0.05" layer="21"/>
<wire x1="2.328590625" y1="4.378940625" x2="2.314640625" y2="4.363040625" width="0.05" layer="21"/>
<wire x1="2.314640625" y1="4.363040625" x2="2.27953125" y2="4.32583125" width="0.05" layer="21"/>
<wire x1="2.27953125" y1="4.32583125" x2="2.155259375" y2="4.19708125" width="0.05" layer="21"/>
<wire x1="2.155259375" y1="4.19708125" x2="1.97455" y2="4.01203125" width="0.05" layer="21"/>
<wire x1="1.97455" y1="4.01203125" x2="1.756190625" y2="3.789990625" width="0.05" layer="21"/>
<wire x1="1.756190625" y1="3.789990625" x2="1.19638125" y2="3.222490625" width="0.05" layer="21"/>
<wire x1="1.19638125" y1="3.222490625" x2="1.075690625" y2="3.222359375" width="0.05" layer="21"/>
<wire x1="1.075690625" y1="3.222359375" x2="0.99045" y2="3.223059375" width="0.05" layer="21"/>
<wire x1="0.99045" y1="3.223059375" x2="0.96451875" y2="3.22388125" width="0.05" layer="21"/>
<wire x1="0.96451875" y1="3.22388125" x2="0.955" y2="3.2249" width="0.05" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SOLAR_CELL">
<circle x="0" y="0" radius="3.5921" width="0.254" layer="94"/>
<pin name="+" x="0" y="7.62" length="short" rot="R270"/>
<pin name="-" x="0" y="-7.62" length="short" rot="R90"/>
<wire x1="0" y1="3.556" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-3.556" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.286" y1="1.27" x2="2.032" y2="1.27" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0.254" x2="0.508" y2="0.254" width="0.254" layer="94"/>
<wire x1="-2.286" y1="-1.016" x2="2.032" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-2.032" x2="0.508" y2="-2.032" width="0.254" layer="94"/>
<wire x1="4.572" y1="4.572" x2="6.096" y2="6.096" width="0.254" layer="94"/>
<wire x1="6.096" y1="6.096" x2="6.858" y2="5.08" width="0.254" layer="94"/>
<wire x1="6.858" y1="5.08" x2="8.636" y2="6.858" width="0.254" layer="94"/>
<wire x1="4.064" y1="5.334" x2="5.08" y2="4.064" width="0.254" layer="94"/>
<wire x1="5.08" y1="4.064" x2="3.556" y2="3.81" width="0.254" layer="94"/>
<wire x1="3.556" y1="3.81" x2="4.064" y2="5.334" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="4.064" y="5.334"/>
<vertex x="3.556" y="3.81"/>
<vertex x="5.08" y="4.064"/>
</polygon>
<wire x1="5.334" y1="1.27" x2="6.858" y2="2.794" width="0.254" layer="94"/>
<wire x1="6.858" y1="2.794" x2="7.62" y2="1.778" width="0.254" layer="94"/>
<wire x1="7.62" y1="1.778" x2="9.398" y2="3.556" width="0.254" layer="94"/>
<wire x1="4.826" y1="2.032" x2="5.842" y2="0.762" width="0.254" layer="94"/>
<wire x1="5.842" y1="0.762" x2="4.318" y2="0.508" width="0.254" layer="94"/>
<wire x1="4.318" y1="0.508" x2="4.826" y2="2.032" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="4.826" y="2.032"/>
<vertex x="4.318" y="0.508"/>
<vertex x="5.842" y="0.762"/>
</polygon>
<text x="-5.08" y="-7.62" size="1.778" layer="95" rot="R90">&gt;NAME</text>
</symbol>
<symbol name="ATMEGA328P(1-18)">
<pin name="PCINT19/OC2B/INT1/PD3" x="12.7" y="17.78" length="middle" rot="R180"/>
<pin name="PCINT20/XCK/T0/PD4" x="12.7" y="15.24" length="middle" rot="R180"/>
<pin name="GND" x="12.7" y="12.7" length="middle" rot="R180"/>
<pin name="VCC" x="12.7" y="10.16" length="middle" rot="R180"/>
<pin name="PCINT6/XTAL/OSC1/PB6" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="PCINT7/XTAL2/OSC2/PB7" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="PCINT21/OC0B/T1/PD5" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="PCINT22/OC0A/AIN0/PD6" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="PCINT23/AIN1/PD7" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="PCINT0/CLKO/ICP1/PB0" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="PCINT1/OC1A/PB1" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="PCINT2/SS/OC1B/PB2" x="12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="PCINT3/OC2A/MOSI/PB3" x="12.7" y="-12.7" length="middle" rot="R180"/>
<pin name="PCINT4/MISO/PB4" x="12.7" y="-15.24" length="middle" rot="R180"/>
<pin name="SCK/PCINT5/PB5" x="12.7" y="-17.78" length="middle" rot="R180"/>
<pin name="AVCC" x="12.7" y="-20.32" length="middle" rot="R180"/>
<wire x1="-5.08" y1="20.32" x2="7.62" y2="20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="20.32" x2="7.62" y2="-22.86" width="0.254" layer="94"/>
<wire x1="7.62" y1="-22.86" x2="-2.54" y2="-22.86" width="0.254" layer="94"/>
<text x="-2.54" y="22.86" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="ATMEGA328P(19-32)">
<pin name="ADC6" x="10.16" y="15.24" length="middle" rot="R180"/>
<pin name="AREF" x="10.16" y="12.7" length="middle" rot="R180"/>
<pin name="GND" x="10.16" y="10.16" length="middle" rot="R180"/>
<pin name="ADC7" x="10.16" y="7.62" length="middle" rot="R180"/>
<pin name="ADC0/PCINT8/PC0" x="10.16" y="5.08" length="middle" rot="R180"/>
<pin name="ADC1/PCINT9/PC1" x="10.16" y="2.54" length="middle" rot="R180"/>
<pin name="ADC2/PCINT10/PC2" x="10.16" y="0" length="middle" rot="R180"/>
<pin name="ADC3/PCINT11/PC3" x="10.16" y="-2.54" length="middle" rot="R180"/>
<pin name="ADC4/SDA//PCINT12/PC4" x="10.16" y="-5.08" length="middle" rot="R180"/>
<pin name="ADC5/SCL/PCINT13/PC5" x="10.16" y="-7.62" length="middle" rot="R180"/>
<pin name="RESET/PCINT14/PC6" x="10.16" y="-10.16" length="middle" rot="R180"/>
<pin name="RXD/PCINT16/PD0" x="10.16" y="-12.7" length="middle" rot="R180"/>
<pin name="TXD/PCINT17/PD1" x="10.16" y="-15.24" length="middle" rot="R180"/>
<pin name="INT0/PCINT18/PD2" x="10.16" y="-17.78" length="middle" rot="R180"/>
<wire x1="-7.62" y1="17.78" x2="5.08" y2="17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="17.78" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="-7.62" y2="-20.32" width="0.254" layer="94"/>
<text x="-5.08" y="20.32" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="BQ25570">
<pin name="VSS" x="-33.02" y="7.62" length="middle"/>
<pin name="VIN_DC" x="-33.02" y="2.54" length="middle"/>
<pin name="VCC_SAMP" x="-5.08" y="27.94" length="middle" rot="R270"/>
<pin name="VREF_SAMP" x="-33.02" y="12.7" length="middle"/>
<pin name="EN" x="-33.02" y="-7.62" length="middle"/>
<pin name="LBOOST" x="-33.02" y="17.78" length="middle"/>
<pin name="VSTOR" x="0" y="27.94" length="middle" rot="R270"/>
<pin name="VBAT" x="10.16" y="27.94" length="middle" rot="R270"/>
<pin name="LBUCK" x="33.02" y="12.7" length="middle" rot="R180"/>
<pin name="VOUT" x="33.02" y="7.62" length="middle" rot="R180"/>
<pin name="VBAT_OK" x="-33.02" y="-17.78" length="middle"/>
<pin name="VOUT_SET" x="20.32" y="-27.94" length="middle" rot="R90"/>
<pin name="OK_PROG" x="5.08" y="-27.94" length="middle" rot="R90"/>
<pin name="OK_HYST" x="12.7" y="-27.94" length="middle" rot="R90"/>
<pin name="VRDIV" x="-10.16" y="-27.94" length="middle" rot="R90"/>
<pin name="VBAT_OV" x="-2.54" y="-27.94" length="middle" rot="R90"/>
<pin name="VOUT_EN" x="-33.02" y="-12.7" length="middle"/>
<wire x1="-27.94" y1="22.86" x2="27.94" y2="22.86" width="0.254" layer="94"/>
<wire x1="27.94" y1="22.86" x2="27.94" y2="-22.86" width="0.254" layer="94"/>
<wire x1="27.94" y1="-22.86" x2="-27.94" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-22.86" x2="-27.94" y2="22.86" width="0.254" layer="94"/>
<text x="-27.94" y="25.4" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="TMP102">
<pin name="SCL" x="-15.24" y="5.08" length="middle"/>
<pin name="GND" x="-15.24" y="0" length="middle"/>
<pin name="ALERT" x="-15.24" y="-5.08" length="middle"/>
<pin name="ADD0" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="V+" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="SDA" x="15.24" y="5.08" length="middle" rot="R180"/>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<text x="-10.16" y="10.16" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="SI1147">
<pin name="SDA" x="-15.24" y="7.62" length="middle"/>
<pin name="SCL" x="-15.24" y="2.54" length="middle"/>
<pin name="VDD" x="-15.24" y="-2.54" length="middle"/>
<pin name="INT" x="-15.24" y="-7.62" length="middle"/>
<pin name="DNC" x="0" y="-15.24" length="middle" rot="R90"/>
<pin name="LED2" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="LED3" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="GND" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="LED1" x="15.24" y="7.62" length="middle" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="12.7" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="LSM9DS1">
<pin name="VDDIO" x="-20.32" y="20.32" length="middle"/>
<pin name="SCL" x="-20.32" y="15.24" length="middle"/>
<pin name="SDA" x="-20.32" y="10.16" length="middle"/>
<pin name="SDO_A/G" x="-20.32" y="5.08" length="middle"/>
<pin name="SDO_M" x="-20.32" y="0" length="middle"/>
<pin name="CS_A/G" x="-20.32" y="-5.08" length="middle"/>
<pin name="CS_M" x="-20.32" y="-10.16" length="middle"/>
<pin name="DRDY_M" x="-20.32" y="-15.24" length="middle"/>
<pin name="INT_M" x="-20.32" y="-20.32" length="middle"/>
<pin name="INT1_A/G" x="20.32" y="-15.24" length="middle" rot="R180"/>
<pin name="INT2_A/G" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="DEN_A/G" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="RES" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="GND" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="CAP" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="C1" x="20.32" y="15.24" length="middle" rot="R180"/>
<pin name="VDD" x="20.32" y="20.32" length="middle" rot="R180"/>
<wire x1="-15.24" y1="22.86" x2="15.24" y2="22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="22.86" x2="15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="15.24" y1="-22.86" x2="-15.24" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-22.86" x2="-15.24" y2="22.86" width="0.254" layer="94"/>
<text x="-15.24" y="25.4" size="1.27" layer="95">&gt;NAME</text>
</symbol>
<symbol name="BATTERY">
<pin name="+" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="-" x="0" y="-5.08" visible="off" length="short" rot="R90"/>
<wire x1="-2.286" y1="2.032" x2="0" y2="2.032" width="0.254" layer="94"/>
<wire x1="0" y1="2.032" x2="2.286" y2="2.032" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.016" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.286" y1="-0.254" x2="2.286" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.524" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="1.016" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="2.032" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-1.524" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
</symbol>
<symbol name="L-US">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
<symbol name="RFM95W/96W/98W">
<pin name="GND" x="-15.24" y="-7.62" length="middle"/>
<pin name="MISO" x="-15.24" y="7.62" length="middle"/>
<pin name="MOSI" x="-15.24" y="5.08" length="middle"/>
<pin name="SCK" x="-15.24" y="2.54" length="middle"/>
<pin name="NSS" x="-15.24" y="0" length="middle"/>
<pin name="RESET" x="-15.24" y="-2.54" length="middle"/>
<pin name="DIO5" x="-15.24" y="-5.08" length="middle"/>
<pin name="ANT" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="DIO3" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="DIO4" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="VCC" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="DIO0" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="DIO1" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="DIO2" x="12.7" y="10.16" length="middle" rot="R180"/>
<text x="-10.16" y="15.24" size="1.778" layer="95">&gt;NAME</text>
<wire x1="-10.16" y1="12.7" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
</symbol>
<symbol name="RESONATOR">
<pin name="P$1" x="-10.16" y="10.16" length="middle" rot="R270"/>
<pin name="P$2" x="10.16" y="10.16" length="middle" rot="R270"/>
<pin name="P$3" x="0" y="-12.7" length="middle" rot="R90"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.778" x2="1.524" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.778" x2="1.524" y2="-1.778" width="0.254" layer="94"/>
<wire x1="1.524" y1="-1.778" x2="-1.27" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.778" x2="-1.27" y2="1.778" width="0.254" layer="94"/>
<wire x1="10.16" y1="0" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="0" width="0.254" layer="94"/>
<wire x1="-10.16" y1="0" x2="-7.62" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="10.16" y2="0" width="0.254" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="6.35" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="8.89" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="-3.81" x2="7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="7.62" y1="-3.81" x2="8.89" y2="-3.81" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-2.54" x2="-6.35" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-8.89" y1="-3.556" x2="-7.62" y2="-3.556" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-3.556" x2="-6.35" y2="-3.556" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="-3.556" width="0.254" layer="94"/>
<wire x1="-12.7" y1="5.08" x2="12.7" y2="5.08" width="0.254" layer="94" style="longdash"/>
<wire x1="12.7" y1="5.08" x2="12.7" y2="-10.16" width="0.254" layer="94" style="longdash"/>
<wire x1="12.7" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94" style="longdash"/>
<wire x1="-12.7" y1="-10.16" x2="-12.7" y2="5.08" width="0.254" layer="94" style="longdash"/>
</symbol>
<symbol name="ANTENNA">
<pin name="1" x="0" y="0" visible="off" length="middle" rot="R90"/>
<wire x1="-2.54" y1="10.16" x2="2.54" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="-2.54" y2="10.16" width="0.254" layer="94"/>
</symbol>
<symbol name="AXOLOTL_LOGO">
<wire x1="-0.162940625" y1="-3.309265625" x2="-0.243490625" y2="-3.307765625" width="0.001" layer="94"/>
<wire x1="-0.243490625" y1="-3.307765625" x2="-0.241571875" y2="-3.099134375" width="0.001" layer="94"/>
<wire x1="-0.241571875" y1="-3.099134375" x2="-0.2393625" y2="-2.929384375" width="0.001" layer="94"/>
<wire x1="-0.2393625" y1="-2.929384375" x2="-0.23673125" y2="-2.81425625" width="0.001" layer="94"/>
<wire x1="-0.23673125" y1="-2.81425625" x2="-0.233803125" y2="-2.73800625" width="0.001" layer="94"/>
<wire x1="-0.233803125" y1="-2.73800625" x2="0.065128125" y2="-2.73800625" width="0.001" layer="94"/>
<wire x1="0.065128125" y1="-2.73800625" x2="0.065128125" y2="-3.166975" width="0.001" layer="94"/>
<wire x1="0.065128125" y1="-3.166975" x2="0.133878125" y2="-3.170696875" width="0.001" layer="94"/>
<wire x1="0.133878125" y1="-3.170696875" x2="0.189609375" y2="-3.17185625" width="0.001" layer="94"/>
<wire x1="0.189609375" y1="-3.17185625" x2="0.2905375" y2="-3.172275" width="0.001" layer="94"/>
<wire x1="0.2905375" y1="-3.172275" x2="0.570659375" y2="-3.170834375" width="0.001" layer="94"/>
<wire x1="0.570659375" y1="-3.170834375" x2="0.938696875" y2="-3.167246875" width="0.001" layer="94"/>
<wire x1="0.938696875" y1="-3.167246875" x2="0.941859375" y2="-3.186375" width="0.001" layer="94"/>
<wire x1="0.941859375" y1="-3.186375" x2="0.944109375" y2="-3.214715625" width="0.001" layer="94"/>
<wire x1="0.944109375" y1="-3.214715625" x2="0.94506875" y2="-3.255975" width="0.001" layer="94"/>
<wire x1="0.94506875" y1="-3.255975" x2="0.945128125" y2="-3.306434375" width="0.001" layer="94"/>
<wire x1="0.945128125" y1="-3.306434375" x2="0.866378125" y2="-3.30970625" width="0.001" layer="94"/>
<wire x1="0.866378125" y1="-3.30970625" x2="0.807746875" y2="-3.31070625" width="0.001" layer="94"/>
<wire x1="0.807746875" y1="-3.31070625" x2="0.698178125" y2="-3.311396875" width="0.001" layer="94"/>
<wire x1="0.698178125" y1="-3.311396875" x2="0.38806875" y2="-3.311846875" width="0.001" layer="94"/>
<wire x1="0.38806875" y1="-3.311846875" x2="0.0597875" y2="-3.311125" width="0.001" layer="94"/>
<wire x1="0.0597875" y1="-3.311125" x2="-0.162921875" y2="-3.309265625" width="0.001" layer="94"/>
<wire x1="-0.162940625" y1="-3.309265625" x2="-0.162921875" y2="-3.309265625" width="0.001" layer="94"/>
<wire x1="1.238059375" y1="-3.297825" x2="1.21356875" y2="-3.292475" width="0.001" layer="94"/>
<wire x1="1.21356875" y1="-3.292475" x2="1.19046875" y2="-3.285146875" width="0.001" layer="94"/>
<wire x1="1.19046875" y1="-3.285146875" x2="1.168978125" y2="-3.275996875" width="0.001" layer="94"/>
<wire x1="1.168978125" y1="-3.275996875" x2="1.149346875" y2="-3.265165625" width="0.001" layer="94"/>
<wire x1="1.149346875" y1="-3.265165625" x2="1.1317875" y2="-3.252784375" width="0.001" layer="94"/>
<wire x1="1.1317875" y1="-3.252784375" x2="1.116528125" y2="-3.239015625" width="0.001" layer="94"/>
<wire x1="1.116528125" y1="-3.239015625" x2="1.103796875" y2="-3.223984375" width="0.001" layer="94"/>
<wire x1="1.103796875" y1="-3.223984375" x2="1.098446875" y2="-3.216046875" width="0.001" layer="94"/>
<wire x1="1.098446875" y1="-3.216046875" x2="1.093828125" y2="-3.207846875" width="0.001" layer="94"/>
<wire x1="1.093828125" y1="-3.207846875" x2="1.079828125" y2="-3.180465625" width="0.001" layer="94"/>
<wire x1="1.079828125" y1="-3.180465625" x2="1.076696875" y2="-3.029734375" width="0.001" layer="94"/>
<wire x1="1.076696875" y1="-3.029734375" x2="1.0754875" y2="-2.955715625" width="0.001" layer="94"/>
<wire x1="1.0754875" y1="-2.955715625" x2="1.0755875" y2="-2.905596875" width="0.001" layer="94"/>
<wire x1="1.0755875" y1="-2.905596875" x2="1.077109375" y2="-2.873996875" width="0.001" layer="94"/>
<wire x1="1.077109375" y1="-2.873996875" x2="1.07841875" y2="-2.863475" width="0.001" layer="94"/>
<wire x1="1.07841875" y1="-2.863475" x2="1.080128125" y2="-2.855565625" width="0.001" layer="94"/>
<wire x1="1.080128125" y1="-2.855565625" x2="1.08466875" y2="-2.843515625" width="0.001" layer="94"/>
<wire x1="1.08466875" y1="-2.843515625" x2="1.091528125" y2="-2.830996875" width="0.001" layer="94"/>
<wire x1="1.091528125" y1="-2.830996875" x2="1.100359375" y2="-2.818346875" width="0.001" layer="94"/>
<wire x1="1.100359375" y1="-2.818346875" x2="1.11081875" y2="-2.805965625" width="0.001" layer="94"/>
<wire x1="1.11081875" y1="-2.805965625" x2="1.12256875" y2="-2.79420625" width="0.001" layer="94"/>
<wire x1="1.12256875" y1="-2.79420625" x2="1.13526875" y2="-2.783446875" width="0.001" layer="94"/>
<wire x1="1.13526875" y1="-2.783446875" x2="1.148578125" y2="-2.77405625" width="0.001" layer="94"/>
<wire x1="1.148578125" y1="-2.77405625" x2="1.162159375" y2="-2.76640625" width="0.001" layer="94"/>
<wire x1="1.162159375" y1="-2.76640625" x2="1.1817875" y2="-2.757325" width="0.001" layer="94"/>
<wire x1="1.1817875" y1="-2.757325" x2="1.19161875" y2="-2.753615625" width="0.001" layer="94"/>
<wire x1="1.19161875" y1="-2.753615625" x2="1.202378125" y2="-2.75040625" width="0.001" layer="94"/>
<wire x1="1.202378125" y1="-2.75040625" x2="1.214759375" y2="-2.747646875" width="0.001" layer="94"/>
<wire x1="1.214759375" y1="-2.747646875" x2="1.229459375" y2="-2.745275" width="0.001" layer="94"/>
<wire x1="1.229459375" y1="-2.745275" x2="1.26856875" y2="-2.741584375" width="0.001" layer="94"/>
<wire x1="1.26856875" y1="-2.741584375" x2="1.325246875" y2="-2.738946875" width="0.001" layer="94"/>
<wire x1="1.325246875" y1="-2.738946875" x2="1.40501875" y2="-2.737015625" width="0.001" layer="94"/>
<wire x1="1.40501875" y1="-2.737015625" x2="1.656009375" y2="-2.733765625" width="0.001" layer="94"/>
<wire x1="1.656009375" y1="-2.733765625" x2="1.944946875" y2="-2.731865625" width="0.001" layer="94"/>
<wire x1="1.944946875" y1="-2.731865625" x2="2.03876875" y2="-2.732225" width="0.001" layer="94"/>
<wire x1="2.03876875" y1="-2.732225" x2="2.080259375" y2="-2.733465625" width="0.001" layer="94"/>
<wire x1="2.080259375" y1="-2.733465625" x2="2.107459375" y2="-2.739025" width="0.001" layer="94"/>
<wire x1="2.107459375" y1="-2.739025" x2="2.133228125" y2="-2.74680625" width="0.001" layer="94"/>
<wire x1="2.133228125" y1="-2.74680625" x2="2.15726875" y2="-2.756615625" width="0.001" layer="94"/>
<wire x1="2.15726875" y1="-2.756615625" x2="2.17926875" y2="-2.768275" width="0.001" layer="94"/>
<wire x1="2.17926875" y1="-2.768275" x2="2.1989375" y2="-2.781615625" width="0.001" layer="94"/>
<wire x1="2.1989375" y1="-2.781615625" x2="2.2159875" y2="-2.79645625" width="0.001" layer="94"/>
<wire x1="2.2159875" y1="-2.79645625" x2="2.2234375" y2="-2.804375" width="0.001" layer="94"/>
<wire x1="2.2234375" y1="-2.804375" x2="2.230109375" y2="-2.812615625" width="0.001" layer="94"/>
<wire x1="2.230109375" y1="-2.812615625" x2="2.235978125" y2="-2.821125" width="0.001" layer="94"/>
<wire x1="2.235978125" y1="-2.821125" x2="2.241009375" y2="-2.82990625" width="0.001" layer="94"/>
<wire x1="2.241009375" y1="-2.82990625" x2="2.244028125" y2="-2.836625" width="0.001" layer="94"/>
<wire x1="2.244028125" y1="-2.836625" x2="2.2464875" y2="-2.844675" width="0.001" layer="94"/>
<wire x1="2.2464875" y1="-2.844675" x2="2.248509375" y2="-2.855325" width="0.001" layer="94"/>
<wire x1="2.248509375" y1="-2.855325" x2="2.250209375" y2="-2.86985625" width="0.001" layer="94"/>
<wire x1="2.250209375" y1="-2.86985625" x2="2.253109375" y2="-2.91555625" width="0.001" layer="94"/>
<wire x1="2.253109375" y1="-2.91555625" x2="2.256146875" y2="-2.991915625" width="0.001" layer="94"/>
<wire x1="2.256146875" y1="-2.991915625" x2="2.2582375" y2="-3.059265625" width="0.001" layer="94"/>
<wire x1="2.2582375" y1="-3.059265625" x2="2.258859375" y2="-3.106615625" width="0.001" layer="94"/>
<wire x1="2.258859375" y1="-3.106615625" x2="2.257996875" y2="-3.136965625" width="0.001" layer="94"/>
<wire x1="2.257996875" y1="-3.136965625" x2="2.257009375" y2="-3.146684375" width="0.001" layer="94"/>
<wire x1="2.257009375" y1="-3.146684375" x2="2.255659375" y2="-3.153284375" width="0.001" layer="94"/>
<wire x1="2.255659375" y1="-3.153284375" x2="2.252296875" y2="-3.162346875" width="0.001" layer="94"/>
<wire x1="2.252296875" y1="-3.162346875" x2="2.247696875" y2="-3.171375" width="0.001" layer="94"/>
<wire x1="2.247696875" y1="-3.171375" x2="2.241928125" y2="-3.180325" width="0.001" layer="94"/>
<wire x1="2.241928125" y1="-3.180325" x2="2.235078125" y2="-3.189146875" width="0.001" layer="94"/>
<wire x1="2.235078125" y1="-3.189146875" x2="2.227209375" y2="-3.197775" width="0.001" layer="94"/>
<wire x1="2.227209375" y1="-3.197775" x2="2.218396875" y2="-3.206165625" width="0.001" layer="94"/>
<wire x1="2.218396875" y1="-3.206165625" x2="2.208728125" y2="-3.214265625" width="0.001" layer="94"/>
<wire x1="2.208728125" y1="-3.214265625" x2="2.198259375" y2="-3.222025" width="0.001" layer="94"/>
<wire x1="2.198259375" y1="-3.222025" x2="2.17526875" y2="-3.236296875" width="0.001" layer="94"/>
<wire x1="2.17526875" y1="-3.236296875" x2="2.15001875" y2="-3.248565625" width="0.001" layer="94"/>
<wire x1="2.15001875" y1="-3.248565625" x2="2.123109375" y2="-3.258415625" width="0.001" layer="94"/>
<wire x1="2.123109375" y1="-3.258415625" x2="2.10921875" y2="-3.262296875" width="0.001" layer="94"/>
<wire x1="2.10921875" y1="-3.262296875" x2="2.095146875" y2="-3.265415625" width="0.001" layer="94"/>
<wire x1="2.095146875" y1="-3.265415625" x2="2.07401875" y2="-3.268175" width="0.001" layer="94"/>
<wire x1="2.07401875" y1="-3.268175" x2="2.03831875" y2="-3.271184375" width="0.001" layer="94"/>
<wire x1="2.03831875" y1="-3.271184375" x2="1.925496875" y2="-3.277825" width="0.001" layer="94"/>
<wire x1="1.925496875" y1="-3.277825" x2="1.761246875" y2="-3.285125" width="0.001" layer="94"/>
<wire x1="1.761246875" y1="-3.285125" x2="1.550146875" y2="-3.292846875" width="0.001" layer="94"/>
<wire x1="1.550146875" y1="-3.292846875" x2="1.39341875" y2="-3.297984375" width="0.001" layer="94"/>
<wire x1="1.39341875" y1="-3.297984375" x2="1.304359375" y2="-3.300284375" width="0.001" layer="94"/>
<wire x1="1.304359375" y1="-3.300284375" x2="1.26016875" y2="-3.300115625" width="0.001" layer="94"/>
<wire x1="1.26016875" y1="-3.300115625" x2="1.247778125" y2="-3.299215625" width="0.001" layer="94"/>
<wire x1="1.247778125" y1="-3.299215625" x2="1.238046875" y2="-3.297825" width="0.001" layer="94"/>
<wire x1="1.238046875" y1="-3.297825" x2="1.238059375" y2="-3.297825" width="0.001" layer="94"/>
<wire x1="1.777646875" y1="-3.147284375" x2="1.901678125" y2="-3.142925" width="0.001" layer="94"/>
<wire x1="1.901678125" y1="-3.142925" x2="1.9541375" y2="-3.140396875" width="0.001" layer="94"/>
<wire x1="1.9541375" y1="-3.140396875" x2="1.954159375" y2="-3.099565625" width="0.001" layer="94"/>
<wire x1="1.954159375" y1="-3.099565625" x2="1.952096875" y2="-3.003084375" width="0.001" layer="94"/>
<wire x1="1.952096875" y1="-3.003084375" x2="1.94856875" y2="-2.866984375" width="0.001" layer="94"/>
<wire x1="1.94856875" y1="-2.866984375" x2="1.740609375" y2="-2.870065625" width="0.001" layer="94"/>
<wire x1="1.740609375" y1="-2.870065625" x2="1.5713375" y2="-2.873115625" width="0.001" layer="94"/>
<wire x1="1.5713375" y1="-2.873115625" x2="1.456396875" y2="-2.87610625" width="0.001" layer="94"/>
<wire x1="1.456396875" y1="-2.87610625" x2="1.380146875" y2="-2.87905625" width="0.001" layer="94"/>
<wire x1="1.380146875" y1="-2.87905625" x2="1.380146875" y2="-2.995334375" width="0.001" layer="94"/>
<wire x1="1.380146875" y1="-2.995334375" x2="1.381109375" y2="-3.084484375" width="0.001" layer="94"/>
<wire x1="1.381109375" y1="-3.084484375" x2="1.382159375" y2="-3.117075" width="0.001" layer="94"/>
<wire x1="1.382159375" y1="-3.117075" x2="1.383428125" y2="-3.135546875" width="0.001" layer="94"/>
<wire x1="1.383428125" y1="-3.135546875" x2="1.386709375" y2="-3.159475" width="0.001" layer="94"/>
<wire x1="1.386709375" y1="-3.159475" x2="1.494678125" y2="-3.156225" width="0.001" layer="94"/>
<wire x1="1.494678125" y1="-3.156225" x2="1.777646875" y2="-3.147284375" width="0.001" layer="94"/>
<wire x1="-0.892353125" y1="-3.290084375" x2="-1.1015625" y2="-3.281196875" width="0.001" layer="94"/>
<wire x1="-1.1015625" y1="-3.281196875" x2="-1.256703125" y2="-3.273325" width="0.001" layer="94"/>
<wire x1="-1.256703125" y1="-3.273325" x2="-1.358671875" y2="-3.266425" width="0.001" layer="94"/>
<wire x1="-1.358671875" y1="-3.266425" x2="-1.390003125" y2="-3.263325" width="0.001" layer="94"/>
<wire x1="-1.390003125" y1="-3.263325" x2="-1.4083625" y2="-3.260446875" width="0.001" layer="94"/>
<wire x1="-1.4083625" y1="-3.260446875" x2="-1.422190625" y2="-3.256725" width="0.001" layer="94"/>
<wire x1="-1.422190625" y1="-3.256725" x2="-1.436640625" y2="-3.25190625" width="0.001" layer="94"/>
<wire x1="-1.436640625" y1="-3.25190625" x2="-1.465840625" y2="-3.239675" width="0.001" layer="94"/>
<wire x1="-1.465840625" y1="-3.239675" x2="-1.479790625" y2="-3.232665625" width="0.001" layer="94"/>
<wire x1="-1.479790625" y1="-3.232665625" x2="-1.492771875" y2="-3.225296875" width="0.001" layer="94"/>
<wire x1="-1.492771875" y1="-3.225296875" x2="-1.504371875" y2="-3.217775" width="0.001" layer="94"/>
<wire x1="-1.504371875" y1="-3.217775" x2="-1.514203125" y2="-3.210275" width="0.001" layer="94"/>
<wire x1="-1.514203125" y1="-3.210275" x2="-1.52143125" y2="-3.203275" width="0.001" layer="94"/>
<wire x1="-1.52143125" y1="-3.203275" x2="-1.529403125" y2="-3.194046875" width="0.001" layer="94"/>
<wire x1="-1.529403125" y1="-3.194046875" x2="-1.537153125" y2="-3.183796875" width="0.001" layer="94"/>
<wire x1="-1.537153125" y1="-3.183796875" x2="-1.543703125" y2="-3.173725" width="0.001" layer="94"/>
<wire x1="-1.543703125" y1="-3.173725" x2="-1.557371875" y2="-3.150515625" width="0.001" layer="94"/>
<wire x1="-1.557371875" y1="-3.150515625" x2="-1.555790625" y2="-3.045515625" width="0.001" layer="94"/>
<wire x1="-1.555790625" y1="-3.045515625" x2="-1.553540625" y2="-2.955515625" width="0.001" layer="94"/>
<wire x1="-1.553540625" y1="-2.955515625" x2="-1.5503625" y2="-2.886584375" width="0.001" layer="94"/>
<wire x1="-1.5503625" y1="-2.886584375" x2="-1.5478625" y2="-2.857184375" width="0.001" layer="94"/>
<wire x1="-1.5478625" y1="-2.857184375" x2="-1.544753125" y2="-2.838125" width="0.001" layer="94"/>
<wire x1="-1.544753125" y1="-2.838125" x2="-1.542590625" y2="-2.830984375" width="0.001" layer="94"/>
<wire x1="-1.542590625" y1="-2.830984375" x2="-1.53983125" y2="-2.824634375" width="0.001" layer="94"/>
<wire x1="-1.53983125" y1="-2.824634375" x2="-1.531940625" y2="-2.811925" width="0.001" layer="94"/>
<wire x1="-1.531940625" y1="-2.811925" x2="-1.520371875" y2="-2.797684375" width="0.001" layer="94"/>
<wire x1="-1.520371875" y1="-2.797684375" x2="-1.50648125" y2="-2.784465625" width="0.001" layer="94"/>
<wire x1="-1.50648125" y1="-2.784465625" x2="-1.4904625" y2="-2.772384375" width="0.001" layer="94"/>
<wire x1="-1.4904625" y1="-2.772384375" x2="-1.472521875" y2="-2.761534375" width="0.001" layer="94"/>
<wire x1="-1.472521875" y1="-2.761534375" x2="-1.452871875" y2="-2.752034375" width="0.001" layer="94"/>
<wire x1="-1.452871875" y1="-2.752034375" x2="-1.431703125" y2="-2.743996875" width="0.001" layer="94"/>
<wire x1="-1.431703125" y1="-2.743996875" x2="-1.409221875" y2="-2.737525" width="0.001" layer="94"/>
<wire x1="-1.409221875" y1="-2.737525" x2="-1.385621875" y2="-2.732734375" width="0.001" layer="94"/>
<wire x1="-1.385621875" y1="-2.732734375" x2="-1.37223125" y2="-2.73190625" width="0.001" layer="94"/>
<wire x1="-1.37223125" y1="-2.73190625" x2="-1.34463125" y2="-2.731346875" width="0.001" layer="94"/>
<wire x1="-1.34463125" y1="-2.731346875" x2="-1.252153125" y2="-2.730996875" width="0.001" layer="94"/>
<wire x1="-1.252153125" y1="-2.730996875" x2="-1.118890625" y2="-2.73165625" width="0.001" layer="94"/>
<wire x1="-1.118890625" y1="-2.73165625" x2="-0.95553125" y2="-2.733315625" width="0.001" layer="94"/>
<wire x1="-0.95553125" y1="-2.733315625" x2="-0.704190625" y2="-2.736815625" width="0.001" layer="94"/>
<wire x1="-0.704190625" y1="-2.736815625" x2="-0.6251625" y2="-2.738765625" width="0.001" layer="94"/>
<wire x1="-0.6251625" y1="-2.738765625" x2="-0.5695625" y2="-2.74135625" width="0.001" layer="94"/>
<wire x1="-0.5695625" y1="-2.74135625" x2="-0.531671875" y2="-2.744965625" width="0.001" layer="94"/>
<wire x1="-0.531671875" y1="-2.744965625" x2="-0.5175625" y2="-2.747265625" width="0.001" layer="94"/>
<wire x1="-0.5175625" y1="-2.747265625" x2="-0.50573125" y2="-2.749965625" width="0.001" layer="94"/>
<wire x1="-0.50573125" y1="-2.749965625" x2="-0.495453125" y2="-2.753115625" width="0.001" layer="94"/>
<wire x1="-0.495453125" y1="-2.753115625" x2="-0.4860125" y2="-2.75675625" width="0.001" layer="94"/>
<wire x1="-0.4860125" y1="-2.75675625" x2="-0.4667625" y2="-2.765696875" width="0.001" layer="94"/>
<wire x1="-0.4667625" y1="-2.765696875" x2="-0.453521875" y2="-2.772784375" width="0.001" layer="94"/>
<wire x1="-0.453521875" y1="-2.772784375" x2="-0.440740625" y2="-2.780934375" width="0.001" layer="94"/>
<wire x1="-0.440740625" y1="-2.780934375" x2="-0.428621875" y2="-2.789946875" width="0.001" layer="94"/>
<wire x1="-0.428621875" y1="-2.789946875" x2="-0.4173625" y2="-2.799634375" width="0.001" layer="94"/>
<wire x1="-0.4173625" y1="-2.799634375" x2="-0.4071625" y2="-2.809825" width="0.001" layer="94"/>
<wire x1="-0.4071625" y1="-2.809825" x2="-0.398221875" y2="-2.820325" width="0.001" layer="94"/>
<wire x1="-0.398221875" y1="-2.820325" x2="-0.390740625" y2="-2.83095625" width="0.001" layer="94"/>
<wire x1="-0.390740625" y1="-2.83095625" x2="-0.384921875" y2="-2.841534375" width="0.001" layer="94"/>
<wire x1="-0.384921875" y1="-2.841534375" x2="-0.381303125" y2="-2.84965625" width="0.001" layer="94"/>
<wire x1="-0.381303125" y1="-2.84965625" x2="-0.378603125" y2="-2.857896875" width="0.001" layer="94"/>
<wire x1="-0.378603125" y1="-2.857896875" x2="-0.376703125" y2="-2.868225" width="0.001" layer="94"/>
<wire x1="-0.376703125" y1="-2.868225" x2="-0.3755125" y2="-2.882615625" width="0.001" layer="94"/>
<wire x1="-0.3755125" y1="-2.882615625" x2="-0.374853125" y2="-2.931515625" width="0.001" layer="94"/>
<wire x1="-0.374853125" y1="-2.931515625" x2="-0.3758125" y2="-3.020384375" width="0.001" layer="94"/>
<wire x1="-0.3758125" y1="-3.020384375" x2="-0.377190625" y2="-3.105615625" width="0.001" layer="94"/>
<wire x1="-0.377190625" y1="-3.105615625" x2="-0.3791125" y2="-3.15510625" width="0.001" layer="94"/>
<wire x1="-0.3791125" y1="-3.15510625" x2="-0.380540625" y2="-3.170434375" width="0.001" layer="94"/>
<wire x1="-0.380540625" y1="-3.170434375" x2="-0.382440625" y2="-3.181634375" width="0.001" layer="94"/>
<wire x1="-0.382440625" y1="-3.181634375" x2="-0.384921875" y2="-3.190296875" width="0.001" layer="94"/>
<wire x1="-0.384921875" y1="-3.190296875" x2="-0.38808125" y2="-3.198015625" width="0.001" layer="94"/>
<wire x1="-0.38808125" y1="-3.198015625" x2="-0.3937625" y2="-3.20840625" width="0.001" layer="94"/>
<wire x1="-0.3937625" y1="-3.20840625" x2="-0.40138125" y2="-3.219246875" width="0.001" layer="94"/>
<wire x1="-0.40138125" y1="-3.219246875" x2="-0.41058125" y2="-3.230165625" width="0.001" layer="94"/>
<wire x1="-0.41058125" y1="-3.230165625" x2="-0.420971875" y2="-3.24080625" width="0.001" layer="94"/>
<wire x1="-0.420971875" y1="-3.24080625" x2="-0.4321625" y2="-3.25080625" width="0.001" layer="94"/>
<wire x1="-0.4321625" y1="-3.25080625" x2="-0.443790625" y2="-3.259784375" width="0.001" layer="94"/>
<wire x1="-0.443790625" y1="-3.259784375" x2="-0.4554625" y2="-3.26740625" width="0.001" layer="94"/>
<wire x1="-0.4554625" y1="-3.26740625" x2="-0.466803125" y2="-3.273275" width="0.001" layer="94"/>
<wire x1="-0.466803125" y1="-3.273275" x2="-0.489971875" y2="-3.282375" width="0.001" layer="94"/>
<wire x1="-0.489971875" y1="-3.282375" x2="-0.50213125" y2="-3.286065625" width="0.001" layer="94"/>
<wire x1="-0.50213125" y1="-3.286065625" x2="-0.51523125" y2="-3.289215625" width="0.001" layer="94"/>
<wire x1="-0.51523125" y1="-3.289215625" x2="-0.5296625" y2="-3.291825" width="0.001" layer="94"/>
<wire x1="-0.5296625" y1="-3.291825" x2="-0.545821875" y2="-3.293925" width="0.001" layer="94"/>
<wire x1="-0.545821875" y1="-3.293925" x2="-0.585003125" y2="-3.296646875" width="0.001" layer="94"/>
<wire x1="-0.585003125" y1="-3.296646875" x2="-0.636003125" y2="-3.297484375" width="0.001" layer="94"/>
<wire x1="-0.636003125" y1="-3.297484375" x2="-0.7020625" y2="-3.296596875" width="0.001" layer="94"/>
<wire x1="-0.7020625" y1="-3.296596875" x2="-0.786440625" y2="-3.294084375" width="0.001" layer="94"/>
<wire x1="-0.786440625" y1="-3.294084375" x2="-0.892371875" y2="-3.290096875" width="0.001" layer="94"/>
<wire x1="-0.892371875" y1="-3.290096875" x2="-0.892353125" y2="-3.290084375" width="0.001" layer="94"/>
<wire x1="-0.683140625" y1="-3.10175625" x2="-0.680840625" y2="-3.037515625" width="0.001" layer="94"/>
<wire x1="-0.680840625" y1="-3.037515625" x2="-0.679871875" y2="-2.96205625" width="0.001" layer="94"/>
<wire x1="-0.679871875" y1="-2.96205625" x2="-0.679871875" y2="-2.878615625" width="0.001" layer="94"/>
<wire x1="-0.679871875" y1="-2.878615625" x2="-0.701121875" y2="-2.875965625" width="0.001" layer="94"/>
<wire x1="-0.701121875" y1="-2.875965625" x2="-0.730140625" y2="-2.874684375" width="0.001" layer="94"/>
<wire x1="-0.730140625" y1="-2.874684375" x2="-0.793421875" y2="-2.87315625" width="0.001" layer="94"/>
<wire x1="-0.793421875" y1="-2.87315625" x2="-0.985490625" y2="-2.870125" width="0.001" layer="94"/>
<wire x1="-0.985490625" y1="-2.870125" x2="-1.248621875" y2="-2.866934375" width="0.001" layer="94"/>
<wire x1="-1.248621875" y1="-2.866934375" x2="-1.252321875" y2="-3.000225" width="0.001" layer="94"/>
<wire x1="-1.252321875" y1="-3.000225" x2="-1.2541625" y2="-3.095146875" width="0.001" layer="94"/>
<wire x1="-1.2541625" y1="-3.095146875" x2="-1.2541125" y2="-3.124615625" width="0.001" layer="94"/>
<wire x1="-1.2541125" y1="-3.124615625" x2="-1.25338125" y2="-3.136165625" width="0.001" layer="94"/>
<wire x1="-1.25338125" y1="-3.136165625" x2="-1.240121875" y2="-3.137434375" width="0.001" layer="94"/>
<wire x1="-1.240121875" y1="-3.137434375" x2="-1.205271875" y2="-3.139396875" width="0.001" layer="94"/>
<wire x1="-1.205271875" y1="-3.139396875" x2="-1.090053125" y2="-3.144575" width="0.001" layer="94"/>
<wire x1="-1.090053125" y1="-3.144575" x2="-0.946253125" y2="-3.15010625" width="0.001" layer="94"/>
<wire x1="-0.946253125" y1="-3.15010625" x2="-0.812371875" y2="-3.154396875" width="0.001" layer="94"/>
<wire x1="-0.812371875" y1="-3.154396875" x2="-0.721890625" y2="-3.156946875" width="0.001" layer="94"/>
<wire x1="-0.721890625" y1="-3.156946875" x2="-0.686403125" y2="-3.15800625" width="0.001" layer="94"/>
<wire x1="-0.686403125" y1="-3.15800625" x2="-0.6831625" y2="-3.10175625" width="0.001" layer="94"/>
<wire x1="-0.6831625" y1="-3.10175625" x2="-0.683140625" y2="-3.10175625" width="0.001" layer="94"/>
<wire x1="-1.845821875" y1="-3.24010625" x2="-2.01428125" y2="-3.228334375" width="0.001" layer="94"/>
<wire x1="-2.01428125" y1="-3.228334375" x2="-2.12973125" y2="-3.147984375" width="0.001" layer="94"/>
<wire x1="-2.12973125" y1="-3.147984375" x2="-2.245171875" y2="-3.067625" width="0.001" layer="94"/>
<wire x1="-2.245171875" y1="-3.067625" x2="-2.362390625" y2="-3.127815625" width="0.001" layer="94"/>
<wire x1="-2.362390625" y1="-3.127815625" x2="-2.4212625" y2="-3.157575" width="0.001" layer="94"/>
<wire x1="-2.4212625" y1="-3.157575" x2="-2.4603625" y2="-3.17600625" width="0.001" layer="94"/>
<wire x1="-2.4603625" y1="-3.17600625" x2="-2.474140625" y2="-3.181684375" width="0.001" layer="94"/>
<wire x1="-2.474140625" y1="-3.181684375" x2="-2.484953125" y2="-3.185384375" width="0.001" layer="94"/>
<wire x1="-2.484953125" y1="-3.185384375" x2="-2.49343125" y2="-3.187396875" width="0.001" layer="94"/>
<wire x1="-2.49343125" y1="-3.187396875" x2="-2.500253125" y2="-3.18800625" width="0.001" layer="94"/>
<wire x1="-2.500253125" y1="-3.18800625" x2="-2.555140625" y2="-3.18425625" width="0.001" layer="94"/>
<wire x1="-2.555140625" y1="-3.18425625" x2="-2.6557125" y2="-3.175684375" width="0.001" layer="94"/>
<wire x1="-2.6557125" y1="-3.175684375" x2="-2.757103125" y2="-3.166284375" width="0.001" layer="94"/>
<wire x1="-2.757103125" y1="-3.166284375" x2="-2.81443125" y2="-3.160034375" width="0.001" layer="94"/>
<wire x1="-2.81443125" y1="-3.160034375" x2="-2.819653125" y2="-3.15905625" width="0.001" layer="94"/>
<wire x1="-2.819653125" y1="-3.15905625" x2="-2.8228125" y2="-3.157934375" width="0.001" layer="94"/>
<wire x1="-2.8228125" y1="-3.157934375" x2="-2.8235125" y2="-3.156384375" width="0.001" layer="94"/>
<wire x1="-2.8235125" y1="-3.156384375" x2="-2.821390625" y2="-3.154125" width="0.001" layer="94"/>
<wire x1="-2.821390625" y1="-3.154125" x2="-2.807121875" y2="-3.146365625" width="0.001" layer="94"/>
<wire x1="-2.807121875" y1="-3.146365625" x2="-2.77693125" y2="-3.132384375" width="0.001" layer="94"/>
<wire x1="-2.77693125" y1="-3.132384375" x2="-2.686153125" y2="-3.089015625" width="0.001" layer="94"/>
<wire x1="-2.686153125" y1="-3.089015625" x2="-2.567303125" y2="-3.030134375" width="0.001" layer="94"/>
<wire x1="-2.567303125" y1="-3.030134375" x2="-2.464003125" y2="-2.977625" width="0.001" layer="94"/>
<wire x1="-2.464003125" y1="-2.977625" x2="-2.4318125" y2="-2.960596875" width="0.001" layer="94"/>
<wire x1="-2.4318125" y1="-2.960596875" x2="-2.419871875" y2="-2.953365625" width="0.001" layer="94"/>
<wire x1="-2.419871875" y1="-2.953365625" x2="-2.42543125" y2="-2.94830625" width="0.001" layer="94"/>
<wire x1="-2.42543125" y1="-2.94830625" x2="-2.441290625" y2="-2.936934375" width="0.001" layer="94"/>
<wire x1="-2.441290625" y1="-2.936934375" x2="-2.4988625" y2="-2.89855625" width="0.001" layer="94"/>
<wire x1="-2.4988625" y1="-2.89855625" x2="-2.58253125" y2="-2.844775" width="0.001" layer="94"/>
<wire x1="-2.58253125" y1="-2.844775" x2="-2.68228125" y2="-2.782146875" width="0.001" layer="94"/>
<wire x1="-2.68228125" y1="-2.782146875" x2="-2.764971875" y2="-2.730646875" width="0.001" layer="94"/>
<wire x1="-2.764971875" y1="-2.730646875" x2="-2.7879625" y2="-2.71560625" width="0.001" layer="94"/>
<wire x1="-2.7879625" y1="-2.71560625" x2="-2.8004125" y2="-2.706134375" width="0.001" layer="94"/>
<wire x1="-2.8004125" y1="-2.706134375" x2="-2.803071875" y2="-2.703115625" width="0.001" layer="94"/>
<wire x1="-2.803071875" y1="-2.703115625" x2="-2.8035625" y2="-2.70105625" width="0.001" layer="94"/>
<wire x1="-2.8035625" y1="-2.70105625" x2="-2.802053125" y2="-2.699796875" width="0.001" layer="94"/>
<wire x1="-2.802053125" y1="-2.699796875" x2="-2.798690625" y2="-2.699196875" width="0.001" layer="94"/>
<wire x1="-2.798690625" y1="-2.699196875" x2="-2.78703125" y2="-2.699375" width="0.001" layer="94"/>
<wire x1="-2.78703125" y1="-2.699375" x2="-2.769871875" y2="-2.700434375" width="0.001" layer="94"/>
<wire x1="-2.769871875" y1="-2.700434375" x2="-2.701671875" y2="-2.702965625" width="0.001" layer="94"/>
<wire x1="-2.701671875" y1="-2.702965625" x2="-2.597371875" y2="-2.705696875" width="0.001" layer="94"/>
<wire x1="-2.597371875" y1="-2.705696875" x2="-2.467371875" y2="-2.70855625" width="0.001" layer="94"/>
<wire x1="-2.467371875" y1="-2.70855625" x2="-2.409871875" y2="-2.744984375" width="0.001" layer="94"/>
<wire x1="-2.409871875" y1="-2.744984375" x2="-2.294371875" y2="-2.81920625" width="0.001" layer="94"/>
<wire x1="-2.294371875" y1="-2.81920625" x2="-2.236371875" y2="-2.85700625" width="0.001" layer="94"/>
<wire x1="-2.236371875" y1="-2.85700625" x2="-2.114921875" y2="-2.787246875" width="0.001" layer="94"/>
<wire x1="-2.114921875" y1="-2.787246875" x2="-1.993471875" y2="-2.717484375" width="0.001" layer="94"/>
<wire x1="-1.993471875" y1="-2.717484375" x2="-1.82048125" y2="-2.721515625" width="0.001" layer="94"/>
<wire x1="-1.82048125" y1="-2.721515625" x2="-1.647490625" y2="-2.725534375" width="0.001" layer="94"/>
<wire x1="-1.647490625" y1="-2.725534375" x2="-1.78743125" y2="-2.808796875" width="0.001" layer="94"/>
<wire x1="-1.78743125" y1="-2.808796875" x2="-1.9064625" y2="-2.878915625" width="0.001" layer="94"/>
<wire x1="-1.9064625" y1="-2.878915625" x2="-1.996121875" y2="-2.930584375" width="0.001" layer="94"/>
<wire x1="-1.996121875" y1="-2.930584375" x2="-2.04468125" y2="-2.958215625" width="0.001" layer="94"/>
<wire x1="-2.04468125" y1="-2.958215625" x2="-2.064871875" y2="-2.970515625" width="0.001" layer="94"/>
<wire x1="-2.064871875" y1="-2.970515625" x2="-2.029253125" y2="-2.996165625" width="0.001" layer="94"/>
<wire x1="-2.029253125" y1="-2.996165625" x2="-1.943621875" y2="-3.055865625" width="0.001" layer="94"/>
<wire x1="-1.943621875" y1="-3.055865625" x2="-1.74888125" y2="-3.192234375" width="0.001" layer="94"/>
<wire x1="-1.74888125" y1="-3.192234375" x2="-1.690940625" y2="-3.234034375" width="0.001" layer="94"/>
<wire x1="-1.690940625" y1="-3.234034375" x2="-1.675303125" y2="-3.245865625" width="0.001" layer="94"/>
<wire x1="-1.675303125" y1="-3.245865625" x2="-1.669871875" y2="-3.250715625" width="0.001" layer="94"/>
<wire x1="-1.669871875" y1="-3.250715625" x2="-1.670971875" y2="-3.252184375" width="0.001" layer="94"/>
<wire x1="-1.670971875" y1="-3.252184375" x2="-1.673621875" y2="-3.252475" width="0.001" layer="94"/>
<wire x1="-1.673621875" y1="-3.252475" x2="-1.845840625" y2="-3.240146875" width="0.001" layer="94"/>
<wire x1="-1.845840625" y1="-3.240146875" x2="-1.845821875" y2="-3.24010625" width="0.001" layer="94"/>
<wire x1="2.833196875" y1="-3.192396875" x2="2.829496875" y2="-3.12360625" width="0.001" layer="94"/>
<wire x1="2.829496875" y1="-3.12360625" x2="2.824628125" y2="-3.00300625" width="0.001" layer="94"/>
<wire x1="2.824628125" y1="-3.00300625" x2="2.82021875" y2="-2.891325" width="0.001" layer="94"/>
<wire x1="2.82021875" y1="-2.891325" x2="2.81761875" y2="-2.843996875" width="0.001" layer="94"/>
<wire x1="2.81761875" y1="-2.843996875" x2="2.7720875" y2="-2.844415625" width="0.001" layer="94"/>
<wire x1="2.7720875" y1="-2.844415625" x2="2.66446875" y2="-2.84755625" width="0.001" layer="94"/>
<wire x1="2.66446875" y1="-2.84755625" x2="2.450146875" y2="-2.85395625" width="0.001" layer="94"/>
<wire x1="2.450146875" y1="-2.85395625" x2="2.387646875" y2="-2.855284375" width="0.001" layer="94"/>
<wire x1="2.387646875" y1="-2.855284375" x2="2.384709375" y2="-2.792896875" width="0.001" layer="94"/>
<wire x1="2.384709375" y1="-2.792896875" x2="2.383496875" y2="-2.747925" width="0.001" layer="94"/>
<wire x1="2.383496875" y1="-2.747925" x2="2.383796875" y2="-2.733546875" width="0.001" layer="94"/>
<wire x1="2.383796875" y1="-2.733546875" x2="2.384709375" y2="-2.727425" width="0.001" layer="94"/>
<wire x1="2.384709375" y1="-2.727425" x2="2.410909375" y2="-2.725775" width="0.001" layer="94"/>
<wire x1="2.410909375" y1="-2.725775" x2="2.480046875" y2="-2.72355625" width="0.001" layer="94"/>
<wire x1="2.480046875" y1="-2.72355625" x2="2.705146875" y2="-2.718596875" width="0.001" layer="94"/>
<wire x1="2.705146875" y1="-2.718596875" x2="3.207978125" y2="-2.708646875" width="0.001" layer="94"/>
<wire x1="3.207978125" y1="-2.708646875" x2="3.503896875" y2="-2.70140625" width="0.001" layer="94"/>
<wire x1="3.503896875" y1="-2.70140625" x2="3.545146875" y2="-2.699796875" width="0.001" layer="94"/>
<wire x1="3.545146875" y1="-2.699796875" x2="3.545146875" y2="-2.73320625" width="0.001" layer="94"/>
<wire x1="3.545146875" y1="-2.73320625" x2="3.54606875" y2="-2.76350625" width="0.001" layer="94"/>
<wire x1="3.54606875" y1="-2.76350625" x2="3.548278125" y2="-2.789465625" width="0.001" layer="94"/>
<wire x1="3.548278125" y1="-2.789465625" x2="3.551409375" y2="-2.812315625" width="0.001" layer="94"/>
<wire x1="3.551409375" y1="-2.812315625" x2="3.534528125" y2="-2.814915625" width="0.001" layer="94"/>
<wire x1="3.534528125" y1="-2.814915625" x2="3.464809375" y2="-2.818934375" width="0.001" layer="94"/>
<wire x1="3.464809375" y1="-2.818934375" x2="3.320878125" y2="-2.824946875" width="0.001" layer="94"/>
<wire x1="3.320878125" y1="-2.824946875" x2="3.1812375" y2="-2.830865625" width="0.001" layer="94"/>
<wire x1="3.1812375" y1="-2.830865625" x2="3.138228125" y2="-2.833196875" width="0.001" layer="94"/>
<wire x1="3.138228125" y1="-2.833196875" x2="3.121809375" y2="-2.834675" width="0.001" layer="94"/>
<wire x1="3.121809375" y1="-2.834675" x2="3.1215375" y2="-2.849684375" width="0.001" layer="94"/>
<wire x1="3.1215375" y1="-2.849684375" x2="3.1224875" y2="-2.888865625" width="0.001" layer="94"/>
<wire x1="3.1224875" y1="-2.888865625" x2="3.127346875" y2="-3.015925" width="0.001" layer="94"/>
<wire x1="3.127346875" y1="-3.015925" x2="3.130478125" y2="-3.094596875" width="0.001" layer="94"/>
<wire x1="3.130478125" y1="-3.094596875" x2="3.131978125" y2="-3.151084375" width="0.001" layer="94"/>
<wire x1="3.131978125" y1="-3.151084375" x2="3.13186875" y2="-3.185515625" width="0.001" layer="94"/>
<wire x1="3.13186875" y1="-3.185515625" x2="3.131209375" y2="-3.194496875" width="0.001" layer="94"/>
<wire x1="3.131209375" y1="-3.194496875" x2="3.130146875" y2="-3.19800625" width="0.001" layer="94"/>
<wire x1="3.130146875" y1="-3.19800625" x2="3.119209375" y2="-3.199896875" width="0.001" layer="94"/>
<wire x1="3.119209375" y1="-3.199896875" x2="3.09316875" y2="-3.202746875" width="0.001" layer="94"/>
<wire x1="3.09316875" y1="-3.202746875" x2="3.01136875" y2="-3.209775" width="0.001" layer="94"/>
<wire x1="3.01136875" y1="-3.209775" x2="2.86686875" y2="-3.22135625" width="0.001" layer="94"/>
<wire x1="2.86686875" y1="-3.22135625" x2="2.836078125" y2="-3.224284375" width="0.001" layer="94"/>
<wire x1="2.836078125" y1="-3.224284375" x2="2.833196875" y2="-3.192396875" width="0.001" layer="94"/>
<wire x1="-3.087371875" y1="-3.132815625" x2="-3.2217625" y2="-3.11835625" width="0.001" layer="94"/>
<wire x1="-3.2217625" y1="-3.11835625" x2="-3.224703125" y2="-3.117975" width="0.001" layer="94"/>
<wire x1="-3.224703125" y1="-3.117975" x2="-3.226890625" y2="-3.11665625" width="0.001" layer="94"/>
<wire x1="-3.226890625" y1="-3.11665625" x2="-3.228390625" y2="-3.113746875" width="0.001" layer="94"/>
<wire x1="-3.228390625" y1="-3.113746875" x2="-3.229271875" y2="-3.108615625" width="0.001" layer="94"/>
<wire x1="-3.229271875" y1="-3.108615625" x2="-3.229340625" y2="-3.08910625" width="0.001" layer="94"/>
<wire x1="-3.229340625" y1="-3.08910625" x2="-3.2275625" y2="-3.053015625" width="0.001" layer="94"/>
<wire x1="-3.2275625" y1="-3.053015625" x2="-3.225790625" y2="-3.017084375" width="0.001" layer="94"/>
<wire x1="-3.225790625" y1="-3.017084375" x2="-3.225853125" y2="-2.997596875" width="0.001" layer="94"/>
<wire x1="-3.225853125" y1="-2.997596875" x2="-3.226703125" y2="-2.99245625" width="0.001" layer="94"/>
<wire x1="-3.226703125" y1="-2.99245625" x2="-3.228171875" y2="-2.98955625" width="0.001" layer="94"/>
<wire x1="-3.228171875" y1="-2.98955625" x2="-3.230303125" y2="-2.988265625" width="0.001" layer="94"/>
<wire x1="-3.230303125" y1="-2.988265625" x2="-3.233171875" y2="-2.987965625" width="0.001" layer="94"/>
<wire x1="-3.233171875" y1="-2.987965625" x2="-3.429871875" y2="-2.970475" width="0.001" layer="94"/>
<wire x1="-3.429871875" y1="-2.970475" x2="-3.618621875" y2="-2.953025" width="0.001" layer="94"/>
<wire x1="-3.618621875" y1="-2.953025" x2="-3.619503125" y2="-2.939796875" width="0.001" layer="94"/>
<wire x1="-3.619503125" y1="-2.939796875" x2="-3.619871875" y2="-2.908015625" width="0.001" layer="94"/>
<wire x1="-3.619871875" y1="-2.908015625" x2="-3.619871875" y2="-2.863015625" width="0.001" layer="94"/>
<wire x1="-3.619871875" y1="-2.863015625" x2="-3.606121875" y2="-2.863015625" width="0.001" layer="94"/>
<wire x1="-3.606121875" y1="-2.863015625" x2="-3.545740625" y2="-2.866675" width="0.001" layer="94"/>
<wire x1="-3.545740625" y1="-2.866675" x2="-3.419871875" y2="-2.875465625" width="0.001" layer="94"/>
<wire x1="-3.419871875" y1="-2.875465625" x2="-3.233621875" y2="-2.88795625" width="0.001" layer="94"/>
<wire x1="-3.233621875" y1="-2.88795625" x2="-3.219871875" y2="-2.88800625" width="0.001" layer="94"/>
<wire x1="-3.219871875" y1="-2.88800625" x2="-3.219871875" y2="-2.794484375" width="0.001" layer="94"/>
<wire x1="-3.219871875" y1="-2.794484375" x2="-3.296121875" y2="-2.791065625" width="0.001" layer="94"/>
<wire x1="-3.296121875" y1="-2.791065625" x2="-3.549871875" y2="-2.778034375" width="0.001" layer="94"/>
<wire x1="-3.549871875" y1="-2.778034375" x2="-3.754871875" y2="-2.767175" width="0.001" layer="94"/>
<wire x1="-3.754871875" y1="-2.767175" x2="-3.782371875" y2="-2.765946875" width="0.001" layer="94"/>
<wire x1="-3.782371875" y1="-2.765946875" x2="-3.785390625" y2="-2.904575" width="0.001" layer="94"/>
<wire x1="-3.785390625" y1="-2.904575" x2="-3.788390625" y2="-3.003346875" width="0.001" layer="94"/>
<wire x1="-3.788390625" y1="-3.003346875" x2="-3.7899625" y2="-3.03405625" width="0.001" layer="94"/>
<wire x1="-3.7899625" y1="-3.03405625" x2="-3.79133125" y2="-3.046125" width="0.001" layer="94"/>
<wire x1="-3.79133125" y1="-3.046125" x2="-3.80373125" y2="-3.04575625" width="0.001" layer="94"/>
<wire x1="-3.80373125" y1="-3.04575625" x2="-3.835340625" y2="-3.042584375" width="0.001" layer="94"/>
<wire x1="-3.835340625" y1="-3.042584375" x2="-3.937053125" y2="-3.029975" width="0.001" layer="94"/>
<wire x1="-3.937053125" y1="-3.029975" x2="-4.079871875" y2="-3.010915625" width="0.001" layer="94"/>
<wire x1="-4.079871875" y1="-3.010915625" x2="-4.079871875" y2="-2.736825" width="0.001" layer="94"/>
<wire x1="-4.079871875" y1="-2.736825" x2="-4.061121875" y2="-2.718175" width="0.001" layer="94"/>
<wire x1="-4.061121875" y1="-2.718175" x2="-4.05148125" y2="-2.70995625" width="0.001" layer="94"/>
<wire x1="-4.05148125" y1="-2.70995625" x2="-4.03948125" y2="-2.701796875" width="0.001" layer="94"/>
<wire x1="-4.03948125" y1="-2.701796875" x2="-4.026403125" y2="-2.694475" width="0.001" layer="94"/>
<wire x1="-4.026403125" y1="-2.694475" x2="-4.0135125" y2="-2.688796875" width="0.001" layer="94"/>
<wire x1="-4.0135125" y1="-2.688796875" x2="-3.99308125" y2="-2.681746875" width="0.001" layer="94"/>
<wire x1="-3.99308125" y1="-2.681746875" x2="-3.97298125" y2="-2.676384375" width="0.001" layer="94"/>
<wire x1="-3.97298125" y1="-2.676384375" x2="-3.950890625" y2="-2.672584375" width="0.001" layer="94"/>
<wire x1="-3.950890625" y1="-2.672584375" x2="-3.924553125" y2="-2.670215625" width="0.001" layer="94"/>
<wire x1="-3.924553125" y1="-2.670215625" x2="-3.891640625" y2="-2.669134375" width="0.001" layer="94"/>
<wire x1="-3.891640625" y1="-2.669134375" x2="-3.84988125" y2="-2.669225" width="0.001" layer="94"/>
<wire x1="-3.84988125" y1="-2.669225" x2="-3.7306125" y2="-2.672334375" width="0.001" layer="94"/>
<wire x1="-3.7306125" y1="-2.672334375" x2="-3.314871875" y2="-2.685575" width="0.001" layer="94"/>
<wire x1="-3.314871875" y1="-2.685575" x2="-3.169553125" y2="-2.69070625" width="0.001" layer="94"/>
<wire x1="-3.169553125" y1="-2.69070625" x2="-3.121021875" y2="-2.69330625" width="0.001" layer="94"/>
<wire x1="-3.121021875" y1="-2.69330625" x2="-3.084503125" y2="-2.696375" width="0.001" layer="94"/>
<wire x1="-3.084503125" y1="-2.696375" x2="-3.056921875" y2="-2.70025625" width="0.001" layer="94"/>
<wire x1="-3.056921875" y1="-2.70025625" x2="-3.0352125" y2="-2.70525625" width="0.001" layer="94"/>
<wire x1="-3.0352125" y1="-2.70525625" x2="-3.016303125" y2="-2.711725" width="0.001" layer="94"/>
<wire x1="-3.016303125" y1="-2.711725" x2="-2.997140625" y2="-2.719996875" width="0.001" layer="94"/>
<wire x1="-2.997140625" y1="-2.719996875" x2="-2.98583125" y2="-2.725796875" width="0.001" layer="94"/>
<wire x1="-2.98583125" y1="-2.725796875" x2="-2.9736625" y2="-2.733096875" width="0.001" layer="94"/>
<wire x1="-2.9736625" y1="-2.733096875" x2="-2.962121875" y2="-2.740946875" width="0.001" layer="94"/>
<wire x1="-2.962121875" y1="-2.740946875" x2="-2.952653125" y2="-2.748425" width="0.001" layer="94"/>
<wire x1="-2.952653125" y1="-2.748425" x2="-2.941421875" y2="-2.758715625" width="0.001" layer="94"/>
<wire x1="-2.941421875" y1="-2.758715625" x2="-2.93303125" y2="-2.768675" width="0.001" layer="94"/>
<wire x1="-2.93303125" y1="-2.768675" x2="-2.929803125" y2="-2.774246875" width="0.001" layer="94"/>
<wire x1="-2.929803125" y1="-2.774246875" x2="-2.927171875" y2="-2.780565625" width="0.001" layer="94"/>
<wire x1="-2.927171875" y1="-2.780565625" x2="-2.92508125" y2="-2.787934375" width="0.001" layer="94"/>
<wire x1="-2.92508125" y1="-2.787934375" x2="-2.9235125" y2="-2.796625" width="0.001" layer="94"/>
<wire x1="-2.9235125" y1="-2.796625" x2="-2.921740625" y2="-2.819115625" width="0.001" layer="94"/>
<wire x1="-2.921740625" y1="-2.819115625" x2="-2.921553125" y2="-2.850275" width="0.001" layer="94"/>
<wire x1="-2.921553125" y1="-2.850275" x2="-2.924621875" y2="-2.94760625" width="0.001" layer="94"/>
<wire x1="-2.924621875" y1="-2.94760625" x2="-2.929371875" y2="-3.059765625" width="0.001" layer="94"/>
<wire x1="-2.929371875" y1="-3.059765625" x2="-2.9329125" y2="-3.121746875" width="0.001" layer="94"/>
<wire x1="-2.9329125" y1="-3.121746875" x2="-2.933540625" y2="-3.132015625" width="0.001" layer="94"/>
<wire x1="-2.933540625" y1="-3.132015625" x2="-2.934640625" y2="-3.13940625" width="0.001" layer="94"/>
<wire x1="-2.934640625" y1="-3.13940625" x2="-2.93613125" y2="-3.142065625" width="0.001" layer="94"/>
<wire x1="-2.93613125" y1="-3.142065625" x2="-2.938653125" y2="-3.144075" width="0.001" layer="94"/>
<wire x1="-2.938653125" y1="-3.144075" x2="-2.9425125" y2="-3.145446875" width="0.001" layer="94"/>
<wire x1="-2.9425125" y1="-3.145446875" x2="-2.948003125" y2="-3.14620625" width="0.001" layer="94"/>
<wire x1="-2.948003125" y1="-3.14620625" x2="-2.9651125" y2="-3.145965625" width="0.001" layer="94"/>
<wire x1="-2.9651125" y1="-3.145965625" x2="-2.992421875" y2="-3.143534375" width="0.001" layer="94"/>
<wire x1="-2.992421875" y1="-3.143534375" x2="-3.087371875" y2="-3.13280625" width="0.001" layer="94"/>
<wire x1="-3.087371875" y1="-3.13280625" x2="-3.087371875" y2="-3.132815625" width="0.001" layer="94"/>
<wire x1="3.695028125" y1="-3.119625" x2="3.692809375" y2="-3.04985625" width="0.001" layer="94"/>
<wire x1="3.692809375" y1="-3.04985625" x2="3.6876375" y2="-2.923015625" width="0.001" layer="94"/>
<wire x1="3.6876375" y1="-2.923015625" x2="3.68246875" y2="-2.79640625" width="0.001" layer="94"/>
<wire x1="3.68246875" y1="-2.79640625" x2="3.680246875" y2="-2.727215625" width="0.001" layer="94"/>
<wire x1="3.680246875" y1="-2.727215625" x2="3.6801375" y2="-2.69890625" width="0.001" layer="94"/>
<wire x1="3.6801375" y1="-2.69890625" x2="3.7213875" y2="-2.696125" width="0.001" layer="94"/>
<wire x1="3.7213875" y1="-2.696125" x2="3.782259375" y2="-2.693346875" width="0.001" layer="94"/>
<wire x1="3.782259375" y1="-2.693346875" x2="3.870678125" y2="-2.69055625" width="0.001" layer="94"/>
<wire x1="3.870678125" y1="-2.69055625" x2="3.97871875" y2="-2.68775625" width="0.001" layer="94"/>
<wire x1="3.97871875" y1="-2.68775625" x2="3.9819875" y2="-2.786634375" width="0.001" layer="94"/>
<wire x1="3.9819875" y1="-2.786634375" x2="3.98841875" y2="-2.947396875" width="0.001" layer="94"/>
<wire x1="3.98841875" y1="-2.947396875" x2="3.99156875" y2="-3.009275" width="0.001" layer="94"/>
<wire x1="3.99156875" y1="-3.009275" x2="4.034609375" y2="-3.005834375" width="0.001" layer="94"/>
<wire x1="4.034609375" y1="-3.005834375" x2="4.195278125" y2="-2.990746875" width="0.001" layer="94"/>
<wire x1="4.195278125" y1="-2.990746875" x2="4.446996875" y2="-2.965396875" width="0.001" layer="94"/>
<wire x1="4.446996875" y1="-2.965396875" x2="4.6898375" y2="-2.940034375" width="0.001" layer="94"/>
<wire x1="4.6898375" y1="-2.940034375" x2="4.8238875" y2="-2.92490625" width="0.001" layer="94"/>
<wire x1="4.8238875" y1="-2.92490625" x2="4.8451375" y2="-2.921875" width="0.001" layer="94"/>
<wire x1="4.8451375" y1="-2.921875" x2="4.8451375" y2="-2.964846875" width="0.001" layer="94"/>
<wire x1="4.8451375" y1="-2.964846875" x2="4.84486875" y2="-2.98870625" width="0.001" layer="94"/>
<wire x1="4.84486875" y1="-2.98870625" x2="4.8434875" y2="-3.001884375" width="0.001" layer="94"/>
<wire x1="4.8434875" y1="-3.001884375" x2="4.842109375" y2="-3.005575" width="0.001" layer="94"/>
<wire x1="4.842109375" y1="-3.005575" x2="4.84011875" y2="-3.007925" width="0.001" layer="94"/>
<wire x1="4.84011875" y1="-3.007925" x2="4.83741875" y2="-3.009365625" width="0.001" layer="94"/>
<wire x1="4.83741875" y1="-3.009365625" x2="4.8338875" y2="-3.010334375" width="0.001" layer="94"/>
<wire x1="4.8338875" y1="-3.010334375" x2="4.796259375" y2="-3.01645625" width="0.001" layer="94"/>
<wire x1="4.796259375" y1="-3.01645625" x2="4.721796875" y2="-3.026775" width="0.001" layer="94"/>
<wire x1="4.721796875" y1="-3.026775" x2="4.496059375" y2="-3.05580625" width="0.001" layer="94"/>
<wire x1="4.496059375" y1="-3.05580625" x2="4.223928125" y2="-3.089015625" width="0.001" layer="94"/>
<wire x1="4.223928125" y1="-3.089015625" x2="3.9726375" y2="-3.11800625" width="0.001" layer="94"/>
<wire x1="3.9726375" y1="-3.11800625" x2="3.7213875" y2="-3.145775" width="0.001" layer="94"/>
<wire x1="3.7213875" y1="-3.145775" x2="3.6951375" y2="-3.148734375" width="0.001" layer="94"/>
<wire x1="3.6951375" y1="-3.148734375" x2="3.695028125" y2="-3.119625" width="0.001" layer="94"/>
<wire x1="-2.10508125" y1="-2.128365625" x2="-2.120503125" y2="-2.124396875" width="0.001" layer="94"/>
<wire x1="-2.120503125" y1="-2.124396875" x2="-2.143253125" y2="-2.117025" width="0.001" layer="94"/>
<wire x1="-2.143253125" y1="-2.117025" x2="-2.205403125" y2="-2.094134375" width="0.001" layer="94"/>
<wire x1="-2.205403125" y1="-2.094134375" x2="-2.280821875" y2="-2.063815625" width="0.001" layer="94"/>
<wire x1="-2.280821875" y1="-2.063815625" x2="-2.358840625" y2="-2.030225" width="0.001" layer="94"/>
<wire x1="-2.358840625" y1="-2.030225" x2="-2.433253125" y2="-1.995515625" width="0.001" layer="94"/>
<wire x1="-2.433253125" y1="-1.995515625" x2="-2.46743125" y2="-1.97835625" width="0.001" layer="94"/>
<wire x1="-2.46743125" y1="-1.97835625" x2="-2.500171875" y2="-1.961034375" width="0.001" layer="94"/>
<wire x1="-2.500171875" y1="-1.961034375" x2="-2.531890625" y2="-1.94330625" width="0.001" layer="94"/>
<wire x1="-2.531890625" y1="-1.94330625" x2="-2.563003125" y2="-1.92495625" width="0.001" layer="94"/>
<wire x1="-2.563003125" y1="-1.92495625" x2="-2.59393125" y2="-1.905734375" width="0.001" layer="94"/>
<wire x1="-2.59393125" y1="-1.905734375" x2="-2.6251125" y2="-1.885425" width="0.001" layer="94"/>
<wire x1="-2.6251125" y1="-1.885425" x2="-2.6772125" y2="-1.849475" width="0.001" layer="94"/>
<wire x1="-2.6772125" y1="-1.849475" x2="-2.722190625" y2="-1.815634375" width="0.001" layer="94"/>
<wire x1="-2.722190625" y1="-1.815634375" x2="-2.742171875" y2="-1.799334375" width="0.001" layer="94"/>
<wire x1="-2.742171875" y1="-1.799334375" x2="-2.760540625" y2="-1.78335625" width="0.001" layer="94"/>
<wire x1="-2.760540625" y1="-1.78335625" x2="-2.777390625" y2="-1.767646875" width="0.001" layer="94"/>
<wire x1="-2.777390625" y1="-1.767646875" x2="-2.7927625" y2="-1.752134375" width="0.001" layer="94"/>
<wire x1="-2.7927625" y1="-1.752134375" x2="-2.80673125" y2="-1.73675625" width="0.001" layer="94"/>
<wire x1="-2.80673125" y1="-1.73675625" x2="-2.8193625" y2="-1.721434375" width="0.001" layer="94"/>
<wire x1="-2.8193625" y1="-1.721434375" x2="-2.830703125" y2="-1.70610625" width="0.001" layer="94"/>
<wire x1="-2.830703125" y1="-1.70610625" x2="-2.840821875" y2="-1.690725" width="0.001" layer="94"/>
<wire x1="-2.840821875" y1="-1.690725" x2="-2.849790625" y2="-1.675196875" width="0.001" layer="94"/>
<wire x1="-2.849790625" y1="-1.675196875" x2="-2.8576625" y2="-1.659475" width="0.001" layer="94"/>
<wire x1="-2.8576625" y1="-1.659475" x2="-2.864503125" y2="-1.643496875" width="0.001" layer="94"/>
<wire x1="-2.864503125" y1="-1.643496875" x2="-2.870371875" y2="-1.627175" width="0.001" layer="94"/>
<wire x1="-2.870371875" y1="-1.627175" x2="-2.874940625" y2="-1.610946875" width="0.001" layer="94"/>
<wire x1="-2.874940625" y1="-1.610946875" x2="-2.878403125" y2="-1.592875" width="0.001" layer="94"/>
<wire x1="-2.878403125" y1="-1.592875" x2="-2.880790625" y2="-1.571896875" width="0.001" layer="94"/>
<wire x1="-2.880790625" y1="-1.571896875" x2="-2.882140625" y2="-1.54690625" width="0.001" layer="94"/>
<wire x1="-2.882140625" y1="-1.54690625" x2="-2.88248125" y2="-1.516834375" width="0.001" layer="94"/>
<wire x1="-2.88248125" y1="-1.516834375" x2="-2.881853125" y2="-1.48060625" width="0.001" layer="94"/>
<wire x1="-2.881853125" y1="-1.48060625" x2="-2.8778125" y2="-1.38535625" width="0.001" layer="94"/>
<wire x1="-2.8778125" y1="-1.38535625" x2="-2.876003125" y2="-1.343615625" width="0.001" layer="94"/>
<wire x1="-2.876003125" y1="-1.343615625" x2="-2.87568125" y2="-1.314234375" width="0.001" layer="94"/>
<wire x1="-2.87568125" y1="-1.314234375" x2="-2.876903125" y2="-1.294325" width="0.001" layer="94"/>
<wire x1="-2.876903125" y1="-1.294325" x2="-2.879721875" y2="-1.28100625" width="0.001" layer="94"/>
<wire x1="-2.879721875" y1="-1.28100625" x2="-2.885040625" y2="-1.270265625" width="0.001" layer="94"/>
<wire x1="-2.885040625" y1="-1.270265625" x2="-2.894790625" y2="-1.255834375" width="0.001" layer="94"/>
<wire x1="-2.894790625" y1="-1.255834375" x2="-2.907990625" y2="-1.239034375" width="0.001" layer="94"/>
<wire x1="-2.907990625" y1="-1.239034375" x2="-2.923671875" y2="-1.221184375" width="0.001" layer="94"/>
<wire x1="-2.923671875" y1="-1.221184375" x2="-2.9462625" y2="-1.195246875" width="0.001" layer="94"/>
<wire x1="-2.9462625" y1="-1.195246875" x2="-2.9557625" y2="-1.182884375" width="0.001" layer="94"/>
<wire x1="-2.9557625" y1="-1.182884375" x2="-2.964121875" y2="-1.170696875" width="0.001" layer="94"/>
<wire x1="-2.964121875" y1="-1.170696875" x2="-2.9714125" y2="-1.158496875" width="0.001" layer="94"/>
<wire x1="-2.9714125" y1="-1.158496875" x2="-2.977671875" y2="-1.14610625" width="0.001" layer="94"/>
<wire x1="-2.977671875" y1="-1.14610625" x2="-2.982940625" y2="-1.133346875" width="0.001" layer="94"/>
<wire x1="-2.982940625" y1="-1.133346875" x2="-2.987290625" y2="-1.12005625" width="0.001" layer="94"/>
<wire x1="-2.987290625" y1="-1.12005625" x2="-2.9907625" y2="-1.106046875" width="0.001" layer="94"/>
<wire x1="-2.9907625" y1="-1.106046875" x2="-2.9934125" y2="-1.091134375" width="0.001" layer="94"/>
<wire x1="-2.9934125" y1="-1.091134375" x2="-2.99528125" y2="-1.07515625" width="0.001" layer="94"/>
<wire x1="-2.99528125" y1="-1.07515625" x2="-2.99643125" y2="-1.057925" width="0.001" layer="94"/>
<wire x1="-2.99643125" y1="-1.057925" x2="-2.996753125" y2="-1.019015625" width="0.001" layer="94"/>
<wire x1="-2.996753125" y1="-1.019015625" x2="-2.99478125" y2="-0.972975" width="0.001" layer="94"/>
<wire x1="-2.99478125" y1="-0.972975" x2="-2.99108125" y2="-0.919015625" width="0.001" layer="94"/>
<wire x1="-2.99108125" y1="-0.919015625" x2="-2.986521875" y2="-0.871825" width="0.001" layer="94"/>
<wire x1="-2.986521875" y1="-0.871825" x2="-2.980503125" y2="-0.828684375" width="0.001" layer="94"/>
<wire x1="-2.980503125" y1="-0.828684375" x2="-2.972403125" y2="-0.78685625" width="0.001" layer="94"/>
<wire x1="-2.972403125" y1="-0.78685625" x2="-2.961621875" y2="-0.74360625" width="0.001" layer="94"/>
<wire x1="-2.961621875" y1="-0.74360625" x2="-2.947553125" y2="-0.69620625" width="0.001" layer="94"/>
<wire x1="-2.947553125" y1="-0.69620625" x2="-2.92958125" y2="-0.641925" width="0.001" layer="94"/>
<wire x1="-2.92958125" y1="-0.641925" x2="-2.907121875" y2="-0.578034375" width="0.001" layer="94"/>
<wire x1="-2.907121875" y1="-0.578034375" x2="-2.8725625" y2="-0.480425" width="0.001" layer="94"/>
<wire x1="-2.8725625" y1="-0.480425" x2="-2.845671875" y2="-0.401525" width="0.001" layer="94"/>
<wire x1="-2.845671875" y1="-0.401525" x2="-2.825753125" y2="-0.338496875" width="0.001" layer="94"/>
<wire x1="-2.825753125" y1="-0.338496875" x2="-2.81213125" y2="-0.288475" width="0.001" layer="94"/>
<wire x1="-2.81213125" y1="-0.288475" x2="-2.807471875" y2="-0.267446875" width="0.001" layer="94"/>
<wire x1="-2.807471875" y1="-0.267446875" x2="-2.804121875" y2="-0.24860625" width="0.001" layer="94"/>
<wire x1="-2.804121875" y1="-0.24860625" x2="-2.8020125" y2="-0.231596875" width="0.001" layer="94"/>
<wire x1="-2.8020125" y1="-0.231596875" x2="-2.801040625" y2="-0.21605625" width="0.001" layer="94"/>
<wire x1="-2.801040625" y1="-0.21605625" x2="-2.801121875" y2="-0.201625" width="0.001" layer="94"/>
<wire x1="-2.801121875" y1="-0.201625" x2="-2.80218125" y2="-0.18795625" width="0.001" layer="94"/>
<wire x1="-2.80218125" y1="-0.18795625" x2="-2.80413125" y2="-0.174696875" width="0.001" layer="94"/>
<wire x1="-2.80413125" y1="-0.174696875" x2="-2.80688125" y2="-0.161465625" width="0.001" layer="94"/>
<wire x1="-2.80688125" y1="-0.161465625" x2="-2.812003125" y2="-0.142025" width="0.001" layer="94"/>
<wire x1="-2.812003125" y1="-0.142025" x2="-2.81798125" y2="-0.124184375" width="0.001" layer="94"/>
<wire x1="-2.81798125" y1="-0.124184375" x2="-2.825171875" y2="-0.107384375" width="0.001" layer="94"/>
<wire x1="-2.825171875" y1="-0.107384375" x2="-2.833921875" y2="-0.091025" width="0.001" layer="94"/>
<wire x1="-2.833921875" y1="-0.091025" x2="-2.84458125" y2="-0.074525" width="0.001" layer="94"/>
<wire x1="-2.84458125" y1="-0.074525" x2="-2.85748125" y2="-0.057296875" width="0.001" layer="94"/>
<wire x1="-2.85748125" y1="-0.057296875" x2="-2.8729625" y2="-0.038775" width="0.001" layer="94"/>
<wire x1="-2.8729625" y1="-0.038775" x2="-2.891390625" y2="-0.01835625" width="0.001" layer="94"/>
<wire x1="-2.891390625" y1="-0.01835625" x2="-2.9110625" y2="0.003303125" width="0.001" layer="94"/>
<wire x1="-2.9110625" y1="0.003303125" x2="-2.927740625" y2="0.022784375" width="0.001" layer="94"/>
<wire x1="-2.927740625" y1="0.022784375" x2="-2.941840625" y2="0.040725" width="0.001" layer="94"/>
<wire x1="-2.941840625" y1="0.040725" x2="-2.953790625" y2="0.057753125" width="0.001" layer="94"/>
<wire x1="-2.953790625" y1="0.057753125" x2="-2.96398125" y2="0.074515625" width="0.001" layer="94"/>
<wire x1="-2.96398125" y1="0.074515625" x2="-2.972853125" y2="0.09164375" width="0.001" layer="94"/>
<wire x1="-2.972853125" y1="0.09164375" x2="-2.980803125" y2="0.109765625" width="0.001" layer="94"/>
<wire x1="-2.980803125" y1="0.109765625" x2="-2.988253125" y2="0.129515625" width="0.001" layer="94"/>
<wire x1="-2.988253125" y1="0.129515625" x2="-3.002371875" y2="0.169484375" width="0.001" layer="94"/>
<wire x1="-3.002371875" y1="0.169484375" x2="-3.003790625" y2="0.50324375" width="0.001" layer="94"/>
<wire x1="-3.003790625" y1="0.50324375" x2="-3.0052125" y2="0.837003125" width="0.001" layer="94"/>
<wire x1="-3.0052125" y1="0.837003125" x2="-2.992940625" y2="0.872275" width="0.001" layer="94"/>
<wire x1="-2.992940625" y1="0.872275" x2="-2.985590625" y2="0.891584375" width="0.001" layer="94"/>
<wire x1="-2.985590625" y1="0.891584375" x2="-2.9771625" y2="0.910303125" width="0.001" layer="94"/>
<wire x1="-2.9771625" y1="0.910303125" x2="-2.9677125" y2="0.92839375" width="0.001" layer="94"/>
<wire x1="-2.9677125" y1="0.92839375" x2="-2.957253125" y2="0.94579375" width="0.001" layer="94"/>
<wire x1="-2.957253125" y1="0.94579375" x2="-2.945840625" y2="0.96244375" width="0.001" layer="94"/>
<wire x1="-2.945840625" y1="0.96244375" x2="-2.933503125" y2="0.97829375" width="0.001" layer="94"/>
<wire x1="-2.933503125" y1="0.97829375" x2="-2.920271875" y2="0.993275" width="0.001" layer="94"/>
<wire x1="-2.920271875" y1="0.993275" x2="-2.906203125" y2="1.007334375" width="0.001" layer="94"/>
<wire x1="-2.906203125" y1="1.007334375" x2="-2.889290625" y2="1.021534375" width="0.001" layer="94"/>
<wire x1="-2.889290625" y1="1.021534375" x2="-2.8621125" y2="1.04259375" width="0.001" layer="94"/>
<wire x1="-2.8621125" y1="1.04259375" x2="-2.79183125" y2="1.094525" width="0.001" layer="94"/>
<wire x1="-2.79183125" y1="1.094525" x2="-2.725240625" y2="1.141515625" width="0.001" layer="94"/>
<wire x1="-2.725240625" y1="1.141515625" x2="-2.70268125" y2="1.156415625" width="0.001" layer="94"/>
<wire x1="-2.70268125" y1="1.156415625" x2="-2.692240625" y2="1.161984375" width="0.001" layer="94"/>
<wire x1="-2.692240625" y1="1.161984375" x2="-2.691321875" y2="1.160753125" width="0.001" layer="94"/>
<wire x1="-2.691321875" y1="1.160753125" x2="-2.690571875" y2="1.15739375" width="0.001" layer="94"/>
<wire x1="-2.690571875" y1="1.15739375" x2="-2.689871875" y2="1.146365625" width="0.001" layer="94"/>
<wire x1="-2.689871875" y1="1.146365625" x2="-2.688053125" y2="1.124553125" width="0.001" layer="94"/>
<wire x1="-2.688053125" y1="1.124553125" x2="-2.683053125" y2="1.094853125" width="0.001" layer="94"/>
<wire x1="-2.683053125" y1="1.094853125" x2="-2.675553125" y2="1.05984375" width="0.001" layer="94"/>
<wire x1="-2.675553125" y1="1.05984375" x2="-2.666221875" y2="1.022115625" width="0.001" layer="94"/>
<wire x1="-2.666221875" y1="1.022115625" x2="-2.655740625" y2="0.98424375" width="0.001" layer="94"/>
<wire x1="-2.655740625" y1="0.98424375" x2="-2.644803125" y2="0.948834375" width="0.001" layer="94"/>
<wire x1="-2.644803125" y1="0.948834375" x2="-2.6340625" y2="0.918465625" width="0.001" layer="94"/>
<wire x1="-2.6340625" y1="0.918465625" x2="-2.624203125" y2="0.895734375" width="0.001" layer="94"/>
<wire x1="-2.624203125" y1="0.895734375" x2="-2.61848125" y2="0.885303125" width="0.001" layer="94"/>
<wire x1="-2.61848125" y1="0.885303125" x2="-2.6139125" y2="0.879025" width="0.001" layer="94"/>
<wire x1="-2.6139125" y1="0.879025" x2="-2.612053125" y2="0.87749375" width="0.001" layer="94"/>
<wire x1="-2.612053125" y1="0.87749375" x2="-2.610440625" y2="0.877075" width="0.001" layer="94"/>
<wire x1="-2.610440625" y1="0.877075" x2="-2.609103125" y2="0.877775" width="0.001" layer="94"/>
<wire x1="-2.609103125" y1="0.877775" x2="-2.608003125" y2="0.879615625" width="0.001" layer="94"/>
<wire x1="-2.608003125" y1="0.879615625" x2="-2.606521875" y2="0.886834375" width="0.001" layer="94"/>
<wire x1="-2.606521875" y1="0.886834375" x2="-2.605921875" y2="0.89889375" width="0.001" layer="94"/>
<wire x1="-2.605921875" y1="0.89889375" x2="-2.607103125" y2="0.938234375" width="0.001" layer="94"/>
<wire x1="-2.607103125" y1="0.938234375" x2="-2.60808125" y2="0.963934375" width="0.001" layer="94"/>
<wire x1="-2.60808125" y1="0.963934375" x2="-2.608121875" y2="0.984703125" width="0.001" layer="94"/>
<wire x1="-2.608121875" y1="0.984703125" x2="-2.607240625" y2="0.999353125" width="0.001" layer="94"/>
<wire x1="-2.607240625" y1="0.999353125" x2="-2.60548125" y2="1.006665625" width="0.001" layer="94"/>
<wire x1="-2.60548125" y1="1.006665625" x2="-2.60138125" y2="1.010525" width="0.001" layer="94"/>
<wire x1="-2.60138125" y1="1.010525" x2="-2.598953125" y2="1.011253125" width="0.001" layer="94"/>
<wire x1="-2.598953125" y1="1.011253125" x2="-2.596221875" y2="1.011153125" width="0.001" layer="94"/>
<wire x1="-2.596221875" y1="1.011153125" x2="-2.589771875" y2="1.008315625" width="0.001" layer="94"/>
<wire x1="-2.589771875" y1="1.008315625" x2="-2.581790625" y2="1.00179375" width="0.001" layer="94"/>
<wire x1="-2.581790625" y1="1.00179375" x2="-2.572053125" y2="0.991365625" width="0.001" layer="94"/>
<wire x1="-2.572053125" y1="0.991365625" x2="-2.560303125" y2="0.97679375" width="0.001" layer="94"/>
<wire x1="-2.560303125" y1="0.97679375" x2="-2.529871875" y2="0.934353125" width="0.001" layer="94"/>
<wire x1="-2.529871875" y1="0.934353125" x2="-2.494271875" y2="0.884825" width="0.001" layer="94"/>
<wire x1="-2.494271875" y1="0.884825" x2="-2.481840625" y2="0.869234375" width="0.001" layer="94"/>
<wire x1="-2.481840625" y1="0.869234375" x2="-2.475371875" y2="0.862903125" width="0.001" layer="94"/>
<wire x1="-2.475371875" y1="0.862903125" x2="-2.473721875" y2="0.862684375" width="0.001" layer="94"/>
<wire x1="-2.473721875" y1="0.862684375" x2="-2.4724125" y2="0.863234375" width="0.001" layer="94"/>
<wire x1="-2.4724125" y1="0.863234375" x2="-2.4708625" y2="0.866665625" width="0.001" layer="94"/>
<wire x1="-2.4708625" y1="0.866665625" x2="-2.470753125" y2="0.873353125" width="0.001" layer="94"/>
<wire x1="-2.470753125" y1="0.873353125" x2="-2.4721125" y2="0.883465625" width="0.001" layer="94"/>
<wire x1="-2.4721125" y1="0.883465625" x2="-2.479340625" y2="0.91454375" width="0.001" layer="94"/>
<wire x1="-2.479340625" y1="0.91454375" x2="-2.4927625" y2="0.961125" width="0.001" layer="94"/>
<wire x1="-2.4927625" y1="0.961125" x2="-2.504853125" y2="1.003653125" width="0.001" layer="94"/>
<wire x1="-2.504853125" y1="1.003653125" x2="-2.51213125" y2="1.035353125" width="0.001" layer="94"/>
<wire x1="-2.51213125" y1="1.035353125" x2="-2.5139125" y2="1.046803125" width="0.001" layer="94"/>
<wire x1="-2.5139125" y1="1.046803125" x2="-2.5144125" y2="1.05514375" width="0.001" layer="94"/>
<wire x1="-2.5144125" y1="1.05514375" x2="-2.5136125" y2="1.060253125" width="0.001" layer="94"/>
<wire x1="-2.5136125" y1="1.060253125" x2="-2.5127125" y2="1.06154375" width="0.001" layer="94"/>
<wire x1="-2.5127125" y1="1.06154375" x2="-2.51148125" y2="1.061984375" width="0.001" layer="94"/>
<wire x1="-2.51148125" y1="1.061984375" x2="-2.506840625" y2="1.060384375" width="0.001" layer="94"/>
<wire x1="-2.506840625" y1="1.060384375" x2="-2.5002125" y2="1.054934375" width="0.001" layer="94"/>
<wire x1="-2.5002125" y1="1.054934375" x2="-2.490871875" y2="1.044684375" width="0.001" layer="94"/>
<wire x1="-2.490871875" y1="1.044684375" x2="-2.478071875" y2="1.02864375" width="0.001" layer="94"/>
<wire x1="-2.478071875" y1="1.02864375" x2="-2.439103125" y2="0.975365625" width="0.001" layer="94"/>
<wire x1="-2.439103125" y1="0.975365625" x2="-2.377371875" y2="0.887353125" width="0.001" layer="94"/>
<wire x1="-2.377371875" y1="0.887353125" x2="-2.360940625" y2="0.864325" width="0.001" layer="94"/>
<wire x1="-2.360940625" y1="0.864325" x2="-2.348453125" y2="0.848184375" width="0.001" layer="94"/>
<wire x1="-2.348453125" y1="0.848184375" x2="-2.339890625" y2="0.83894375" width="0.001" layer="94"/>
<wire x1="-2.339890625" y1="0.83894375" x2="-2.337071875" y2="0.836925" width="0.001" layer="94"/>
<wire x1="-2.337071875" y1="0.836925" x2="-2.33523125" y2="0.836625" width="0.001" layer="94"/>
<wire x1="-2.33523125" y1="0.836625" x2="-2.334353125" y2="0.838065625" width="0.001" layer="94"/>
<wire x1="-2.334353125" y1="0.838065625" x2="-2.33443125" y2="0.841234375" width="0.001" layer="94"/>
<wire x1="-2.33443125" y1="0.841234375" x2="-2.33748125" y2="0.852775" width="0.001" layer="94"/>
<wire x1="-2.33748125" y1="0.852775" x2="-2.344353125" y2="0.871253125" width="0.001" layer="94"/>
<wire x1="-2.344353125" y1="0.871253125" x2="-2.3550125" y2="0.896703125" width="0.001" layer="94"/>
<wire x1="-2.3550125" y1="0.896703125" x2="-2.371503125" y2="0.937775" width="0.001" layer="94"/>
<wire x1="-2.371503125" y1="0.937775" x2="-2.38443125" y2="0.977334375" width="0.001" layer="94"/>
<wire x1="-2.38443125" y1="0.977334375" x2="-2.39453125" y2="1.017903125" width="0.001" layer="94"/>
<wire x1="-2.39453125" y1="1.017903125" x2="-2.4025125" y2="1.061984375" width="0.001" layer="94"/>
<wire x1="-2.4025125" y1="1.061984375" x2="-2.405121875" y2="1.084084375" width="0.001" layer="94"/>
<wire x1="-2.405121875" y1="1.084084375" x2="-2.4072125" y2="1.111525" width="0.001" layer="94"/>
<wire x1="-2.4072125" y1="1.111525" x2="-2.409621875" y2="1.17244375" width="0.001" layer="94"/>
<wire x1="-2.409621875" y1="1.17244375" x2="-2.409853125" y2="1.20094375" width="0.001" layer="94"/>
<wire x1="-2.409853125" y1="1.20094375" x2="-2.4093625" y2="1.224825" width="0.001" layer="94"/>
<wire x1="-2.4093625" y1="1.224825" x2="-2.4081125" y2="1.241584375" width="0.001" layer="94"/>
<wire x1="-2.4081125" y1="1.241584375" x2="-2.407190625" y2="1.246525" width="0.001" layer="94"/>
<wire x1="-2.407190625" y1="1.246525" x2="-2.4060625" y2="1.248753125" width="0.001" layer="94"/>
<wire x1="-2.4060625" y1="1.248753125" x2="-2.401040625" y2="1.246725" width="0.001" layer="94"/>
<wire x1="-2.401040625" y1="1.246725" x2="-2.390203125" y2="1.239475" width="0.001" layer="94"/>
<wire x1="-2.390203125" y1="1.239475" x2="-2.357440625" y2="1.213853125" width="0.001" layer="94"/>
<wire x1="-2.357440625" y1="1.213853125" x2="-2.324271875" y2="1.187303125" width="0.001" layer="94"/>
<wire x1="-2.324271875" y1="1.187303125" x2="-2.289990625" y2="1.161925" width="0.001" layer="94"/>
<wire x1="-2.289990625" y1="1.161925" x2="-2.25463125" y2="1.137715625" width="0.001" layer="94"/>
<wire x1="-2.25463125" y1="1.137715625" x2="-2.2182125" y2="1.11469375" width="0.001" layer="94"/>
<wire x1="-2.2182125" y1="1.11469375" x2="-2.180740625" y2="1.092865625" width="0.001" layer="94"/>
<wire x1="-2.180740625" y1="1.092865625" x2="-2.142253125" y2="1.072234375" width="0.001" layer="94"/>
<wire x1="-2.142253125" y1="1.072234375" x2="-2.102753125" y2="1.052803125" width="0.001" layer="94"/>
<wire x1="-2.102753125" y1="1.052803125" x2="-2.062253125" y2="1.034584375" width="0.001" layer="94"/>
<wire x1="-2.062253125" y1="1.034584375" x2="-2.020790625" y2="1.01759375" width="0.001" layer="94"/>
<wire x1="-2.020790625" y1="1.01759375" x2="-1.978371875" y2="1.001834375" width="0.001" layer="94"/>
<wire x1="-1.978371875" y1="1.001834375" x2="-1.935021875" y2="0.987315625" width="0.001" layer="94"/>
<wire x1="-1.935021875" y1="0.987315625" x2="-1.890753125" y2="0.974034375" width="0.001" layer="94"/>
<wire x1="-1.890753125" y1="0.974034375" x2="-1.84558125" y2="0.962015625" width="0.001" layer="94"/>
<wire x1="-1.84558125" y1="0.962015625" x2="-1.79953125" y2="0.95124375" width="0.001" layer="94"/>
<wire x1="-1.79953125" y1="0.95124375" x2="-1.752621875" y2="0.941753125" width="0.001" layer="94"/>
<wire x1="-1.752621875" y1="0.941753125" x2="-1.704871875" y2="0.93354375" width="0.001" layer="94"/>
<wire x1="-1.704871875" y1="0.93354375" x2="-1.668071875" y2="0.92929375" width="0.001" layer="94"/>
<wire x1="-1.668071875" y1="0.92929375" x2="-1.62153125" y2="0.926284375" width="0.001" layer="94"/>
<wire x1="-1.62153125" y1="0.926284375" x2="-1.568521875" y2="0.924484375" width="0.001" layer="94"/>
<wire x1="-1.568521875" y1="0.924484375" x2="-1.5122625" y2="0.92389375" width="0.001" layer="94"/>
<wire x1="-1.5122625" y1="0.92389375" x2="-1.456021875" y2="0.924534375" width="0.001" layer="94"/>
<wire x1="-1.456021875" y1="0.924534375" x2="-1.403040625" y2="0.926384375" width="0.001" layer="94"/>
<wire x1="-1.403040625" y1="0.926384375" x2="-1.35658125" y2="0.92944375" width="0.001" layer="94"/>
<wire x1="-1.35658125" y1="0.92944375" x2="-1.319871875" y2="0.933725" width="0.001" layer="94"/>
<wire x1="-1.319871875" y1="0.933725" x2="-1.263321875" y2="0.943925" width="0.001" layer="94"/>
<wire x1="-1.263321875" y1="0.943925" x2="-1.208240625" y2="0.956353125" width="0.001" layer="94"/>
<wire x1="-1.208240625" y1="0.956353125" x2="-1.154703125" y2="0.970965625" width="0.001" layer="94"/>
<wire x1="-1.154703125" y1="0.970965625" x2="-1.1027625" y2="0.987715625" width="0.001" layer="94"/>
<wire x1="-1.1027625" y1="0.987715625" x2="-1.05248125" y2="1.006565625" width="0.001" layer="94"/>
<wire x1="-1.05248125" y1="1.006565625" x2="-1.00393125" y2="1.027484375" width="0.001" layer="94"/>
<wire x1="-1.00393125" y1="1.027484375" x2="-0.957171875" y2="1.050425" width="0.001" layer="94"/>
<wire x1="-0.957171875" y1="1.050425" x2="-0.912271875" y2="1.075365625" width="0.001" layer="94"/>
<wire x1="-0.912271875" y1="1.075365625" x2="-0.869290625" y2="1.102253125" width="0.001" layer="94"/>
<wire x1="-0.869290625" y1="1.102253125" x2="-0.828290625" y2="1.131065625" width="0.001" layer="94"/>
<wire x1="-0.828290625" y1="1.131065625" x2="-0.789340625" y2="1.16174375" width="0.001" layer="94"/>
<wire x1="-0.789340625" y1="1.16174375" x2="-0.7525125" y2="1.194265625" width="0.001" layer="94"/>
<wire x1="-0.7525125" y1="1.194265625" x2="-0.717840625" y2="1.228584375" width="0.001" layer="94"/>
<wire x1="-0.717840625" y1="1.228584375" x2="-0.685421875" y2="1.264675" width="0.001" layer="94"/>
<wire x1="-0.685421875" y1="1.264675" x2="-0.6553125" y2="1.302484375" width="0.001" layer="94"/>
<wire x1="-0.6553125" y1="1.302484375" x2="-0.6275625" y2="1.341984375" width="0.001" layer="94"/>
<wire x1="-0.6275625" y1="1.341984375" x2="-0.61033125" y2="1.369615625" width="0.001" layer="94"/>
<wire x1="-0.61033125" y1="1.369615625" x2="-0.594453125" y2="1.39804375" width="0.001" layer="94"/>
<wire x1="-0.594453125" y1="1.39804375" x2="-0.579921875" y2="1.427203125" width="0.001" layer="94"/>
<wire x1="-0.579921875" y1="1.427203125" x2="-0.56678125" y2="1.457015625" width="0.001" layer="94"/>
<wire x1="-0.56678125" y1="1.457015625" x2="-0.55503125" y2="1.487415625" width="0.001" layer="94"/>
<wire x1="-0.55503125" y1="1.487415625" x2="-0.544690625" y2="1.518325" width="0.001" layer="94"/>
<wire x1="-0.544690625" y1="1.518325" x2="-0.53578125" y2="1.549675" width="0.001" layer="94"/>
<wire x1="-0.53578125" y1="1.549675" x2="-0.528303125" y2="1.581384375" width="0.001" layer="94"/>
<wire x1="-0.528303125" y1="1.581384375" x2="-0.52228125" y2="1.613384375" width="0.001" layer="94"/>
<wire x1="-0.52228125" y1="1.613384375" x2="-0.51773125" y2="1.645603125" width="0.001" layer="94"/>
<wire x1="-0.51773125" y1="1.645603125" x2="-0.514671875" y2="1.677975" width="0.001" layer="94"/>
<wire x1="-0.514671875" y1="1.677975" x2="-0.5131125" y2="1.710425" width="0.001" layer="94"/>
<wire x1="-0.5131125" y1="1.710425" x2="-0.5130625" y2="1.742865625" width="0.001" layer="94"/>
<wire x1="-0.5130625" y1="1.742865625" x2="-0.514553125" y2="1.77524375" width="0.001" layer="94"/>
<wire x1="-0.514553125" y1="1.77524375" x2="-0.517590625" y2="1.807475" width="0.001" layer="94"/>
<wire x1="-0.517590625" y1="1.807475" x2="-0.522190625" y2="1.839484375" width="0.001" layer="94"/>
<wire x1="-0.522190625" y1="1.839484375" x2="-0.5299625" y2="1.877484375" width="0.001" layer="94"/>
<wire x1="-0.5299625" y1="1.877484375" x2="-0.540603125" y2="1.917175" width="0.001" layer="94"/>
<wire x1="-0.540603125" y1="1.917175" x2="-0.55383125" y2="1.957853125" width="0.001" layer="94"/>
<wire x1="-0.55383125" y1="1.957853125" x2="-0.569371875" y2="1.99884375" width="0.001" layer="94"/>
<wire x1="-0.569371875" y1="1.99884375" x2="-0.58693125" y2="2.039453125" width="0.001" layer="94"/>
<wire x1="-0.58693125" y1="2.039453125" x2="-0.606253125" y2="2.078965625" width="0.001" layer="94"/>
<wire x1="-0.606253125" y1="2.078965625" x2="-0.62703125" y2="2.116715625" width="0.001" layer="94"/>
<wire x1="-0.62703125" y1="2.116715625" x2="-0.648990625" y2="2.151984375" width="0.001" layer="94"/>
<wire x1="-0.648990625" y1="2.151984375" x2="-0.6659125" y2="2.179575" width="0.001" layer="94"/>
<wire x1="-0.6659125" y1="2.179575" x2="-0.670521875" y2="2.188825" width="0.001" layer="94"/>
<wire x1="-0.670521875" y1="2.188825" x2="-0.671590625" y2="2.193234375" width="0.001" layer="94"/>
<wire x1="-0.671590625" y1="2.193234375" x2="-0.669621875" y2="2.19469375" width="0.001" layer="94"/>
<wire x1="-0.669621875" y1="2.19469375" x2="-0.665971875" y2="2.195884375" width="0.001" layer="94"/>
<wire x1="-0.665971875" y1="2.195884375" x2="-0.655703125" y2="2.196984375" width="0.001" layer="94"/>
<wire x1="-0.655703125" y1="2.196984375" x2="-0.644490625" y2="2.198175" width="0.001" layer="94"/>
<wire x1="-0.644490625" y1="2.198175" x2="-0.626790625" y2="2.20149375" width="0.001" layer="94"/>
<wire x1="-0.626790625" y1="2.20149375" x2="-0.578140625" y2="2.212915625" width="0.001" layer="94"/>
<wire x1="-0.578140625" y1="2.212915625" x2="-0.522121875" y2="2.228103125" width="0.001" layer="94"/>
<wire x1="-0.522121875" y1="2.228103125" x2="-0.471121875" y2="2.243915625" width="0.001" layer="94"/>
<wire x1="-0.471121875" y1="2.243915625" x2="-0.434921875" y2="2.255403125" width="0.001" layer="94"/>
<wire x1="-0.434921875" y1="2.255403125" x2="-0.4239125" y2="2.258275" width="0.001" layer="94"/>
<wire x1="-0.4239125" y1="2.258275" x2="-0.419871875" y2="2.258584375" width="0.001" layer="94"/>
<wire x1="-0.419871875" y1="2.258584375" x2="-0.418853125" y2="2.258184375" width="0.001" layer="94"/>
<wire x1="-0.418853125" y1="2.258184375" x2="-0.41608125" y2="2.259125" width="0.001" layer="94"/>
<wire x1="-0.41608125" y1="2.259125" x2="-0.4069625" y2="2.26429375" width="0.001" layer="94"/>
<wire x1="-0.4069625" y1="2.26429375" x2="-0.401421875" y2="2.267365625" width="0.001" layer="94"/>
<wire x1="-0.401421875" y1="2.267365625" x2="-0.3958625" y2="2.26949375" width="0.001" layer="94"/>
<wire x1="-0.3958625" y1="2.26949375" x2="-0.39093125" y2="2.270484375" width="0.001" layer="94"/>
<wire x1="-0.39093125" y1="2.270484375" x2="-0.387290625" y2="2.27014375" width="0.001" layer="94"/>
<wire x1="-0.387290625" y1="2.27014375" x2="-0.382040625" y2="2.269384375" width="0.001" layer="94"/>
<wire x1="-0.382040625" y1="2.269384375" x2="-0.38008125" y2="2.26999375" width="0.001" layer="94"/>
<wire x1="-0.38008125" y1="2.26999375" x2="-0.378940625" y2="2.271203125" width="0.001" layer="94"/>
<wire x1="-0.378940625" y1="2.271203125" x2="-0.3726125" y2="2.275534375" width="0.001" layer="94"/>
<wire x1="-0.3726125" y1="2.275534375" x2="-0.35653125" y2="2.284615625" width="0.001" layer="94"/>
<wire x1="-0.35653125" y1="2.284615625" x2="-0.304871875" y2="2.311715625" width="0.001" layer="94"/>
<wire x1="-0.304871875" y1="2.311715625" x2="-0.248653125" y2="2.341225" width="0.001" layer="94"/>
<wire x1="-0.248653125" y1="2.341225" x2="-0.200771875" y2="2.368503125" width="0.001" layer="94"/>
<wire x1="-0.200771875" y1="2.368503125" x2="-0.159553125" y2="2.394534375" width="0.001" layer="94"/>
<wire x1="-0.159553125" y1="2.394534375" x2="-0.123303125" y2="2.42034375" width="0.001" layer="94"/>
<wire x1="-0.123303125" y1="2.42034375" x2="-0.085021875" y2="2.451203125" width="0.001" layer="94"/>
<wire x1="-0.085021875" y1="2.451203125" x2="-0.046490625" y2="2.48549375" width="0.001" layer="94"/>
<wire x1="-0.046490625" y1="2.48549375" x2="-0.01113125" y2="2.52004375" width="0.001" layer="94"/>
<wire x1="-0.01113125" y1="2.52004375" x2="0.0042875" y2="2.536415625" width="0.001" layer="94"/>
<wire x1="0.0042875" y1="2.536415625" x2="0.017628125" y2="2.55164375" width="0.001" layer="94"/>
<wire x1="0.017628125" y1="2.55164375" x2="0.035259375" y2="2.572253125" width="0.001" layer="94"/>
<wire x1="0.035259375" y1="2.572253125" x2="0.041409375" y2="2.57854375" width="0.001" layer="94"/>
<wire x1="0.041409375" y1="2.57854375" x2="0.046559375" y2="2.582675" width="0.001" layer="94"/>
<wire x1="0.046559375" y1="2.582675" x2="0.05131875" y2="2.585025" width="0.001" layer="94"/>
<wire x1="0.05131875" y1="2.585025" x2="0.0562875" y2="2.58599375" width="0.001" layer="94"/>
<wire x1="0.0562875" y1="2.58599375" x2="0.069196875" y2="2.585325" width="0.001" layer="94"/>
<wire x1="0.069196875" y1="2.585325" x2="0.08331875" y2="2.582575" width="0.001" layer="94"/>
<wire x1="0.08331875" y1="2.582575" x2="0.08951875" y2="2.580303125" width="0.001" layer="94"/>
<wire x1="0.08951875" y1="2.580303125" x2="0.0951375" y2="2.57744375" width="0.001" layer="94"/>
<wire x1="0.0951375" y1="2.57744375" x2="0.100178125" y2="2.574003125" width="0.001" layer="94"/>
<wire x1="0.100178125" y1="2.574003125" x2="0.104628125" y2="2.569984375" width="0.001" layer="94"/>
<wire x1="0.104628125" y1="2.569984375" x2="0.1084875" y2="2.565403125" width="0.001" layer="94"/>
<wire x1="0.1084875" y1="2.565403125" x2="0.111746875" y2="2.560253125" width="0.001" layer="94"/>
<wire x1="0.111746875" y1="2.560253125" x2="0.11441875" y2="2.554553125" width="0.001" layer="94"/>
<wire x1="0.11441875" y1="2.554553125" x2="0.1164875" y2="2.548315625" width="0.001" layer="94"/>
<wire x1="0.1164875" y1="2.548315625" x2="0.118796875" y2="2.534203125" width="0.001" layer="94"/>
<wire x1="0.118796875" y1="2.534203125" x2="0.118659375" y2="2.517984375" width="0.001" layer="94"/>
<wire x1="0.118659375" y1="2.517984375" x2="0.1160375" y2="2.499715625" width="0.001" layer="94"/>
<wire x1="0.1160375" y1="2.499715625" x2="0.11141875" y2="2.48079375" width="0.001" layer="94"/>
<wire x1="0.11141875" y1="2.48079375" x2="0.104846875" y2="2.459984375" width="0.001" layer="94"/>
<wire x1="0.104846875" y1="2.459984375" x2="0.09646875" y2="2.437525" width="0.001" layer="94"/>
<wire x1="0.09646875" y1="2.437525" x2="0.086409375" y2="2.413653125" width="0.001" layer="94"/>
<wire x1="0.086409375" y1="2.413653125" x2="0.074828125" y2="2.388615625" width="0.001" layer="94"/>
<wire x1="0.074828125" y1="2.388615625" x2="0.0618375" y2="2.36264375" width="0.001" layer="94"/>
<wire x1="0.0618375" y1="2.36264375" x2="0.047596875" y2="2.335984375" width="0.001" layer="94"/>
<wire x1="0.047596875" y1="2.335984375" x2="0.032246875" y2="2.308875" width="0.001" layer="94"/>
<wire x1="0.032246875" y1="2.308875" x2="-0.001240625" y2="2.254253125" width="0.001" layer="94"/>
<wire x1="-0.001240625" y1="2.254253125" x2="-0.0191125" y2="2.227225" width="0.001" layer="94"/>
<wire x1="-0.0191125" y1="2.227225" x2="-0.03753125" y2="2.200703125" width="0.001" layer="94"/>
<wire x1="-0.03753125" y1="2.200703125" x2="-0.0563625" y2="2.174925" width="0.001" layer="94"/>
<wire x1="-0.0563625" y1="2.174925" x2="-0.07548125" y2="2.15014375" width="0.001" layer="94"/>
<wire x1="-0.07548125" y1="2.15014375" x2="-0.09473125" y2="2.126584375" width="0.001" layer="94"/>
<wire x1="-0.09473125" y1="2.126584375" x2="-0.113990625" y2="2.104484375" width="0.001" layer="94"/>
<wire x1="-0.113990625" y1="2.104484375" x2="-0.140840625" y2="2.075915625" width="0.001" layer="94"/>
<wire x1="-0.140840625" y1="2.075915625" x2="-0.171553125" y2="2.045284375" width="0.001" layer="94"/>
<wire x1="-0.171553125" y1="2.045284375" x2="-0.205171875" y2="2.013453125" width="0.001" layer="94"/>
<wire x1="-0.205171875" y1="2.013453125" x2="-0.240740625" y2="1.981253125" width="0.001" layer="94"/>
<wire x1="-0.240740625" y1="1.981253125" x2="-0.27733125" y2="1.949515625" width="0.001" layer="94"/>
<wire x1="-0.27733125" y1="1.949515625" x2="-0.31398125" y2="1.91909375" width="0.001" layer="94"/>
<wire x1="-0.31398125" y1="1.91909375" x2="-0.3497625" y2="1.890803125" width="0.001" layer="94"/>
<wire x1="-0.3497625" y1="1.890803125" x2="-0.383703125" y2="1.865503125" width="0.001" layer="94"/>
<wire x1="-0.383703125" y1="1.865503125" x2="-0.391903125" y2="1.85904375" width="0.001" layer="94"/>
<wire x1="-0.391903125" y1="1.85904375" x2="-0.397740625" y2="1.853265625" width="0.001" layer="94"/>
<wire x1="-0.397740625" y1="1.853265625" x2="-0.400803125" y2="1.848675" width="0.001" layer="94"/>
<wire x1="-0.400803125" y1="1.848675" x2="-0.401140625" y2="1.846975" width="0.001" layer="94"/>
<wire x1="-0.401140625" y1="1.846975" x2="-0.400640625" y2="1.845753125" width="0.001" layer="94"/>
<wire x1="-0.400640625" y1="1.845753125" x2="-0.39443125" y2="1.843275" width="0.001" layer="94"/>
<wire x1="-0.39443125" y1="1.843275" x2="-0.380803125" y2="1.839753125" width="0.001" layer="94"/>
<wire x1="-0.380803125" y1="1.839753125" x2="-0.339303125" y2="1.831525" width="0.001" layer="94"/>
<wire x1="-0.339303125" y1="1.831525" x2="-0.307240625" y2="1.82704375" width="0.001" layer="94"/>
<wire x1="-0.307240625" y1="1.82704375" x2="-0.268703125" y2="1.823365625" width="0.001" layer="94"/>
<wire x1="-0.268703125" y1="1.823365625" x2="-0.225771875" y2="1.820534375" width="0.001" layer="94"/>
<wire x1="-0.225771875" y1="1.820534375" x2="-0.180590625" y2="1.818634375" width="0.001" layer="94"/>
<wire x1="-0.180590625" y1="1.818634375" x2="-0.135253125" y2="1.817725" width="0.001" layer="94"/>
<wire x1="-0.135253125" y1="1.817725" x2="-0.091871875" y2="1.817865625" width="0.001" layer="94"/>
<wire x1="-0.091871875" y1="1.817865625" x2="-0.052571875" y2="1.819134375" width="0.001" layer="94"/>
<wire x1="-0.052571875" y1="1.819134375" x2="-0.0194625" y2="1.821584375" width="0.001" layer="94"/>
<wire x1="-0.0194625" y1="1.821584375" x2="0.008728125" y2="1.824934375" width="0.001" layer="94"/>
<wire x1="0.008728125" y1="1.824934375" x2="0.037209375" y2="1.829103125" width="0.001" layer="94"/>
<wire x1="0.037209375" y1="1.829103125" x2="0.094696875" y2="1.839803125" width="0.001" layer="94"/>
<wire x1="0.094696875" y1="1.839803125" x2="0.1523875" y2="1.853484375" width="0.001" layer="94"/>
<wire x1="0.1523875" y1="1.853484375" x2="0.2096375" y2="1.86994375" width="0.001" layer="94"/>
<wire x1="0.2096375" y1="1.86994375" x2="0.26581875" y2="1.888984375" width="0.001" layer="94"/>
<wire x1="0.26581875" y1="1.888984375" x2="0.3202875" y2="1.910415625" width="0.001" layer="94"/>
<wire x1="0.3202875" y1="1.910415625" x2="0.346678125" y2="1.921965625" width="0.001" layer="94"/>
<wire x1="0.346678125" y1="1.921965625" x2="0.372409375" y2="1.934034375" width="0.001" layer="94"/>
<wire x1="0.372409375" y1="1.934034375" x2="0.397396875" y2="1.94659375" width="0.001" layer="94"/>
<wire x1="0.397396875" y1="1.94659375" x2="0.421559375" y2="1.959634375" width="0.001" layer="94"/>
<wire x1="0.421559375" y1="1.959634375" x2="0.439996875" y2="1.96954375" width="0.001" layer="94"/>
<wire x1="0.439996875" y1="1.96954375" x2="0.455909375" y2="1.977175" width="0.001" layer="94"/>
<wire x1="0.455909375" y1="1.977175" x2="0.46951875" y2="1.982575" width="0.001" layer="94"/>
<wire x1="0.46951875" y1="1.982575" x2="0.4810875" y2="1.98579375" width="0.001" layer="94"/>
<wire x1="0.4810875" y1="1.98579375" x2="0.490859375" y2="1.986875" width="0.001" layer="94"/>
<wire x1="0.490859375" y1="1.986875" x2="0.499078125" y2="1.985884375" width="0.001" layer="94"/>
<wire x1="0.499078125" y1="1.985884375" x2="0.505996875" y2="1.982853125" width="0.001" layer="94"/>
<wire x1="0.505996875" y1="1.982853125" x2="0.511859375" y2="1.97784375" width="0.001" layer="94"/>
<wire x1="0.511859375" y1="1.97784375" x2="0.51506875" y2="1.97294375" width="0.001" layer="94"/>
<wire x1="0.51506875" y1="1.97294375" x2="0.517678125" y2="1.966365625" width="0.001" layer="94"/>
<wire x1="0.517678125" y1="1.966365625" x2="0.5194375" y2="1.958953125" width="0.001" layer="94"/>
<wire x1="0.5194375" y1="1.958953125" x2="0.520059375" y2="1.95159375" width="0.001" layer="94"/>
<wire x1="0.520059375" y1="1.95159375" x2="0.518409375" y2="1.938765625" width="0.001" layer="94"/>
<wire x1="0.518409375" y1="1.938765625" x2="0.513878125" y2="1.920025" width="0.001" layer="94"/>
<wire x1="0.513878125" y1="1.920025" x2="0.50696875" y2="1.896775" width="0.001" layer="94"/>
<wire x1="0.50696875" y1="1.896775" x2="0.498146875" y2="1.87044375" width="0.001" layer="94"/>
<wire x1="0.498146875" y1="1.87044375" x2="0.4879375" y2="1.842434375" width="0.001" layer="94"/>
<wire x1="0.4879375" y1="1.842434375" x2="0.47681875" y2="1.814153125" width="0.001" layer="94"/>
<wire x1="0.47681875" y1="1.814153125" x2="0.4652875" y2="1.787003125" width="0.001" layer="94"/>
<wire x1="0.4652875" y1="1.787003125" x2="0.453828125" y2="1.762425" width="0.001" layer="94"/>
<wire x1="0.453828125" y1="1.762425" x2="0.425778125" y2="1.704484375" width="0.001" layer="94"/>
<wire x1="0.425778125" y1="1.704484375" x2="0.425778125" y2="1.703853125" width="0.001" layer="94"/>
<wire x1="0.425778125" y1="1.703853125" x2="0.4265375" y2="1.703984375" width="0.001" layer="94"/>
<wire x1="0.4265375" y1="1.703984375" x2="0.430078125" y2="1.706325" width="0.001" layer="94"/>
<wire x1="0.430078125" y1="1.706325" x2="0.443209375" y2="1.717775" width="0.001" layer="94"/>
<wire x1="0.443209375" y1="1.717775" x2="0.4852375" y2="1.754815625" width="0.001" layer="94"/>
<wire x1="0.4852375" y1="1.754815625" x2="0.5369375" y2="1.797375" width="0.001" layer="94"/>
<wire x1="0.5369375" y1="1.797375" x2="0.582028125" y2="1.832403125" width="0.001" layer="94"/>
<wire x1="0.582028125" y1="1.832403125" x2="0.596996875" y2="1.843025" width="0.001" layer="94"/>
<wire x1="0.596996875" y1="1.843025" x2="0.604196875" y2="1.84684375" width="0.001" layer="94"/>
<wire x1="0.604196875" y1="1.84684375" x2="0.60651875" y2="1.845415625" width="0.001" layer="94"/>
<wire x1="0.60651875" y1="1.845415625" x2="0.610296875" y2="1.841603125" width="0.001" layer="94"/>
<wire x1="0.610296875" y1="1.841603125" x2="0.620128125" y2="1.82919375" width="0.001" layer="94"/>
<wire x1="0.620128125" y1="1.82919375" x2="0.627546875" y2="1.820434375" width="0.001" layer="94"/>
<wire x1="0.627546875" y1="1.820434375" x2="0.638509375" y2="1.809525" width="0.001" layer="94"/>
<wire x1="0.638509375" y1="1.809525" x2="0.651528125" y2="1.797853125" width="0.001" layer="94"/>
<wire x1="0.651528125" y1="1.797853125" x2="0.665128125" y2="1.786825" width="0.001" layer="94"/>
<wire x1="0.665128125" y1="1.786825" x2="0.677828125" y2="1.777615625" width="0.001" layer="94"/>
<wire x1="0.677828125" y1="1.777615625" x2="0.690709375" y2="1.769284375" width="0.001" layer="94"/>
<wire x1="0.690709375" y1="1.769284375" x2="0.7038375" y2="1.761834375" width="0.001" layer="94"/>
<wire x1="0.7038375" y1="1.761834375" x2="0.71726875" y2="1.75524375" width="0.001" layer="94"/>
<wire x1="0.71726875" y1="1.75524375" x2="0.731059375" y2="1.74949375" width="0.001" layer="94"/>
<wire x1="0.731059375" y1="1.74949375" x2="0.745259375" y2="1.74459375" width="0.001" layer="94"/>
<wire x1="0.745259375" y1="1.74459375" x2="0.759928125" y2="1.740525" width="0.001" layer="94"/>
<wire x1="0.759928125" y1="1.740525" x2="0.77511875" y2="1.737265625" width="0.001" layer="94"/>
<wire x1="0.77511875" y1="1.737265625" x2="0.7908875" y2="1.734825" width="0.001" layer="94"/>
<wire x1="0.7908875" y1="1.734825" x2="0.807296875" y2="1.733175" width="0.001" layer="94"/>
<wire x1="0.807296875" y1="1.733175" x2="0.8243875" y2="1.732325" width="0.001" layer="94"/>
<wire x1="0.8243875" y1="1.732325" x2="0.8422375" y2="1.73224375" width="0.001" layer="94"/>
<wire x1="0.8422375" y1="1.73224375" x2="0.8803875" y2="1.734384375" width="0.001" layer="94"/>
<wire x1="0.8803875" y1="1.734384375" x2="0.922196875" y2="1.739515625" width="0.001" layer="94"/>
<wire x1="0.922196875" y1="1.739515625" x2="0.96181875" y2="1.745115625" width="0.001" layer="94"/>
<wire x1="0.96181875" y1="1.745115625" x2="0.9836875" y2="1.746815625" width="0.001" layer="94"/>
<wire x1="0.9836875" y1="1.746815625" x2="0.989259375" y2="1.746184375" width="0.001" layer="94"/>
<wire x1="0.989259375" y1="1.746184375" x2="0.991959375" y2="1.744553125" width="0.001" layer="94"/>
<wire x1="0.991959375" y1="1.744553125" x2="0.992296875" y2="1.741903125" width="0.001" layer="94"/>
<wire x1="0.992296875" y1="1.741903125" x2="0.990796875" y2="1.738234375" width="0.001" layer="94"/>
<wire x1="0.990796875" y1="1.738234375" x2="0.985509375" y2="1.73224375" width="0.001" layer="94"/>
<wire x1="0.985509375" y1="1.73224375" x2="0.974978125" y2="1.72414375" width="0.001" layer="94"/>
<wire x1="0.974978125" y1="1.72414375" x2="0.9593375" y2="1.71399375" width="0.001" layer="94"/>
<wire x1="0.9593375" y1="1.71399375" x2="0.938696875" y2="1.701865625" width="0.001" layer="94"/>
<wire x1="0.938696875" y1="1.701865625" x2="0.8829875" y2="1.671984375" width="0.001" layer="94"/>
<wire x1="0.8829875" y1="1.671984375" x2="0.808878125" y2="1.635084375" width="0.001" layer="94"/>
<wire x1="0.808878125" y1="1.635084375" x2="0.7705375" y2="1.616084375" width="0.001" layer="94"/>
<wire x1="0.7705375" y1="1.616084375" x2="0.7391375" y2="1.59974375" width="0.001" layer="94"/>
<wire x1="0.7391375" y1="1.59974375" x2="0.717928125" y2="1.587803125" width="0.001" layer="94"/>
<wire x1="0.717928125" y1="1.587803125" x2="0.712146875" y2="1.584025" width="0.001" layer="94"/>
<wire x1="0.712146875" y1="1.584025" x2="0.710128125" y2="1.582003125" width="0.001" layer="94"/>
<wire x1="0.710128125" y1="1.582003125" x2="0.71041875" y2="1.580003125" width="0.001" layer="94"/>
<wire x1="0.71041875" y1="1.580003125" x2="0.71221875" y2="1.57894375" width="0.001" layer="94"/>
<wire x1="0.71221875" y1="1.57894375" x2="0.716909375" y2="1.57914375" width="0.001" layer="94"/>
<wire x1="0.716909375" y1="1.57914375" x2="0.725896875" y2="1.580915625" width="0.001" layer="94"/>
<wire x1="0.725896875" y1="1.580915625" x2="0.762296875" y2="1.590475" width="0.001" layer="94"/>
<wire x1="0.762296875" y1="1.590475" x2="0.83256875" y2="1.61019375" width="0.001" layer="94"/>
<wire x1="0.83256875" y1="1.61019375" x2="0.9040375" y2="1.629075" width="0.001" layer="94"/>
<wire x1="0.9040375" y1="1.629075" x2="0.929228125" y2="1.634775" width="0.001" layer="94"/>
<wire x1="0.929228125" y1="1.634775" x2="0.94256875" y2="1.636825" width="0.001" layer="94"/>
<wire x1="0.94256875" y1="1.636825" x2="0.957628125" y2="1.636665625" width="0.001" layer="94"/>
<wire x1="0.957628125" y1="1.636665625" x2="0.944678125" y2="1.625253125" width="0.001" layer="94"/>
<wire x1="0.944678125" y1="1.625253125" x2="0.933709375" y2="1.617375" width="0.001" layer="94"/>
<wire x1="0.933709375" y1="1.617375" x2="0.913396875" y2="1.604375" width="0.001" layer="94"/>
<wire x1="0.913396875" y1="1.604375" x2="0.85636875" y2="1.570203125" width="0.001" layer="94"/>
<wire x1="0.85636875" y1="1.570203125" x2="0.812559375" y2="1.544284375" width="0.001" layer="94"/>
<wire x1="0.812559375" y1="1.544284375" x2="0.78521875" y2="1.526603125" width="0.001" layer="94"/>
<wire x1="0.78521875" y1="1.526603125" x2="0.777028125" y2="1.52034375" width="0.001" layer="94"/>
<wire x1="0.777028125" y1="1.52034375" x2="0.772096875" y2="1.515553125" width="0.001" layer="94"/>
<wire x1="0.772096875" y1="1.515553125" x2="0.770159375" y2="1.512015625" width="0.001" layer="94"/>
<wire x1="0.770159375" y1="1.512015625" x2="0.77091875" y2="1.509525" width="0.001" layer="94"/>
<wire x1="0.77091875" y1="1.509525" x2="0.77826875" y2="1.510815625" width="0.001" layer="94"/>
<wire x1="0.77826875" y1="1.510815625" x2="0.796646875" y2="1.515965625" width="0.001" layer="94"/>
<wire x1="0.796646875" y1="1.515965625" x2="0.8553875" y2="1.534534375" width="0.001" layer="94"/>
<wire x1="0.8553875" y1="1.534534375" x2="0.887259375" y2="1.544725" width="0.001" layer="94"/>
<wire x1="0.887259375" y1="1.544725" x2="0.914296875" y2="1.552575" width="0.001" layer="94"/>
<wire x1="0.914296875" y1="1.552575" x2="0.9366875" y2="1.558103125" width="0.001" layer="94"/>
<wire x1="0.9366875" y1="1.558103125" x2="0.954609375" y2="1.561334375" width="0.001" layer="94"/>
<wire x1="0.954609375" y1="1.561334375" x2="0.968228125" y2="1.562284375" width="0.001" layer="94"/>
<wire x1="0.968228125" y1="1.562284375" x2="0.9777375" y2="1.56099375" width="0.001" layer="94"/>
<wire x1="0.9777375" y1="1.56099375" x2="0.981009375" y2="1.559515625" width="0.001" layer="94"/>
<wire x1="0.981009375" y1="1.559515625" x2="0.98331875" y2="1.557475" width="0.001" layer="94"/>
<wire x1="0.98331875" y1="1.557475" x2="0.984678125" y2="1.55489375" width="0.001" layer="94"/>
<wire x1="0.984678125" y1="1.55489375" x2="0.985128125" y2="1.551765625" width="0.001" layer="94"/>
<wire x1="0.985128125" y1="1.551765625" x2="0.984296875" y2="1.548703125" width="0.001" layer="94"/>
<wire x1="0.984296875" y1="1.548703125" x2="0.981878125" y2="1.544225" width="0.001" layer="94"/>
<wire x1="0.981878125" y1="1.544225" x2="0.972696875" y2="1.531503125" width="0.001" layer="94"/>
<wire x1="0.972696875" y1="1.531503125" x2="0.958478125" y2="1.514725" width="0.001" layer="94"/>
<wire x1="0.958478125" y1="1.514725" x2="0.940128125" y2="1.495015625" width="0.001" layer="94"/>
<wire x1="0.940128125" y1="1.495015625" x2="0.908346875" y2="1.460475" width="0.001" layer="94"/>
<wire x1="0.908346875" y1="1.460475" x2="0.898678125" y2="1.448653125" width="0.001" layer="94"/>
<wire x1="0.898678125" y1="1.448653125" x2="0.895128125" y2="1.44274375" width="0.001" layer="94"/>
<wire x1="0.895128125" y1="1.44274375" x2="0.89556875" y2="1.44064375" width="0.001" layer="94"/>
<wire x1="0.89556875" y1="1.44064375" x2="0.896846875" y2="1.439053125" width="0.001" layer="94"/>
<wire x1="0.896846875" y1="1.439053125" x2="0.901796875" y2="1.437334375" width="0.001" layer="94"/>
<wire x1="0.901796875" y1="1.437334375" x2="0.909709375" y2="1.437453125" width="0.001" layer="94"/>
<wire x1="0.909709375" y1="1.437453125" x2="0.9202875" y2="1.43929375" width="0.001" layer="94"/>
<wire x1="0.9202875" y1="1.43929375" x2="0.93326875" y2="1.442753125" width="0.001" layer="94"/>
<wire x1="0.93326875" y1="1.442753125" x2="0.9483375" y2="1.447684375" width="0.001" layer="94"/>
<wire x1="0.9483375" y1="1.447684375" x2="0.983678125" y2="1.461534375" width="0.001" layer="94"/>
<wire x1="0.983678125" y1="1.461534375" x2="1.024046875" y2="1.479865625" width="0.001" layer="94"/>
<wire x1="1.024046875" y1="1.479865625" x2="1.067178125" y2="1.501725" width="0.001" layer="94"/>
<wire x1="1.067178125" y1="1.501725" x2="1.1107875" y2="1.526153125" width="0.001" layer="94"/>
<wire x1="1.1107875" y1="1.526153125" x2="1.152628125" y2="1.552184375" width="0.001" layer="94"/>
<wire x1="1.152628125" y1="1.552184375" x2="1.173859375" y2="1.566803125" width="0.001" layer="94"/>
<wire x1="1.173859375" y1="1.566803125" x2="1.1947875" y2="1.582353125" width="0.001" layer="94"/>
<wire x1="1.1947875" y1="1.582353125" x2="1.214909375" y2="1.598415625" width="0.001" layer="94"/>
<wire x1="1.214909375" y1="1.598415625" x2="1.23371875" y2="1.614503125" width="0.001" layer="94"/>
<wire x1="1.23371875" y1="1.614503125" x2="1.250709375" y2="1.630203125" width="0.001" layer="94"/>
<wire x1="1.250709375" y1="1.630203125" x2="1.265359375" y2="1.64504375" width="0.001" layer="94"/>
<wire x1="1.265359375" y1="1.64504375" x2="1.2771875" y2="1.658584375" width="0.001" layer="94"/>
<wire x1="1.2771875" y1="1.658584375" x2="1.285678125" y2="1.670365625" width="0.001" layer="94"/>
<wire x1="1.285678125" y1="1.670365625" x2="1.293959375" y2="1.685334375" width="0.001" layer="94"/>
<wire x1="1.293959375" y1="1.685334375" x2="1.29936875" y2="1.699065625" width="0.001" layer="94"/>
<wire x1="1.29936875" y1="1.699065625" x2="1.301846875" y2="1.71189375" width="0.001" layer="94"/>
<wire x1="1.301846875" y1="1.71189375" x2="1.30196875" y2="1.718084375" width="0.001" layer="94"/>
<wire x1="1.30196875" y1="1.718084375" x2="1.301328125" y2="1.724175" width="0.001" layer="94"/>
<wire x1="1.301328125" y1="1.724175" x2="1.29776875" y2="1.736234375" width="0.001" layer="94"/>
<wire x1="1.29776875" y1="1.736234375" x2="1.291096875" y2="1.748434375" width="0.001" layer="94"/>
<wire x1="1.291096875" y1="1.748434375" x2="1.28126875" y2="1.76109375" width="0.001" layer="94"/>
<wire x1="1.28126875" y1="1.76109375" x2="1.26821875" y2="1.774565625" width="0.001" layer="94"/>
<wire x1="1.26821875" y1="1.774565625" x2="1.251709375" y2="1.792103125" width="0.001" layer="94"/>
<wire x1="1.251709375" y1="1.792103125" x2="1.237778125" y2="1.810825" width="0.001" layer="94"/>
<wire x1="1.237778125" y1="1.810825" x2="1.2263875" y2="1.830784375" width="0.001" layer="94"/>
<wire x1="1.2263875" y1="1.830784375" x2="1.217528125" y2="1.852025" width="0.001" layer="94"/>
<wire x1="1.217528125" y1="1.852025" x2="1.2111875" y2="1.87459375" width="0.001" layer="94"/>
<wire x1="1.2111875" y1="1.87459375" x2="1.207346875" y2="1.89854375" width="0.001" layer="94"/>
<wire x1="1.207346875" y1="1.89854375" x2="1.2059875" y2="1.923915625" width="0.001" layer="94"/>
<wire x1="1.2059875" y1="1.923915625" x2="1.207096875" y2="1.950765625" width="0.001" layer="94"/>
<wire x1="1.207096875" y1="1.950765625" x2="1.210296875" y2="1.997503125" width="0.001" layer="94"/>
<wire x1="1.210296875" y1="1.997503125" x2="1.213396875" y2="2.055515625" width="0.001" layer="94"/>
<wire x1="1.213396875" y1="2.055515625" x2="1.21641875" y2="2.12154375" width="0.001" layer="94"/>
<wire x1="1.21641875" y1="2.12154375" x2="1.242028125" y2="2.126653125" width="0.001" layer="94"/>
<wire x1="1.242028125" y1="2.126653125" x2="1.275396875" y2="2.132575" width="0.001" layer="94"/>
<wire x1="1.275396875" y1="2.132575" x2="1.3089375" y2="2.137125" width="0.001" layer="94"/>
<wire x1="1.3089375" y1="2.137125" x2="1.342009375" y2="2.140265625" width="0.001" layer="94"/>
<wire x1="1.342009375" y1="2.140265625" x2="1.373946875" y2="2.142003125" width="0.001" layer="94"/>
<wire x1="1.373946875" y1="2.142003125" x2="1.404109375" y2="2.14229375" width="0.001" layer="94"/>
<wire x1="1.404109375" y1="2.14229375" x2="1.431846875" y2="2.141125" width="0.001" layer="94"/>
<wire x1="1.431846875" y1="2.141125" x2="1.456496875" y2="2.138475" width="0.001" layer="94"/>
<wire x1="1.456496875" y1="2.138475" x2="1.477428125" y2="2.134325" width="0.001" layer="94"/>
<wire x1="1.477428125" y1="2.134325" x2="1.4941375" y2="2.129415625" width="0.001" layer="94"/>
<wire x1="1.4941375" y1="2.129415625" x2="1.510009375" y2="2.123584375" width="0.001" layer="94"/>
<wire x1="1.510009375" y1="2.123584375" x2="1.525028125" y2="2.116853125" width="0.001" layer="94"/>
<wire x1="1.525028125" y1="2.116853125" x2="1.539209375" y2="2.109215625" width="0.001" layer="94"/>
<wire x1="1.539209375" y1="2.109215625" x2="1.5525375" y2="2.100665625" width="0.001" layer="94"/>
<wire x1="1.5525375" y1="2.100665625" x2="1.565028125" y2="2.091215625" width="0.001" layer="94"/>
<wire x1="1.565028125" y1="2.091215625" x2="1.57666875" y2="2.080853125" width="0.001" layer="94"/>
<wire x1="1.57666875" y1="2.080853125" x2="1.587459375" y2="2.06959375" width="0.001" layer="94"/>
<wire x1="1.587459375" y1="2.06959375" x2="1.597409375" y2="2.057434375" width="0.001" layer="94"/>
<wire x1="1.597409375" y1="2.057434375" x2="1.606509375" y2="2.044353125" width="0.001" layer="94"/>
<wire x1="1.606509375" y1="2.044353125" x2="1.614759375" y2="2.030384375" width="0.001" layer="94"/>
<wire x1="1.614759375" y1="2.030384375" x2="1.622159375" y2="2.015503125" width="0.001" layer="94"/>
<wire x1="1.622159375" y1="2.015503125" x2="1.628709375" y2="1.999734375" width="0.001" layer="94"/>
<wire x1="1.628709375" y1="1.999734375" x2="1.634409375" y2="1.98304375" width="0.001" layer="94"/>
<wire x1="1.634409375" y1="1.98304375" x2="1.63926875" y2="1.965465625" width="0.001" layer="94"/>
<wire x1="1.63926875" y1="1.965465625" x2="1.64326875" y2="1.946984375" width="0.001" layer="94"/>
<wire x1="1.64326875" y1="1.946984375" x2="1.647009375" y2="1.922565625" width="0.001" layer="94"/>
<wire x1="1.647009375" y1="1.922565625" x2="1.649046875" y2="1.896075" width="0.001" layer="94"/>
<wire x1="1.649046875" y1="1.896075" x2="1.6495375" y2="1.863803125" width="0.001" layer="94"/>
<wire x1="1.6495375" y1="1.863803125" x2="1.648628125" y2="1.821984375" width="0.001" layer="94"/>
<wire x1="1.648628125" y1="1.821984375" x2="1.64731875" y2="1.790384375" width="0.001" layer="94"/>
<wire x1="1.64731875" y1="1.790384375" x2="1.645409375" y2="1.76294375" width="0.001" layer="94"/>
<wire x1="1.645409375" y1="1.76294375" x2="1.642659375" y2="1.738415625" width="0.001" layer="94"/>
<wire x1="1.642659375" y1="1.738415625" x2="1.63881875" y2="1.715575" width="0.001" layer="94"/>
<wire x1="1.63881875" y1="1.715575" x2="1.633646875" y2="1.693184375" width="0.001" layer="94"/>
<wire x1="1.633646875" y1="1.693184375" x2="1.626896875" y2="1.670015625" width="0.001" layer="94"/>
<wire x1="1.626896875" y1="1.670015625" x2="1.618328125" y2="1.644815625" width="0.001" layer="94"/>
<wire x1="1.618328125" y1="1.644815625" x2="1.607678125" y2="1.616365625" width="0.001" layer="94"/>
<wire x1="1.607678125" y1="1.616365625" x2="1.593578125" y2="1.582353125" width="0.001" layer="94"/>
<wire x1="1.593578125" y1="1.582353125" x2="1.576746875" y2="1.546315625" width="0.001" layer="94"/>
<wire x1="1.576746875" y1="1.546315625" x2="1.5572875" y2="1.508384375" width="0.001" layer="94"/>
<wire x1="1.5572875" y1="1.508384375" x2="1.535309375" y2="1.468684375" width="0.001" layer="94"/>
<wire x1="1.535309375" y1="1.468684375" x2="1.5108875" y2="1.427384375" width="0.001" layer="94"/>
<wire x1="1.5108875" y1="1.427384375" x2="1.484146875" y2="1.384625" width="0.001" layer="94"/>
<wire x1="1.484146875" y1="1.384625" x2="1.45516875" y2="1.340534375" width="0.001" layer="94"/>
<wire x1="1.45516875" y1="1.340534375" x2="1.424078125" y2="1.295265625" width="0.001" layer="94"/>
<wire x1="1.424078125" y1="1.295265625" x2="1.390946875" y2="1.248953125" width="0.001" layer="94"/>
<wire x1="1.390946875" y1="1.248953125" x2="1.355896875" y2="1.201753125" width="0.001" layer="94"/>
<wire x1="1.355896875" y1="1.201753125" x2="1.319028125" y2="1.153803125" width="0.001" layer="94"/>
<wire x1="1.319028125" y1="1.153803125" x2="1.280428125" y2="1.10524375" width="0.001" layer="94"/>
<wire x1="1.280428125" y1="1.10524375" x2="1.240196875" y2="1.056225" width="0.001" layer="94"/>
<wire x1="1.240196875" y1="1.056225" x2="1.198446875" y2="1.006884375" width="0.001" layer="94"/>
<wire x1="1.198446875" y1="1.006884375" x2="1.155278125" y2="0.957365625" width="0.001" layer="94"/>
<wire x1="1.155278125" y1="0.957365625" x2="1.1107875" y2="0.907815625" width="0.001" layer="94"/>
<wire x1="1.1107875" y1="0.907815625" x2="1.066346875" y2="0.857865625" width="0.001" layer="94"/>
<wire x1="1.066346875" y1="0.857865625" x2="1.051628125" y2="0.840434375" width="0.001" layer="94"/>
<wire x1="1.051628125" y1="0.840434375" x2="1.044809375" y2="0.831375" width="0.001" layer="94"/>
<wire x1="1.044809375" y1="0.831375" x2="1.0416375" y2="0.823103125" width="0.001" layer="94"/>
<wire x1="1.0416375" y1="0.823103125" x2="1.041359375" y2="0.815715625" width="0.001" layer="94"/>
<wire x1="1.041359375" y1="0.815715625" x2="1.043878125" y2="0.809203125" width="0.001" layer="94"/>
<wire x1="1.043878125" y1="0.809203125" x2="1.04906875" y2="0.80359375" width="0.001" layer="94"/>
<wire x1="1.04906875" y1="0.80359375" x2="1.056828125" y2="0.79889375" width="0.001" layer="94"/>
<wire x1="1.056828125" y1="0.79889375" x2="1.0670375" y2="0.795125" width="0.001" layer="94"/>
<wire x1="1.0670375" y1="0.795125" x2="1.0795875" y2="0.792275" width="0.001" layer="94"/>
<wire x1="1.0795875" y1="0.792275" x2="1.09436875" y2="0.790365625" width="0.001" layer="94"/>
<wire x1="1.09436875" y1="0.790365625" x2="1.111259375" y2="0.78939375" width="0.001" layer="94"/>
<wire x1="1.111259375" y1="0.78939375" x2="1.130159375" y2="0.78939375" width="0.001" layer="94"/>
<wire x1="1.130159375" y1="0.78939375" x2="1.150946875" y2="0.790365625" width="0.001" layer="94"/>
<wire x1="1.150946875" y1="0.790365625" x2="1.173528125" y2="0.792315625" width="0.001" layer="94"/>
<wire x1="1.173528125" y1="0.792315625" x2="1.19776875" y2="0.795253125" width="0.001" layer="94"/>
<wire x1="1.19776875" y1="0.795253125" x2="1.223559375" y2="0.79919375" width="0.001" layer="94"/>
<wire x1="1.223559375" y1="0.79919375" x2="1.250796875" y2="0.804134375" width="0.001" layer="94"/>
<wire x1="1.250796875" y1="0.804134375" x2="1.279378125" y2="0.810103125" width="0.001" layer="94"/>
<wire x1="1.279378125" y1="0.810103125" x2="1.335628125" y2="0.823925" width="0.001" layer="94"/>
<wire x1="1.335628125" y1="0.823925" x2="1.391909375" y2="0.84034375" width="0.001" layer="94"/>
<wire x1="1.391909375" y1="0.84034375" x2="1.4480375" y2="0.859265625" width="0.001" layer="94"/>
<wire x1="1.4480375" y1="0.859265625" x2="1.503796875" y2="0.880565625" width="0.001" layer="94"/>
<wire x1="1.503796875" y1="0.880565625" x2="1.559009375" y2="0.904115625" width="0.001" layer="94"/>
<wire x1="1.559009375" y1="0.904115625" x2="1.613478125" y2="0.929825" width="0.001" layer="94"/>
<wire x1="1.613478125" y1="0.929825" x2="1.6669875" y2="0.957553125" width="0.001" layer="94"/>
<wire x1="1.6669875" y1="0.957553125" x2="1.719359375" y2="0.987203125" width="0.001" layer="94"/>
<wire x1="1.719359375" y1="0.987203125" x2="1.7703875" y2="1.018653125" width="0.001" layer="94"/>
<wire x1="1.7703875" y1="1.018653125" x2="1.8198875" y2="1.051784375" width="0.001" layer="94"/>
<wire x1="1.8198875" y1="1.051784375" x2="1.867646875" y2="1.086475" width="0.001" layer="94"/>
<wire x1="1.867646875" y1="1.086475" x2="1.913478125" y2="1.122615625" width="0.001" layer="94"/>
<wire x1="1.913478125" y1="1.122615625" x2="1.957178125" y2="1.16009375" width="0.001" layer="94"/>
<wire x1="1.957178125" y1="1.16009375" x2="1.99856875" y2="1.198784375" width="0.001" layer="94"/>
<wire x1="1.99856875" y1="1.198784375" x2="2.037428125" y2="1.238584375" width="0.001" layer="94"/>
<wire x1="2.037428125" y1="1.238584375" x2="2.073578125" y2="1.279365625" width="0.001" layer="94"/>
<wire x1="2.073578125" y1="1.279365625" x2="2.094296875" y2="1.304665625" width="0.001" layer="94"/>
<wire x1="2.094296875" y1="1.304665625" x2="2.114209375" y2="1.330375" width="0.001" layer="94"/>
<wire x1="2.114209375" y1="1.330375" x2="2.133296875" y2="1.356484375" width="0.001" layer="94"/>
<wire x1="2.133296875" y1="1.356484375" x2="2.15156875" y2="1.382984375" width="0.001" layer="94"/>
<wire x1="2.15156875" y1="1.382984375" x2="2.16901875" y2="1.409865625" width="0.001" layer="94"/>
<wire x1="2.16901875" y1="1.409865625" x2="2.1856375" y2="1.437103125" width="0.001" layer="94"/>
<wire x1="2.1856375" y1="1.437103125" x2="2.201428125" y2="1.46469375" width="0.001" layer="94"/>
<wire x1="2.201428125" y1="1.46469375" x2="2.216396875" y2="1.492615625" width="0.001" layer="94"/>
<wire x1="2.216396875" y1="1.492615625" x2="2.23051875" y2="1.520875" width="0.001" layer="94"/>
<wire x1="2.23051875" y1="1.520875" x2="2.243809375" y2="1.549434375" width="0.001" layer="94"/>
<wire x1="2.243809375" y1="1.549434375" x2="2.256246875" y2="1.578303125" width="0.001" layer="94"/>
<wire x1="2.256246875" y1="1.578303125" x2="2.267846875" y2="1.607453125" width="0.001" layer="94"/>
<wire x1="2.267846875" y1="1.607453125" x2="2.278596875" y2="1.636875" width="0.001" layer="94"/>
<wire x1="2.278596875" y1="1.636875" x2="2.2884875" y2="1.666565625" width="0.001" layer="94"/>
<wire x1="2.2884875" y1="1.666565625" x2="2.2975375" y2="1.696503125" width="0.001" layer="94"/>
<wire x1="2.2975375" y1="1.696503125" x2="2.30571875" y2="1.726675" width="0.001" layer="94"/>
<wire x1="2.30571875" y1="1.726675" x2="2.3130375" y2="1.757075" width="0.001" layer="94"/>
<wire x1="2.3130375" y1="1.757075" x2="2.319496875" y2="1.787684375" width="0.001" layer="94"/>
<wire x1="2.319496875" y1="1.787684375" x2="2.325078125" y2="1.818484375" width="0.001" layer="94"/>
<wire x1="2.325078125" y1="1.818484375" x2="2.329796875" y2="1.849475" width="0.001" layer="94"/>
<wire x1="2.329796875" y1="1.849475" x2="2.3336375" y2="1.88064375" width="0.001" layer="94"/>
<wire x1="2.3336375" y1="1.88064375" x2="2.336609375" y2="1.911965625" width="0.001" layer="94"/>
<wire x1="2.336609375" y1="1.911965625" x2="2.3386875" y2="1.943434375" width="0.001" layer="94"/>
<wire x1="2.3386875" y1="1.943434375" x2="2.3398875" y2="1.97504375" width="0.001" layer="94"/>
<wire x1="2.3398875" y1="1.97504375" x2="2.340196875" y2="2.006765625" width="0.001" layer="94"/>
<wire x1="2.340196875" y1="2.006765625" x2="2.339609375" y2="2.038603125" width="0.001" layer="94"/>
<wire x1="2.339609375" y1="2.038603125" x2="2.3381375" y2="2.07054375" width="0.001" layer="94"/>
<wire x1="2.3381375" y1="2.07054375" x2="2.33576875" y2="2.102553125" width="0.001" layer="94"/>
<wire x1="2.33576875" y1="2.102553125" x2="2.3324875" y2="2.13464375" width="0.001" layer="94"/>
<wire x1="2.3324875" y1="2.13464375" x2="2.328309375" y2="2.16679375" width="0.001" layer="94"/>
<wire x1="2.328309375" y1="2.16679375" x2="2.323228125" y2="2.19899375" width="0.001" layer="94"/>
<wire x1="2.323228125" y1="2.19899375" x2="2.317228125" y2="2.231215625" width="0.001" layer="94"/>
<wire x1="2.317228125" y1="2.231215625" x2="2.305009375" y2="2.285753125" width="0.001" layer="94"/>
<wire x1="2.305009375" y1="2.285753125" x2="2.29046875" y2="2.338184375" width="0.001" layer="94"/>
<wire x1="2.29046875" y1="2.338184375" x2="2.273296875" y2="2.389275" width="0.001" layer="94"/>
<wire x1="2.273296875" y1="2.389275" x2="2.253146875" y2="2.439784375" width="0.001" layer="94"/>
<wire x1="2.253146875" y1="2.439784375" x2="2.2296875" y2="2.490475" width="0.001" layer="94"/>
<wire x1="2.2296875" y1="2.490475" x2="2.202609375" y2="2.542134375" width="0.001" layer="94"/>
<wire x1="2.202609375" y1="2.542134375" x2="2.171559375" y2="2.595503125" width="0.001" layer="94"/>
<wire x1="2.171559375" y1="2.595503125" x2="2.136209375" y2="2.651375" width="0.001" layer="94"/>
<wire x1="2.136209375" y1="2.651375" x2="2.09891875" y2="2.708265625" width="0.001" layer="94"/>
<wire x1="2.09891875" y1="2.708265625" x2="3.138278125" y2="3.747775" width="0.001" layer="94"/>
<wire x1="3.138278125" y1="3.747775" x2="3.6610375" y2="4.270075" width="0.001" layer="94"/>
<wire x1="3.6610375" y1="4.270075" x2="3.982346875" y2="4.589425" width="0.001" layer="94"/>
<wire x1="3.982346875" y1="4.589425" x2="4.081759375" y2="4.687125" width="0.001" layer="94"/>
<wire x1="4.081759375" y1="4.687125" x2="4.14796875" y2="4.751034375" width="0.001" layer="94"/>
<wire x1="4.14796875" y1="4.751034375" x2="4.186696875" y2="4.786815625" width="0.001" layer="94"/>
<wire x1="4.186696875" y1="4.786815625" x2="4.197546875" y2="4.795925" width="0.001" layer="94"/>
<wire x1="4.197546875" y1="4.795925" x2="4.203678125" y2="4.800115625" width="0.001" layer="94"/>
<wire x1="4.203678125" y1="4.800115625" x2="4.214009375" y2="4.804603125" width="0.001" layer="94"/>
<wire x1="4.214009375" y1="4.804603125" x2="4.2249375" y2="4.80824375" width="0.001" layer="94"/>
<wire x1="4.2249375" y1="4.80824375" x2="4.236246875" y2="4.811003125" width="0.001" layer="94"/>
<wire x1="4.236246875" y1="4.811003125" x2="4.24771875" y2="4.812853125" width="0.001" layer="94"/>
<wire x1="4.24771875" y1="4.812853125" x2="4.259128125" y2="4.813765625" width="0.001" layer="94"/>
<wire x1="4.259128125" y1="4.813765625" x2="4.270246875" y2="4.813725" width="0.001" layer="94"/>
<wire x1="4.270246875" y1="4.813725" x2="4.28086875" y2="4.81269375" width="0.001" layer="94"/>
<wire x1="4.28086875" y1="4.81269375" x2="4.29076875" y2="4.810653125" width="0.001" layer="94"/>
<wire x1="4.29076875" y1="4.810653125" x2="4.29686875" y2="4.808365625" width="0.001" layer="94"/>
<wire x1="4.29686875" y1="4.808365625" x2="4.3023375" y2="4.805015625" width="0.001" layer="94"/>
<wire x1="4.3023375" y1="4.805015625" x2="4.3071875" y2="4.800684375" width="0.001" layer="94"/>
<wire x1="4.3071875" y1="4.800684375" x2="4.3113875" y2="4.79544375" width="0.001" layer="94"/>
<wire x1="4.3113875" y1="4.79544375" x2="4.3149375" y2="4.789375" width="0.001" layer="94"/>
<wire x1="4.3149375" y1="4.789375" x2="4.317828125" y2="4.78254375" width="0.001" layer="94"/>
<wire x1="4.317828125" y1="4.78254375" x2="4.3200375" y2="4.77504375" width="0.001" layer="94"/>
<wire x1="4.3200375" y1="4.77504375" x2="4.32156875" y2="4.766934375" width="0.001" layer="94"/>
<wire x1="4.32156875" y1="4.766934375" x2="4.322528125" y2="4.749215625" width="0.001" layer="94"/>
<wire x1="4.322528125" y1="4.749215625" x2="4.320609375" y2="4.72999375" width="0.001" layer="94"/>
<wire x1="4.320609375" y1="4.72999375" x2="4.315746875" y2="4.709875" width="0.001" layer="94"/>
<wire x1="4.315746875" y1="4.709875" x2="4.3078375" y2="4.689475" width="0.001" layer="94"/>
<wire x1="4.3078375" y1="4.689475" x2="4.304296875" y2="4.683353125" width="0.001" layer="94"/>
<wire x1="4.304296875" y1="4.683353125" x2="4.297678125" y2="4.674434375" width="0.001" layer="94"/>
<wire x1="4.297678125" y1="4.674434375" x2="4.27071875" y2="4.643675" width="0.001" layer="94"/>
<wire x1="4.27071875" y1="4.643675" x2="4.2179875" y2="4.588084375" width="0.001" layer="94"/>
<wire x1="4.2179875" y1="4.588084375" x2="4.130496875" y2="4.49854375" width="0.001" layer="94"/>
<wire x1="4.130496875" y1="4.49854375" x2="3.815378125" y2="4.181115625" width="0.001" layer="94"/>
<wire x1="3.815378125" y1="4.181115625" x2="3.25351875" y2="3.61844375" width="0.001" layer="94"/>
<wire x1="3.25351875" y1="3.61844375" x2="2.213146875" y2="2.577415625" width="0.001" layer="94"/>
<wire x1="2.213146875" y1="2.577415625" x2="2.263046875" y2="2.52594375" width="0.001" layer="94"/>
<wire x1="2.263046875" y1="2.52594375" x2="2.2967375" y2="2.489815625" width="0.001" layer="94"/>
<wire x1="2.2967375" y1="2.489815625" x2="2.328578125" y2="2.452865625" width="0.001" layer="94"/>
<wire x1="2.328578125" y1="2.452865625" x2="2.358546875" y2="2.415125" width="0.001" layer="94"/>
<wire x1="2.358546875" y1="2.415125" x2="2.38661875" y2="2.37664375" width="0.001" layer="94"/>
<wire x1="2.38661875" y1="2.37664375" x2="2.4127875" y2="2.33744375" width="0.001" layer="94"/>
<wire x1="2.4127875" y1="2.33744375" x2="2.4370375" y2="2.297575" width="0.001" layer="94"/>
<wire x1="2.4370375" y1="2.297575" x2="2.4593375" y2="2.257065625" width="0.001" layer="94"/>
<wire x1="2.4593375" y1="2.257065625" x2="2.4796875" y2="2.21594375" width="0.001" layer="94"/>
<wire x1="2.4796875" y1="2.21594375" x2="2.498059375" y2="2.174253125" width="0.001" layer="94"/>
<wire x1="2.498059375" y1="2.174253125" x2="2.514446875" y2="2.132034375" width="0.001" layer="94"/>
<wire x1="2.514446875" y1="2.132034375" x2="2.528828125" y2="2.089315625" width="0.001" layer="94"/>
<wire x1="2.528828125" y1="2.089315625" x2="2.541178125" y2="2.046134375" width="0.001" layer="94"/>
<wire x1="2.541178125" y1="2.046134375" x2="2.551496875" y2="2.002525" width="0.001" layer="94"/>
<wire x1="2.551496875" y1="2.002525" x2="2.559759375" y2="1.958515625" width="0.001" layer="94"/>
<wire x1="2.559759375" y1="1.958515625" x2="2.565946875" y2="1.914153125" width="0.001" layer="94"/>
<wire x1="2.565946875" y1="1.914153125" x2="2.5700375" y2="1.869475" width="0.001" layer="94"/>
<wire x1="2.5700375" y1="1.869475" x2="2.572028125" y2="1.825053125" width="0.001" layer="94"/>
<wire x1="2.572028125" y1="1.825053125" x2="2.571978125" y2="1.78049375" width="0.001" layer="94"/>
<wire x1="2.571978125" y1="1.78049375" x2="2.56991875" y2="1.736003125" width="0.001" layer="94"/>
<wire x1="2.56991875" y1="1.736003125" x2="2.565878125" y2="1.691775" width="0.001" layer="94"/>
<wire x1="2.565878125" y1="1.691775" x2="2.55986875" y2="1.648025" width="0.001" layer="94"/>
<wire x1="2.55986875" y1="1.648025" x2="2.55191875" y2="1.60494375" width="0.001" layer="94"/>
<wire x1="2.55191875" y1="1.60494375" x2="2.542046875" y2="1.56274375" width="0.001" layer="94"/>
<wire x1="2.542046875" y1="1.56274375" x2="2.530278125" y2="1.521603125" width="0.001" layer="94"/>
<wire x1="2.530278125" y1="1.521603125" x2="2.515978125" y2="1.475984375" width="0.001" layer="94"/>
<wire x1="2.515978125" y1="1.475984375" x2="2.530878125" y2="1.450703125" width="0.001" layer="94"/>
<wire x1="2.530878125" y1="1.450703125" x2="2.548928125" y2="1.418475" width="0.001" layer="94"/>
<wire x1="2.548928125" y1="1.418475" x2="2.56556875" y2="1.38564375" width="0.001" layer="94"/>
<wire x1="2.56556875" y1="1.38564375" x2="2.579796875" y2="1.354284375" width="0.001" layer="94"/>
<wire x1="2.579796875" y1="1.354284375" x2="2.590646875" y2="1.326425" width="0.001" layer="94"/>
<wire x1="2.590646875" y1="1.326425" x2="2.596359375" y2="1.306815625" width="0.001" layer="94"/>
<wire x1="2.596359375" y1="1.306815625" x2="2.6001875" y2="1.283984375" width="0.001" layer="94"/>
<wire x1="2.6001875" y1="1.283984375" x2="2.602759375" y2="1.252653125" width="0.001" layer="94"/>
<wire x1="2.602759375" y1="1.252653125" x2="2.6046875" y2="1.207553125" width="0.001" layer="94"/>
<wire x1="2.6046875" y1="1.207553125" x2="2.607628125" y2="1.120634375" width="0.001" layer="94"/>
<wire x1="2.607628125" y1="1.120634375" x2="2.985128125" y2="1.494715625" width="0.001" layer="94"/>
<wire x1="2.985128125" y1="1.494715625" x2="3.425628125" y2="1.93119375" width="0.001" layer="94"/>
<wire x1="3.425628125" y1="1.93119375" x2="3.553559375" y2="2.05749375" width="0.001" layer="94"/>
<wire x1="3.553559375" y1="2.05749375" x2="3.635359375" y2="2.137334375" width="0.001" layer="94"/>
<wire x1="3.635359375" y1="2.137334375" x2="3.682546875" y2="2.181853125" width="0.001" layer="94"/>
<wire x1="3.682546875" y1="2.181853125" x2="3.6967875" y2="2.194353125" width="0.001" layer="94"/>
<wire x1="3.6967875" y1="2.194353125" x2="3.706696875" y2="2.20219375" width="0.001" layer="94"/>
<wire x1="3.706696875" y1="2.20219375" x2="3.7137375" y2="2.206765625" width="0.001" layer="94"/>
<wire x1="3.7137375" y1="2.206765625" x2="3.719346875" y2="2.209475" width="0.001" layer="94"/>
<wire x1="3.719346875" y1="2.209475" x2="3.7320375" y2="2.21484375" width="0.001" layer="94"/>
<wire x1="3.7320375" y1="2.21484375" x2="3.752959375" y2="2.224484375" width="0.001" layer="94"/>
<wire x1="3.752959375" y1="2.224484375" x2="3.76956875" y2="2.230415625" width="0.001" layer="94"/>
<wire x1="3.76956875" y1="2.230415625" x2="3.7846875" y2="2.233425" width="0.001" layer="94"/>
<wire x1="3.7846875" y1="2.233425" x2="3.8011875" y2="2.234315625" width="0.001" layer="94"/>
<wire x1="3.8011875" y1="2.234315625" x2="3.817878125" y2="2.233965625" width="0.001" layer="94"/>
<wire x1="3.817878125" y1="2.233965625" x2="3.823828125" y2="2.233215625" width="0.001" layer="94"/>
<wire x1="3.823828125" y1="2.233215625" x2="3.828728125" y2="2.231865625" width="0.001" layer="94"/>
<wire x1="3.828728125" y1="2.231865625" x2="3.832996875" y2="2.229775" width="0.001" layer="94"/>
<wire x1="3.832996875" y1="2.229775" x2="3.837046875" y2="2.226775" width="0.001" layer="94"/>
<wire x1="3.837046875" y1="2.226775" x2="3.846159375" y2="2.217453125" width="0.001" layer="94"/>
<wire x1="3.846159375" y1="2.217453125" x2="3.854509375" y2="2.20729375" width="0.001" layer="94"/>
<wire x1="3.854509375" y1="2.20729375" x2="3.8570875" y2="2.202903125" width="0.001" layer="94"/>
<wire x1="3.8570875" y1="2.202903125" x2="3.858759375" y2="2.198353125" width="0.001" layer="94"/>
<wire x1="3.858759375" y1="2.198353125" x2="3.859659375" y2="2.19319375" width="0.001" layer="94"/>
<wire x1="3.859659375" y1="2.19319375" x2="3.859896875" y2="2.186953125" width="0.001" layer="94"/>
<wire x1="3.859896875" y1="2.186953125" x2="3.8588875" y2="2.169384375" width="0.001" layer="94"/>
<wire x1="3.8588875" y1="2.169384375" x2="3.85671875" y2="2.152975" width="0.001" layer="94"/>
<wire x1="3.85671875" y1="2.152975" x2="3.8526875" y2="2.137015625" width="0.001" layer="94"/>
<wire x1="3.8526875" y1="2.137015625" x2="3.846396875" y2="2.12024375" width="0.001" layer="94"/>
<wire x1="3.846396875" y1="2.12024375" x2="3.8374375" y2="2.101403125" width="0.001" layer="94"/>
<wire x1="3.8374375" y1="2.101403125" x2="3.833578125" y2="2.094525" width="0.001" layer="94"/>
<wire x1="3.833578125" y1="2.094525" x2="3.8279875" y2="2.086275" width="0.001" layer="94"/>
<wire x1="3.8279875" y1="2.086275" x2="3.8195875" y2="2.07554375" width="0.001" layer="94"/>
<wire x1="3.8195875" y1="2.07554375" x2="3.807278125" y2="2.06119375" width="0.001" layer="94"/>
<wire x1="3.807278125" y1="2.06119375" x2="3.766528125" y2="2.017203125" width="0.001" layer="94"/>
<wire x1="3.766528125" y1="2.017203125" x2="3.696946875" y2="1.945325" width="0.001" layer="94"/>
<wire x1="3.696946875" y1="1.945325" x2="3.436228125" y2="1.682003125" width="0.001" layer="94"/>
<wire x1="3.436228125" y1="1.682003125" x2="2.954878125" y2="1.199475" width="0.001" layer="94"/>
<wire x1="2.954878125" y1="1.199475" x2="2.09126875" y2="0.334475" width="0.001" layer="94"/>
<wire x1="2.09126875" y1="0.334475" x2="2.046459375" y2="0.266975" width="0.001" layer="94"/>
<wire x1="2.046459375" y1="0.266975" x2="2.0083375" y2="0.208734375" width="0.001" layer="94"/>
<wire x1="2.0083375" y1="0.208734375" x2="1.970409375" y2="0.14914375" width="0.001" layer="94"/>
<wire x1="1.970409375" y1="0.14914375" x2="1.932959375" y2="0.088703125" width="0.001" layer="94"/>
<wire x1="1.932959375" y1="0.088703125" x2="1.896309375" y2="0.02794375" width="0.001" layer="94"/>
<wire x1="1.896309375" y1="0.02794375" x2="1.86076875" y2="-0.032625" width="0.001" layer="94"/>
<wire x1="1.86076875" y1="-0.032625" x2="1.826628125" y2="-0.092484375" width="0.001" layer="94"/>
<wire x1="1.826628125" y1="-0.092484375" x2="1.794209375" y2="-0.151125" width="0.001" layer="94"/>
<wire x1="1.794209375" y1="-0.151125" x2="1.763809375" y2="-0.208025" width="0.001" layer="94"/>
<wire x1="1.763809375" y1="-0.208025" x2="1.745359375" y2="-0.244346875" width="0.001" layer="94"/>
<wire x1="1.745359375" y1="-0.244346875" x2="1.732659375" y2="-0.272396875" width="0.001" layer="94"/>
<wire x1="1.732659375" y1="-0.272396875" x2="1.7245375" y2="-0.295034375" width="0.001" layer="94"/>
<wire x1="1.7245375" y1="-0.295034375" x2="1.719809375" y2="-0.315125" width="0.001" layer="94"/>
<wire x1="1.719809375" y1="-0.315125" x2="1.7176875" y2="-0.330146875" width="0.001" layer="94"/>
<wire x1="1.7176875" y1="-0.330146875" x2="1.7164375" y2="-0.345596875" width="0.001" layer="94"/>
<wire x1="1.7164375" y1="-0.345596875" x2="1.716046875" y2="-0.361384375" width="0.001" layer="94"/>
<wire x1="1.716046875" y1="-0.361384375" x2="1.7164875" y2="-0.37745625" width="0.001" layer="94"/>
<wire x1="1.7164875" y1="-0.37745625" x2="1.717759375" y2="-0.393734375" width="0.001" layer="94"/>
<wire x1="1.717759375" y1="-0.393734375" x2="1.719828125" y2="-0.41015625" width="0.001" layer="94"/>
<wire x1="1.719828125" y1="-0.41015625" x2="1.7226875" y2="-0.426634375" width="0.001" layer="94"/>
<wire x1="1.7226875" y1="-0.426634375" x2="1.72631875" y2="-0.443125" width="0.001" layer="94"/>
<wire x1="1.72631875" y1="-0.443125" x2="1.735828125" y2="-0.475825" width="0.001" layer="94"/>
<wire x1="1.735828125" y1="-0.475825" x2="1.74166875" y2="-0.491896875" width="0.001" layer="94"/>
<wire x1="1.74166875" y1="-0.491896875" x2="1.748209375" y2="-0.507684375" width="0.001" layer="94"/>
<wire x1="1.748209375" y1="-0.507684375" x2="1.7554375" y2="-0.523125" width="0.001" layer="94"/>
<wire x1="1.7554375" y1="-0.523125" x2="1.7633375" y2="-0.53815625" width="0.001" layer="94"/>
<wire x1="1.7633375" y1="-0.53815625" x2="1.7718875" y2="-0.552684375" width="0.001" layer="94"/>
<wire x1="1.7718875" y1="-0.552684375" x2="1.781078125" y2="-0.566665625" width="0.001" layer="94"/>
<wire x1="1.781078125" y1="-0.566665625" x2="1.7976375" y2="-0.590525" width="0.001" layer="94"/>
<wire x1="1.7976375" y1="-0.590525" x2="1.797046875" y2="-0.660525" width="0.001" layer="94"/>
<wire x1="1.797046875" y1="-0.660525" x2="1.796278125" y2="-0.698396875" width="0.001" layer="94"/>
<wire x1="1.796278125" y1="-0.698396875" x2="1.79421875" y2="-0.725884375" width="0.001" layer="94"/>
<wire x1="1.79421875" y1="-0.725884375" x2="1.7901375" y2="-0.749184375" width="0.001" layer="94"/>
<wire x1="1.7901375" y1="-0.749184375" x2="1.78326875" y2="-0.774465625" width="0.001" layer="94"/>
<wire x1="1.78326875" y1="-0.774465625" x2="1.77731875" y2="-0.792896875" width="0.001" layer="94"/>
<wire x1="1.77731875" y1="-0.792896875" x2="1.77051875" y2="-0.811434375" width="0.001" layer="94"/>
<wire x1="1.77051875" y1="-0.811434375" x2="1.754396875" y2="-0.848765625" width="0.001" layer="94"/>
<wire x1="1.754396875" y1="-0.848765625" x2="1.734978125" y2="-0.886325" width="0.001" layer="94"/>
<wire x1="1.734978125" y1="-0.886325" x2="1.712309375" y2="-0.92395625" width="0.001" layer="94"/>
<wire x1="1.712309375" y1="-0.92395625" x2="1.662246875" y2="-0.996696875" width="0.001" layer="94"/>
<wire x1="1.662246875" y1="-0.996696875" x2="1.58086875" y2="-1.11120625" width="0.001" layer="94"/>
<wire x1="1.58086875" y1="-1.11120625" x2="1.4963875" y2="-1.228065625" width="0.001" layer="94"/>
<wire x1="1.4963875" y1="-1.228065625" x2="1.437078125" y2="-1.307884375" width="0.001" layer="94"/>
<wire x1="1.437078125" y1="-1.307884375" x2="1.414178125" y2="-1.336775" width="0.001" layer="94"/>
<wire x1="1.414178125" y1="-1.336775" x2="1.3919875" y2="-1.363284375" width="0.001" layer="94"/>
<wire x1="1.3919875" y1="-1.363284375" x2="1.370228125" y2="-1.387575" width="0.001" layer="94"/>
<wire x1="1.370228125" y1="-1.387575" x2="1.3486375" y2="-1.409834375" width="0.001" layer="94"/>
<wire x1="1.3486375" y1="-1.409834375" x2="1.3269375" y2="-1.430225" width="0.001" layer="94"/>
<wire x1="1.3269375" y1="-1.430225" x2="1.304859375" y2="-1.448925" width="0.001" layer="94"/>
<wire x1="1.304859375" y1="-1.448925" x2="1.282109375" y2="-1.466096875" width="0.001" layer="94"/>
<wire x1="1.282109375" y1="-1.466096875" x2="1.2584375" y2="-1.481915625" width="0.001" layer="94"/>
<wire x1="1.2584375" y1="-1.481915625" x2="1.233559375" y2="-1.49655625" width="0.001" layer="94"/>
<wire x1="1.233559375" y1="-1.49655625" x2="1.207196875" y2="-1.510184375" width="0.001" layer="94"/>
<wire x1="1.207196875" y1="-1.510184375" x2="1.1790875" y2="-1.522984375" width="0.001" layer="94"/>
<wire x1="1.1790875" y1="-1.522984375" x2="1.148946875" y2="-1.535115625" width="0.001" layer="94"/>
<wire x1="1.148946875" y1="-1.535115625" x2="1.116509375" y2="-1.546746875" width="0.001" layer="94"/>
<wire x1="1.116509375" y1="-1.546746875" x2="1.0814875" y2="-1.558065625" width="0.001" layer="94"/>
<wire x1="1.0814875" y1="-1.558065625" x2="1.04361875" y2="-1.569234375" width="0.001" layer="94"/>
<wire x1="1.04361875" y1="-1.569234375" x2="1.0026375" y2="-1.580425" width="0.001" layer="94"/>
<wire x1="1.0026375" y1="-1.580425" x2="0.932196875" y2="-1.597875" width="0.001" layer="94"/>
<wire x1="0.932196875" y1="-1.597875" x2="0.863309375" y2="-1.612496875" width="0.001" layer="94"/>
<wire x1="0.863309375" y1="-1.612496875" x2="0.7944875" y2="-1.624434375" width="0.001" layer="94"/>
<wire x1="0.7944875" y1="-1.624434375" x2="0.724278125" y2="-1.633884375" width="0.001" layer="94"/>
<wire x1="0.724278125" y1="-1.633884375" x2="0.651209375" y2="-1.64100625" width="0.001" layer="94"/>
<wire x1="0.651209375" y1="-1.64100625" x2="0.573796875" y2="-1.64595625" width="0.001" layer="94"/>
<wire x1="0.573796875" y1="-1.64595625" x2="0.490596875" y2="-1.648915625" width="0.001" layer="94"/>
<wire x1="0.490596875" y1="-1.648915625" x2="0.4001375" y2="-1.65005625" width="0.001" layer="94"/>
<wire x1="0.4001375" y1="-1.65005625" x2="0.296659375" y2="-1.649484375" width="0.001" layer="94"/>
<wire x1="0.296659375" y1="-1.649484375" x2="0.259428125" y2="-1.648084375" width="0.001" layer="94"/>
<wire x1="0.259428125" y1="-1.648084375" x2="0.228709375" y2="-1.645596875" width="0.001" layer="94"/>
<wire x1="0.228709375" y1="-1.645596875" x2="0.2020875" y2="-1.641734375" width="0.001" layer="94"/>
<wire x1="0.2020875" y1="-1.641734375" x2="0.17721875" y2="-1.636265625" width="0.001" layer="94"/>
<wire x1="0.17721875" y1="-1.636265625" x2="0.151709375" y2="-1.62890625" width="0.001" layer="94"/>
<wire x1="0.151709375" y1="-1.62890625" x2="0.12316875" y2="-1.619396875" width="0.001" layer="94"/>
<wire x1="0.12316875" y1="-1.619396875" x2="0.095359375" y2="-1.610415625" width="0.001" layer="94"/>
<wire x1="0.095359375" y1="-1.610415625" x2="0.071796875" y2="-1.604725" width="0.001" layer="94"/>
<wire x1="0.071796875" y1="-1.604725" x2="0.047546875" y2="-1.601375" width="0.001" layer="94"/>
<wire x1="0.047546875" y1="-1.601375" x2="0.0176375" y2="-1.599446875" width="0.001" layer="94"/>
<wire x1="0.0176375" y1="-1.599446875" x2="-0.012690625" y2="-1.59895625" width="0.001" layer="94"/>
<wire x1="-0.012690625" y1="-1.59895625" x2="-0.0400625" y2="-1.60040625" width="0.001" layer="94"/>
<wire x1="-0.0400625" y1="-1.60040625" x2="-0.06478125" y2="-1.603915625" width="0.001" layer="94"/>
<wire x1="-0.06478125" y1="-1.603915625" x2="-0.087171875" y2="-1.609565625" width="0.001" layer="94"/>
<wire x1="-0.087171875" y1="-1.609565625" x2="-0.107553125" y2="-1.617475" width="0.001" layer="94"/>
<wire x1="-0.107553125" y1="-1.617475" x2="-0.126253125" y2="-1.627734375" width="0.001" layer="94"/>
<wire x1="-0.126253125" y1="-1.627734375" x2="-0.14358125" y2="-1.640446875" width="0.001" layer="94"/>
<wire x1="-0.14358125" y1="-1.640446875" x2="-0.1598625" y2="-1.655715625" width="0.001" layer="94"/>
<wire x1="-0.1598625" y1="-1.655715625" x2="-0.1741125" y2="-1.669896875" width="0.001" layer="94"/>
<wire x1="-0.1741125" y1="-1.669896875" x2="-0.190990625" y2="-1.685525" width="0.001" layer="94"/>
<wire x1="-0.190990625" y1="-1.685525" x2="-0.229990625" y2="-1.718925" width="0.001" layer="94"/>
<wire x1="-0.229990625" y1="-1.718925" x2="-0.2715125" y2="-1.751525" width="0.001" layer="94"/>
<wire x1="-0.2715125" y1="-1.751525" x2="-0.310203125" y2="-1.778946875" width="0.001" layer="94"/>
<wire x1="-0.310203125" y1="-1.778946875" x2="-0.337921875" y2="-1.79640625" width="0.001" layer="94"/>
<wire x1="-0.337921875" y1="-1.79640625" x2="-0.366453125" y2="-1.81285625" width="0.001" layer="94"/>
<wire x1="-0.366453125" y1="-1.81285625" x2="-0.395753125" y2="-1.828265625" width="0.001" layer="94"/>
<wire x1="-0.395753125" y1="-1.828265625" x2="-0.425771875" y2="-1.842634375" width="0.001" layer="94"/>
<wire x1="-0.425771875" y1="-1.842634375" x2="-0.456440625" y2="-1.855946875" width="0.001" layer="94"/>
<wire x1="-0.456440625" y1="-1.855946875" x2="-0.48773125" y2="-1.868175" width="0.001" layer="94"/>
<wire x1="-0.48773125" y1="-1.868175" x2="-0.5195625" y2="-1.879325" width="0.001" layer="94"/>
<wire x1="-0.5195625" y1="-1.879325" x2="-0.551903125" y2="-1.88935625" width="0.001" layer="94"/>
<wire x1="-0.551903125" y1="-1.88935625" x2="-0.584690625" y2="-1.898275" width="0.001" layer="94"/>
<wire x1="-0.584690625" y1="-1.898275" x2="-0.61788125" y2="-1.906046875" width="0.001" layer="94"/>
<wire x1="-0.61788125" y1="-1.906046875" x2="-0.6514125" y2="-1.912665625" width="0.001" layer="94"/>
<wire x1="-0.6514125" y1="-1.912665625" x2="-0.68523125" y2="-1.918125" width="0.001" layer="94"/>
<wire x1="-0.68523125" y1="-1.918125" x2="-0.719290625" y2="-1.922396875" width="0.001" layer="94"/>
<wire x1="-0.719290625" y1="-1.922396875" x2="-0.75353125" y2="-1.925465625" width="0.001" layer="94"/>
<wire x1="-0.75353125" y1="-1.925465625" x2="-0.7879125" y2="-1.927325" width="0.001" layer="94"/>
<wire x1="-0.7879125" y1="-1.927325" x2="-0.8223625" y2="-1.92795625" width="0.001" layer="94"/>
<wire x1="-0.8223625" y1="-1.92795625" x2="-0.849240625" y2="-1.927465625" width="0.001" layer="94"/>
<wire x1="-0.849240625" y1="-1.927465625" x2="-0.875853125" y2="-1.925946875" width="0.001" layer="94"/>
<wire x1="-0.875853125" y1="-1.925946875" x2="-0.902703125" y2="-1.923315625" width="0.001" layer="94"/>
<wire x1="-0.902703125" y1="-1.923315625" x2="-0.9303625" y2="-1.919484375" width="0.001" layer="94"/>
<wire x1="-0.9303625" y1="-1.919484375" x2="-0.959353125" y2="-1.914365625" width="0.001" layer="94"/>
<wire x1="-0.959353125" y1="-1.914365625" x2="-0.9902125" y2="-1.907865625" width="0.001" layer="94"/>
<wire x1="-0.9902125" y1="-1.907865625" x2="-1.023490625" y2="-1.899915625" width="0.001" layer="94"/>
<wire x1="-1.023490625" y1="-1.899915625" x2="-1.059721875" y2="-1.890415625" width="0.001" layer="94"/>
<wire x1="-1.059721875" y1="-1.890415625" x2="-1.127140625" y2="-1.873034375" width="0.001" layer="94"/>
<wire x1="-1.127140625" y1="-1.873034375" x2="-1.17258125" y2="-1.86295625" width="0.001" layer="94"/>
<wire x1="-1.17258125" y1="-1.86295625" x2="-1.201921875" y2="-1.859415625" width="0.001" layer="94"/>
<wire x1="-1.201921875" y1="-1.859415625" x2="-1.233021875" y2="-1.858046875" width="0.001" layer="94"/>
<wire x1="-1.233021875" y1="-1.858046875" x2="-1.265703125" y2="-1.858796875" width="0.001" layer="94"/>
<wire x1="-1.265703125" y1="-1.858796875" x2="-1.299771875" y2="-1.86160625" width="0.001" layer="94"/>
<wire x1="-1.299771875" y1="-1.86160625" x2="-1.335053125" y2="-1.866425" width="0.001" layer="94"/>
<wire x1="-1.335053125" y1="-1.866425" x2="-1.371353125" y2="-1.873196875" width="0.001" layer="94"/>
<wire x1="-1.371353125" y1="-1.873196875" x2="-1.408490625" y2="-1.881865625" width="0.001" layer="94"/>
<wire x1="-1.408490625" y1="-1.881865625" x2="-1.44628125" y2="-1.892375" width="0.001" layer="94"/>
<wire x1="-1.44628125" y1="-1.892375" x2="-1.484540625" y2="-1.904675" width="0.001" layer="94"/>
<wire x1="-1.484540625" y1="-1.904675" x2="-1.52308125" y2="-1.918696875" width="0.001" layer="94"/>
<wire x1="-1.52308125" y1="-1.918696875" x2="-1.5617125" y2="-1.93440625" width="0.001" layer="94"/>
<wire x1="-1.5617125" y1="-1.93440625" x2="-1.6002625" y2="-1.951725" width="0.001" layer="94"/>
<wire x1="-1.6002625" y1="-1.951725" x2="-1.63853125" y2="-1.97060625" width="0.001" layer="94"/>
<wire x1="-1.63853125" y1="-1.97060625" x2="-1.676353125" y2="-1.990996875" width="0.001" layer="94"/>
<wire x1="-1.676353125" y1="-1.990996875" x2="-1.713521875" y2="-2.012846875" width="0.001" layer="94"/>
<wire x1="-1.713521875" y1="-2.012846875" x2="-1.7498625" y2="-2.036084375" width="0.001" layer="94"/>
<wire x1="-1.7498625" y1="-2.036084375" x2="-1.78653125" y2="-2.05950625" width="0.001" layer="94"/>
<wire x1="-1.78653125" y1="-2.05950625" x2="-1.820753125" y2="-2.079046875" width="0.001" layer="94"/>
<wire x1="-1.820753125" y1="-2.079046875" x2="-1.853340625" y2="-2.094984375" width="0.001" layer="94"/>
<wire x1="-1.853340625" y1="-2.094984375" x2="-1.885090625" y2="-2.10760625" width="0.001" layer="94"/>
<wire x1="-1.885090625" y1="-2.10760625" x2="-1.916821875" y2="-2.11720625" width="0.001" layer="94"/>
<wire x1="-1.916821875" y1="-2.11720625" x2="-1.949321875" y2="-2.124046875" width="0.001" layer="94"/>
<wire x1="-1.949321875" y1="-2.124046875" x2="-1.983403125" y2="-2.128415625" width="0.001" layer="94"/>
<wire x1="-1.983403125" y1="-2.128415625" x2="-2.0198625" y2="-2.13060625" width="0.001" layer="94"/>
<wire x1="-2.0198625" y1="-2.13060625" x2="-2.07068125" y2="-2.130746875" width="0.001" layer="94"/>
<wire x1="-2.07068125" y1="-2.130746875" x2="-2.091321875" y2="-2.129825" width="0.001" layer="94"/>
<wire x1="-2.091321875" y1="-2.129825" x2="-2.105071875" y2="-2.128346875" width="0.001" layer="94"/>
<wire x1="-2.105071875" y1="-2.128346875" x2="-2.10508125" y2="-2.128365625" width="0.001" layer="94"/>
<wire x1="-2.014871875" y1="-1.310165625" x2="-1.99633125" y2="-1.305484375" width="0.001" layer="94"/>
<wire x1="-1.99633125" y1="-1.305484375" x2="-1.9777625" y2="-1.299965625" width="0.001" layer="94"/>
<wire x1="-1.9777625" y1="-1.299965625" x2="-1.95918125" y2="-1.293615625" width="0.001" layer="94"/>
<wire x1="-1.95918125" y1="-1.293615625" x2="-1.940621875" y2="-1.28645625" width="0.001" layer="94"/>
<wire x1="-1.940621875" y1="-1.28645625" x2="-1.903603125" y2="-1.269775" width="0.001" layer="94"/>
<wire x1="-1.903603125" y1="-1.269775" x2="-1.86683125" y2="-1.25005625" width="0.001" layer="94"/>
<wire x1="-1.86683125" y1="-1.25005625" x2="-1.830453125" y2="-1.227425" width="0.001" layer="94"/>
<wire x1="-1.830453125" y1="-1.227425" x2="-1.7946125" y2="-1.202015625" width="0.001" layer="94"/>
<wire x1="-1.7946125" y1="-1.202015625" x2="-1.75943125" y2="-1.173965625" width="0.001" layer="94"/>
<wire x1="-1.75943125" y1="-1.173965625" x2="-1.72508125" y2="-1.143384375" width="0.001" layer="94"/>
<wire x1="-1.72508125" y1="-1.143384375" x2="-1.691671875" y2="-1.110434375" width="0.001" layer="94"/>
<wire x1="-1.691671875" y1="-1.110434375" x2="-1.6593625" y2="-1.075215625" width="0.001" layer="94"/>
<wire x1="-1.6593625" y1="-1.075215625" x2="-1.62828125" y2="-1.037884375" width="0.001" layer="94"/>
<wire x1="-1.62828125" y1="-1.037884375" x2="-1.598571875" y2="-0.99855625" width="0.001" layer="94"/>
<wire x1="-1.598571875" y1="-0.99855625" x2="-1.570371875" y2="-0.95735625" width="0.001" layer="94"/>
<wire x1="-1.570371875" y1="-0.95735625" x2="-1.543821875" y2="-0.914434375" width="0.001" layer="94"/>
<wire x1="-1.543821875" y1="-0.914434375" x2="-1.519071875" y2="-0.869915625" width="0.001" layer="94"/>
<wire x1="-1.519071875" y1="-0.869915625" x2="-1.496240625" y2="-0.823925" width="0.001" layer="94"/>
<wire x1="-1.496240625" y1="-0.823925" x2="-1.4853625" y2="-0.799325" width="0.001" layer="94"/>
<wire x1="-1.4853625" y1="-0.799325" x2="-1.474253125" y2="-0.77175625" width="0.001" layer="94"/>
<wire x1="-1.474253125" y1="-0.77175625" x2="-1.463203125" y2="-0.742096875" width="0.001" layer="94"/>
<wire x1="-1.463203125" y1="-0.742096875" x2="-1.452503125" y2="-0.711215625" width="0.001" layer="94"/>
<wire x1="-1.452503125" y1="-0.711215625" x2="-1.433340625" y2="-0.64930625" width="0.001" layer="94"/>
<wire x1="-1.433340625" y1="-0.64930625" x2="-1.425453125" y2="-0.620025" width="0.001" layer="94"/>
<wire x1="-1.425453125" y1="-0.620025" x2="-1.419090625" y2="-0.593034375" width="0.001" layer="94"/>
<wire x1="-1.419090625" y1="-0.593034375" x2="-1.41323125" y2="-0.561996875" width="0.001" layer="94"/>
<wire x1="-1.41323125" y1="-0.561996875" x2="-1.409771875" y2="-0.530734375" width="0.001" layer="94"/>
<wire x1="-1.409771875" y1="-0.530734375" x2="-1.408090625" y2="-0.490625" width="0.001" layer="94"/>
<wire x1="-1.408090625" y1="-0.490625" x2="-1.40758125" y2="-0.433034375" width="0.001" layer="94"/>
<wire x1="-1.40758125" y1="-0.433034375" x2="-1.407821875" y2="-0.374115625" width="0.001" layer="94"/>
<wire x1="-1.407821875" y1="-0.374115625" x2="-1.409303125" y2="-0.33570625" width="0.001" layer="94"/>
<wire x1="-1.409303125" y1="-0.33570625" x2="-1.410740625" y2="-0.321075" width="0.001" layer="94"/>
<wire x1="-1.410740625" y1="-0.321075" x2="-1.412771875" y2="-0.307834375" width="0.001" layer="94"/>
<wire x1="-1.412771875" y1="-0.307834375" x2="-1.418990625" y2="-0.280534375" width="0.001" layer="94"/>
<wire x1="-1.418990625" y1="-0.280534375" x2="-1.425503125" y2="-0.258915625" width="0.001" layer="94"/>
<wire x1="-1.425503125" y1="-0.258915625" x2="-1.43463125" y2="-0.233325" width="0.001" layer="94"/>
<wire x1="-1.43463125" y1="-0.233325" x2="-1.445171875" y2="-0.206965625" width="0.001" layer="94"/>
<wire x1="-1.445171875" y1="-0.206965625" x2="-1.455903125" y2="-0.183034375" width="0.001" layer="94"/>
<wire x1="-1.455903125" y1="-0.183034375" x2="-1.470440625" y2="-0.154434375" width="0.001" layer="94"/>
<wire x1="-1.470440625" y1="-0.154434375" x2="-1.483940625" y2="-0.132684375" width="0.001" layer="94"/>
<wire x1="-1.483940625" y1="-0.132684375" x2="-1.499890625" y2="-0.112875" width="0.001" layer="94"/>
<wire x1="-1.499890625" y1="-0.112875" x2="-1.52178125" y2="-0.090065625" width="0.001" layer="94"/>
<wire x1="-1.52178125" y1="-0.090065625" x2="-1.534590625" y2="-0.077834375" width="0.001" layer="94"/>
<wire x1="-1.534590625" y1="-0.077834375" x2="-1.547571875" y2="-0.066525" width="0.001" layer="94"/>
<wire x1="-1.547571875" y1="-0.066525" x2="-1.560740625" y2="-0.056125" width="0.001" layer="94"/>
<wire x1="-1.560740625" y1="-0.056125" x2="-1.57413125" y2="-0.046625" width="0.001" layer="94"/>
<wire x1="-1.57413125" y1="-0.046625" x2="-1.5877625" y2="-0.038015625" width="0.001" layer="94"/>
<wire x1="-1.5877625" y1="-0.038015625" x2="-1.6016625" y2="-0.030296875" width="0.001" layer="94"/>
<wire x1="-1.6016625" y1="-0.030296875" x2="-1.615840625" y2="-0.023446875" width="0.001" layer="94"/>
<wire x1="-1.615840625" y1="-0.023446875" x2="-1.630321875" y2="-0.01745625" width="0.001" layer="94"/>
<wire x1="-1.630321875" y1="-0.01745625" x2="-1.645140625" y2="-0.012334375" width="0.001" layer="94"/>
<wire x1="-1.645140625" y1="-0.012334375" x2="-1.660303125" y2="-0.00805625" width="0.001" layer="94"/>
<wire x1="-1.660303125" y1="-0.00805625" x2="-1.675840625" y2="-0.004625" width="0.001" layer="94"/>
<wire x1="-1.675840625" y1="-0.004625" x2="-1.691771875" y2="-0.002025" width="0.001" layer="94"/>
<wire x1="-1.691771875" y1="-0.002025" x2="-1.7081125" y2="-0.000246875" width="0.001" layer="94"/>
<wire x1="-1.7081125" y1="-0.000246875" x2="-1.724903125" y2="0.000715625" width="0.001" layer="94"/>
<wire x1="-1.724903125" y1="0.000715625" x2="-1.742140625" y2="0.000865625" width="0.001" layer="94"/>
<wire x1="-1.742140625" y1="0.000865625" x2="-1.759871875" y2="0.000215625" width="0.001" layer="94"/>
<wire x1="-1.759871875" y1="0.000215625" x2="-1.786503125" y2="-0.002234375" width="0.001" layer="94"/>
<wire x1="-1.786503125" y1="-0.002234375" x2="-1.813190625" y2="-0.00645625" width="0.001" layer="94"/>
<wire x1="-1.813190625" y1="-0.00645625" x2="-1.839890625" y2="-0.012415625" width="0.001" layer="94"/>
<wire x1="-1.839890625" y1="-0.012415625" x2="-1.8665625" y2="-0.02005625" width="0.001" layer="94"/>
<wire x1="-1.8665625" y1="-0.02005625" x2="-1.893153125" y2="-0.029334375" width="0.001" layer="94"/>
<wire x1="-1.893153125" y1="-0.029334375" x2="-1.91963125" y2="-0.040196875" width="0.001" layer="94"/>
<wire x1="-1.91963125" y1="-0.040196875" x2="-1.945940625" y2="-0.052596875" width="0.001" layer="94"/>
<wire x1="-1.945940625" y1="-0.052596875" x2="-1.972053125" y2="-0.066496875" width="0.001" layer="94"/>
<wire x1="-1.972053125" y1="-0.066496875" x2="-1.9979125" y2="-0.081846875" width="0.001" layer="94"/>
<wire x1="-1.9979125" y1="-0.081846875" x2="-2.02348125" y2="-0.098584375" width="0.001" layer="94"/>
<wire x1="-2.02348125" y1="-0.098584375" x2="-2.048721875" y2="-0.116684375" width="0.001" layer="94"/>
<wire x1="-2.048721875" y1="-0.116684375" x2="-2.07358125" y2="-0.136084375" width="0.001" layer="94"/>
<wire x1="-2.07358125" y1="-0.136084375" x2="-2.098021875" y2="-0.156746875" width="0.001" layer="94"/>
<wire x1="-2.098021875" y1="-0.156746875" x2="-2.121990625" y2="-0.178615625" width="0.001" layer="94"/>
<wire x1="-2.121990625" y1="-0.178615625" x2="-2.1454625" y2="-0.201646875" width="0.001" layer="94"/>
<wire x1="-2.1454625" y1="-0.201646875" x2="-2.16838125" y2="-0.225796875" width="0.001" layer="94"/>
<wire x1="-2.16838125" y1="-0.225796875" x2="-2.190703125" y2="-0.251015625" width="0.001" layer="94"/>
<wire x1="-2.190703125" y1="-0.251015625" x2="-2.212390625" y2="-0.277265625" width="0.001" layer="94"/>
<wire x1="-2.212390625" y1="-0.277265625" x2="-2.233403125" y2="-0.304475" width="0.001" layer="94"/>
<wire x1="-2.233403125" y1="-0.304475" x2="-2.25368125" y2="-0.332625" width="0.001" layer="94"/>
<wire x1="-2.25368125" y1="-0.332625" x2="-2.273203125" y2="-0.361646875" width="0.001" layer="94"/>
<wire x1="-2.273203125" y1="-0.361646875" x2="-2.2919125" y2="-0.39150625" width="0.001" layer="94"/>
<wire x1="-2.2919125" y1="-0.39150625" x2="-2.3097625" y2="-0.422146875" width="0.001" layer="94"/>
<wire x1="-2.3097625" y1="-0.422146875" x2="-2.326721875" y2="-0.453534375" width="0.001" layer="94"/>
<wire x1="-2.326721875" y1="-0.453534375" x2="-2.342740625" y2="-0.48560625" width="0.001" layer="94"/>
<wire x1="-2.342740625" y1="-0.48560625" x2="-2.357771875" y2="-0.518325" width="0.001" layer="94"/>
<wire x1="-2.357771875" y1="-0.518325" x2="-2.37178125" y2="-0.551646875" width="0.001" layer="94"/>
<wire x1="-2.37178125" y1="-0.551646875" x2="-2.38473125" y2="-0.58550625" width="0.001" layer="94"/>
<wire x1="-2.38473125" y1="-0.58550625" x2="-2.3965625" y2="-0.619875" width="0.001" layer="94"/>
<wire x1="-2.3965625" y1="-0.619875" x2="-2.40723125" y2="-0.65470625" width="0.001" layer="94"/>
<wire x1="-2.40723125" y1="-0.65470625" x2="-2.416703125" y2="-0.689934375" width="0.001" layer="94"/>
<wire x1="-2.416703125" y1="-0.689934375" x2="-2.424940625" y2="-0.725534375" width="0.001" layer="94"/>
<wire x1="-2.424940625" y1="-0.725534375" x2="-2.430190625" y2="-0.755375" width="0.001" layer="94"/>
<wire x1="-2.430190625" y1="-0.755375" x2="-2.434290625" y2="-0.789234375" width="0.001" layer="94"/>
<wire x1="-2.434290625" y1="-0.789234375" x2="-2.437203125" y2="-0.825725" width="0.001" layer="94"/>
<wire x1="-2.437203125" y1="-0.825725" x2="-2.438890625" y2="-0.863434375" width="0.001" layer="94"/>
<wire x1="-2.438890625" y1="-0.863434375" x2="-2.439303125" y2="-0.900946875" width="0.001" layer="94"/>
<wire x1="-2.439303125" y1="-0.900946875" x2="-2.438421875" y2="-0.936875" width="0.001" layer="94"/>
<wire x1="-2.438421875" y1="-0.936875" x2="-2.43618125" y2="-0.96980625" width="0.001" layer="94"/>
<wire x1="-2.43618125" y1="-0.96980625" x2="-2.432571875" y2="-0.998325" width="0.001" layer="94"/>
<wire x1="-2.432571875" y1="-0.998325" x2="-2.42848125" y2="-1.02025625" width="0.001" layer="94"/>
<wire x1="-2.42848125" y1="-1.02025625" x2="-2.423671875" y2="-1.04150625" width="0.001" layer="94"/>
<wire x1="-2.423671875" y1="-1.04150625" x2="-2.418153125" y2="-1.062065625" width="0.001" layer="94"/>
<wire x1="-2.418153125" y1="-1.062065625" x2="-2.411953125" y2="-1.081925" width="0.001" layer="94"/>
<wire x1="-2.411953125" y1="-1.081925" x2="-2.4050625" y2="-1.101065625" width="0.001" layer="94"/>
<wire x1="-2.4050625" y1="-1.101065625" x2="-2.397521875" y2="-1.119484375" width="0.001" layer="94"/>
<wire x1="-2.397521875" y1="-1.119484375" x2="-2.389321875" y2="-1.137175" width="0.001" layer="94"/>
<wire x1="-2.389321875" y1="-1.137175" x2="-2.380490625" y2="-1.154125" width="0.001" layer="94"/>
<wire x1="-2.380490625" y1="-1.154125" x2="-2.37103125" y2="-1.170315625" width="0.001" layer="94"/>
<wire x1="-2.37103125" y1="-1.170315625" x2="-2.36098125" y2="-1.185746875" width="0.001" layer="94"/>
<wire x1="-2.36098125" y1="-1.185746875" x2="-2.350321875" y2="-1.200396875" width="0.001" layer="94"/>
<wire x1="-2.350321875" y1="-1.200396875" x2="-2.339090625" y2="-1.21425625" width="0.001" layer="94"/>
<wire x1="-2.339090625" y1="-1.21425625" x2="-2.327303125" y2="-1.227325" width="0.001" layer="94"/>
<wire x1="-2.327303125" y1="-1.227325" x2="-2.314953125" y2="-1.239584375" width="0.001" layer="94"/>
<wire x1="-2.314953125" y1="-1.239584375" x2="-2.302071875" y2="-1.251025" width="0.001" layer="94"/>
<wire x1="-2.302071875" y1="-1.251025" x2="-2.288671875" y2="-1.261646875" width="0.001" layer="94"/>
<wire x1="-2.288671875" y1="-1.261646875" x2="-2.2747625" y2="-1.271425" width="0.001" layer="94"/>
<wire x1="-2.2747625" y1="-1.271425" x2="-2.2603625" y2="-1.280346875" width="0.001" layer="94"/>
<wire x1="-2.2603625" y1="-1.280346875" x2="-2.245471875" y2="-1.288415625" width="0.001" layer="94"/>
<wire x1="-2.245471875" y1="-1.288415625" x2="-2.23013125" y2="-1.29560625" width="0.001" layer="94"/>
<wire x1="-2.23013125" y1="-1.29560625" x2="-2.21433125" y2="-1.301925" width="0.001" layer="94"/>
<wire x1="-2.21433125" y1="-1.301925" x2="-2.198090625" y2="-1.307346875" width="0.001" layer="94"/>
<wire x1="-2.198090625" y1="-1.307346875" x2="-2.181440625" y2="-1.311865625" width="0.001" layer="94"/>
<wire x1="-2.181440625" y1="-1.311865625" x2="-2.164371875" y2="-1.315465625" width="0.001" layer="94"/>
<wire x1="-2.164371875" y1="-1.315465625" x2="-2.146921875" y2="-1.318146875" width="0.001" layer="94"/>
<wire x1="-2.146921875" y1="-1.318146875" x2="-2.12908125" y2="-1.319896875" width="0.001" layer="94"/>
<wire x1="-2.12908125" y1="-1.319896875" x2="-2.110871875" y2="-1.320696875" width="0.001" layer="94"/>
<wire x1="-2.110871875" y1="-1.320696875" x2="-2.092321875" y2="-1.320546875" width="0.001" layer="94"/>
<wire x1="-2.092321875" y1="-1.320546875" x2="-2.07343125" y2="-1.319425" width="0.001" layer="94"/>
<wire x1="-2.07343125" y1="-1.319425" x2="-2.0542125" y2="-1.317325" width="0.001" layer="94"/>
<wire x1="-2.0542125" y1="-1.317325" x2="-2.034690625" y2="-1.314246875" width="0.001" layer="94"/>
<wire x1="-2.034690625" y1="-1.314246875" x2="-2.014871875" y2="-1.310165625" width="0.001" layer="94"/>
<wire x1="0.688078125" y1="-1.225196875" x2="0.721009375" y2="-1.218425" width="0.001" layer="94"/>
<wire x1="0.721009375" y1="-1.218425" x2="0.754909375" y2="-1.209315625" width="0.001" layer="94"/>
<wire x1="0.754909375" y1="-1.209315625" x2="0.789396875" y2="-1.19800625" width="0.001" layer="94"/>
<wire x1="0.789396875" y1="-1.19800625" x2="0.824096875" y2="-1.18465625" width="0.001" layer="94"/>
<wire x1="0.824096875" y1="-1.18465625" x2="0.858646875" y2="-1.169415625" width="0.001" layer="94"/>
<wire x1="0.858646875" y1="-1.169415625" x2="0.89266875" y2="-1.15245625" width="0.001" layer="94"/>
<wire x1="0.89266875" y1="-1.15245625" x2="0.9257875" y2="-1.133915625" width="0.001" layer="94"/>
<wire x1="0.9257875" y1="-1.133915625" x2="0.957628125" y2="-1.11395625" width="0.001" layer="94"/>
<wire x1="0.957628125" y1="-1.11395625" x2="0.9897375" y2="-1.091515625" width="0.001" layer="94"/>
<wire x1="0.9897375" y1="-1.091515625" x2="1.020546875" y2="-1.067725" width="0.001" layer="94"/>
<wire x1="1.020546875" y1="-1.067725" x2="1.050009375" y2="-1.042634375" width="0.001" layer="94"/>
<wire x1="1.050009375" y1="-1.042634375" x2="1.07806875" y2="-1.016315625" width="0.001" layer="94"/>
<wire x1="1.07806875" y1="-1.016315625" x2="1.104709375" y2="-0.988825" width="0.001" layer="94"/>
<wire x1="1.104709375" y1="-0.988825" x2="1.129859375" y2="-0.960234375" width="0.001" layer="94"/>
<wire x1="1.129859375" y1="-0.960234375" x2="1.153496875" y2="-0.930596875" width="0.001" layer="94"/>
<wire x1="1.153496875" y1="-0.930596875" x2="1.17556875" y2="-0.899975" width="0.001" layer="94"/>
<wire x1="1.17556875" y1="-0.899975" x2="1.196028125" y2="-0.868434375" width="0.001" layer="94"/>
<wire x1="1.196028125" y1="-0.868434375" x2="1.2148375" y2="-0.836046875" width="0.001" layer="94"/>
<wire x1="1.2148375" y1="-0.836046875" x2="1.231959375" y2="-0.802865625" width="0.001" layer="94"/>
<wire x1="1.231959375" y1="-0.802865625" x2="1.2473375" y2="-0.76895625" width="0.001" layer="94"/>
<wire x1="1.2473375" y1="-0.76895625" x2="1.2609375" y2="-0.734384375" width="0.001" layer="94"/>
<wire x1="1.2609375" y1="-0.734384375" x2="1.27271875" y2="-0.699215625" width="0.001" layer="94"/>
<wire x1="1.27271875" y1="-0.699215625" x2="1.282628125" y2="-0.66350625" width="0.001" layer="94"/>
<wire x1="1.282628125" y1="-0.66350625" x2="1.2906375" y2="-0.627315625" width="0.001" layer="94"/>
<wire x1="1.2906375" y1="-0.627315625" x2="1.2934375" y2="-0.60765625" width="0.001" layer="94"/>
<wire x1="1.2934375" y1="-0.60765625" x2="1.295328125" y2="-0.583534375" width="0.001" layer="94"/>
<wire x1="1.295328125" y1="-0.583534375" x2="1.296309375" y2="-0.556465625" width="0.001" layer="94"/>
<wire x1="1.296309375" y1="-0.556465625" x2="1.296409375" y2="-0.527946875" width="0.001" layer="94"/>
<wire x1="1.296409375" y1="-0.527946875" x2="1.2956375" y2="-0.499465625" width="0.001" layer="94"/>
<wire x1="1.2956375" y1="-0.499465625" x2="1.29401875" y2="-0.472534375" width="0.001" layer="94"/>
<wire x1="1.29401875" y1="-0.472534375" x2="1.29156875" y2="-0.44865625" width="0.001" layer="94"/>
<wire x1="1.29156875" y1="-0.44865625" x2="1.288296875" y2="-0.429315625" width="0.001" layer="94"/>
<wire x1="1.288296875" y1="-0.429315625" x2="1.281796875" y2="-0.403346875" width="0.001" layer="94"/>
<wire x1="1.281796875" y1="-0.403346875" x2="1.27401875" y2="-0.378246875" width="0.001" layer="94"/>
<wire x1="1.27401875" y1="-0.378246875" x2="1.264996875" y2="-0.354046875" width="0.001" layer="94"/>
<wire x1="1.264996875" y1="-0.354046875" x2="1.25476875" y2="-0.330765625" width="0.001" layer="94"/>
<wire x1="1.25476875" y1="-0.330765625" x2="1.24336875" y2="-0.308415625" width="0.001" layer="94"/>
<wire x1="1.24336875" y1="-0.308415625" x2="1.2308375" y2="-0.287025" width="0.001" layer="94"/>
<wire x1="1.2308375" y1="-0.287025" x2="1.21721875" y2="-0.266596875" width="0.001" layer="94"/>
<wire x1="1.21721875" y1="-0.266596875" x2="1.202528125" y2="-0.247146875" width="0.001" layer="94"/>
<wire x1="1.202528125" y1="-0.247146875" x2="1.186828125" y2="-0.22870625" width="0.001" layer="94"/>
<wire x1="1.186828125" y1="-0.22870625" x2="1.170146875" y2="-0.211284375" width="0.001" layer="94"/>
<wire x1="1.170146875" y1="-0.211284375" x2="1.152509375" y2="-0.19490625" width="0.001" layer="94"/>
<wire x1="1.152509375" y1="-0.19490625" x2="1.13396875" y2="-0.179575" width="0.001" layer="94"/>
<wire x1="1.13396875" y1="-0.179575" x2="1.114559375" y2="-0.165315625" width="0.001" layer="94"/>
<wire x1="1.114559375" y1="-0.165315625" x2="1.094309375" y2="-0.152146875" width="0.001" layer="94"/>
<wire x1="1.094309375" y1="-0.152146875" x2="1.073259375" y2="-0.140084375" width="0.001" layer="94"/>
<wire x1="1.073259375" y1="-0.140084375" x2="1.051446875" y2="-0.129146875" width="0.001" layer="94"/>
<wire x1="1.051446875" y1="-0.129146875" x2="1.028909375" y2="-0.11935625" width="0.001" layer="94"/>
<wire x1="1.028909375" y1="-0.11935625" x2="1.0056875" y2="-0.110715625" width="0.001" layer="94"/>
<wire x1="1.0056875" y1="-0.110715625" x2="0.98181875" y2="-0.10325625" width="0.001" layer="94"/>
<wire x1="0.98181875" y1="-0.10325625" x2="0.957328125" y2="-0.096984375" width="0.001" layer="94"/>
<wire x1="0.957328125" y1="-0.096984375" x2="0.93226875" y2="-0.091925" width="0.001" layer="94"/>
<wire x1="0.93226875" y1="-0.091925" x2="0.90666875" y2="-0.088096875" width="0.001" layer="94"/>
<wire x1="0.90666875" y1="-0.088096875" x2="0.88056875" y2="-0.08550625" width="0.001" layer="94"/>
<wire x1="0.88056875" y1="-0.08550625" x2="0.853996875" y2="-0.084184375" width="0.001" layer="94"/>
<wire x1="0.853996875" y1="-0.084184375" x2="0.826996875" y2="-0.084134375" width="0.001" layer="94"/>
<wire x1="0.826996875" y1="-0.084134375" x2="0.799609375" y2="-0.085396875" width="0.001" layer="94"/>
<wire x1="0.799609375" y1="-0.085396875" x2="0.77186875" y2="-0.08795625" width="0.001" layer="94"/>
<wire x1="0.77186875" y1="-0.08795625" x2="0.743809375" y2="-0.09185625" width="0.001" layer="94"/>
<wire x1="0.743809375" y1="-0.09185625" x2="0.71546875" y2="-0.09710625" width="0.001" layer="94"/>
<wire x1="0.71546875" y1="-0.09710625" x2="0.6868875" y2="-0.103715625" width="0.001" layer="94"/>
<wire x1="0.6868875" y1="-0.103715625" x2="0.658096875" y2="-0.111715625" width="0.001" layer="94"/>
<wire x1="0.658096875" y1="-0.111715625" x2="0.629146875" y2="-0.121115625" width="0.001" layer="94"/>
<wire x1="0.629146875" y1="-0.121115625" x2="0.593028125" y2="-0.134725" width="0.001" layer="94"/>
<wire x1="0.593028125" y1="-0.134725" x2="0.557896875" y2="-0.150034375" width="0.001" layer="94"/>
<wire x1="0.557896875" y1="-0.150034375" x2="0.5237875" y2="-0.166965625" width="0.001" layer="94"/>
<wire x1="0.5237875" y1="-0.166965625" x2="0.490746875" y2="-0.185434375" width="0.001" layer="94"/>
<wire x1="0.490746875" y1="-0.185434375" x2="0.458828125" y2="-0.205365625" width="0.001" layer="94"/>
<wire x1="0.458828125" y1="-0.205365625" x2="0.428046875" y2="-0.226665625" width="0.001" layer="94"/>
<wire x1="0.428046875" y1="-0.226665625" x2="0.39846875" y2="-0.249265625" width="0.001" layer="94"/>
<wire x1="0.39846875" y1="-0.249265625" x2="0.37011875" y2="-0.273096875" width="0.001" layer="94"/>
<wire x1="0.37011875" y1="-0.273096875" x2="0.3430375" y2="-0.29805625" width="0.001" layer="94"/>
<wire x1="0.3430375" y1="-0.29805625" x2="0.3172875" y2="-0.324084375" width="0.001" layer="94"/>
<wire x1="0.3172875" y1="-0.324084375" x2="0.2928875" y2="-0.351084375" width="0.001" layer="94"/>
<wire x1="0.2928875" y1="-0.351084375" x2="0.269896875" y2="-0.378996875" width="0.001" layer="94"/>
<wire x1="0.269896875" y1="-0.378996875" x2="0.2483375" y2="-0.407725" width="0.001" layer="94"/>
<wire x1="0.2483375" y1="-0.407725" x2="0.22826875" y2="-0.437196875" width="0.001" layer="94"/>
<wire x1="0.22826875" y1="-0.437196875" x2="0.209728125" y2="-0.467325" width="0.001" layer="94"/>
<wire x1="0.209728125" y1="-0.467325" x2="0.192746875" y2="-0.498034375" width="0.001" layer="94"/>
<wire x1="0.192746875" y1="-0.498034375" x2="0.177378125" y2="-0.52925625" width="0.001" layer="94"/>
<wire x1="0.177378125" y1="-0.52925625" x2="0.16366875" y2="-0.560896875" width="0.001" layer="94"/>
<wire x1="0.16366875" y1="-0.560896875" x2="0.1516375" y2="-0.592875" width="0.001" layer="94"/>
<wire x1="0.1516375" y1="-0.592875" x2="0.141346875" y2="-0.625125" width="0.001" layer="94"/>
<wire x1="0.141346875" y1="-0.625125" x2="0.1328375" y2="-0.65755625" width="0.001" layer="94"/>
<wire x1="0.1328375" y1="-0.65755625" x2="0.1261375" y2="-0.690084375" width="0.001" layer="94"/>
<wire x1="0.1261375" y1="-0.690084375" x2="0.1212875" y2="-0.722646875" width="0.001" layer="94"/>
<wire x1="0.1212875" y1="-0.722646875" x2="0.118346875" y2="-0.75515625" width="0.001" layer="94"/>
<wire x1="0.118346875" y1="-0.75515625" x2="0.117346875" y2="-0.787525" width="0.001" layer="94"/>
<wire x1="0.117346875" y1="-0.787525" x2="0.1183375" y2="-0.819675" width="0.001" layer="94"/>
<wire x1="0.1183375" y1="-0.819675" x2="0.121346875" y2="-0.851546875" width="0.001" layer="94"/>
<wire x1="0.121346875" y1="-0.851546875" x2="0.12641875" y2="-0.883034375" width="0.001" layer="94"/>
<wire x1="0.12641875" y1="-0.883034375" x2="0.133609375" y2="-0.914065625" width="0.001" layer="94"/>
<wire x1="0.133609375" y1="-0.914065625" x2="0.1429375" y2="-0.944575" width="0.001" layer="94"/>
<wire x1="0.1429375" y1="-0.944575" x2="0.154459375" y2="-0.974465625" width="0.001" layer="94"/>
<wire x1="0.154459375" y1="-0.974465625" x2="0.16821875" y2="-1.00365625" width="0.001" layer="94"/>
<wire x1="0.16821875" y1="-1.00365625" x2="0.181359375" y2="-1.02720625" width="0.001" layer="94"/>
<wire x1="0.181359375" y1="-1.02720625" x2="0.195878125" y2="-1.049665625" width="0.001" layer="94"/>
<wire x1="0.195878125" y1="-1.049665625" x2="0.211728125" y2="-1.071015625" width="0.001" layer="94"/>
<wire x1="0.211728125" y1="-1.071015625" x2="0.228859375" y2="-1.091225" width="0.001" layer="94"/>
<wire x1="0.228859375" y1="-1.091225" x2="0.2472375" y2="-1.11025625" width="0.001" layer="94"/>
<wire x1="0.2472375" y1="-1.11025625" x2="0.266796875" y2="-1.128084375" width="0.001" layer="94"/>
<wire x1="0.266796875" y1="-1.128084375" x2="0.287496875" y2="-1.144675" width="0.001" layer="94"/>
<wire x1="0.287496875" y1="-1.144675" x2="0.309296875" y2="-1.159996875" width="0.001" layer="94"/>
<wire x1="0.309296875" y1="-1.159996875" x2="0.3321375" y2="-1.174025" width="0.001" layer="94"/>
<wire x1="0.3321375" y1="-1.174025" x2="0.3559875" y2="-1.186715625" width="0.001" layer="94"/>
<wire x1="0.3559875" y1="-1.186715625" x2="0.380778125" y2="-1.198046875" width="0.001" layer="94"/>
<wire x1="0.380778125" y1="-1.198046875" x2="0.4064875" y2="-1.207984375" width="0.001" layer="94"/>
<wire x1="0.4064875" y1="-1.207984375" x2="0.433046875" y2="-1.21650625" width="0.001" layer="94"/>
<wire x1="0.433046875" y1="-1.21650625" x2="0.46041875" y2="-1.223565625" width="0.001" layer="94"/>
<wire x1="0.46041875" y1="-1.223565625" x2="0.488559375" y2="-1.229146875" width="0.001" layer="94"/>
<wire x1="0.488559375" y1="-1.229146875" x2="0.517409375" y2="-1.23320625" width="0.001" layer="94"/>
<wire x1="0.517409375" y1="-1.23320625" x2="0.534759375" y2="-1.234625" width="0.001" layer="94"/>
<wire x1="0.534759375" y1="-1.234625" x2="0.554409375" y2="-1.23530625" width="0.001" layer="94"/>
<wire x1="0.554409375" y1="-1.23530625" x2="0.598246875" y2="-1.234525" width="0.001" layer="94"/>
<wire x1="0.598246875" y1="-1.234525" x2="0.6443375" y2="-1.231084375" width="0.001" layer="94"/>
<wire x1="0.6443375" y1="-1.231084375" x2="0.6667875" y2="-1.228434375" width="0.001" layer="94"/>
<wire x1="0.6667875" y1="-1.228434375" x2="0.688078125" y2="-1.225196875" width="0.001" layer="94"/>
<wire x1="-0.445771875" y1="0.45734375" x2="-0.40768125" y2="0.481434375" width="0.001" layer="94"/>
<wire x1="-0.40768125" y1="0.481434375" x2="-0.368503125" y2="0.50934375" width="0.001" layer="94"/>
<wire x1="-0.368503125" y1="0.50934375" x2="-0.32908125" y2="0.54029375" width="0.001" layer="94"/>
<wire x1="-0.32908125" y1="0.54029375" x2="-0.2903125" y2="0.573503125" width="0.001" layer="94"/>
<wire x1="-0.2903125" y1="0.573503125" x2="-0.2530625" y2="0.60819375" width="0.001" layer="94"/>
<wire x1="-0.2530625" y1="0.60819375" x2="-0.2182125" y2="0.643575" width="0.001" layer="94"/>
<wire x1="-0.2182125" y1="0.643575" x2="-0.186621875" y2="0.678884375" width="0.001" layer="94"/>
<wire x1="-0.186621875" y1="0.678884375" x2="-0.15918125" y2="0.713325" width="0.001" layer="94"/>
<wire x1="-0.15918125" y1="0.713325" x2="-0.130590625" y2="0.754234375" width="0.001" layer="94"/>
<wire x1="-0.130590625" y1="0.754234375" x2="-0.101521875" y2="0.799415625" width="0.001" layer="94"/>
<wire x1="-0.101521875" y1="0.799415625" x2="-0.078953125" y2="0.837603125" width="0.001" layer="94"/>
<wire x1="-0.078953125" y1="0.837603125" x2="-0.07228125" y2="0.85054375" width="0.001" layer="94"/>
<wire x1="-0.07228125" y1="0.85054375" x2="-0.069871875" y2="0.857503125" width="0.001" layer="94"/>
<wire x1="-0.069871875" y1="0.857503125" x2="-0.07078125" y2="0.859215625" width="0.001" layer="94"/>
<wire x1="-0.07078125" y1="0.859215625" x2="-0.073503125" y2="0.86029375" width="0.001" layer="94"/>
<wire x1="-0.073503125" y1="0.86029375" x2="-0.084371875" y2="0.860575" width="0.001" layer="94"/>
<wire x1="-0.084371875" y1="0.860575" x2="-0.10248125" y2="0.858334375" width="0.001" layer="94"/>
<wire x1="-0.10248125" y1="0.858334375" x2="-0.12783125" y2="0.853565625" width="0.001" layer="94"/>
<wire x1="-0.12783125" y1="0.853565625" x2="-0.139990625" y2="0.852375" width="0.001" layer="94"/>
<wire x1="-0.139990625" y1="0.852375" x2="-0.16233125" y2="0.851365625" width="0.001" layer="94"/>
<wire x1="-0.16233125" y1="0.851365625" x2="-0.224871875" y2="0.85029375" width="0.001" layer="94"/>
<wire x1="-0.224871875" y1="0.85029375" x2="-0.274321875" y2="0.85089375" width="0.001" layer="94"/>
<wire x1="-0.274321875" y1="0.85089375" x2="-0.316990625" y2="0.85324375" width="0.001" layer="94"/>
<wire x1="-0.316990625" y1="0.85324375" x2="-0.356721875" y2="0.857675" width="0.001" layer="94"/>
<wire x1="-0.356721875" y1="0.857675" x2="-0.397371875" y2="0.864484375" width="0.001" layer="94"/>
<wire x1="-0.397371875" y1="0.864484375" x2="-0.4021625" y2="0.865215625" width="0.001" layer="94"/>
<wire x1="-0.4021625" y1="0.865215625" x2="-0.406253125" y2="0.865175" width="0.001" layer="94"/>
<wire x1="-0.406253125" y1="0.865175" x2="-0.41003125" y2="0.864053125" width="0.001" layer="94"/>
<wire x1="-0.41003125" y1="0.864053125" x2="-0.4138625" y2="0.86154375" width="0.001" layer="94"/>
<wire x1="-0.4138625" y1="0.86154375" x2="-0.418140625" y2="0.857334375" width="0.001" layer="94"/>
<wire x1="-0.418140625" y1="0.857334375" x2="-0.42323125" y2="0.85109375" width="0.001" layer="94"/>
<wire x1="-0.42323125" y1="0.85109375" x2="-0.437371875" y2="0.831325" width="0.001" layer="94"/>
<wire x1="-0.437371875" y1="0.831325" x2="-0.4526125" y2="0.810915625" width="0.001" layer="94"/>
<wire x1="-0.4526125" y1="0.810915625" x2="-0.471421875" y2="0.78819375" width="0.001" layer="94"/>
<wire x1="-0.471421875" y1="0.78819375" x2="-0.492803125" y2="0.76419375" width="0.001" layer="94"/>
<wire x1="-0.492803125" y1="0.76419375" x2="-0.515740625" y2="0.739953125" width="0.001" layer="94"/>
<wire x1="-0.515740625" y1="0.739953125" x2="-0.53923125" y2="0.71649375" width="0.001" layer="94"/>
<wire x1="-0.53923125" y1="0.71649375" x2="-0.562271875" y2="0.694865625" width="0.001" layer="94"/>
<wire x1="-0.562271875" y1="0.694865625" x2="-0.5838625" y2="0.676084375" width="0.001" layer="94"/>
<wire x1="-0.5838625" y1="0.676084375" x2="-0.60298125" y2="0.661184375" width="0.001" layer="94"/>
<wire x1="-0.60298125" y1="0.661184375" x2="-0.621821875" y2="0.648425" width="0.001" layer="94"/>
<wire x1="-0.621821875" y1="0.648425" x2="-0.644021875" y2="0.634575" width="0.001" layer="94"/>
<wire x1="-0.644021875" y1="0.634575" x2="-0.666790625" y2="0.621303125" width="0.001" layer="94"/>
<wire x1="-0.666790625" y1="0.621303125" x2="-0.687371875" y2="0.610284375" width="0.001" layer="94"/>
<wire x1="-0.687371875" y1="0.610284375" x2="-0.722090625" y2="0.592053125" width="0.001" layer="94"/>
<wire x1="-0.722090625" y1="0.592053125" x2="-0.742371875" y2="0.579975" width="0.001" layer="94"/>
<wire x1="-0.742371875" y1="0.579975" x2="-0.747871875" y2="0.575584375" width="0.001" layer="94"/>
<wire x1="-0.747871875" y1="0.575584375" x2="-0.749490625" y2="0.573275" width="0.001" layer="94"/>
<wire x1="-0.749490625" y1="0.573275" x2="-0.7470625" y2="0.572353125" width="0.001" layer="94"/>
<wire x1="-0.7470625" y1="0.572353125" x2="-0.740403125" y2="0.572125" width="0.001" layer="94"/>
<wire x1="-0.740403125" y1="0.572125" x2="-0.727940625" y2="0.574253125" width="0.001" layer="94"/>
<wire x1="-0.727940625" y1="0.574253125" x2="-0.702840625" y2="0.580175" width="0.001" layer="94"/>
<wire x1="-0.702840625" y1="0.580175" x2="-0.629340625" y2="0.59974375" width="0.001" layer="94"/>
<wire x1="-0.629340625" y1="0.59974375" x2="-0.5614125" y2="0.618184375" width="0.001" layer="94"/>
<wire x1="-0.5614125" y1="0.618184375" x2="-0.51538125" y2="0.629134375" width="0.001" layer="94"/>
<wire x1="-0.51538125" y1="0.629134375" x2="-0.4994125" y2="0.632034375" width="0.001" layer="94"/>
<wire x1="-0.4994125" y1="0.632034375" x2="-0.487540625" y2="0.633334375" width="0.001" layer="94"/>
<wire x1="-0.487540625" y1="0.633334375" x2="-0.479290625" y2="0.633153125" width="0.001" layer="94"/>
<wire x1="-0.479290625" y1="0.633153125" x2="-0.4742125" y2="0.631565625" width="0.001" layer="94"/>
<wire x1="-0.4742125" y1="0.631565625" x2="-0.471571875" y2="0.628984375" width="0.001" layer="94"/>
<wire x1="-0.471571875" y1="0.628984375" x2="-0.47133125" y2="0.625675" width="0.001" layer="94"/>
<wire x1="-0.47133125" y1="0.625675" x2="-0.473853125" y2="0.621315625" width="0.001" layer="94"/>
<wire x1="-0.473853125" y1="0.621315625" x2="-0.4794625" y2="0.615603125" width="0.001" layer="94"/>
<wire x1="-0.4794625" y1="0.615603125" x2="-0.501321875" y2="0.598934375" width="0.001" layer="94"/>
<wire x1="-0.501321875" y1="0.598934375" x2="-0.539640625" y2="0.573225" width="0.001" layer="94"/>
<wire x1="-0.539640625" y1="0.573225" x2="-0.58703125" y2="0.541015625" width="0.001" layer="94"/>
<wire x1="-0.58703125" y1="0.541015625" x2="-0.603440625" y2="0.528725" width="0.001" layer="94"/>
<wire x1="-0.603440625" y1="0.528725" x2="-0.61513125" y2="0.518865625" width="0.001" layer="94"/>
<wire x1="-0.61513125" y1="0.518865625" x2="-0.62218125" y2="0.51134375" width="0.001" layer="94"/>
<wire x1="-0.62218125" y1="0.51134375" x2="-0.623990625" y2="0.508434375" width="0.001" layer="94"/>
<wire x1="-0.623990625" y1="0.508434375" x2="-0.62468125" y2="0.506075" width="0.001" layer="94"/>
<wire x1="-0.62468125" y1="0.506075" x2="-0.6242625" y2="0.504265625" width="0.001" layer="94"/>
<wire x1="-0.6242625" y1="0.504265625" x2="-0.62273125" y2="0.502984375" width="0.001" layer="94"/>
<wire x1="-0.62273125" y1="0.502984375" x2="-0.616421875" y2="0.501965625" width="0.001" layer="94"/>
<wire x1="-0.616421875" y1="0.501965625" x2="-0.607990625" y2="0.503515625" width="0.001" layer="94"/>
<wire x1="-0.607990625" y1="0.503515625" x2="-0.591271875" y2="0.507715625" width="0.001" layer="94"/>
<wire x1="-0.591271875" y1="0.507715625" x2="-0.542671875" y2="0.521515625" width="0.001" layer="94"/>
<wire x1="-0.542671875" y1="0.521515625" x2="-0.495053125" y2="0.535275" width="0.001" layer="94"/>
<wire x1="-0.495053125" y1="0.535275" x2="-0.463990625" y2="0.542784375" width="0.001" layer="94"/>
<wire x1="-0.463990625" y1="0.542784375" x2="-0.45338125" y2="0.544453125" width="0.001" layer="94"/>
<wire x1="-0.45338125" y1="0.544453125" x2="-0.44538125" y2="0.54484375" width="0.001" layer="94"/>
<wire x1="-0.44538125" y1="0.54484375" x2="-0.43948125" y2="0.544084375" width="0.001" layer="94"/>
<wire x1="-0.43948125" y1="0.544084375" x2="-0.435153125" y2="0.54224375" width="0.001" layer="94"/>
<wire x1="-0.435153125" y1="0.54224375" x2="-0.4319625" y2="0.539034375" width="0.001" layer="94"/>
<wire x1="-0.4319625" y1="0.539034375" x2="-0.430953125" y2="0.534653125" width="0.001" layer="94"/>
<wire x1="-0.430953125" y1="0.534653125" x2="-0.432240625" y2="0.52889375" width="0.001" layer="94"/>
<wire x1="-0.432240625" y1="0.52889375" x2="-0.435940625" y2="0.521575" width="0.001" layer="94"/>
<wire x1="-0.435940625" y1="0.521575" x2="-0.442171875" y2="0.51249375" width="0.001" layer="94"/>
<wire x1="-0.442171875" y1="0.51249375" x2="-0.451053125" y2="0.501465625" width="0.001" layer="94"/>
<wire x1="-0.451053125" y1="0.501465625" x2="-0.4772125" y2="0.472775" width="0.001" layer="94"/>
<wire x1="-0.4772125" y1="0.472775" x2="-0.5038125" y2="0.443834375" width="0.001" layer="94"/>
<wire x1="-0.5038125" y1="0.443834375" x2="-0.511903125" y2="0.434184375" width="0.001" layer="94"/>
<wire x1="-0.511903125" y1="0.434184375" x2="-0.514871875" y2="0.429625" width="0.001" layer="94"/>
<wire x1="-0.514871875" y1="0.429625" x2="-0.514540625" y2="0.428415625" width="0.001" layer="94"/>
<wire x1="-0.514540625" y1="0.428415625" x2="-0.513571875" y2="0.42764375" width="0.001" layer="94"/>
<wire x1="-0.513571875" y1="0.42764375" x2="-0.509790625" y2="0.427375" width="0.001" layer="94"/>
<wire x1="-0.509790625" y1="0.427375" x2="-0.503721875" y2="0.42874375" width="0.001" layer="94"/>
<wire x1="-0.503721875" y1="0.42874375" x2="-0.495553125" y2="0.431665625" width="0.001" layer="94"/>
<wire x1="-0.495553125" y1="0.431665625" x2="-0.473703125" y2="0.441853125" width="0.001" layer="94"/>
<wire x1="-0.473703125" y1="0.441853125" x2="-0.445771875" y2="0.45734375" width="0.001" layer="94"/>
<wire x1="0.0774375" y1="1.214875" x2="0.080509375" y2="1.220425" width="0.001" layer="94"/>
<wire x1="0.080509375" y1="1.220425" x2="0.08261875" y2="1.226025" width="0.001" layer="94"/>
<wire x1="0.08261875" y1="1.226025" x2="0.0835875" y2="1.231003125" width="0.001" layer="94"/>
<wire x1="0.0835875" y1="1.231003125" x2="0.083228125" y2="1.234715625" width="0.001" layer="94"/>
<wire x1="0.083228125" y1="1.234715625" x2="0.082609375" y2="1.237665625" width="0.001" layer="94"/>
<wire x1="0.082609375" y1="1.237665625" x2="0.0828875" y2="1.240565625" width="0.001" layer="94"/>
<wire x1="0.0828875" y1="1.240565625" x2="0.0839875" y2="1.243084375" width="0.001" layer="94"/>
<wire x1="0.0839875" y1="1.243084375" x2="0.08581875" y2="1.24489375" width="0.001" layer="94"/>
<wire x1="0.08581875" y1="1.24489375" x2="0.087678125" y2="1.247103125" width="0.001" layer="94"/>
<wire x1="0.087678125" y1="1.247103125" x2="0.088846875" y2="1.250725" width="0.001" layer="94"/>
<wire x1="0.088846875" y1="1.250725" x2="0.089246875" y2="1.25524375" width="0.001" layer="94"/>
<wire x1="0.089246875" y1="1.25524375" x2="0.08876875" y2="1.260175" width="0.001" layer="94"/>
<wire x1="0.08876875" y1="1.260175" x2="0.0881375" y2="1.269284375" width="0.001" layer="94"/>
<wire x1="0.0881375" y1="1.269284375" x2="0.08871875" y2="1.272534375" width="0.001" layer="94"/>
<wire x1="0.08871875" y1="1.272534375" x2="0.089859375" y2="1.27429375" width="0.001" layer="94"/>
<wire x1="0.089859375" y1="1.27429375" x2="0.0910875" y2="1.276715625" width="0.001" layer="94"/>
<wire x1="0.0910875" y1="1.276715625" x2="0.0919375" y2="1.281734375" width="0.001" layer="94"/>
<wire x1="0.0919375" y1="1.281734375" x2="0.092209375" y2="1.296675" width="0.001" layer="94"/>
<wire x1="0.092209375" y1="1.296675" x2="0.092159375" y2="1.304553125" width="0.001" layer="94"/>
<wire x1="0.092159375" y1="1.304553125" x2="0.0927875" y2="1.311003125" width="0.001" layer="94"/>
<wire x1="0.0927875" y1="1.311003125" x2="0.0939875" y2="1.315365625" width="0.001" layer="94"/>
<wire x1="0.0939875" y1="1.315365625" x2="0.095646875" y2="1.316965625" width="0.001" layer="94"/>
<wire x1="0.095646875" y1="1.316965625" x2="0.097346875" y2="1.318465625" width="0.001" layer="94"/>
<wire x1="0.097346875" y1="1.318465625" x2="0.0986375" y2="1.322634375" width="0.001" layer="94"/>
<wire x1="0.0986375" y1="1.322634375" x2="0.09991875" y2="1.337125" width="0.001" layer="94"/>
<wire x1="0.09991875" y1="1.337125" x2="0.099409375" y2="1.356684375" width="0.001" layer="94"/>
<wire x1="0.099409375" y1="1.356684375" x2="0.097028125" y2="1.377565625" width="0.001" layer="94"/>
<wire x1="0.097028125" y1="1.377565625" x2="0.094028125" y2="1.395653125" width="0.001" layer="94"/>
<wire x1="0.094028125" y1="1.395653125" x2="0.040828125" y2="1.37184375" width="0.001" layer="94"/>
<wire x1="0.040828125" y1="1.37184375" x2="-0.00623125" y2="1.352384375" width="0.001" layer="94"/>
<wire x1="-0.00623125" y1="1.352384375" x2="-0.0561125" y2="1.33479375" width="0.001" layer="94"/>
<wire x1="-0.0561125" y1="1.33479375" x2="-0.1076625" y2="1.31944375" width="0.001" layer="94"/>
<wire x1="-0.1076625" y1="1.31944375" x2="-0.159740625" y2="1.306703125" width="0.001" layer="94"/>
<wire x1="-0.159740625" y1="1.306703125" x2="-0.193740625" y2="1.298875" width="0.001" layer="94"/>
<wire x1="-0.193740625" y1="1.298875" x2="-0.2089625" y2="1.29454375" width="0.001" layer="94"/>
<wire x1="-0.2089625" y1="1.29454375" x2="-0.208103125" y2="1.292825" width="0.001" layer="94"/>
<wire x1="-0.208103125" y1="1.292825" x2="-0.20438125" y2="1.289515625" width="0.001" layer="94"/>
<wire x1="-0.20438125" y1="1.289515625" x2="-0.1907625" y2="1.280034375" width="0.001" layer="94"/>
<wire x1="-0.1907625" y1="1.280034375" x2="-0.175721875" y2="1.269665625" width="0.001" layer="94"/>
<wire x1="-0.175721875" y1="1.269665625" x2="-0.167721875" y2="1.262534375" width="0.001" layer="94"/>
<wire x1="-0.167721875" y1="1.262534375" x2="-0.161990625" y2="1.258275" width="0.001" layer="94"/>
<wire x1="-0.161990625" y1="1.258275" x2="-0.149821875" y2="1.25234375" width="0.001" layer="94"/>
<wire x1="-0.149821875" y1="1.25234375" x2="-0.11223125" y2="1.237625" width="0.001" layer="94"/>
<wire x1="-0.11223125" y1="1.237625" x2="-0.0671125" y2="1.222725" width="0.001" layer="94"/>
<wire x1="-0.0671125" y1="1.222725" x2="-0.04553125" y2="1.216565625" width="0.001" layer="94"/>
<wire x1="-0.04553125" y1="1.216565625" x2="-0.026621875" y2="1.21199375" width="0.001" layer="94"/>
<wire x1="-0.026621875" y1="1.21199375" x2="0.01216875" y2="1.205303125" width="0.001" layer="94"/>
<wire x1="0.01216875" y1="1.205303125" x2="0.0298375" y2="1.20319375" width="0.001" layer="94"/>
<wire x1="0.0298375" y1="1.20319375" x2="0.043309375" y2="1.202315625" width="0.001" layer="94"/>
<wire x1="0.043309375" y1="1.202315625" x2="0.057009375" y2="1.202575" width="0.001" layer="94"/>
<wire x1="0.057009375" y1="1.202575" x2="0.06596875" y2="1.204284375" width="0.001" layer="94"/>
<wire x1="0.06596875" y1="1.204284375" x2="0.0692875" y2="1.20589375" width="0.001" layer="94"/>
<wire x1="0.0692875" y1="1.20589375" x2="0.0721375" y2="1.20814375" width="0.001" layer="94"/>
<wire x1="0.0721375" y1="1.20814375" x2="0.0774375" y2="1.214875" width="0.001" layer="94"/>
<wire x1="-1.143253125" y1="1.384165625" x2="-1.1589125" y2="1.390025" width="0.001" layer="94"/>
<wire x1="-1.1589125" y1="1.390025" x2="-1.17398125" y2="1.397575" width="0.001" layer="94"/>
<wire x1="-1.17398125" y1="1.397575" x2="-1.188340625" y2="1.406725" width="0.001" layer="94"/>
<wire x1="-1.188340625" y1="1.406725" x2="-1.201903125" y2="1.417375" width="0.001" layer="94"/>
<wire x1="-1.201903125" y1="1.417375" x2="-1.214521875" y2="1.429425" width="0.001" layer="94"/>
<wire x1="-1.214521875" y1="1.429425" x2="-1.2261125" y2="1.442775" width="0.001" layer="94"/>
<wire x1="-1.2261125" y1="1.442775" x2="-1.236540625" y2="1.457325" width="0.001" layer="94"/>
<wire x1="-1.236540625" y1="1.457325" x2="-1.2457125" y2="1.47299375" width="0.001" layer="94"/>
<wire x1="-1.2457125" y1="1.47299375" x2="-1.251071875" y2="1.484965625" width="0.001" layer="94"/>
<wire x1="-1.251071875" y1="1.484965625" x2="-1.25458125" y2="1.497475" width="0.001" layer="94"/>
<wire x1="-1.25458125" y1="1.497475" x2="-1.256521875" y2="1.51194375" width="0.001" layer="94"/>
<wire x1="-1.256521875" y1="1.51194375" x2="-1.25718125" y2="1.529815625" width="0.001" layer="94"/>
<wire x1="-1.25718125" y1="1.529815625" x2="-1.256790625" y2="1.548965625" width="0.001" layer="94"/>
<wire x1="-1.256790625" y1="1.548965625" x2="-1.2548625" y2="1.562684375" width="0.001" layer="94"/>
<wire x1="-1.2548625" y1="1.562684375" x2="-1.250621875" y2="1.574134375" width="0.001" layer="94"/>
<wire x1="-1.250621875" y1="1.574134375" x2="-1.243290625" y2="1.586453125" width="0.001" layer="94"/>
<wire x1="-1.243290625" y1="1.586453125" x2="-1.2376125" y2="1.59404375" width="0.001" layer="94"/>
<wire x1="-1.2376125" y1="1.59404375" x2="-1.23113125" y2="1.60089375" width="0.001" layer="94"/>
<wire x1="-1.23113125" y1="1.60089375" x2="-1.2239125" y2="1.60699375" width="0.001" layer="94"/>
<wire x1="-1.2239125" y1="1.60699375" x2="-1.21603125" y2="1.612325" width="0.001" layer="94"/>
<wire x1="-1.21603125" y1="1.612325" x2="-1.20753125" y2="1.616884375" width="0.001" layer="94"/>
<wire x1="-1.20753125" y1="1.616884375" x2="-1.198503125" y2="1.620653125" width="0.001" layer="94"/>
<wire x1="-1.198503125" y1="1.620653125" x2="-1.18898125" y2="1.623634375" width="0.001" layer="94"/>
<wire x1="-1.18898125" y1="1.623634375" x2="-1.179053125" y2="1.625803125" width="0.001" layer="94"/>
<wire x1="-1.179053125" y1="1.625803125" x2="-1.168771875" y2="1.627153125" width="0.001" layer="94"/>
<wire x1="-1.168771875" y1="1.627153125" x2="-1.158203125" y2="1.627684375" width="0.001" layer="94"/>
<wire x1="-1.158203125" y1="1.627684375" x2="-1.147403125" y2="1.627365625" width="0.001" layer="94"/>
<wire x1="-1.147403125" y1="1.627365625" x2="-1.136453125" y2="1.626184375" width="0.001" layer="94"/>
<wire x1="-1.136453125" y1="1.626184375" x2="-1.1254125" y2="1.624153125" width="0.001" layer="94"/>
<wire x1="-1.1254125" y1="1.624153125" x2="-1.11433125" y2="1.62124375" width="0.001" layer="94"/>
<wire x1="-1.11433125" y1="1.62124375" x2="-1.103290625" y2="1.61744375" width="0.001" layer="94"/>
<wire x1="-1.103290625" y1="1.61744375" x2="-1.092353125" y2="1.612753125" width="0.001" layer="94"/>
<wire x1="-1.092353125" y1="1.612753125" x2="-1.078290625" y2="1.605034375" width="0.001" layer="94"/>
<wire x1="-1.078290625" y1="1.605034375" x2="-1.064290625" y2="1.595353125" width="0.001" layer="94"/>
<wire x1="-1.064290625" y1="1.595353125" x2="-1.0507125" y2="1.584103125" width="0.001" layer="94"/>
<wire x1="-1.0507125" y1="1.584103125" x2="-1.0379625" y2="1.571715625" width="0.001" layer="94"/>
<wire x1="-1.0379625" y1="1.571715625" x2="-1.0264125" y2="1.558575" width="0.001" layer="94"/>
<wire x1="-1.0264125" y1="1.558575" x2="-1.0164625" y2="1.54509375" width="0.001" layer="94"/>
<wire x1="-1.0164625" y1="1.54509375" x2="-1.008490625" y2="1.531684375" width="0.001" layer="94"/>
<wire x1="-1.008490625" y1="1.531684375" x2="-1.002903125" y2="1.518753125" width="0.001" layer="94"/>
<wire x1="-1.002903125" y1="1.518753125" x2="-0.99818125" y2="1.501553125" width="0.001" layer="94"/>
<wire x1="-0.99818125" y1="1.501553125" x2="-0.99583125" y2="1.484984375" width="0.001" layer="94"/>
<wire x1="-0.99583125" y1="1.484984375" x2="-0.995740625" y2="1.46914375" width="0.001" layer="94"/>
<wire x1="-0.995740625" y1="1.46914375" x2="-0.997771875" y2="1.454153125" width="0.001" layer="94"/>
<wire x1="-0.997771875" y1="1.454153125" x2="-1.0018125" y2="1.44014375" width="0.001" layer="94"/>
<wire x1="-1.0018125" y1="1.44014375" x2="-1.007740625" y2="1.427225" width="0.001" layer="94"/>
<wire x1="-1.007740625" y1="1.427225" x2="-1.0154125" y2="1.415515625" width="0.001" layer="94"/>
<wire x1="-1.0154125" y1="1.415515625" x2="-1.02473125" y2="1.405134375" width="0.001" layer="94"/>
<wire x1="-1.02473125" y1="1.405134375" x2="-1.035553125" y2="1.396203125" width="0.001" layer="94"/>
<wire x1="-1.035553125" y1="1.396203125" x2="-1.047771875" y2="1.388834375" width="0.001" layer="94"/>
<wire x1="-1.047771875" y1="1.388834375" x2="-1.061240625" y2="1.383153125" width="0.001" layer="94"/>
<wire x1="-1.061240625" y1="1.383153125" x2="-1.0758625" y2="1.379275" width="0.001" layer="94"/>
<wire x1="-1.0758625" y1="1.379275" x2="-1.091490625" y2="1.377315625" width="0.001" layer="94"/>
<wire x1="-1.091490625" y1="1.377315625" x2="-1.1080125" y2="1.377403125" width="0.001" layer="94"/>
<wire x1="-1.1080125" y1="1.377403125" x2="-1.1253125" y2="1.37964375" width="0.001" layer="94"/>
<wire x1="-1.1253125" y1="1.37964375" x2="-1.143253125" y2="1.384165625" width="0.001" layer="94"/>
<wire x1="-2.742371875" y1="1.542634375" x2="-2.783740625" y2="1.54749375" width="0.001" layer="94"/>
<wire x1="-2.783740625" y1="1.54749375" x2="-2.8253125" y2="1.554184375" width="0.001" layer="94"/>
<wire x1="-2.8253125" y1="1.554184375" x2="-2.8669125" y2="1.562625" width="0.001" layer="94"/>
<wire x1="-2.8669125" y1="1.562625" x2="-2.908390625" y2="1.572765625" width="0.001" layer="94"/>
<wire x1="-2.908390625" y1="1.572765625" x2="-2.9496125" y2="1.584525" width="0.001" layer="94"/>
<wire x1="-2.9496125" y1="1.584525" x2="-2.990403125" y2="1.59784375" width="0.001" layer="94"/>
<wire x1="-2.990403125" y1="1.59784375" x2="-3.030621875" y2="1.612665625" width="0.001" layer="94"/>
<wire x1="-3.030621875" y1="1.612665625" x2="-3.0701125" y2="1.628903125" width="0.001" layer="94"/>
<wire x1="-3.0701125" y1="1.628903125" x2="-3.10873125" y2="1.646503125" width="0.001" layer="94"/>
<wire x1="-3.10873125" y1="1.646503125" x2="-3.1463125" y2="1.665403125" width="0.001" layer="94"/>
<wire x1="-3.1463125" y1="1.665403125" x2="-3.1827125" y2="1.685525" width="0.001" layer="94"/>
<wire x1="-3.1827125" y1="1.685525" x2="-3.217771875" y2="1.706815625" width="0.001" layer="94"/>
<wire x1="-3.217771875" y1="1.706815625" x2="-3.251340625" y2="1.729203125" width="0.001" layer="94"/>
<wire x1="-3.251340625" y1="1.729203125" x2="-3.283271875" y2="1.752615625" width="0.001" layer="94"/>
<wire x1="-3.283271875" y1="1.752615625" x2="-3.313403125" y2="1.77699375" width="0.001" layer="94"/>
<wire x1="-3.313403125" y1="1.77699375" x2="-3.34158125" y2="1.802275" width="0.001" layer="94"/>
<wire x1="-3.34158125" y1="1.802275" x2="-3.3616625" y2="1.822025" width="0.001" layer="94"/>
<wire x1="-3.3616625" y1="1.822025" x2="-3.38083125" y2="1.842403125" width="0.001" layer="94"/>
<wire x1="-3.38083125" y1="1.842403125" x2="-3.399103125" y2="1.863415625" width="0.001" layer="94"/>
<wire x1="-3.399103125" y1="1.863415625" x2="-3.416440625" y2="1.885015625" width="0.001" layer="94"/>
<wire x1="-3.416440625" y1="1.885015625" x2="-3.432853125" y2="1.907184375" width="0.001" layer="94"/>
<wire x1="-3.432853125" y1="1.907184375" x2="-3.448303125" y2="1.929884375" width="0.001" layer="94"/>
<wire x1="-3.448303125" y1="1.929884375" x2="-3.462790625" y2="1.953115625" width="0.001" layer="94"/>
<wire x1="-3.462790625" y1="1.953115625" x2="-3.476303125" y2="1.976825" width="0.001" layer="94"/>
<wire x1="-3.476303125" y1="1.976825" x2="-3.488821875" y2="2.001003125" width="0.001" layer="94"/>
<wire x1="-3.488821875" y1="2.001003125" x2="-3.50033125" y2="2.025625" width="0.001" layer="94"/>
<wire x1="-3.50033125" y1="2.025625" x2="-3.510821875" y2="2.050653125" width="0.001" layer="94"/>
<wire x1="-3.510821875" y1="2.050653125" x2="-3.52028125" y2="2.076084375" width="0.001" layer="94"/>
<wire x1="-3.52028125" y1="2.076084375" x2="-3.528690625" y2="2.101865625" width="0.001" layer="94"/>
<wire x1="-3.528690625" y1="2.101865625" x2="-3.536040625" y2="2.12799375" width="0.001" layer="94"/>
<wire x1="-3.536040625" y1="2.12799375" x2="-3.5423125" y2="2.154434375" width="0.001" layer="94"/>
<wire x1="-3.5423125" y1="2.154434375" x2="-3.547490625" y2="2.181153125" width="0.001" layer="94"/>
<wire x1="-3.547490625" y1="2.181153125" x2="-3.550990625" y2="2.206384375" width="0.001" layer="94"/>
<wire x1="-3.550990625" y1="2.206384375" x2="-3.5536625" y2="2.235475" width="0.001" layer="94"/>
<wire x1="-3.5536625" y1="2.235475" x2="-3.5554625" y2="2.266565625" width="0.001" layer="94"/>
<wire x1="-3.5554625" y1="2.266565625" x2="-3.5563625" y2="2.297734375" width="0.001" layer="94"/>
<wire x1="-3.5563625" y1="2.297734375" x2="-3.556353125" y2="2.327115625" width="0.001" layer="94"/>
<wire x1="-3.556353125" y1="2.327115625" x2="-3.55538125" y2="2.352815625" width="0.001" layer="94"/>
<wire x1="-3.55538125" y1="2.352815625" x2="-3.553440625" y2="2.372925" width="0.001" layer="94"/>
<wire x1="-3.553440625" y1="2.372925" x2="-3.550503125" y2="2.385575" width="0.001" layer="94"/>
<wire x1="-3.550503125" y1="2.385575" x2="-3.547553125" y2="2.391853125" width="0.001" layer="94"/>
<wire x1="-3.547553125" y1="2.391853125" x2="-3.544371875" y2="2.39699375" width="0.001" layer="94"/>
<wire x1="-3.544371875" y1="2.39699375" x2="-3.540903125" y2="2.400965625" width="0.001" layer="94"/>
<wire x1="-3.540903125" y1="2.400965625" x2="-3.537090625" y2="2.403765625" width="0.001" layer="94"/>
<wire x1="-3.537090625" y1="2.403765625" x2="-3.53288125" y2="2.40534375" width="0.001" layer="94"/>
<wire x1="-3.53288125" y1="2.40534375" x2="-3.528240625" y2="2.40569375" width="0.001" layer="94"/>
<wire x1="-3.528240625" y1="2.40569375" x2="-3.523090625" y2="2.404784375" width="0.001" layer="94"/>
<wire x1="-3.523090625" y1="2.404784375" x2="-3.517403125" y2="2.402584375" width="0.001" layer="94"/>
<wire x1="-3.517403125" y1="2.402584375" x2="-3.511103125" y2="2.399084375" width="0.001" layer="94"/>
<wire x1="-3.511103125" y1="2.399084375" x2="-3.5041625" y2="2.39424375" width="0.001" layer="94"/>
<wire x1="-3.5041625" y1="2.39424375" x2="-3.488090625" y2="2.380453125" width="0.001" layer="94"/>
<wire x1="-3.488090625" y1="2.380453125" x2="-3.46878125" y2="2.36104375" width="0.001" layer="94"/>
<wire x1="-3.46878125" y1="2.36104375" x2="-3.445821875" y2="2.335784375" width="0.001" layer="94"/>
<wire x1="-3.445821875" y1="2.335784375" x2="-3.402371875" y2="2.28829375" width="0.001" layer="94"/>
<wire x1="-3.402371875" y1="2.28829375" x2="-3.36063125" y2="2.245875" width="0.001" layer="94"/>
<wire x1="-3.36063125" y1="2.245875" x2="-3.3230125" y2="2.21089375" width="0.001" layer="94"/>
<wire x1="-3.3230125" y1="2.21089375" x2="-3.3065125" y2="2.196934375" width="0.001" layer="94"/>
<wire x1="-3.3065125" y1="2.196934375" x2="-3.2919625" y2="2.185725" width="0.001" layer="94"/>
<wire x1="-3.2919625" y1="2.185725" x2="-3.2677125" y2="2.16979375" width="0.001" layer="94"/>
<wire x1="-3.2677125" y1="2.16979375" x2="-3.24058125" y2="2.154765625" width="0.001" layer="94"/>
<wire x1="-3.24058125" y1="2.154765625" x2="-3.21128125" y2="2.140903125" width="0.001" layer="94"/>
<wire x1="-3.21128125" y1="2.140903125" x2="-3.180540625" y2="2.12849375" width="0.001" layer="94"/>
<wire x1="-3.180540625" y1="2.12849375" x2="-3.149071875" y2="2.11779375" width="0.001" layer="94"/>
<wire x1="-3.149071875" y1="2.11779375" x2="-3.117590625" y2="2.109065625" width="0.001" layer="94"/>
<wire x1="-3.117590625" y1="2.109065625" x2="-3.086821875" y2="2.102575" width="0.001" layer="94"/>
<wire x1="-3.086821875" y1="2.102575" x2="-3.057471875" y2="2.098584375" width="0.001" layer="94"/>
<wire x1="-3.057471875" y1="2.098584375" x2="-3.02258125" y2="2.09534375" width="0.001" layer="94"/>
<wire x1="-3.02258125" y1="2.09534375" x2="-3.038971875" y2="2.114925" width="0.001" layer="94"/>
<wire x1="-3.038971875" y1="2.114925" x2="-3.091903125" y2="2.179665625" width="0.001" layer="94"/>
<wire x1="-3.091903125" y1="2.179665625" x2="-3.112303125" y2="2.20629375" width="0.001" layer="94"/>
<wire x1="-3.112303125" y1="2.20629375" x2="-3.129790625" y2="2.23074375" width="0.001" layer="94"/>
<wire x1="-3.129790625" y1="2.23074375" x2="-3.145203125" y2="2.25424375" width="0.001" layer="94"/>
<wire x1="-3.145203125" y1="2.25424375" x2="-3.159403125" y2="2.278015625" width="0.001" layer="94"/>
<wire x1="-3.159403125" y1="2.278015625" x2="-3.173221875" y2="2.30329375" width="0.001" layer="94"/>
<wire x1="-3.173221875" y1="2.30329375" x2="-3.187503125" y2="2.331315625" width="0.001" layer="94"/>
<wire x1="-3.187503125" y1="2.331315625" x2="-3.205903125" y2="2.370475" width="0.001" layer="94"/>
<wire x1="-3.205903125" y1="2.370475" x2="-3.221703125" y2="2.408753125" width="0.001" layer="94"/>
<wire x1="-3.221703125" y1="2.408753125" x2="-3.235021875" y2="2.446575" width="0.001" layer="94"/>
<wire x1="-3.235021875" y1="2.446575" x2="-3.2459625" y2="2.484403125" width="0.001" layer="94"/>
<wire x1="-3.2459625" y1="2.484403125" x2="-3.254640625" y2="2.522665625" width="0.001" layer="94"/>
<wire x1="-3.254640625" y1="2.522665625" x2="-3.26118125" y2="2.561803125" width="0.001" layer="94"/>
<wire x1="-3.26118125" y1="2.561803125" x2="-3.265690625" y2="2.602265625" width="0.001" layer="94"/>
<wire x1="-3.265690625" y1="2.602265625" x2="-3.26828125" y2="2.64449375" width="0.001" layer="94"/>
<wire x1="-3.26828125" y1="2.64449375" x2="-3.2689625" y2="2.685303125" width="0.001" layer="94"/>
<wire x1="-3.2689625" y1="2.685303125" x2="-3.267753125" y2="2.724815625" width="0.001" layer="94"/>
<wire x1="-3.267753125" y1="2.724815625" x2="-3.2646125" y2="2.763303125" width="0.001" layer="94"/>
<wire x1="-3.2646125" y1="2.763303125" x2="-3.25948125" y2="2.801075" width="0.001" layer="94"/>
<wire x1="-3.25948125" y1="2.801075" x2="-3.252321875" y2="2.838415625" width="0.001" layer="94"/>
<wire x1="-3.252321875" y1="2.838415625" x2="-3.243071875" y2="2.875625" width="0.001" layer="94"/>
<wire x1="-3.243071875" y1="2.875625" x2="-3.2317125" y2="2.912984375" width="0.001" layer="94"/>
<wire x1="-3.2317125" y1="2.912984375" x2="-3.2181625" y2="2.95079375" width="0.001" layer="94"/>
<wire x1="-3.2181625" y1="2.95079375" x2="-3.21128125" y2="2.967625" width="0.001" layer="94"/>
<wire x1="-3.21128125" y1="2.967625" x2="-3.204740625" y2="2.98119375" width="0.001" layer="94"/>
<wire x1="-3.204740625" y1="2.98119375" x2="-3.198353125" y2="2.991684375" width="0.001" layer="94"/>
<wire x1="-3.198353125" y1="2.991684375" x2="-3.191921875" y2="2.999275" width="0.001" layer="94"/>
<wire x1="-3.191921875" y1="2.999275" x2="-3.1852625" y2="3.004153125" width="0.001" layer="94"/>
<wire x1="-3.1852625" y1="3.004153125" x2="-3.178171875" y2="3.006503125" width="0.001" layer="94"/>
<wire x1="-3.178171875" y1="3.006503125" x2="-3.170471875" y2="3.006503125" width="0.001" layer="94"/>
<wire x1="-3.170471875" y1="3.006503125" x2="-3.161971875" y2="3.004334375" width="0.001" layer="94"/>
<wire x1="-3.161971875" y1="3.004334375" x2="-3.15813125" y2="3.002603125" width="0.001" layer="94"/>
<wire x1="-3.15813125" y1="3.002603125" x2="-3.154571875" y2="3.000175" width="0.001" layer="94"/>
<wire x1="-3.154571875" y1="3.000175" x2="-3.14778125" y2="2.992434375" width="0.001" layer="94"/>
<wire x1="-3.14778125" y1="2.992434375" x2="-3.140540625" y2="2.979575" width="0.001" layer="94"/>
<wire x1="-3.140540625" y1="2.979575" x2="-3.13183125" y2="2.96009375" width="0.001" layer="94"/>
<wire x1="-3.13183125" y1="2.96009375" x2="-3.115603125" y2="2.92329375" width="0.001" layer="94"/>
<wire x1="-3.115603125" y1="2.92329375" x2="-3.098203125" y2="2.887325" width="0.001" layer="94"/>
<wire x1="-3.098203125" y1="2.887325" x2="-3.079621875" y2="2.85219375" width="0.001" layer="94"/>
<wire x1="-3.079621875" y1="2.85219375" x2="-3.059890625" y2="2.817925" width="0.001" layer="94"/>
<wire x1="-3.059890625" y1="2.817925" x2="-3.039021875" y2="2.784534375" width="0.001" layer="94"/>
<wire x1="-3.039021875" y1="2.784534375" x2="-3.017021875" y2="2.75204375" width="0.001" layer="94"/>
<wire x1="-3.017021875" y1="2.75204375" x2="-2.9939125" y2="2.720465625" width="0.001" layer="94"/>
<wire x1="-2.9939125" y1="2.720465625" x2="-2.969703125" y2="2.689815625" width="0.001" layer="94"/>
<wire x1="-2.969703125" y1="2.689815625" x2="-2.944421875" y2="2.660115625" width="0.001" layer="94"/>
<wire x1="-2.944421875" y1="2.660115625" x2="-2.9180625" y2="2.631384375" width="0.001" layer="94"/>
<wire x1="-2.9180625" y1="2.631384375" x2="-2.890653125" y2="2.603625" width="0.001" layer="94"/>
<wire x1="-2.890653125" y1="2.603625" x2="-2.862203125" y2="2.576875" width="0.001" layer="94"/>
<wire x1="-2.862203125" y1="2.576875" x2="-2.832721875" y2="2.55114375" width="0.001" layer="94"/>
<wire x1="-2.832721875" y1="2.55114375" x2="-2.802240625" y2="2.526453125" width="0.001" layer="94"/>
<wire x1="-2.802240625" y1="2.526453125" x2="-2.7707625" y2="2.502803125" width="0.001" layer="94"/>
<wire x1="-2.7707625" y1="2.502803125" x2="-2.738290625" y2="2.480234375" width="0.001" layer="94"/>
<wire x1="-2.738290625" y1="2.480234375" x2="-2.701721875" y2="2.457215625" width="0.001" layer="94"/>
<wire x1="-2.701721875" y1="2.457215625" x2="-2.660703125" y2="2.433325" width="0.001" layer="94"/>
<wire x1="-2.660703125" y1="2.433325" x2="-2.625921875" y2="2.414584375" width="0.001" layer="94"/>
<wire x1="-2.625921875" y1="2.414584375" x2="-2.6142125" y2="2.409015625" width="0.001" layer="94"/>
<wire x1="-2.6142125" y1="2.409015625" x2="-2.60808125" y2="2.40699375" width="0.001" layer="94"/>
<wire x1="-2.60808125" y1="2.40699375" x2="-2.6075625" y2="2.408553125" width="0.001" layer="94"/>
<wire x1="-2.6075625" y1="2.408553125" x2="-2.60818125" y2="2.412803125" width="0.001" layer="94"/>
<wire x1="-2.60818125" y1="2.412803125" x2="-2.612290625" y2="2.426765625" width="0.001" layer="94"/>
<wire x1="-2.612290625" y1="2.426765625" x2="-2.621453125" y2="2.458925" width="0.001" layer="94"/>
<wire x1="-2.621453125" y1="2.458925" x2="-2.632690625" y2="2.507025" width="0.001" layer="94"/>
<wire x1="-2.632690625" y1="2.507025" x2="-2.6440125" y2="2.561934375" width="0.001" layer="94"/>
<wire x1="-2.6440125" y1="2.561934375" x2="-2.653403125" y2="2.61449375" width="0.001" layer="94"/>
<wire x1="-2.653403125" y1="2.61449375" x2="-2.657740625" y2="2.650553125" width="0.001" layer="94"/>
<wire x1="-2.657740625" y1="2.650553125" x2="-2.6607125" y2="2.694353125" width="0.001" layer="94"/>
<wire x1="-2.6607125" y1="2.694353125" x2="-2.66233125" y2="2.74349375" width="0.001" layer="94"/>
<wire x1="-2.66233125" y1="2.74349375" x2="-2.6626125" y2="2.795575" width="0.001" layer="94"/>
<wire x1="-2.6626125" y1="2.795575" x2="-2.661571875" y2="2.848203125" width="0.001" layer="94"/>
<wire x1="-2.661571875" y1="2.848203125" x2="-2.659221875" y2="2.898984375" width="0.001" layer="94"/>
<wire x1="-2.659221875" y1="2.898984375" x2="-2.655603125" y2="2.945515625" width="0.001" layer="94"/>
<wire x1="-2.655603125" y1="2.945515625" x2="-2.650703125" y2="2.985403125" width="0.001" layer="94"/>
<wire x1="-2.650703125" y1="2.985403125" x2="-2.645421875" y2="3.017284375" width="0.001" layer="94"/>
<wire x1="-2.645421875" y1="3.017284375" x2="-2.639840625" y2="3.046575" width="0.001" layer="94"/>
<wire x1="-2.639840625" y1="3.046575" x2="-2.633940625" y2="3.073284375" width="0.001" layer="94"/>
<wire x1="-2.633940625" y1="3.073284375" x2="-2.627721875" y2="3.09744375" width="0.001" layer="94"/>
<wire x1="-2.627721875" y1="3.09744375" x2="-2.621171875" y2="3.119065625" width="0.001" layer="94"/>
<wire x1="-2.621171875" y1="3.119065625" x2="-2.614271875" y2="3.138153125" width="0.001" layer="94"/>
<wire x1="-2.614271875" y1="3.138153125" x2="-2.607021875" y2="3.154734375" width="0.001" layer="94"/>
<wire x1="-2.607021875" y1="3.154734375" x2="-2.599403125" y2="3.168815625" width="0.001" layer="94"/>
<wire x1="-2.599403125" y1="3.168815625" x2="-2.591421875" y2="3.180425" width="0.001" layer="94"/>
<wire x1="-2.591421875" y1="3.180425" x2="-2.5830625" y2="3.189553125" width="0.001" layer="94"/>
<wire x1="-2.5830625" y1="3.189553125" x2="-2.5743125" y2="3.19624375" width="0.001" layer="94"/>
<wire x1="-2.5743125" y1="3.19624375" x2="-2.5651625" y2="3.200484375" width="0.001" layer="94"/>
<wire x1="-2.5651625" y1="3.200484375" x2="-2.555603125" y2="3.202315625" width="0.001" layer="94"/>
<wire x1="-2.555603125" y1="3.202315625" x2="-2.54563125" y2="3.201734375" width="0.001" layer="94"/>
<wire x1="-2.54563125" y1="3.201734375" x2="-2.53523125" y2="3.198765625" width="0.001" layer="94"/>
<wire x1="-2.53523125" y1="3.198765625" x2="-2.524390625" y2="3.193415625" width="0.001" layer="94"/>
<wire x1="-2.524390625" y1="3.193415625" x2="-2.516440625" y2="3.18794375" width="0.001" layer="94"/>
<wire x1="-2.516440625" y1="3.18794375" x2="-2.51388125" y2="3.185034375" width="0.001" layer="94"/>
<wire x1="-2.51388125" y1="3.185034375" x2="-2.512071875" y2="3.18154375" width="0.001" layer="94"/>
<wire x1="-2.512071875" y1="3.18154375" x2="-2.5102125" y2="3.17139375" width="0.001" layer="94"/>
<wire x1="-2.5102125" y1="3.17139375" x2="-2.50978125" y2="3.154665625" width="0.001" layer="94"/>
<wire x1="-2.50978125" y1="3.154665625" x2="-2.50758125" y2="3.11834375" width="0.001" layer="94"/>
<wire x1="-2.50758125" y1="3.11834375" x2="-2.502421875" y2="3.073384375" width="0.001" layer="94"/>
<wire x1="-2.502421875" y1="3.073384375" x2="-2.496821875" y2="3.038803125" width="0.001" layer="94"/>
<wire x1="-2.496821875" y1="3.038803125" x2="-2.489903125" y2="3.004725" width="0.001" layer="94"/>
<wire x1="-2.489903125" y1="3.004725" x2="-2.48168125" y2="2.971153125" width="0.001" layer="94"/>
<wire x1="-2.48168125" y1="2.971153125" x2="-2.472153125" y2="2.938084375" width="0.001" layer="94"/>
<wire x1="-2.472153125" y1="2.938084375" x2="-2.4613125" y2="2.905515625" width="0.001" layer="94"/>
<wire x1="-2.4613125" y1="2.905515625" x2="-2.449171875" y2="2.873465625" width="0.001" layer="94"/>
<wire x1="-2.449171875" y1="2.873465625" x2="-2.435721875" y2="2.841925" width="0.001" layer="94"/>
<wire x1="-2.435721875" y1="2.841925" x2="-2.420971875" y2="2.81089375" width="0.001" layer="94"/>
<wire x1="-2.420971875" y1="2.81089375" x2="-2.404921875" y2="2.780375" width="0.001" layer="94"/>
<wire x1="-2.404921875" y1="2.780375" x2="-2.3875625" y2="2.750375" width="0.001" layer="94"/>
<wire x1="-2.3875625" y1="2.750375" x2="-2.3689125" y2="2.72089375" width="0.001" layer="94"/>
<wire x1="-2.3689125" y1="2.72089375" x2="-2.348953125" y2="2.691934375" width="0.001" layer="94"/>
<wire x1="-2.348953125" y1="2.691934375" x2="-2.327703125" y2="2.66349375" width="0.001" layer="94"/>
<wire x1="-2.327703125" y1="2.66349375" x2="-2.305140625" y2="2.635575" width="0.001" layer="94"/>
<wire x1="-2.305140625" y1="2.635575" x2="-2.281290625" y2="2.608184375" width="0.001" layer="94"/>
<wire x1="-2.281290625" y1="2.608184375" x2="-2.256153125" y2="2.581325" width="0.001" layer="94"/>
<wire x1="-2.256153125" y1="2.581325" x2="-2.228821875" y2="2.55314375" width="0.001" layer="94"/>
<wire x1="-2.228821875" y1="2.55314375" x2="-2.278190625" y2="2.525434375" width="0.001" layer="94"/>
<wire x1="-2.278190625" y1="2.525434375" x2="-2.3065125" y2="2.50889375" width="0.001" layer="94"/>
<wire x1="-2.3065125" y1="2.50889375" x2="-2.334140625" y2="2.49144375" width="0.001" layer="94"/>
<wire x1="-2.334140625" y1="2.49144375" x2="-2.361040625" y2="2.473134375" width="0.001" layer="94"/>
<wire x1="-2.361040625" y1="2.473134375" x2="-2.387171875" y2="2.45399375" width="0.001" layer="94"/>
<wire x1="-2.387171875" y1="2.45399375" x2="-2.412490625" y2="2.434084375" width="0.001" layer="94"/>
<wire x1="-2.412490625" y1="2.434084375" x2="-2.4369625" y2="2.413434375" width="0.001" layer="94"/>
<wire x1="-2.4369625" y1="2.413434375" x2="-2.46053125" y2="2.392084375" width="0.001" layer="94"/>
<wire x1="-2.46053125" y1="2.392084375" x2="-2.483171875" y2="2.370084375" width="0.001" layer="94"/>
<wire x1="-2.483171875" y1="2.370084375" x2="-2.50483125" y2="2.347465625" width="0.001" layer="94"/>
<wire x1="-2.50483125" y1="2.347465625" x2="-2.525471875" y2="2.324275" width="0.001" layer="94"/>
<wire x1="-2.525471875" y1="2.324275" x2="-2.5450625" y2="2.300565625" width="0.001" layer="94"/>
<wire x1="-2.5450625" y1="2.300565625" x2="-2.563540625" y2="2.276365625" width="0.001" layer="94"/>
<wire x1="-2.563540625" y1="2.276365625" x2="-2.58088125" y2="2.251715625" width="0.001" layer="94"/>
<wire x1="-2.58088125" y1="2.251715625" x2="-2.59703125" y2="2.226665625" width="0.001" layer="94"/>
<wire x1="-2.59703125" y1="2.226665625" x2="-2.6119625" y2="2.201253125" width="0.001" layer="94"/>
<wire x1="-2.6119625" y1="2.201253125" x2="-2.6256125" y2="2.175515625" width="0.001" layer="94"/>
<wire x1="-2.6256125" y1="2.175515625" x2="-2.640303125" y2="2.144265625" width="0.001" layer="94"/>
<wire x1="-2.640303125" y1="2.144265625" x2="-2.653303125" y2="2.112275" width="0.001" layer="94"/>
<wire x1="-2.653303125" y1="2.112275" x2="-2.66463125" y2="2.07964375" width="0.001" layer="94"/>
<wire x1="-2.66463125" y1="2.07964375" x2="-2.6742625" y2="2.046434375" width="0.001" layer="94"/>
<wire x1="-2.6742625" y1="2.046434375" x2="-2.6822125" y2="2.012725" width="0.001" layer="94"/>
<wire x1="-2.6822125" y1="2.012725" x2="-2.6884625" y2="1.978603125" width="0.001" layer="94"/>
<wire x1="-2.6884625" y1="1.978603125" x2="-2.693021875" y2="1.944134375" width="0.001" layer="94"/>
<wire x1="-2.693021875" y1="1.944134375" x2="-2.6958625" y2="1.90939375" width="0.001" layer="94"/>
<wire x1="-2.6958625" y1="1.90939375" x2="-2.6970125" y2="1.874465625" width="0.001" layer="94"/>
<wire x1="-2.6970125" y1="1.874465625" x2="-2.69643125" y2="1.839425" width="0.001" layer="94"/>
<wire x1="-2.69643125" y1="1.839425" x2="-2.694140625" y2="1.804334375" width="0.001" layer="94"/>
<wire x1="-2.694140625" y1="1.804334375" x2="-2.690140625" y2="1.76929375" width="0.001" layer="94"/>
<wire x1="-2.690140625" y1="1.76929375" x2="-2.684403125" y2="1.734365625" width="0.001" layer="94"/>
<wire x1="-2.684403125" y1="1.734365625" x2="-2.67693125" y2="1.699625" width="0.001" layer="94"/>
<wire x1="-2.67693125" y1="1.699625" x2="-2.66773125" y2="1.665153125" width="0.001" layer="94"/>
<wire x1="-2.66773125" y1="1.665153125" x2="-2.656803125" y2="1.631034375" width="0.001" layer="94"/>
<wire x1="-2.656803125" y1="1.631034375" x2="-2.641471875" y2="1.589515625" width="0.001" layer="94"/>
<wire x1="-2.641471875" y1="1.589515625" x2="-2.629653125" y2="1.561575" width="0.001" layer="94"/>
<wire x1="-2.629653125" y1="1.561575" x2="-2.62033125" y2="1.54329375" width="0.001" layer="94"/>
<wire x1="-2.62033125" y1="1.54329375" x2="-2.643303125" y2="1.54014375" width="0.001" layer="94"/>
<wire x1="-2.643303125" y1="1.54014375" x2="-2.661340625" y2="1.538175" width="0.001" layer="94"/>
<wire x1="-2.661340625" y1="1.538175" x2="-2.679771875" y2="1.537834375" width="0.001" layer="94"/>
<wire x1="-2.679771875" y1="1.537834375" x2="-2.70473125" y2="1.539265625" width="0.001" layer="94"/>
<wire x1="-2.70473125" y1="1.539265625" x2="-2.742371875" y2="1.542634375" width="0.001" layer="94"/>
<wire x1="-2.331921875" y1="1.734865625" x2="-2.342440625" y2="1.739215625" width="0.001" layer="94"/>
<wire x1="-2.342440625" y1="1.739215625" x2="-2.352253125" y2="1.744284375" width="0.001" layer="94"/>
<wire x1="-2.352253125" y1="1.744284375" x2="-2.3613125" y2="1.750053125" width="0.001" layer="94"/>
<wire x1="-2.3613125" y1="1.750053125" x2="-2.369590625" y2="1.756484375" width="0.001" layer="94"/>
<wire x1="-2.369590625" y1="1.756484375" x2="-2.3770625" y2="1.763565625" width="0.001" layer="94"/>
<wire x1="-2.3770625" y1="1.763565625" x2="-2.383703125" y2="1.771265625" width="0.001" layer="94"/>
<wire x1="-2.383703125" y1="1.771265625" x2="-2.389471875" y2="1.779565625" width="0.001" layer="94"/>
<wire x1="-2.389471875" y1="1.779565625" x2="-2.3943625" y2="1.788434375" width="0.001" layer="94"/>
<wire x1="-2.3943625" y1="1.788434375" x2="-2.400721875" y2="1.804665625" width="0.001" layer="94"/>
<wire x1="-2.400721875" y1="1.804665625" x2="-2.40403125" y2="1.820753125" width="0.001" layer="94"/>
<wire x1="-2.40403125" y1="1.820753125" x2="-2.404540625" y2="1.82874375" width="0.001" layer="94"/>
<wire x1="-2.404540625" y1="1.82874375" x2="-2.404290625" y2="1.83669375" width="0.001" layer="94"/>
<wire x1="-2.404290625" y1="1.83669375" x2="-2.40328125" y2="1.84459375" width="0.001" layer="94"/>
<wire x1="-2.40328125" y1="1.84459375" x2="-2.4015125" y2="1.85244375" width="0.001" layer="94"/>
<wire x1="-2.4015125" y1="1.85244375" x2="-2.395690625" y2="1.868015625" width="0.001" layer="94"/>
<wire x1="-2.395690625" y1="1.868015625" x2="-2.386840625" y2="1.883365625" width="0.001" layer="94"/>
<wire x1="-2.386840625" y1="1.883365625" x2="-2.3749625" y2="1.89849375" width="0.001" layer="94"/>
<wire x1="-2.3749625" y1="1.89849375" x2="-2.360053125" y2="1.913384375" width="0.001" layer="94"/>
<wire x1="-2.360053125" y1="1.913384375" x2="-2.3454125" y2="1.925053125" width="0.001" layer="94"/>
<wire x1="-2.3454125" y1="1.925053125" x2="-2.3297625" y2="1.934803125" width="0.001" layer="94"/>
<wire x1="-2.3297625" y1="1.934803125" x2="-2.31333125" y2="1.942665625" width="0.001" layer="94"/>
<wire x1="-2.31333125" y1="1.942665625" x2="-2.296321875" y2="1.948634375" width="0.001" layer="94"/>
<wire x1="-2.296321875" y1="1.948634375" x2="-2.278971875" y2="1.952775" width="0.001" layer="94"/>
<wire x1="-2.278971875" y1="1.952775" x2="-2.261471875" y2="1.95509375" width="0.001" layer="94"/>
<wire x1="-2.261471875" y1="1.95509375" x2="-2.244071875" y2="1.955625" width="0.001" layer="94"/>
<wire x1="-2.244071875" y1="1.955625" x2="-2.2269625" y2="1.954384375" width="0.001" layer="94"/>
<wire x1="-2.2269625" y1="1.954384375" x2="-2.210371875" y2="1.951415625" width="0.001" layer="94"/>
<wire x1="-2.210371875" y1="1.951415625" x2="-2.194521875" y2="1.946734375" width="0.001" layer="94"/>
<wire x1="-2.194521875" y1="1.946734375" x2="-2.179621875" y2="1.940375" width="0.001" layer="94"/>
<wire x1="-2.179621875" y1="1.940375" x2="-2.165890625" y2="1.932365625" width="0.001" layer="94"/>
<wire x1="-2.165890625" y1="1.932365625" x2="-2.153553125" y2="1.922725" width="0.001" layer="94"/>
<wire x1="-2.153553125" y1="1.922725" x2="-2.1428125" y2="1.911475" width="0.001" layer="94"/>
<wire x1="-2.1428125" y1="1.911475" x2="-2.133903125" y2="1.898665625" width="0.001" layer="94"/>
<wire x1="-2.133903125" y1="1.898665625" x2="-2.12703125" y2="1.884303125" width="0.001" layer="94"/>
<wire x1="-2.12703125" y1="1.884303125" x2="-2.123703125" y2="1.873603125" width="0.001" layer="94"/>
<wire x1="-2.123703125" y1="1.873603125" x2="-2.12178125" y2="1.862775" width="0.001" layer="94"/>
<wire x1="-2.12178125" y1="1.862775" x2="-2.121221875" y2="1.851875" width="0.001" layer="94"/>
<wire x1="-2.121221875" y1="1.851875" x2="-2.121990625" y2="1.840984375" width="0.001" layer="94"/>
<wire x1="-2.121990625" y1="1.840984375" x2="-2.124040625" y2="1.830175" width="0.001" layer="94"/>
<wire x1="-2.124040625" y1="1.830175" x2="-2.12733125" y2="1.81949375" width="0.001" layer="94"/>
<wire x1="-2.12733125" y1="1.81949375" x2="-2.1318125" y2="1.809034375" width="0.001" layer="94"/>
<wire x1="-2.1318125" y1="1.809034375" x2="-2.137440625" y2="1.798865625" width="0.001" layer="94"/>
<wire x1="-2.137440625" y1="1.798865625" x2="-2.144190625" y2="1.789034375" width="0.001" layer="94"/>
<wire x1="-2.144190625" y1="1.789034375" x2="-2.152003125" y2="1.779634375" width="0.001" layer="94"/>
<wire x1="-2.152003125" y1="1.779634375" x2="-2.16083125" y2="1.770725" width="0.001" layer="94"/>
<wire x1="-2.16083125" y1="1.770725" x2="-2.170653125" y2="1.762375" width="0.001" layer="94"/>
<wire x1="-2.170653125" y1="1.762375" x2="-2.181403125" y2="1.754653125" width="0.001" layer="94"/>
<wire x1="-2.181403125" y1="1.754653125" x2="-2.193053125" y2="1.747625" width="0.001" layer="94"/>
<wire x1="-2.193053125" y1="1.747625" x2="-2.205553125" y2="1.741375" width="0.001" layer="94"/>
<wire x1="-2.205553125" y1="1.741375" x2="-2.2188625" y2="1.735953125" width="0.001" layer="94"/>
<wire x1="-2.2188625" y1="1.735953125" x2="-2.230871875" y2="1.73249375" width="0.001" layer="94"/>
<wire x1="-2.230871875" y1="1.73249375" x2="-2.2448625" y2="1.729975" width="0.001" layer="94"/>
<wire x1="-2.2448625" y1="1.729975" x2="-2.260140625" y2="1.72839375" width="0.001" layer="94"/>
<wire x1="-2.260140625" y1="1.72839375" x2="-2.27603125" y2="1.727765625" width="0.001" layer="94"/>
<wire x1="-2.27603125" y1="1.727765625" x2="-2.291840625" y2="1.72809375" width="0.001" layer="94"/>
<wire x1="-2.291840625" y1="1.72809375" x2="-2.30688125" y2="1.729375" width="0.001" layer="94"/>
<wire x1="-2.30688125" y1="1.729375" x2="-2.320471875" y2="1.731634375" width="0.001" layer="94"/>
<wire x1="-2.320471875" y1="1.731634375" x2="-2.331921875" y2="1.734865625" width="0.001" layer="94"/>
<wire x1="0.760128125" y1="3.316884375" x2="0.809546875" y2="3.367275" width="0.001" layer="94"/>
<wire x1="0.809546875" y1="3.367275" x2="0.9440875" y2="3.502503125" width="0.001" layer="94"/>
<wire x1="0.9440875" y1="3.502503125" x2="1.386378125" y2="3.945015625" width="0.001" layer="94"/>
<wire x1="1.386378125" y1="3.945015625" x2="1.67071875" y2="4.228365625" width="0.001" layer="94"/>
<wire x1="1.67071875" y1="4.228365625" x2="1.868946875" y2="4.424403125" width="0.001" layer="94"/>
<wire x1="1.868946875" y1="4.424403125" x2="1.987409375" y2="4.539365625" width="0.001" layer="94"/>
<wire x1="1.987409375" y1="4.539365625" x2="2.018696875" y2="4.568375" width="0.001" layer="94"/>
<wire x1="2.018696875" y1="4.568375" x2="2.032428125" y2="4.579453125" width="0.001" layer="94"/>
<wire x1="2.032428125" y1="4.579453125" x2="2.0417875" y2="4.583125" width="0.001" layer="94"/>
<wire x1="2.0417875" y1="4.583125" x2="2.05261875" y2="4.58649375" width="0.001" layer="94"/>
<wire x1="2.05261875" y1="4.58649375" x2="2.063596875" y2="4.589184375" width="0.001" layer="94"/>
<wire x1="2.063596875" y1="4.589184375" x2="2.07336875" y2="4.590815625" width="0.001" layer="94"/>
<wire x1="2.07336875" y1="4.590815625" x2="2.093009375" y2="4.591953125" width="0.001" layer="94"/>
<wire x1="2.093009375" y1="4.591953125" x2="2.1095375" y2="4.590534375" width="0.001" layer="94"/>
<wire x1="2.1095375" y1="4.590534375" x2="2.11666875" y2="4.588825" width="0.001" layer="94"/>
<wire x1="2.11666875" y1="4.588825" x2="2.123046875" y2="4.586453125" width="0.001" layer="94"/>
<wire x1="2.123046875" y1="4.586453125" x2="2.128696875" y2="4.58339375" width="0.001" layer="94"/>
<wire x1="2.128696875" y1="4.58339375" x2="2.133628125" y2="4.57964375" width="0.001" layer="94"/>
<wire x1="2.133628125" y1="4.57964375" x2="2.137846875" y2="4.57519375" width="0.001" layer="94"/>
<wire x1="2.137846875" y1="4.57519375" x2="2.141378125" y2="4.570025" width="0.001" layer="94"/>
<wire x1="2.141378125" y1="4.570025" x2="2.144209375" y2="4.564125" width="0.001" layer="94"/>
<wire x1="2.144209375" y1="4.564125" x2="2.146378125" y2="4.55749375" width="0.001" layer="94"/>
<wire x1="2.146378125" y1="4.55749375" x2="2.147878125" y2="4.550125" width="0.001" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TRISOLX_SOLAR_WING">
<description>Trisolx solar wing 
Vout  = 2.33V
Iout = 14.6mA</description>
<gates>
<gate name="G$1" symbol="SOLAR_CELL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TRISOLX_SOLAR_WING">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATMEGA328P">
<description>Arduino nano microcontroller</description>
<gates>
<gate name="G$1" symbol="ATMEGA328P(1-18)" x="-2.54" y="0"/>
<gate name="G$2" symbol="ATMEGA328P(19-32)" x="-48.26" y="0"/>
</gates>
<devices>
<device name="" package="TQFP-32A-(7X7MM)">
<connects>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND" pad="3 5"/>
<connect gate="G$1" pin="PCINT0/CLKO/ICP1/PB0" pad="12"/>
<connect gate="G$1" pin="PCINT1/OC1A/PB1" pad="13"/>
<connect gate="G$1" pin="PCINT19/OC2B/INT1/PD3" pad="1"/>
<connect gate="G$1" pin="PCINT2/SS/OC1B/PB2" pad="14"/>
<connect gate="G$1" pin="PCINT20/XCK/T0/PD4" pad="2"/>
<connect gate="G$1" pin="PCINT21/OC0B/T1/PD5" pad="9"/>
<connect gate="G$1" pin="PCINT22/OC0A/AIN0/PD6" pad="10"/>
<connect gate="G$1" pin="PCINT23/AIN1/PD7" pad="11"/>
<connect gate="G$1" pin="PCINT3/OC2A/MOSI/PB3" pad="15"/>
<connect gate="G$1" pin="PCINT4/MISO/PB4" pad="16"/>
<connect gate="G$1" pin="PCINT6/XTAL/OSC1/PB6" pad="7"/>
<connect gate="G$1" pin="PCINT7/XTAL2/OSC2/PB7" pad="8"/>
<connect gate="G$1" pin="SCK/PCINT5/PB5" pad="17"/>
<connect gate="G$1" pin="VCC" pad="4 6"/>
<connect gate="G$2" pin="ADC0/PCINT8/PC0" pad="23"/>
<connect gate="G$2" pin="ADC1/PCINT9/PC1" pad="24"/>
<connect gate="G$2" pin="ADC2/PCINT10/PC2" pad="25"/>
<connect gate="G$2" pin="ADC3/PCINT11/PC3" pad="26"/>
<connect gate="G$2" pin="ADC4/SDA//PCINT12/PC4" pad="27"/>
<connect gate="G$2" pin="ADC5/SCL/PCINT13/PC5" pad="28"/>
<connect gate="G$2" pin="ADC6" pad="19"/>
<connect gate="G$2" pin="ADC7" pad="22"/>
<connect gate="G$2" pin="AREF" pad="20"/>
<connect gate="G$2" pin="GND" pad="21"/>
<connect gate="G$2" pin="INT0/PCINT18/PD2" pad="32"/>
<connect gate="G$2" pin="RESET/PCINT14/PC6" pad="29"/>
<connect gate="G$2" pin="RXD/PCINT16/PD0" pad="30"/>
<connect gate="G$2" pin="TXD/PCINT17/PD1" pad="31"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER" value="Microchip"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BQ25570">
<description>Nano power boost charger and buck converter for energy harvester powered applications</description>
<gates>
<gate name="G$1" symbol="BQ25570" x="0" y="0"/>
</gates>
<devices>
<device name="" package="S-PVQFN-N20">
<connects>
<connect gate="G$1" pin="EN" pad="5"/>
<connect gate="G$1" pin="LBOOST" pad="20"/>
<connect gate="G$1" pin="LBUCK" pad="16"/>
<connect gate="G$1" pin="OK_HYST" pad="10"/>
<connect gate="G$1" pin="OK_PROG" pad="11"/>
<connect gate="G$1" pin="VBAT" pad="18"/>
<connect gate="G$1" pin="VBAT_OK" pad="13"/>
<connect gate="G$1" pin="VBAT_OV" pad="7"/>
<connect gate="G$1" pin="VCC_SAMP" pad="3"/>
<connect gate="G$1" pin="VIN_DC" pad="2"/>
<connect gate="G$1" pin="VOUT" pad="14"/>
<connect gate="G$1" pin="VOUT_EN" pad="6"/>
<connect gate="G$1" pin="VOUT_SET" pad="12"/>
<connect gate="G$1" pin="VRDIV" pad="8"/>
<connect gate="G$1" pin="VREF_SAMP" pad="4"/>
<connect gate="G$1" pin="VSS" pad="1 15"/>
<connect gate="G$1" pin="VSTOR" pad="19"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TMP102">
<description>I2C controlled Temperature Sensor</description>
<gates>
<gate name="G$1" symbol="TMP102" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DRL0006A">
<connects>
<connect gate="G$1" pin="ADD0" pad="4"/>
<connect gate="G$1" pin="ALERT" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="SCL" pad="1"/>
<connect gate="G$1" pin="SDA" pad="6"/>
<connect gate="G$1" pin="V+" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SI1147">
<description>Ambient/UV and IR light sensor</description>
<gates>
<gate name="G$1" symbol="SI1147" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SI1145/46/47">
<connects>
<connect gate="G$1" pin="DNC" pad="5 10"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="INT" pad="4"/>
<connect gate="G$1" pin="LED1" pad="9"/>
<connect gate="G$1" pin="LED2" pad="6"/>
<connect gate="G$1" pin="LED3" pad="7"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="1"/>
<connect gate="G$1" pin="VDD" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LSM9DS1">
<description>StMicrolectronics accelerometer, gyroscope, magnetometer</description>
<gates>
<gate name="G$1" symbol="LSM9DS1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LGA-24-LEAD">
<connects>
<connect gate="G$1" pin="C1" pad="24"/>
<connect gate="G$1" pin="CAP" pad="21"/>
<connect gate="G$1" pin="CS_A/G" pad="7"/>
<connect gate="G$1" pin="CS_M" pad="8"/>
<connect gate="G$1" pin="DEN_A/G" pad="13"/>
<connect gate="G$1" pin="DRDY_M" pad="9"/>
<connect gate="G$1" pin="GND" pad="19 20"/>
<connect gate="G$1" pin="INT1_A/G" pad="11"/>
<connect gate="G$1" pin="INT2_A/G" pad="12"/>
<connect gate="G$1" pin="INT_M" pad="10"/>
<connect gate="G$1" pin="RES" pad="14 15 16 17 18"/>
<connect gate="G$1" pin="SCL" pad="2"/>
<connect gate="G$1" pin="SDA" pad="4"/>
<connect gate="G$1" pin="SDO_A/G" pad="5"/>
<connect gate="G$1" pin="SDO_M" pad="6"/>
<connect gate="G$1" pin="VDD" pad="22 23"/>
<connect gate="G$1" pin="VDDIO" pad="1 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DPT301120">
<description>Li-Ion single cell battery 40mAh</description>
<gates>
<gate name="G$1" symbol="BATTERY" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DPT301120">
<connects>
<connect gate="G$1" pin="+" pad="P$1"/>
<connect gate="G$1" pin="-" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L0805C220MDWIT">
<description>Inductor 22uF 190mA</description>
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L0603B100MPWFT">
<description>Inductor 10uH 200mA</description>
<gates>
<gate name="G$1" symbol="L-US" x="0" y="0"/>
</gates>
<devices>
<device name="" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RFM95W">
<description>LoRA based module 433MHz</description>
<gates>
<gate name="G$1" symbol="RFM95W/96W/98W" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="RFM95/95/97">
<connects>
<connect gate="G$1" pin="ANT" pad="P$9"/>
<connect gate="G$1" pin="DIO0" pad="P$14"/>
<connect gate="G$1" pin="DIO1" pad="P$15"/>
<connect gate="G$1" pin="DIO2" pad="P$18"/>
<connect gate="G$1" pin="DIO3" pad="P$11"/>
<connect gate="G$1" pin="DIO4" pad="P$12"/>
<connect gate="G$1" pin="DIO5" pad="P$7"/>
<connect gate="G$1" pin="GND" pad="P$1 P$8 P$10" route="any"/>
<connect gate="G$1" pin="MISO" pad="P$2"/>
<connect gate="G$1" pin="MOSI" pad="P$3"/>
<connect gate="G$1" pin="NSS" pad="P$5"/>
<connect gate="G$1" pin="RESET" pad="P$6"/>
<connect gate="G$1" pin="SCK" pad="P$4"/>
<connect gate="G$1" pin="VCC" pad="P$13"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CSTCR4M00G53U-R0">
<gates>
<gate name="G$1" symbol="RESONATOR" x="-7.62" y="7.62"/>
</gates>
<devices>
<device name="" package="ECS-SR-B">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$3"/>
<connect gate="G$1" pin="P$3" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V-DIPOLE_433MHZ">
<description>v-dipole antenna 1/4 wavelenght</description>
<gates>
<gate name="G$1" symbol="ANTENNA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="V-DIPOLE">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AXOLOTL_LOGO">
<gates>
<gate name="G$1" symbol="AXOLOTL_LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AXOLOTL_LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl" urn="urn:adsk.eagle:library:334">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
http://www.secc.co.jp/pdf/os_e/2004/e_os_all.pdf &lt;b&gt;(SANYO)&lt;/b&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0&gt;
&lt;tr valign="top"&gt;

&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;</description>
<packages>
<package name="R0402" urn="urn:adsk.eagle:footprint:23043/3" library_version="3">
<description>&lt;b&gt;Chip RESISTOR 0402 EIA (1005 Metric)&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1" y1="0.483" x2="1" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="0.483" x2="1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="-0.483" x2="-1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1" y1="-0.483" x2="-1" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.35" x2="0.1999" y2="0.35" layer="35"/>
</package>
<package name="R0603" urn="urn:adsk.eagle:footprint:23044/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805" urn="urn:adsk.eagle:footprint:23045/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W" urn="urn:adsk.eagle:footprint:23046/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206" urn="urn:adsk.eagle:footprint:23047/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W" urn="urn:adsk.eagle:footprint:23048/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210" urn="urn:adsk.eagle:footprint:23049/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W" urn="urn:adsk.eagle:footprint:23050/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010" urn="urn:adsk.eagle:footprint:23051/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W" urn="urn:adsk.eagle:footprint:23052/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012" urn="urn:adsk.eagle:footprint:23053/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W" urn="urn:adsk.eagle:footprint:23054/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512" urn="urn:adsk.eagle:footprint:23055/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W" urn="urn:adsk.eagle:footprint:23056/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216" urn="urn:adsk.eagle:footprint:23057/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W" urn="urn:adsk.eagle:footprint:23058/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225" urn="urn:adsk.eagle:footprint:23059/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W" urn="urn:adsk.eagle:footprint:23060/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025" urn="urn:adsk.eagle:footprint:23061/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W" urn="urn:adsk.eagle:footprint:23062/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332" urn="urn:adsk.eagle:footprint:23063/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W" urn="urn:adsk.eagle:footprint:25646/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805" urn="urn:adsk.eagle:footprint:23065/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206" urn="urn:adsk.eagle:footprint:23066/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406" urn="urn:adsk.eagle:footprint:23067/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012" urn="urn:adsk.eagle:footprint:23068/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309" urn="urn:adsk.eagle:footprint:23069/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216" urn="urn:adsk.eagle:footprint:23070/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516" urn="urn:adsk.eagle:footprint:23071/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923" urn="urn:adsk.eagle:footprint:23072/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5" urn="urn:adsk.eagle:footprint:22991/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7" urn="urn:adsk.eagle:footprint:22998/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0207/10" urn="urn:adsk.eagle:footprint:22992/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12" urn="urn:adsk.eagle:footprint:22993/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15" urn="urn:adsk.eagle:footprint:22997/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V" urn="urn:adsk.eagle:footprint:22994/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V" urn="urn:adsk.eagle:footprint:22995/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7" urn="urn:adsk.eagle:footprint:22996/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10" urn="urn:adsk.eagle:footprint:23073/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12" urn="urn:adsk.eagle:footprint:23074/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12" urn="urn:adsk.eagle:footprint:23076/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15" urn="urn:adsk.eagle:footprint:23077/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V" urn="urn:adsk.eagle:footprint:23078/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15" urn="urn:adsk.eagle:footprint:23079/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V" urn="urn:adsk.eagle:footprint:23080/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17" urn="urn:adsk.eagle:footprint:23081/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22" urn="urn:adsk.eagle:footprint:23082/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V" urn="urn:adsk.eagle:footprint:23083/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22" urn="urn:adsk.eagle:footprint:23084/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V" urn="urn:adsk.eagle:footprint:23085/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15" urn="urn:adsk.eagle:footprint:23086/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22" urn="urn:adsk.eagle:footprint:23087/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V" urn="urn:adsk.eagle:footprint:23088/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12" urn="urn:adsk.eagle:footprint:23089/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17" urn="urn:adsk.eagle:footprint:23090/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0" urn="urn:adsk.eagle:footprint:23091/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102AX" urn="urn:adsk.eagle:footprint:23100/1" library_version="3">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V" urn="urn:adsk.eagle:footprint:23098/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="MINI_MELF-0102R" urn="urn:adsk.eagle:footprint:23092/1" library_version="3">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W" urn="urn:adsk.eagle:footprint:23093/1" library_version="3">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R" urn="urn:adsk.eagle:footprint:25676/1" library_version="3">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W" urn="urn:adsk.eagle:footprint:25677/1" library_version="3">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R" urn="urn:adsk.eagle:footprint:25678/1" library_version="3">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W" urn="urn:adsk.eagle:footprint:25679/1" library_version="3">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RDH/15" urn="urn:adsk.eagle:footprint:23099/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="0204V" urn="urn:adsk.eagle:footprint:22999/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0309V" urn="urn:adsk.eagle:footprint:23075/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="R0201" urn="urn:adsk.eagle:footprint:25683/1" library_version="3">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VMTA55" urn="urn:adsk.eagle:footprint:25689/1" library_version="3">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60" urn="urn:adsk.eagle:footprint:25690/1" library_version="3">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="VTA52" urn="urn:adsk.eagle:footprint:25684/1" library_version="3">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53" urn="urn:adsk.eagle:footprint:25685/1" library_version="3">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54" urn="urn:adsk.eagle:footprint:25686/1" library_version="3">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55" urn="urn:adsk.eagle:footprint:25687/1" library_version="3">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56" urn="urn:adsk.eagle:footprint:25688/1" library_version="3">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="R4527" urn="urn:adsk.eagle:footprint:13246/1" library_version="3">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001" urn="urn:adsk.eagle:footprint:25692/1" library_version="3">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002" urn="urn:adsk.eagle:footprint:25693/1" library_version="3">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2" urn="urn:adsk.eagle:footprint:25694/1" library_version="3">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515" urn="urn:adsk.eagle:footprint:25695/1" library_version="3">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527" urn="urn:adsk.eagle:footprint:25696/1" library_version="3">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927" urn="urn:adsk.eagle:footprint:25697/1" library_version="3">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218" urn="urn:adsk.eagle:footprint:25698/1" library_version="3">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R" urn="urn:adsk.eagle:footprint:25699/1" library_version="3">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="R01005" urn="urn:adsk.eagle:footprint:25701/1" library_version="3">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="C0402" urn="urn:adsk.eagle:footprint:23121/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0504" urn="urn:adsk.eagle:footprint:23122/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603" urn="urn:adsk.eagle:footprint:23123/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805" urn="urn:adsk.eagle:footprint:23124/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206" urn="urn:adsk.eagle:footprint:23125/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210" urn="urn:adsk.eagle:footprint:23126/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310" urn="urn:adsk.eagle:footprint:23127/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608" urn="urn:adsk.eagle:footprint:23128/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812" urn="urn:adsk.eagle:footprint:23129/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825" urn="urn:adsk.eagle:footprint:23130/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012" urn="urn:adsk.eagle:footprint:23131/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216" urn="urn:adsk.eagle:footprint:23132/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225" urn="urn:adsk.eagle:footprint:23133/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532" urn="urn:adsk.eagle:footprint:23134/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564" urn="urn:adsk.eagle:footprint:23135/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-024X044" urn="urn:adsk.eagle:footprint:23136/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050" urn="urn:adsk.eagle:footprint:23137/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050" urn="urn:adsk.eagle:footprint:23138/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050" urn="urn:adsk.eagle:footprint:23139/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-050X050" urn="urn:adsk.eagle:footprint:23140/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050" urn="urn:adsk.eagle:footprint:23141/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070" urn="urn:adsk.eagle:footprint:23142/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075" urn="urn:adsk.eagle:footprint:23143/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075" urn="urn:adsk.eagle:footprint:23144/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075" urn="urn:adsk.eagle:footprint:23145/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075" urn="urn:adsk.eagle:footprint:23146/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044" urn="urn:adsk.eagle:footprint:23147/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-025X075" urn="urn:adsk.eagle:footprint:23148/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075" urn="urn:adsk.eagle:footprint:23149/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075" urn="urn:adsk.eagle:footprint:23150/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075" urn="urn:adsk.eagle:footprint:23151/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075" urn="urn:adsk.eagle:footprint:23152/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-075X075" urn="urn:adsk.eagle:footprint:23153/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075" urn="urn:adsk.eagle:footprint:23154/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103" urn="urn:adsk.eagle:footprint:23155/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103" urn="urn:adsk.eagle:footprint:23156/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106" urn="urn:adsk.eagle:footprint:23157/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133" urn="urn:adsk.eagle:footprint:23158/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133" urn="urn:adsk.eagle:footprint:23159/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133" urn="urn:adsk.eagle:footprint:23160/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184" urn="urn:adsk.eagle:footprint:23161/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183" urn="urn:adsk.eagle:footprint:23162/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183" urn="urn:adsk.eagle:footprint:23163/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183" urn="urn:adsk.eagle:footprint:23164/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183" urn="urn:adsk.eagle:footprint:23165/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182" urn="urn:adsk.eagle:footprint:23166/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268" urn="urn:adsk.eagle:footprint:23167/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268" urn="urn:adsk.eagle:footprint:23168/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268" urn="urn:adsk.eagle:footprint:23169/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268" urn="urn:adsk.eagle:footprint:23170/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268" urn="urn:adsk.eagle:footprint:23171/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316" urn="urn:adsk.eagle:footprint:23172/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316" urn="urn:adsk.eagle:footprint:23173/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316" urn="urn:adsk.eagle:footprint:23174/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316" urn="urn:adsk.eagle:footprint:23175/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374" urn="urn:adsk.eagle:footprint:23176/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374" urn="urn:adsk.eagle:footprint:23177/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374" urn="urn:adsk.eagle:footprint:23178/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418" urn="urn:adsk.eagle:footprint:23179/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418" urn="urn:adsk.eagle:footprint:23180/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075" urn="urn:adsk.eagle:footprint:23181/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418" urn="urn:adsk.eagle:footprint:23182/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106" urn="urn:adsk.eagle:footprint:23183/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316" urn="urn:adsk.eagle:footprint:23184/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316" urn="urn:adsk.eagle:footprint:23185/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K" urn="urn:adsk.eagle:footprint:23186/1" library_version="3">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K" urn="urn:adsk.eagle:footprint:23187/1" library_version="3">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K" urn="urn:adsk.eagle:footprint:23188/1" library_version="3">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K" urn="urn:adsk.eagle:footprint:23189/1" library_version="3">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K" urn="urn:adsk.eagle:footprint:23190/1" library_version="3">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K" urn="urn:adsk.eagle:footprint:23191/1" library_version="3">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K" urn="urn:adsk.eagle:footprint:23192/1" library_version="3">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K" urn="urn:adsk.eagle:footprint:23193/1" library_version="3">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K" urn="urn:adsk.eagle:footprint:23194/1" library_version="3">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="HPC0201" urn="urn:adsk.eagle:footprint:25783/1" library_version="3">
<description>&lt;b&gt; &lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<smd name="1" x="-0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<smd name="2" x="0.18" y="0" dx="0.2" dy="0.35" layer="1"/>
<text x="-0.75" y="0.74" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.785" y="-1.865" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.305" y1="-0.15" x2="0.305" y2="0.15" layer="51"/>
</package>
<package name="C0201" urn="urn:adsk.eagle:footprint:23196/1" library_version="3">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808" urn="urn:adsk.eagle:footprint:23197/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640" urn="urn:adsk.eagle:footprint:23198/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="C01005" urn="urn:adsk.eagle:footprint:23199/1" library_version="3">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="R0402" urn="urn:adsk.eagle:package:23547/3" type="model" library_version="3">
<description>Chip RESISTOR 0402 EIA (1005 Metric)</description>
<packageinstances>
<packageinstance name="R0402"/>
</packageinstances>
</package3d>
<package3d name="R0603" urn="urn:adsk.eagle:package:23555/3" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0603"/>
</packageinstances>
</package3d>
<package3d name="R0805" urn="urn:adsk.eagle:package:23553/2" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R0805"/>
</packageinstances>
</package3d>
<package3d name="R0805W" urn="urn:adsk.eagle:package:23537/2" type="model" library_version="3">
<description>RESISTOR wave soldering</description>
<packageinstances>
<packageinstance name="R0805W"/>
</packageinstances>
</package3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:23540/2" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R1206W" urn="urn:adsk.eagle:package:23539/2" type="model" library_version="3">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1206W"/>
</packageinstances>
</package3d>
<package3d name="R1210" urn="urn:adsk.eagle:package:23554/2" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1210"/>
</packageinstances>
</package3d>
<package3d name="R1210W" urn="urn:adsk.eagle:package:23541/2" type="model" library_version="3">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R1210W"/>
</packageinstances>
</package3d>
<package3d name="R2010" urn="urn:adsk.eagle:package:23551/2" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2010"/>
</packageinstances>
</package3d>
<package3d name="R2010W" urn="urn:adsk.eagle:package:23542/2" type="model" library_version="3">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2010W"/>
</packageinstances>
</package3d>
<package3d name="R2012" urn="urn:adsk.eagle:package:23543/2" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2012"/>
</packageinstances>
</package3d>
<package3d name="R2012W" urn="urn:adsk.eagle:package:23544/2" type="model" library_version="3">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2012W"/>
</packageinstances>
</package3d>
<package3d name="R2512" urn="urn:adsk.eagle:package:23545/2" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R2512"/>
</packageinstances>
</package3d>
<package3d name="R2512W" urn="urn:adsk.eagle:package:23565/2" type="model" library_version="3">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R2512W"/>
</packageinstances>
</package3d>
<package3d name="R3216" urn="urn:adsk.eagle:package:23557/2" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3216"/>
</packageinstances>
</package3d>
<package3d name="R3216W" urn="urn:adsk.eagle:package:23548/2" type="model" library_version="3">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3216W"/>
</packageinstances>
</package3d>
<package3d name="R3225" urn="urn:adsk.eagle:package:23549/2" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R3225"/>
</packageinstances>
</package3d>
<package3d name="R3225W" urn="urn:adsk.eagle:package:23550/2" type="model" library_version="3">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R3225W"/>
</packageinstances>
</package3d>
<package3d name="R5025" urn="urn:adsk.eagle:package:23552/2" type="model" library_version="3">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R5025"/>
</packageinstances>
</package3d>
<package3d name="R5025W" urn="urn:adsk.eagle:package:23558/2" type="model" library_version="3">
<description>RESISTOR
wave soldering</description>
<packageinstances>
<packageinstance name="R5025W"/>
</packageinstances>
</package3d>
<package3d name="R6332" urn="urn:adsk.eagle:package:23559/2" type="model" library_version="3">
<description>RESISTOR
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332"/>
</packageinstances>
</package3d>
<package3d name="R6332W" urn="urn:adsk.eagle:package:26078/2" type="model" library_version="3">
<description>RESISTOR wave soldering
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<packageinstances>
<packageinstance name="R6332W"/>
</packageinstances>
</package3d>
<package3d name="M0805" urn="urn:adsk.eagle:package:23556/2" type="model" library_version="3">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M0805"/>
</packageinstances>
</package3d>
<package3d name="M1206" urn="urn:adsk.eagle:package:23566/2" type="model" library_version="3">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M1206"/>
</packageinstances>
</package3d>
<package3d name="M1406" urn="urn:adsk.eagle:package:23569/2" type="model" library_version="3">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M1406"/>
</packageinstances>
</package3d>
<package3d name="M2012" urn="urn:adsk.eagle:package:23561/2" type="model" library_version="3">
<description>RESISTOR
MELF 0.10 W</description>
<packageinstances>
<packageinstance name="M2012"/>
</packageinstances>
</package3d>
<package3d name="M2309" urn="urn:adsk.eagle:package:23562/2" type="model" library_version="3">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M2309"/>
</packageinstances>
</package3d>
<package3d name="M3216" urn="urn:adsk.eagle:package:23563/2" type="model" library_version="3">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M3216"/>
</packageinstances>
</package3d>
<package3d name="M3516" urn="urn:adsk.eagle:package:23573/2" type="model" library_version="3">
<description>RESISTOR
MELF 0.12 W</description>
<packageinstances>
<packageinstance name="M3516"/>
</packageinstances>
</package3d>
<package3d name="M5923" urn="urn:adsk.eagle:package:23564/3" type="model" library_version="3">
<description>RESISTOR
MELF 0.25 W</description>
<packageinstances>
<packageinstance name="M5923"/>
</packageinstances>
</package3d>
<package3d name="0204/5" urn="urn:adsk.eagle:package:23488/1" type="box" library_version="3">
<description>RESISTOR
type 0204, grid 5 mm</description>
<packageinstances>
<packageinstance name="0204/5"/>
</packageinstances>
</package3d>
<package3d name="0204/7" urn="urn:adsk.eagle:package:23498/2" type="model" library_version="3">
<description>RESISTOR
type 0204, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0204/7"/>
</packageinstances>
</package3d>
<package3d name="0207/10" urn="urn:adsk.eagle:package:23491/2" type="model" library_version="3">
<description>RESISTOR
type 0207, grid 10 mm</description>
<packageinstances>
<packageinstance name="0207/10"/>
</packageinstances>
</package3d>
<package3d name="0207/12" urn="urn:adsk.eagle:package:23489/1" type="box" library_version="3">
<description>RESISTOR
type 0207, grid 12 mm</description>
<packageinstances>
<packageinstance name="0207/12"/>
</packageinstances>
</package3d>
<package3d name="0207/15" urn="urn:adsk.eagle:package:23492/1" type="box" library_version="3">
<description>RESISTOR
type 0207, grid 15mm</description>
<packageinstances>
<packageinstance name="0207/15"/>
</packageinstances>
</package3d>
<package3d name="0207/2V" urn="urn:adsk.eagle:package:23490/1" type="box" library_version="3">
<description>RESISTOR
type 0207, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0207/2V"/>
</packageinstances>
</package3d>
<package3d name="0207/5V" urn="urn:adsk.eagle:package:23502/1" type="box" library_version="3">
<description>RESISTOR
type 0207, grid 5 mm</description>
<packageinstances>
<packageinstance name="0207/5V"/>
</packageinstances>
</package3d>
<package3d name="0207/7" urn="urn:adsk.eagle:package:23493/2" type="model" library_version="3">
<description>RESISTOR
type 0207, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0207/7"/>
</packageinstances>
</package3d>
<package3d name="0309/10" urn="urn:adsk.eagle:package:23567/2" type="model" library_version="3">
<description>RESISTOR
type 0309, grid 10mm</description>
<packageinstances>
<packageinstance name="0309/10"/>
</packageinstances>
</package3d>
<package3d name="0309/12" urn="urn:adsk.eagle:package:23571/1" type="box" library_version="3">
<description>RESISTOR
type 0309, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0309/12"/>
</packageinstances>
</package3d>
<package3d name="0411/12" urn="urn:adsk.eagle:package:23578/1" type="box" library_version="3">
<description>RESISTOR
type 0411, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="0411/12"/>
</packageinstances>
</package3d>
<package3d name="0411/15" urn="urn:adsk.eagle:package:23568/2" type="model" library_version="3">
<description>RESISTOR
type 0411, grid 15 mm</description>
<packageinstances>
<packageinstance name="0411/15"/>
</packageinstances>
</package3d>
<package3d name="0411V" urn="urn:adsk.eagle:package:23570/1" type="box" library_version="3">
<description>RESISTOR
type 0411, grid 3.81 mm</description>
<packageinstances>
<packageinstance name="0411V"/>
</packageinstances>
</package3d>
<package3d name="0414/15" urn="urn:adsk.eagle:package:23579/2" type="model" library_version="3">
<description>RESISTOR
type 0414, grid 15 mm</description>
<packageinstances>
<packageinstance name="0414/15"/>
</packageinstances>
</package3d>
<package3d name="0414V" urn="urn:adsk.eagle:package:23574/1" type="box" library_version="3">
<description>RESISTOR
type 0414, grid 5 mm</description>
<packageinstances>
<packageinstance name="0414V"/>
</packageinstances>
</package3d>
<package3d name="0617/17" urn="urn:adsk.eagle:package:23575/2" type="model" library_version="3">
<description>RESISTOR
type 0617, grid 17.5 mm</description>
<packageinstances>
<packageinstance name="0617/17"/>
</packageinstances>
</package3d>
<package3d name="0617/22" urn="urn:adsk.eagle:package:23577/1" type="box" library_version="3">
<description>RESISTOR
type 0617, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0617/22"/>
</packageinstances>
</package3d>
<package3d name="0617V" urn="urn:adsk.eagle:package:23576/1" type="box" library_version="3">
<description>RESISTOR
type 0617, grid 5 mm</description>
<packageinstances>
<packageinstance name="0617V"/>
</packageinstances>
</package3d>
<package3d name="0922/22" urn="urn:adsk.eagle:package:23580/2" type="model" library_version="3">
<description>RESISTOR
type 0922, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="0922/22"/>
</packageinstances>
</package3d>
<package3d name="P0613V" urn="urn:adsk.eagle:package:23582/1" type="box" library_version="3">
<description>RESISTOR
type 0613, grid 5 mm</description>
<packageinstances>
<packageinstance name="P0613V"/>
</packageinstances>
</package3d>
<package3d name="P0613/15" urn="urn:adsk.eagle:package:23581/2" type="model" library_version="3">
<description>RESISTOR
type 0613, grid 15 mm</description>
<packageinstances>
<packageinstance name="P0613/15"/>
</packageinstances>
</package3d>
<package3d name="P0817/22" urn="urn:adsk.eagle:package:23583/1" type="box" library_version="3">
<description>RESISTOR
type 0817, grid 22.5 mm</description>
<packageinstances>
<packageinstance name="P0817/22"/>
</packageinstances>
</package3d>
<package3d name="P0817V" urn="urn:adsk.eagle:package:23608/1" type="box" library_version="3">
<description>RESISTOR
type 0817, grid 6.35 mm</description>
<packageinstances>
<packageinstance name="P0817V"/>
</packageinstances>
</package3d>
<package3d name="V234/12" urn="urn:adsk.eagle:package:23592/1" type="box" library_version="3">
<description>RESISTOR
type V234, grid 12.5 mm</description>
<packageinstances>
<packageinstance name="V234/12"/>
</packageinstances>
</package3d>
<package3d name="V235/17" urn="urn:adsk.eagle:package:23586/2" type="model" library_version="3">
<description>RESISTOR
type V235, grid 17.78 mm</description>
<packageinstances>
<packageinstance name="V235/17"/>
</packageinstances>
</package3d>
<package3d name="V526-0" urn="urn:adsk.eagle:package:23590/1" type="box" library_version="3">
<description>RESISTOR
type V526-0, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="V526-0"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102AX" urn="urn:adsk.eagle:package:23594/1" type="box" library_version="3">
<description>Mini MELF 0102 Axial</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102AX"/>
</packageinstances>
</package3d>
<package3d name="0922V" urn="urn:adsk.eagle:package:23589/1" type="box" library_version="3">
<description>RESISTOR
type 0922, grid 7.5 mm</description>
<packageinstances>
<packageinstance name="0922V"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102R" urn="urn:adsk.eagle:package:23591/2" type="model" library_version="3">
<description>CECC Size RC2211 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0102W" urn="urn:adsk.eagle:package:23588/2" type="model" library_version="3">
<description>CECC Size RC2211 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0102W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204R" urn="urn:adsk.eagle:package:26109/2" type="model" library_version="3">
<description>CECC Size RC3715 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0204W" urn="urn:adsk.eagle:package:26111/2" type="model" library_version="3">
<description>CECC Size RC3715 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0204W"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207R" urn="urn:adsk.eagle:package:26113/2" type="model" library_version="3">
<description>CECC Size RC6123 Reflow Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207R"/>
</packageinstances>
</package3d>
<package3d name="MINI_MELF-0207W" urn="urn:adsk.eagle:package:26112/2" type="model" library_version="3">
<description>CECC Size RC6123 Wave Soldering
source Beyschlag</description>
<packageinstances>
<packageinstance name="MINI_MELF-0207W"/>
</packageinstances>
</package3d>
<package3d name="RDH/15" urn="urn:adsk.eagle:package:23595/1" type="box" library_version="3">
<description>RESISTOR
type RDH, grid 15 mm</description>
<packageinstances>
<packageinstance name="RDH/15"/>
</packageinstances>
</package3d>
<package3d name="0204V" urn="urn:adsk.eagle:package:23495/1" type="box" library_version="3">
<description>RESISTOR
type 0204, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0204V"/>
</packageinstances>
</package3d>
<package3d name="0309V" urn="urn:adsk.eagle:package:23572/1" type="box" library_version="3">
<description>RESISTOR
type 0309, grid 2.5 mm</description>
<packageinstances>
<packageinstance name="0309V"/>
</packageinstances>
</package3d>
<package3d name="R0201" urn="urn:adsk.eagle:package:26117/2" type="model" library_version="3">
<description>RESISTOR chip
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R0201"/>
</packageinstances>
</package3d>
<package3d name="VMTA55" urn="urn:adsk.eagle:package:26121/2" type="model" library_version="3">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTA55"/>
</packageinstances>
</package3d>
<package3d name="VMTB60" urn="urn:adsk.eagle:package:26122/2" type="model" library_version="3">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RNC60
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VMTB60"/>
</packageinstances>
</package3d>
<package3d name="VTA52" urn="urn:adsk.eagle:package:26116/2" type="model" library_version="3">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR52
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA52"/>
</packageinstances>
</package3d>
<package3d name="VTA53" urn="urn:adsk.eagle:package:26118/2" type="model" library_version="3">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR53
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA53"/>
</packageinstances>
</package3d>
<package3d name="VTA54" urn="urn:adsk.eagle:package:26119/2" type="model" library_version="3">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR54
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA54"/>
</packageinstances>
</package3d>
<package3d name="VTA55" urn="urn:adsk.eagle:package:26120/2" type="model" library_version="3">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR55
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA55"/>
</packageinstances>
</package3d>
<package3d name="VTA56" urn="urn:adsk.eagle:package:26129/3" type="model" library_version="3">
<description>Bulk Metal® Foil Technology, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements
MIL SIZE RBR56
Source: VISHAY .. vta56.pdf</description>
<packageinstances>
<packageinstance name="VTA56"/>
</packageinstances>
</package3d>
<package3d name="R4527" urn="urn:adsk.eagle:package:13310/2" type="model" library_version="3">
<description>Package 4527
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<packageinstances>
<packageinstance name="R4527"/>
</packageinstances>
</package3d>
<package3d name="WSC0001" urn="urn:adsk.eagle:package:26123/2" type="model" library_version="3">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0001"/>
</packageinstances>
</package3d>
<package3d name="WSC0002" urn="urn:adsk.eagle:package:26125/2" type="model" library_version="3">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC0002"/>
</packageinstances>
</package3d>
<package3d name="WSC01/2" urn="urn:adsk.eagle:package:26127/2" type="model" library_version="3">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC01/2"/>
</packageinstances>
</package3d>
<package3d name="WSC2515" urn="urn:adsk.eagle:package:26134/2" type="model" library_version="3">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC2515"/>
</packageinstances>
</package3d>
<package3d name="WSC4527" urn="urn:adsk.eagle:package:26126/2" type="model" library_version="3">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC4527"/>
</packageinstances>
</package3d>
<package3d name="WSC6927" urn="urn:adsk.eagle:package:26128/2" type="model" library_version="3">
<description>Wirewound Resistors, Precision Power
Source: VISHAY wscwsn.pdf</description>
<packageinstances>
<packageinstance name="WSC6927"/>
</packageinstances>
</package3d>
<package3d name="R1218" urn="urn:adsk.eagle:package:26131/2" type="model" library_version="3">
<description>CRCW1218 Thick Film, Rectangular Chip Resistors
Source: http://www.vishay.com .. dcrcw.pdf</description>
<packageinstances>
<packageinstance name="R1218"/>
</packageinstances>
</package3d>
<package3d name="1812X7R" urn="urn:adsk.eagle:package:26130/2" type="model" library_version="3">
<description>Chip Monolithic Ceramic Capacitors Medium Voltage High Capacitance for General Use
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<packageinstances>
<packageinstance name="1812X7R"/>
</packageinstances>
</package3d>
<package3d name="R01005" urn="urn:adsk.eagle:package:26133/2" type="model" library_version="3">
<description>Chip, 0.40 X 0.20 X 0.16 mm body
&lt;p&gt;Chip package with body size 0.40 X 0.20 X 0.16 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="R01005"/>
</packageinstances>
</package3d>
<package3d name="CAPC1005X60" urn="urn:adsk.eagle:package:23626/2" type="model" library_version="3">
<description>Chip, 1.00 X 0.50 X 0.60 mm body
&lt;p&gt;Chip package with body size 1.00 X 0.50 X 0.60 mm&lt;/p&gt;</description>
<packageinstances>
<packageinstance name="C0402"/>
</packageinstances>
</package3d>
<package3d name="C0504" urn="urn:adsk.eagle:package:23624/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0504"/>
</packageinstances>
</package3d>
<package3d name="C0603" urn="urn:adsk.eagle:package:23616/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0603"/>
</packageinstances>
</package3d>
<package3d name="C0805" urn="urn:adsk.eagle:package:23617/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C0805"/>
</packageinstances>
</package3d>
<package3d name="C1206" urn="urn:adsk.eagle:package:23618/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1206"/>
</packageinstances>
</package3d>
<package3d name="C1210" urn="urn:adsk.eagle:package:23619/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1210"/>
</packageinstances>
</package3d>
<package3d name="C1310" urn="urn:adsk.eagle:package:23620/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1310"/>
</packageinstances>
</package3d>
<package3d name="C1608" urn="urn:adsk.eagle:package:23621/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1608"/>
</packageinstances>
</package3d>
<package3d name="C1812" urn="urn:adsk.eagle:package:23622/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1812"/>
</packageinstances>
</package3d>
<package3d name="C1825" urn="urn:adsk.eagle:package:23623/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1825"/>
</packageinstances>
</package3d>
<package3d name="C2012" urn="urn:adsk.eagle:package:23625/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C2012"/>
</packageinstances>
</package3d>
<package3d name="C3216" urn="urn:adsk.eagle:package:23628/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C3216"/>
</packageinstances>
</package3d>
<package3d name="C3225" urn="urn:adsk.eagle:package:23655/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C3225"/>
</packageinstances>
</package3d>
<package3d name="C4532" urn="urn:adsk.eagle:package:23627/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C4532"/>
</packageinstances>
</package3d>
<package3d name="C4564" urn="urn:adsk.eagle:package:23648/2" type="model" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C4564"/>
</packageinstances>
</package3d>
<package3d name="C025-024X044" urn="urn:adsk.eagle:package:23630/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<packageinstances>
<packageinstance name="C025-024X044"/>
</packageinstances>
</package3d>
<package3d name="C025-025X050" urn="urn:adsk.eagle:package:23629/2" type="model" library_version="3">
<description>CAPACITOR
grid 2.5 mm, outline 2.5 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-025X050"/>
</packageinstances>
</package3d>
<package3d name="C025-030X050" urn="urn:adsk.eagle:package:23631/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 mm, outline 3 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-030X050"/>
</packageinstances>
</package3d>
<package3d name="C025-040X050" urn="urn:adsk.eagle:package:23634/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 mm, outline 4 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-040X050"/>
</packageinstances>
</package3d>
<package3d name="C025-050X050" urn="urn:adsk.eagle:package:23633/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 mm, outline 5 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-050X050"/>
</packageinstances>
</package3d>
<package3d name="C025-060X050" urn="urn:adsk.eagle:package:23632/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 mm, outline 6 x 5 mm</description>
<packageinstances>
<packageinstance name="C025-060X050"/>
</packageinstances>
</package3d>
<package3d name="C025_050-024X070" urn="urn:adsk.eagle:package:23639/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<packageinstances>
<packageinstance name="C025_050-024X070"/>
</packageinstances>
</package3d>
<package3d name="C025_050-025X075" urn="urn:adsk.eagle:package:23641/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-025X075"/>
</packageinstances>
</package3d>
<package3d name="C025_050-035X075" urn="urn:adsk.eagle:package:23651/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-035X075"/>
</packageinstances>
</package3d>
<package3d name="C025_050-045X075" urn="urn:adsk.eagle:package:23635/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-045X075"/>
</packageinstances>
</package3d>
<package3d name="C025_050-055X075" urn="urn:adsk.eagle:package:23636/1" type="box" library_version="3">
<description>CAPACITOR
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C025_050-055X075"/>
</packageinstances>
</package3d>
<package3d name="C050-024X044" urn="urn:adsk.eagle:package:23643/1" type="box" library_version="3">
<description>CAPACITOR
grid 5 mm, outline 2.4 x 4.4 mm</description>
<packageinstances>
<packageinstance name="C050-024X044"/>
</packageinstances>
</package3d>
<package3d name="C050-025X075" urn="urn:adsk.eagle:package:23637/1" type="box" library_version="3">
<description>CAPACITOR
grid 5 mm, outline 2.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-025X075"/>
</packageinstances>
</package3d>
<package3d name="C050-045X075" urn="urn:adsk.eagle:package:23638/1" type="box" library_version="3">
<description>CAPACITOR
grid 5 mm, outline 4.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-045X075"/>
</packageinstances>
</package3d>
<package3d name="C050-030X075" urn="urn:adsk.eagle:package:23640/1" type="box" library_version="3">
<description>CAPACITOR
grid 5 mm, outline 3 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-030X075"/>
</packageinstances>
</package3d>
<package3d name="C050-050X075" urn="urn:adsk.eagle:package:23665/1" type="box" library_version="3">
<description>CAPACITOR
grid 5 mm, outline 5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-050X075"/>
</packageinstances>
</package3d>
<package3d name="C050-055X075" urn="urn:adsk.eagle:package:23642/1" type="box" library_version="3">
<description>CAPACITOR
grid 5 mm, outline 5.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-055X075"/>
</packageinstances>
</package3d>
<package3d name="C050-075X075" urn="urn:adsk.eagle:package:23645/1" type="box" library_version="3">
<description>CAPACITOR
grid 5 mm, outline 7.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-075X075"/>
</packageinstances>
</package3d>
<package3d name="C050H075X075" urn="urn:adsk.eagle:package:23644/1" type="box" library_version="3">
<description>CAPACITOR
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050H075X075"/>
</packageinstances>
</package3d>
<package3d name="C075-032X103" urn="urn:adsk.eagle:package:23646/1" type="box" library_version="3">
<description>CAPACITOR
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<packageinstances>
<packageinstance name="C075-032X103"/>
</packageinstances>
</package3d>
<package3d name="C075-042X103" urn="urn:adsk.eagle:package:23656/1" type="box" library_version="3">
<description>CAPACITOR
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<packageinstances>
<packageinstance name="C075-042X103"/>
</packageinstances>
</package3d>
<package3d name="C075-052X106" urn="urn:adsk.eagle:package:23650/1" type="box" library_version="3">
<description>CAPACITOR
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<packageinstances>
<packageinstance name="C075-052X106"/>
</packageinstances>
</package3d>
<package3d name="C102-043X133" urn="urn:adsk.eagle:package:23647/1" type="box" library_version="3">
<description>CAPACITOR
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<packageinstances>
<packageinstance name="C102-043X133"/>
</packageinstances>
</package3d>
<package3d name="C102-054X133" urn="urn:adsk.eagle:package:23649/1" type="box" library_version="3">
<description>CAPACITOR
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<packageinstances>
<packageinstance name="C102-054X133"/>
</packageinstances>
</package3d>
<package3d name="C102-064X133" urn="urn:adsk.eagle:package:23653/1" type="box" library_version="3">
<description>CAPACITOR
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<packageinstances>
<packageinstance name="C102-064X133"/>
</packageinstances>
</package3d>
<package3d name="C102_152-062X184" urn="urn:adsk.eagle:package:23652/1" type="box" library_version="3">
<description>CAPACITOR
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<packageinstances>
<packageinstance name="C102_152-062X184"/>
</packageinstances>
</package3d>
<package3d name="C150-054X183" urn="urn:adsk.eagle:package:23669/1" type="box" library_version="3">
<description>CAPACITOR
grid 15 mm, outline 5.4 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-054X183"/>
</packageinstances>
</package3d>
<package3d name="C150-064X183" urn="urn:adsk.eagle:package:23654/1" type="box" library_version="3">
<description>CAPACITOR
grid 15 mm, outline 6.4 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-064X183"/>
</packageinstances>
</package3d>
<package3d name="C150-072X183" urn="urn:adsk.eagle:package:23657/1" type="box" library_version="3">
<description>CAPACITOR
grid 15 mm, outline 7.2 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-072X183"/>
</packageinstances>
</package3d>
<package3d name="C150-084X183" urn="urn:adsk.eagle:package:23658/1" type="box" library_version="3">
<description>CAPACITOR
grid 15 mm, outline 8.4 x 18.3 mm</description>
<packageinstances>
<packageinstance name="C150-084X183"/>
</packageinstances>
</package3d>
<package3d name="C150-091X182" urn="urn:adsk.eagle:package:23659/1" type="box" library_version="3">
<description>CAPACITOR
grid 15 mm, outline 9.1 x 18.2 mm</description>
<packageinstances>
<packageinstance name="C150-091X182"/>
</packageinstances>
</package3d>
<package3d name="C225-062X268" urn="urn:adsk.eagle:package:23661/1" type="box" library_version="3">
<description>CAPACITOR
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-062X268"/>
</packageinstances>
</package3d>
<package3d name="C225-074X268" urn="urn:adsk.eagle:package:23660/1" type="box" library_version="3">
<description>CAPACITOR
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-074X268"/>
</packageinstances>
</package3d>
<package3d name="C225-087X268" urn="urn:adsk.eagle:package:23662/1" type="box" library_version="3">
<description>CAPACITOR
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-087X268"/>
</packageinstances>
</package3d>
<package3d name="C225-108X268" urn="urn:adsk.eagle:package:23663/1" type="box" library_version="3">
<description>CAPACITOR
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-108X268"/>
</packageinstances>
</package3d>
<package3d name="C225-113X268" urn="urn:adsk.eagle:package:23667/1" type="box" library_version="3">
<description>CAPACITOR
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<packageinstances>
<packageinstance name="C225-113X268"/>
</packageinstances>
</package3d>
<package3d name="C275-093X316" urn="urn:adsk.eagle:package:23701/1" type="box" library_version="3">
<description>CAPACITOR
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-093X316"/>
</packageinstances>
</package3d>
<package3d name="C275-113X316" urn="urn:adsk.eagle:package:23673/1" type="box" library_version="3">
<description>CAPACITOR
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-113X316"/>
</packageinstances>
</package3d>
<package3d name="C275-134X316" urn="urn:adsk.eagle:package:23664/1" type="box" library_version="3">
<description>CAPACITOR
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-134X316"/>
</packageinstances>
</package3d>
<package3d name="C275-205X316" urn="urn:adsk.eagle:package:23666/1" type="box" library_version="3">
<description>CAPACITOR
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-205X316"/>
</packageinstances>
</package3d>
<package3d name="C325-137X374" urn="urn:adsk.eagle:package:23672/1" type="box" library_version="3">
<description>CAPACITOR
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<packageinstances>
<packageinstance name="C325-137X374"/>
</packageinstances>
</package3d>
<package3d name="C325-162X374" urn="urn:adsk.eagle:package:23670/1" type="box" library_version="3">
<description>CAPACITOR
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<packageinstances>
<packageinstance name="C325-162X374"/>
</packageinstances>
</package3d>
<package3d name="C325-182X374" urn="urn:adsk.eagle:package:23668/1" type="box" library_version="3">
<description>CAPACITOR
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<packageinstances>
<packageinstance name="C325-182X374"/>
</packageinstances>
</package3d>
<package3d name="C375-192X418" urn="urn:adsk.eagle:package:23674/1" type="box" library_version="3">
<description>CAPACITOR
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<packageinstances>
<packageinstance name="C375-192X418"/>
</packageinstances>
</package3d>
<package3d name="C375-203X418" urn="urn:adsk.eagle:package:23671/1" type="box" library_version="3">
<description>CAPACITOR
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<packageinstances>
<packageinstance name="C375-203X418"/>
</packageinstances>
</package3d>
<package3d name="C050-035X075" urn="urn:adsk.eagle:package:23677/1" type="box" library_version="3">
<description>CAPACITOR
grid 5 mm, outline 3.5 x 7.5 mm</description>
<packageinstances>
<packageinstance name="C050-035X075"/>
</packageinstances>
</package3d>
<package3d name="C375-155X418" urn="urn:adsk.eagle:package:23675/1" type="box" library_version="3">
<description>CAPACITOR
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<packageinstances>
<packageinstance name="C375-155X418"/>
</packageinstances>
</package3d>
<package3d name="C075-063X106" urn="urn:adsk.eagle:package:23678/1" type="box" library_version="3">
<description>CAPACITOR
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<packageinstances>
<packageinstance name="C075-063X106"/>
</packageinstances>
</package3d>
<package3d name="C275-154X316" urn="urn:adsk.eagle:package:23685/1" type="box" library_version="3">
<description>CAPACITOR
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-154X316"/>
</packageinstances>
</package3d>
<package3d name="C275-173X316" urn="urn:adsk.eagle:package:23676/1" type="box" library_version="3">
<description>CAPACITOR
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<packageinstances>
<packageinstance name="C275-173X316"/>
</packageinstances>
</package3d>
<package3d name="C0402K" urn="urn:adsk.eagle:package:23679/2" type="model" library_version="3">
<description>Ceramic Chip Capacitor KEMET 0204 reflow solder
Metric Code Size 1005</description>
<packageinstances>
<packageinstance name="C0402K"/>
</packageinstances>
</package3d>
<package3d name="C0603K" urn="urn:adsk.eagle:package:23680/2" type="model" library_version="3">
<description>Ceramic Chip Capacitor KEMET 0603 reflow solder
Metric Code Size 1608</description>
<packageinstances>
<packageinstance name="C0603K"/>
</packageinstances>
</package3d>
<package3d name="C0805K" urn="urn:adsk.eagle:package:23681/2" type="model" library_version="3">
<description>Ceramic Chip Capacitor KEMET 0805 reflow solder
Metric Code Size 2012</description>
<packageinstances>
<packageinstance name="C0805K"/>
</packageinstances>
</package3d>
<package3d name="C1206K" urn="urn:adsk.eagle:package:23682/2" type="model" library_version="3">
<description>Ceramic Chip Capacitor KEMET 1206 reflow solder
Metric Code Size 3216</description>
<packageinstances>
<packageinstance name="C1206K"/>
</packageinstances>
</package3d>
<package3d name="C1210K" urn="urn:adsk.eagle:package:23683/2" type="model" library_version="3">
<description>Ceramic Chip Capacitor KEMET 1210 reflow solder
Metric Code Size 3225</description>
<packageinstances>
<packageinstance name="C1210K"/>
</packageinstances>
</package3d>
<package3d name="C1812K" urn="urn:adsk.eagle:package:23686/2" type="model" library_version="3">
<description>Ceramic Chip Capacitor KEMET 1812 reflow solder
Metric Code Size 4532</description>
<packageinstances>
<packageinstance name="C1812K"/>
</packageinstances>
</package3d>
<package3d name="C1825K" urn="urn:adsk.eagle:package:23684/2" type="model" library_version="3">
<description>Ceramic Chip Capacitor KEMET 1825 reflow solder
Metric Code Size 4564</description>
<packageinstances>
<packageinstance name="C1825K"/>
</packageinstances>
</package3d>
<package3d name="C2220K" urn="urn:adsk.eagle:package:23687/2" type="model" library_version="3">
<description>Ceramic Chip Capacitor KEMET 2220 reflow solderMetric Code Size 5650</description>
<packageinstances>
<packageinstance name="C2220K"/>
</packageinstances>
</package3d>
<package3d name="C2225K" urn="urn:adsk.eagle:package:23692/2" type="model" library_version="3">
<description>Ceramic Chip Capacitor KEMET 2225 reflow solderMetric Code Size 5664</description>
<packageinstances>
<packageinstance name="C2225K"/>
</packageinstances>
</package3d>
<package3d name="HPC0201" urn="urn:adsk.eagle:package:26213/1" type="box" library_version="3">
<description> 
Source: http://www.vishay.com/docs/10129/hpc0201a.pdf</description>
<packageinstances>
<packageinstance name="HPC0201"/>
</packageinstances>
</package3d>
<package3d name="C0201" urn="urn:adsk.eagle:package:23690/2" type="model" library_version="3">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<packageinstances>
<packageinstance name="C0201"/>
</packageinstances>
</package3d>
<package3d name="C1808" urn="urn:adsk.eagle:package:23689/2" type="model" library_version="3">
<description>CAPACITOR
Source: AVX .. aphvc.pdf</description>
<packageinstances>
<packageinstance name="C1808"/>
</packageinstances>
</package3d>
<package3d name="C3640" urn="urn:adsk.eagle:package:23693/2" type="model" library_version="3">
<description>CAPACITOR
Source: AVX .. aphvc.pdf</description>
<packageinstances>
<packageinstance name="C3640"/>
</packageinstances>
</package3d>
<package3d name="C01005" urn="urn:adsk.eagle:package:23691/1" type="box" library_version="3">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C01005"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R-US" urn="urn:adsk.eagle:symbol:23200/1" library_version="3">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="C-EU" urn="urn:adsk.eagle:symbol:23120/1" library_version="3">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-US_" urn="urn:adsk.eagle:component:23792/22" prefix="R" uservalue="yes" library_version="3">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23547/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="34" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23555/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="77" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23553/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="85" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23537/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23540/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="19" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23539/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23554/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23541/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23551/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23542/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23543/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23544/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23545/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23565/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23557/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23548/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23549/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23550/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23552/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23558/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23559/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26078/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23556/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="45" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23566/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="22" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23569/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23561/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23562/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23563/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23573/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23564/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23488/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="18" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23498/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="48" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23491/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="36" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23489/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23492/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23490/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23502/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23493/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="22" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23567/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23571/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="8" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23578/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23568/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23570/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23579/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23574/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23575/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23577/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23576/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23580/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23582/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23581/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23583/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23608/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23592/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23586/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23590/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23594/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23589/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23591/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23588/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26109/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26111/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26113/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26112/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23595/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23495/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23572/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26117/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26121/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26122/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26116/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26118/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26119/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26120/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26129/3"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:13310/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26123/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26125/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26127/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26134/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26126/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26128/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26131/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26130/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26133/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="R">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
<deviceset name="C-EU" urn="urn:adsk.eagle:component:23793/46" prefix="C" uservalue="yes" library_version="3">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23626/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="18" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23624/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23616/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="73" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23617/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="88" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="54" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23619/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23620/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23621/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23622/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23623/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23625/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23628/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23655/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23627/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23648/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23630/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="56" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23629/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="65" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23631/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="14" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23634/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23633/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="16" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23632/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23639/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23641/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23651/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23635/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23636/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23643/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="33" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23637/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="29" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23638/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23640/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="9" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23665/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23642/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23645/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23644/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23646/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23656/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23650/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="4" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23647/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="1" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23649/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23653/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23652/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23669/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="3" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23654/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23657/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23658/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23659/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23661/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23660/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23662/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23663/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23667/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23701/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23673/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23664/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23666/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23672/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23670/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23668/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23674/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23671/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23677/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="2" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23675/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23678/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23685/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23676/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23679/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="15" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23680/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="30" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23681/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="52" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23682/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="13" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23683/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23686/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23684/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23687/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23692/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="HPC0201" package="HPC0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:26213/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23690/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23689/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23693/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
<device name="01005" package="C01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23691/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="0" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="C">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="SC4" library="Axolotl" deviceset="TRISOLX_SOLAR_WING" device=""/>
<part name="SC2" library="Axolotl" deviceset="TRISOLX_SOLAR_WING" device=""/>
<part name="IC1" library="Axolotl" deviceset="ATMEGA328P" device=""/>
<part name="IC2" library="Axolotl" deviceset="BQ25570" device=""/>
<part name="IC4" library="Axolotl" deviceset="TMP102" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="R1" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="10K"/>
<part name="R2" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="10K"/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C1" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.01uF"/>
<part name="SUPPLY1" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY3" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="IC5" library="Axolotl" deviceset="SI1147" device=""/>
<part name="R3" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="10K"/>
<part name="R4" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="10K"/>
<part name="SUPPLY4" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C2" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="SUPPLY5" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="R5" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="10K"/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="IC6" library="Axolotl" deviceset="LSM9DS1" device=""/>
<part name="C3" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="10uF"/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY6" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY7" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C4" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="10nF"/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C5" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100nF"/>
<part name="C6" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100nF"/>
<part name="R6" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="10K"/>
<part name="R7" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="10K"/>
<part name="P+9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="C7" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="100nF"/>
<part name="SUPPLY9" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY10" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY11" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY12" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY13" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SC3" library="Axolotl" deviceset="TRISOLX_SOLAR_WING" device=""/>
<part name="SC1" library="Axolotl" deviceset="TRISOLX_SOLAR_WING" device=""/>
<part name="FRAME2" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
<part name="SUPPLY14" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="B1" library="Axolotl" deviceset="DPT301120" device=""/>
<part name="SUPPLY15" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C10" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="4.7uF"/>
<part name="C11" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="0.1uF"/>
<part name="SUPPLY16" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="R8" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="7.32M"/>
<part name="R9" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="5.49M"/>
<part name="R10" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="3.24M"/>
<part name="R11" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="5.76M"/>
<part name="R12" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="3.92M"/>
<part name="R13" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="8M"/>
<part name="R14" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="R-US_" device="R0402" package3d_urn="urn:adsk.eagle:package:23547/3" value="4.75M"/>
<part name="SUPPLY17" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY18" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C12" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="22uF"/>
<part name="SUPPLY19" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C13" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="10nF"/>
<part name="SUPPLY20" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="C14" library="rcl" library_urn="urn:adsk.eagle:library:334" deviceset="C-EU" device="C0402" package3d_urn="urn:adsk.eagle:package:23626/2" value="4.7uF"/>
<part name="SUPPLY21" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="P+12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY22" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="L1" library="Axolotl" deviceset="L0805C220MDWIT" device="" value="22uH"/>
<part name="L2" library="Axolotl" deviceset="L0603B100MPWFT" device="" value="10uH"/>
<part name="IC3" library="Axolotl" deviceset="RFM95W" device=""/>
<part name="P+13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="SUPPLY23" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="XTAL1" library="Axolotl" deviceset="CSTCR4M00G53U-R0" device=""/>
<part name="ANT1" library="Axolotl" deviceset="V-DIPOLE_433MHZ" device=""/>
<part name="SUPPLY24" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="U$1" library="Axolotl" deviceset="AXOLOTL_LOGO" device=""/>
<part name="U$2" library="Axolotl" deviceset="AXOLOTL_LOGO" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-111.76" y="73.66" size="5.08" layer="94">Temperature Sensor</text>
<wire x1="-125.476" y1="10.16" x2="-38.1" y2="10.16" width="0.508" layer="94"/>
<wire x1="-38.1" y1="10.16" x2="-38.1" y2="88.9" width="0.508" layer="94"/>
<text x="-20.32" y="76.2" size="5.08" layer="94">UV Light Sensor</text>
<wire x1="-38.1" y1="10.16" x2="43.18" y2="10.16" width="0.508" layer="94"/>
<wire x1="43.18" y1="10.16" x2="43.18" y2="88.9" width="0.508" layer="94"/>
<wire x1="43.18" y1="-61.976" x2="43.18" y2="10.16" width="0.508" layer="94"/>
<text x="-63.5" y="-7.62" size="5.08" layer="94">Inertia Sensor</text>
<text x="53.34" y="-58.42" size="5.08" layer="94">Control Subsystem</text>
<text x="33.274" y="-74.422" size="3.81" layer="94">AXOLOTL v1.0</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="-129.54" y="-86.36"/>
<instance part="IC1" gate="G$1" x="68.58" y="53.34"/>
<instance part="IC4" gate="G$1" x="-81.28" y="40.64"/>
<instance part="P+1" gate="VCC" x="-60.96" y="63.5"/>
<instance part="R1" gate="G$1" x="-101.6" y="53.34" rot="R90"/>
<instance part="R2" gate="G$1" x="-60.96" y="53.34" rot="R90"/>
<instance part="P+2" gate="VCC" x="-101.6" y="63.5"/>
<instance part="P+3" gate="VCC" x="-53.34" y="48.26" smashed="yes">
<attribute name="VALUE" x="-48.26" y="45.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C1" gate="G$1" x="-53.34" y="30.48" smashed="yes" rot="R180">
<attribute name="NAME" x="-54.864" y="30.099" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-44.704" y="30.099" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="-111.76" y="33.02"/>
<instance part="SUPPLY2" gate="GND" x="-53.34" y="20.32"/>
<instance part="SUPPLY3" gate="GND" x="-63.5" y="30.48"/>
<instance part="IC5" gate="G$1" x="10.16" y="40.64"/>
<instance part="R3" gate="G$1" x="-10.16" y="58.42" rot="R90"/>
<instance part="R4" gate="G$1" x="-20.32" y="58.42" rot="R90"/>
<instance part="SUPPLY4" gate="GND" x="33.02" y="27.94"/>
<instance part="C2" gate="G$1" x="-25.4" y="27.94" smashed="yes" rot="R180">
<attribute name="NAME" x="-29.464" y="27.559" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-16.764" y="27.559" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUPPLY5" gate="GND" x="-25.4" y="17.78"/>
<instance part="P+4" gate="VCC" x="-15.24" y="71.12"/>
<instance part="P+5" gate="VCC" x="-25.4" y="43.18"/>
<instance part="R5" gate="G$1" x="-10.16" y="30.48" rot="R90"/>
<instance part="P+6" gate="VCC" x="27.94" y="58.42" smashed="yes">
<attribute name="VALUE" x="33.02" y="55.88" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="IC6" gate="G$1" x="-48.26" y="-45.72"/>
<instance part="C3" gate="G$1" x="-2.54" y="-33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="-1.016" y="-30.099" size="1.778" layer="95"/>
<attribute name="VALUE" x="-1.016" y="-35.179" size="1.778" layer="96"/>
</instance>
<instance part="P+7" gate="VCC" x="-76.2" y="-5.08" smashed="yes">
<attribute name="VALUE" x="-78.74" y="-7.62" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY6" gate="GND" x="-17.78" y="-45.72" rot="R90"/>
<instance part="P+8" gate="VCC" x="-22.86" y="-15.24" smashed="yes">
<attribute name="VALUE" x="-17.78" y="-17.78" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY7" gate="GND" x="-2.54" y="-43.18"/>
<instance part="C4" gate="G$1" x="-20.32" y="-35.56" smashed="yes" rot="R270">
<attribute name="NAME" x="-25.908" y="-34.417" size="1.778" layer="95"/>
<attribute name="VALUE" x="-19.812" y="-34.163" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY8" gate="GND" x="-7.62" y="-38.1"/>
<instance part="C5" gate="G$1" x="10.16" y="-33.02" smashed="yes" rot="R180">
<attribute name="NAME" x="11.684" y="-30.099" size="1.778" layer="95"/>
<attribute name="VALUE" x="11.684" y="-35.179" size="1.778" layer="96"/>
</instance>
<instance part="C6" gate="G$1" x="-20.32" y="-30.48" smashed="yes" rot="R270">
<attribute name="NAME" x="-25.908" y="-29.337" size="1.778" layer="95"/>
<attribute name="VALUE" x="-19.812" y="-29.083" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="-88.9" y="-22.86" rot="R90"/>
<instance part="R7" gate="G$1" x="-81.28" y="-20.32" rot="R90"/>
<instance part="P+9" gate="VCC" x="-88.9" y="-7.62" smashed="yes">
<attribute name="VALUE" x="-91.44" y="-10.16" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C7" gate="G$1" x="-71.12" y="-15.24" smashed="yes" rot="R180">
<attribute name="NAME" x="-69.596" y="-11.811" size="1.778" layer="95"/>
<attribute name="VALUE" x="-69.85" y="-16.891" size="1.778" layer="96"/>
</instance>
<instance part="SUPPLY9" gate="GND" x="-71.12" y="-2.54" rot="R180"/>
<instance part="SUPPLY10" gate="GND" x="-76.2" y="-53.34"/>
<instance part="IC1" gate="G$2" x="71.12" y="-27.94"/>
<instance part="P+10" gate="VCC" x="86.36" y="83.82" smashed="yes">
<attribute name="VALUE" x="91.44" y="81.28" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SUPPLY11" gate="GND" x="111.76" y="60.96"/>
<instance part="SUPPLY12" gate="GND" x="111.76" y="-22.86"/>
<instance part="SUPPLY13" gate="GND" x="93.98" y="-5.08"/>
<instance part="P+11" gate="VCC" x="104.14" y="-33.02" smashed="yes">
<attribute name="VALUE" x="109.22" y="-35.56" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="XTAL1" gate="G$1" x="93.98" y="12.7"/>
<instance part="SUPPLY24" gate="GND" x="93.98" y="71.12" rot="R90"/>
<instance part="U$1" gate="G$1" x="77.47" y="-72.898"/>
</instances>
<busses>
</busses>
<nets>
<net name="SCL" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-101.6" y1="48.26" x2="-101.6" y2="45.72" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="SCL"/>
<wire x1="-101.6" y1="45.72" x2="-96.52" y2="45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="SCL"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="43.18" x2="-20.32" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="43.18" x2="-20.32" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="SCL"/>
<wire x1="-68.58" y1="-30.48" x2="-81.28" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="-30.48" x2="-81.28" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC1" gate="G$2" pin="ADC5/SCL/PCINT13/PC5"/>
<wire x1="81.28" y1="-35.56" x2="88.9" y2="-35.56" width="0.1524" layer="91"/>
<label x="88.9" y="-35.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="SDA"/>
<wire x1="-66.04" y1="45.72" x2="-60.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="-60.96" y1="45.72" x2="-60.96" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="SDA"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="48.26" x2="-10.16" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="48.26" x2="-10.16" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="SDA"/>
<wire x1="-68.58" y1="-35.56" x2="-88.9" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="-35.56" x2="-88.9" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC1" gate="G$2" pin="ADC4/SDA//PCINT12/PC4"/>
<wire x1="88.9" y1="-33.02" x2="81.28" y2="-33.02" width="0.1524" layer="91"/>
<label x="88.9" y="-33.02" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<wire x1="-60.96" y1="58.42" x2="-60.96" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
<wire x1="-101.6" y1="58.42" x2="-101.6" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="V+"/>
<wire x1="-66.04" y1="40.64" x2="-53.34" y2="40.64" width="0.1524" layer="91"/>
<pinref part="P+3" gate="VCC" pin="VCC"/>
<wire x1="-53.34" y1="40.64" x2="-53.34" y2="45.72" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="-53.34" y1="35.56" x2="-53.34" y2="40.64" width="0.1524" layer="91"/>
<junction x="-53.34" y="40.64"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="-20.32" y1="63.5" x2="-20.32" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="66.04" x2="-15.24" y2="66.04" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="66.04" x2="-10.16" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="66.04" x2="-10.16" y2="63.5" width="0.1524" layer="91"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
<wire x1="-15.24" y1="68.58" x2="-15.24" y2="66.04" width="0.1524" layer="91"/>
<junction x="-15.24" y="66.04"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="VDD"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="38.1" x2="-10.16" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="38.1" x2="-25.4" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="38.1" x2="-25.4" y2="33.02" width="0.1524" layer="91"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
<wire x1="-25.4" y1="38.1" x2="-25.4" y2="40.64" width="0.1524" layer="91"/>
<junction x="-25.4" y="38.1"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="35.56" x2="-10.16" y2="38.1" width="0.1524" layer="91"/>
<junction x="-10.16" y="38.1"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="LED1"/>
<wire x1="25.4" y1="48.26" x2="27.94" y2="48.26" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="LED2"/>
<wire x1="27.94" y1="48.26" x2="27.94" y2="38.1" width="0.1524" layer="91"/>
<wire x1="27.94" y1="38.1" x2="27.94" y2="33.02" width="0.1524" layer="91"/>
<wire x1="27.94" y1="33.02" x2="25.4" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="LED3"/>
<wire x1="25.4" y1="38.1" x2="27.94" y2="38.1" width="0.1524" layer="91"/>
<junction x="27.94" y="38.1"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<wire x1="27.94" y1="48.26" x2="27.94" y2="55.88" width="0.1524" layer="91"/>
<junction x="27.94" y="48.26"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="VDD"/>
<pinref part="P+8" gate="VCC" pin="VCC"/>
<wire x1="-27.94" y1="-25.4" x2="-22.86" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-25.4" x2="-22.86" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-25.4" x2="-2.54" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-22.86" y="-25.4"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="-25.4" x2="-2.54" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="10.16" y1="-27.94" x2="10.16" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-25.4" x2="-2.54" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-2.54" y="-25.4"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-88.9" y1="-17.78" x2="-88.9" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="-12.7" x2="-81.28" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-81.28" y1="-12.7" x2="-81.28" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="P+9" gate="VCC" pin="VCC"/>
<wire x1="-88.9" y1="-12.7" x2="-88.9" y2="-10.16" width="0.1524" layer="91"/>
<junction x="-88.9" y="-12.7"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="VDDIO"/>
<wire x1="-68.58" y1="-25.4" x2="-71.12" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<wire x1="-71.12" y1="-25.4" x2="-76.2" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="-7.62" x2="-76.2" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-71.12" y1="-17.78" x2="-71.12" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-71.12" y="-25.4"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="VCC"/>
<wire x1="81.28" y1="63.5" x2="86.36" y2="63.5" width="0.1524" layer="91"/>
<wire x1="86.36" y1="63.5" x2="86.36" y2="81.28" width="0.1524" layer="91"/>
<pinref part="P+10" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="IC1" gate="G$2" pin="RESET/PCINT14/PC6"/>
<pinref part="P+11" gate="VCC" pin="VCC"/>
<wire x1="81.28" y1="-38.1" x2="104.14" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-38.1" x2="104.14" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
<wire x1="-111.76" y1="35.56" x2="-111.76" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="GND"/>
<wire x1="-96.52" y1="40.64" x2="-111.76" y2="40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="-53.34" y1="27.94" x2="-53.34" y2="22.86" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="ADD0"/>
<wire x1="-66.04" y1="35.56" x2="-63.5" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="35.56" x2="-63.5" y2="33.02" width="0.1524" layer="91"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="20.32" x2="-25.4" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="GND"/>
<wire x1="25.4" y1="43.18" x2="33.02" y2="43.18" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<wire x1="33.02" y1="43.18" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="RES"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="-27.94" y1="-45.72" x2="-22.86" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="GND"/>
<wire x1="-22.86" y1="-45.72" x2="-20.32" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="-40.64" x2="-22.86" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-40.64" x2="-22.86" y2="-45.72" width="0.1524" layer="91"/>
<junction x="-22.86" y="-45.72"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="-35.56" x2="-2.54" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
<wire x1="-2.54" y1="-38.1" x2="-2.54" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="-38.1" x2="10.16" y2="-38.1" width="0.1524" layer="91"/>
<junction x="-2.54" y="-38.1"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="10.16" y1="-38.1" x2="10.16" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="-35.56" x2="-12.7" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="-35.56" x2="-7.62" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="-30.48" x2="-12.7" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="-30.48" x2="-12.7" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-12.7" y="-35.56"/>
</segment>
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="-71.12" y1="-10.16" x2="-71.12" y2="-5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="SDO_A/G"/>
<wire x1="-68.58" y1="-40.64" x2="-76.2" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="SDO_M"/>
<wire x1="-68.58" y1="-45.72" x2="-76.2" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="-40.64" x2="-76.2" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<wire x1="-76.2" y1="-45.72" x2="-76.2" y2="-50.8" width="0.1524" layer="91"/>
<junction x="-76.2" y="-45.72"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="81.28" y1="66.04" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<wire x1="111.76" y1="66.04" x2="111.76" y2="63.5" width="0.1524" layer="91"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="G$2" pin="GND"/>
<wire x1="81.28" y1="-17.78" x2="111.76" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="111.76" y1="-17.78" x2="111.76" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<pinref part="XTAL1" gate="G$1" pin="P$3"/>
<wire x1="93.98" y1="-2.54" x2="93.98" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="PCINT19/OC2B/INT1/PD3"/>
<wire x1="81.28" y1="71.12" x2="91.44" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
</segment>
</net>
<net name="T-ALERT" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="ALERT"/>
<wire x1="-96.52" y1="35.56" x2="-99.06" y2="35.56" width="0.1524" layer="91"/>
<label x="-99.06" y="35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-10.16" y1="25.4" x2="-10.16" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="INT"/>
<wire x1="-10.16" y1="22.86" x2="-5.08" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="22.86" x2="-5.08" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="IC6" gate="G$1" pin="CAP"/>
<wire x1="-25.4" y1="-35.56" x2="-27.94" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="C1"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-27.94" y1="-30.48" x2="-25.4" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PCINT3/OC2A/MOSI/PB3"/>
<wire x1="81.28" y1="40.64" x2="88.9" y2="40.64" width="0.1524" layer="91"/>
<label x="88.9" y="40.64" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PCINT4/MISO/PB4"/>
<wire x1="81.28" y1="38.1" x2="88.9" y2="38.1" width="0.1524" layer="91"/>
<label x="88.9" y="38.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="SCK/PCINT5/PB5"/>
<wire x1="81.28" y1="35.56" x2="88.9" y2="35.56" width="0.1524" layer="91"/>
<label x="88.9" y="35.56" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="XTAL1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PCINT6/XTAL/OSC1/PB6"/>
<wire x1="81.28" y1="60.96" x2="88.9" y2="60.96" width="0.1524" layer="91"/>
<label x="88.9" y="60.96" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="104.14" y1="22.86" x2="109.22" y2="22.86" width="0.1524" layer="91"/>
<label x="109.22" y="22.86" size="1.27" layer="95" xref="yes"/>
<pinref part="XTAL1" gate="G$1" pin="P$2"/>
</segment>
</net>
<net name="XTAL2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PCINT7/XTAL2/OSC2/PB7"/>
<wire x1="81.28" y1="58.42" x2="88.9" y2="58.42" width="0.1524" layer="91"/>
<label x="88.9" y="58.42" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="83.82" y1="22.86" x2="78.74" y2="22.86" width="0.1524" layer="91"/>
<label x="78.74" y="22.86" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="XTAL1" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PCINT2/SS/OC1B/PB2"/>
<wire x1="81.28" y1="43.18" x2="88.9" y2="43.18" width="0.1524" layer="91"/>
<label x="88.9" y="43.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VBAT_OK" class="0">
<segment>
<pinref part="IC1" gate="G$2" pin="ADC3/PCINT11/PC3"/>
<wire x1="81.28" y1="-30.48" x2="88.9" y2="-30.48" width="0.1524" layer="91"/>
<label x="88.9" y="-30.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="C_RESET" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PCINT20/XCK/T0/PD4"/>
<wire x1="81.28" y1="68.58" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
<label x="88.9" y="68.58" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<text x="-114.3" y="76.2" size="5.08" layer="94">Power management subsystem </text>
<wire x1="-120.396" y1="-5.08" x2="-20.32" y2="-5.08" width="0.508" layer="94"/>
<wire x1="-20.32" y1="-5.08" x2="-20.32" y2="-77.216" width="0.508" layer="94"/>
<text x="-116.84" y="-15.24" size="5.08" layer="94">Communication subsystem </text>
<text x="39.37" y="-69.85" size="3.81" layer="94">AXOLOTL v1.0</text>
</plain>
<instances>
<instance part="SC4" gate="G$1" x="-106.68" y="27.94"/>
<instance part="SC2" gate="G$1" x="-60.96" y="27.94"/>
<instance part="IC2" gate="G$1" x="15.24" y="20.32"/>
<instance part="SC3" gate="G$1" x="-91.44" y="27.94"/>
<instance part="SC1" gate="G$1" x="-76.2" y="27.94"/>
<instance part="FRAME2" gate="G$1" x="-124.46" y="-81.28"/>
<instance part="SUPPLY14" gate="GND" x="-83.82" y="5.08"/>
<instance part="B1" gate="G$1" x="35.56" y="60.96"/>
<instance part="SUPPLY15" gate="GND" x="35.56" y="50.8"/>
<instance part="C10" gate="G$1" x="-2.54" y="71.12"/>
<instance part="C11" gate="G$1" x="7.62" y="71.12"/>
<instance part="SUPPLY16" gate="GND" x="7.62" y="55.88"/>
<instance part="R8" gate="G$1" x="10.16" y="-17.78" rot="R90"/>
<instance part="R9" gate="G$1" x="10.16" y="-33.02" rot="R90"/>
<instance part="R10" gate="G$1" x="25.4" y="-17.78" rot="R90"/>
<instance part="R11" gate="G$1" x="25.4" y="-33.02" rot="R90"/>
<instance part="R12" gate="G$1" x="25.4" y="-48.26" rot="R90"/>
<instance part="R13" gate="G$1" x="33.02" y="-20.32" rot="R90"/>
<instance part="R14" gate="G$1" x="33.02" y="-40.64" rot="R90"/>
<instance part="SUPPLY17" gate="GND" x="25.4" y="-60.96"/>
<instance part="SUPPLY18" gate="GND" x="-22.86" y="27.94" rot="R270"/>
<instance part="C12" gate="G$1" x="71.12" y="22.86"/>
<instance part="SUPPLY19" gate="GND" x="71.12" y="12.7"/>
<instance part="C13" gate="G$1" x="-35.56" y="30.48"/>
<instance part="SUPPLY20" gate="GND" x="-35.56" y="17.78"/>
<instance part="C14" gate="G$1" x="-45.72" y="30.48"/>
<instance part="SUPPLY21" gate="GND" x="-45.72" y="17.78"/>
<instance part="P+12" gate="VCC" x="71.12" y="48.26"/>
<instance part="SUPPLY22" gate="GND" x="-27.94" y="12.7" rot="R270"/>
<instance part="L1" gate="G$1" x="-30.48" y="38.1" smashed="yes" rot="R90">
<attribute name="NAME" x="-25.4" y="36.83" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-25.4" y="41.91" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="L2" gate="G$1" x="58.42" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="63.5" y="31.75" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="63.5" y="36.83" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="IC3" gate="G$1" x="-76.2" y="-48.26"/>
<instance part="P+13" gate="VCC" x="-55.88" y="-38.1"/>
<instance part="SUPPLY23" gate="GND" x="-96.52" y="-60.96"/>
<instance part="ANT1" gate="G$1" x="-48.26" y="-45.72"/>
<instance part="U$2" gate="G$1" x="83.566" y="-68.326"/>
</instances>
<busses>
</busses>
<nets>
<net name="N$5" class="0">
<segment>
<pinref part="SC1" gate="G$1" pin="+"/>
<wire x1="-76.2" y1="35.56" x2="-76.2" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="38.1" x2="-60.96" y2="38.1" width="0.1524" layer="91"/>
<pinref part="SC2" gate="G$1" pin="+"/>
<wire x1="-60.96" y1="38.1" x2="-60.96" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="38.1" x2="-48.26" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="38.1" x2="-48.26" y2="22.86" width="0.1524" layer="91"/>
<junction x="-60.96" y="38.1"/>
<pinref part="IC2" gate="G$1" pin="VIN_DC"/>
<wire x1="-48.26" y1="22.86" x2="-17.78" y2="22.86" width="0.1524" layer="91"/>
<junction x="-48.26" y="38.1"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="-48.26" y1="38.1" x2="-45.72" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="38.1" x2="-38.1" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="33.02" x2="-45.72" y2="38.1" width="0.1524" layer="91"/>
<junction x="-45.72" y="38.1"/>
<pinref part="SC4" gate="G$1" pin="+"/>
<wire x1="-106.68" y1="35.56" x2="-106.68" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="38.1" x2="-91.44" y2="38.1" width="0.1524" layer="91"/>
<junction x="-76.2" y="38.1"/>
<pinref part="SC3" gate="G$1" pin="+"/>
<wire x1="-91.44" y1="38.1" x2="-76.2" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="35.56" x2="-91.44" y2="38.1" width="0.1524" layer="91"/>
<junction x="-91.44" y="38.1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-83.82" y1="12.7" x2="-83.82" y2="7.62" width="0.1524" layer="91"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
<pinref part="SC4" gate="G$1" pin="-"/>
<wire x1="-106.68" y1="20.32" x2="-106.68" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-106.68" y1="12.7" x2="-91.44" y2="12.7" width="0.1524" layer="91"/>
<pinref part="SC3" gate="G$1" pin="-"/>
<wire x1="-91.44" y1="12.7" x2="-83.82" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="20.32" x2="-91.44" y2="12.7" width="0.1524" layer="91"/>
<junction x="-91.44" y="12.7"/>
<pinref part="SC1" gate="G$1" pin="-"/>
<wire x1="-76.2" y1="20.32" x2="-76.2" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="12.7" x2="-83.82" y2="12.7" width="0.1524" layer="91"/>
<junction x="-83.82" y="12.7"/>
<pinref part="SC2" gate="G$1" pin="-"/>
<wire x1="-60.96" y1="20.32" x2="-60.96" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="12.7" x2="-76.2" y2="12.7" width="0.1524" layer="91"/>
<junction x="-76.2" y="12.7"/>
</segment>
<segment>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<pinref part="B1" gate="G$1" pin="-"/>
<wire x1="35.56" y1="53.34" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<wire x1="10.16" y1="-38.1" x2="10.16" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-55.88" x2="25.4" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="25.4" y1="-55.88" x2="25.4" y2="-53.34" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="1"/>
<wire x1="25.4" y1="-55.88" x2="33.02" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-55.88" x2="33.02" y2="-45.72" width="0.1524" layer="91"/>
<junction x="25.4" y="-55.88"/>
<wire x1="25.4" y1="-55.88" x2="25.4" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VSS"/>
<wire x1="-17.78" y1="27.94" x2="-20.32" y2="27.94" width="0.1524" layer="91"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="71.12" y1="15.24" x2="71.12" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="-35.56" y1="25.4" x2="-35.56" y2="20.32" width="0.1524" layer="91"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="-45.72" y1="20.32" x2="-45.72" y2="25.4" width="0.1524" layer="91"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="7.62" y1="58.42" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="7.62" y1="60.96" x2="7.62" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="66.04" x2="-2.54" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="60.96" x2="7.62" y2="60.96" width="0.1524" layer="91"/>
<junction x="7.62" y="60.96"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="EN"/>
<wire x1="-17.78" y1="12.7" x2="-25.4" y2="12.7" width="0.1524" layer="91"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="GND"/>
<wire x1="-91.44" y1="-55.88" x2="-96.52" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="-55.88" x2="-96.52" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VBAT"/>
<wire x1="25.4" y1="48.26" x2="25.4" y2="68.58" width="0.1524" layer="91"/>
<wire x1="25.4" y1="68.58" x2="35.56" y2="68.58" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="+"/>
<wire x1="35.56" y1="68.58" x2="35.56" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VSTOR" class="0">
<segment>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="73.66" x2="-2.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="76.2" x2="7.62" y2="76.2" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="7.62" y1="76.2" x2="7.62" y2="73.66" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VSTOR"/>
<wire x1="7.62" y1="76.2" x2="15.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="15.24" y1="76.2" x2="15.24" y2="50.8" width="0.1524" layer="91"/>
<junction x="7.62" y="76.2"/>
<label x="15.24" y="76.2" size="1.778" layer="95" xref="yes"/>
<pinref part="IC2" gate="G$1" pin="VCC_SAMP"/>
<wire x1="15.24" y1="50.8" x2="15.24" y2="48.26" width="0.1524" layer="91"/>
<wire x1="10.16" y1="48.26" x2="10.16" y2="50.8" width="0.1524" layer="91"/>
<wire x1="10.16" y1="50.8" x2="15.24" y2="50.8" width="0.1524" layer="91"/>
<junction x="15.24" y="50.8"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VOUT_EN"/>
<wire x1="-17.78" y1="7.62" x2="-27.94" y2="7.62" width="0.1524" layer="91"/>
<label x="-27.94" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="R11" gate="G$1" pin="2"/>
<wire x1="25.4" y1="-27.94" x2="25.4" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-25.4" x2="25.4" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-25.4" x2="27.94" y2="-25.4" width="0.1524" layer="91"/>
<junction x="25.4" y="-25.4"/>
<pinref part="IC2" gate="G$1" pin="OK_HYST"/>
<wire x1="27.94" y1="-25.4" x2="27.94" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VRDIV"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="5.08" y1="-10.16" x2="10.16" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<wire x1="10.16" y1="-10.16" x2="25.4" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-10.16" x2="33.02" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-10.16" x2="33.02" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="R10" gate="G$1" pin="2"/>
<wire x1="25.4" y1="-12.7" x2="25.4" y2="-10.16" width="0.1524" layer="91"/>
<junction x="25.4" y="-10.16"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="-10.16" width="0.1524" layer="91"/>
<junction x="10.16" y="-10.16"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="10.16" y1="-22.86" x2="10.16" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VBAT_OV"/>
<wire x1="10.16" y1="-25.4" x2="10.16" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-7.62" x2="12.7" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="12.7" y1="-25.4" x2="10.16" y2="-25.4" width="0.1524" layer="91"/>
<junction x="10.16" y="-25.4"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="R12" gate="G$1" pin="2"/>
<wire x1="25.4" y1="-38.1" x2="25.4" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="OK_PROG"/>
<wire x1="25.4" y1="-40.64" x2="25.4" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-7.62" x2="20.32" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="20.32" y1="-40.64" x2="25.4" y2="-40.64" width="0.1524" layer="91"/>
<junction x="25.4" y="-40.64"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="2"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="33.02" y1="-35.56" x2="33.02" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VOUT_SET"/>
<wire x1="33.02" y1="-30.48" x2="33.02" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-7.62" x2="35.56" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="35.56" y1="-30.48" x2="33.02" y2="-30.48" width="0.1524" layer="91"/>
<junction x="33.02" y="-30.48"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VREF_SAMP"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-17.78" y1="33.02" x2="-35.56" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+12" gate="VCC" pin="VCC"/>
<wire x1="71.12" y1="27.94" x2="71.12" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="71.12" y1="33.02" x2="71.12" y2="45.72" width="0.1524" layer="91"/>
<wire x1="71.12" y1="25.4" x2="71.12" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VOUT"/>
<wire x1="71.12" y1="27.94" x2="48.26" y2="27.94" width="0.1524" layer="91"/>
<junction x="71.12" y="27.94"/>
<pinref part="L2" gate="G$1" pin="2"/>
<wire x1="66.04" y1="33.02" x2="71.12" y2="33.02" width="0.1524" layer="91"/>
<junction x="71.12" y="33.02"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="VCC"/>
<wire x1="-63.5" y1="-45.72" x2="-55.88" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-45.72" x2="-55.88" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="P+13" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="VBAT_OK" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VBAT_OK"/>
<wire x1="-17.78" y1="2.54" x2="-27.94" y2="2.54" width="0.1524" layer="91"/>
<label x="-27.94" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="IC2" gate="G$1" pin="LBOOST"/>
<wire x1="-22.86" y1="38.1" x2="-17.78" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="LBUCK"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="48.26" y1="33.02" x2="50.8" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="MISO"/>
<wire x1="-91.44" y1="-40.64" x2="-96.52" y2="-40.64" width="0.1524" layer="91"/>
<label x="-96.52" y="-40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="MOSI"/>
<wire x1="-91.44" y1="-43.18" x2="-96.52" y2="-43.18" width="0.1524" layer="91"/>
<label x="-96.52" y="-43.18" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="SCK"/>
<wire x1="-91.44" y1="-45.72" x2="-96.52" y2="-45.72" width="0.1524" layer="91"/>
<label x="-96.52" y="-45.72" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ANT" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="ANT"/>
<wire x1="-63.5" y1="-55.88" x2="-48.26" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="-55.88" x2="-48.26" y2="-45.72" width="0.1524" layer="91"/>
<pinref part="ANT1" gate="G$1" pin="1"/>
<label x="-48.26" y="-45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="C_RESET" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="RESET"/>
<wire x1="-91.44" y1="-50.8" x2="-96.52" y2="-50.8" width="0.1524" layer="91"/>
<label x="-96.52" y="-50.8" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CS" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="NSS"/>
<wire x1="-91.44" y1="-48.26" x2="-96.52" y2="-48.26" width="0.1524" layer="91"/>
<label x="-96.52" y="-48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
